//+------------------------------------------------------------------+
//|                                               EconomicEvents.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_type1   DRAW_NONE
#property indicator_buffers 1
#property indicator_plots   1
#property indicator_color1 Red

//--- インジケータバッファ
double EventDataBuffer[];

struct events_struct
  {
   string            EventName;
   string            EventTime;
  };

// SQLiteファイル名
string dbName = "EconomicEvents.sqlite";
string tableName = "Events";
int handle;
events_struct event_data;
//+------------------------------------------------------------------+
//| 初期化関数                                                       |
//+------------------------------------------------------------------+
int OnInit()
  {
// インジケータバッファの初期化
   SetIndexBuffer(0, EventDataBuffer);

// SQLiteデータベースを開く
   handle = DatabaseOpen(dbName, DATABASE_OPEN_COMMON | DATABASE_OPEN_READONLY);
   if(handle == INVALID_HANDLE)
     {
      Print("DB: ", dbName, " 新規作成 エラーコード: ", GetLastError());
      return INIT_FAILED;
     }


// 最後のイベントの日時を取得
   datetime lastEventTime = GetLastEventTime();
   if(lastEventTime == 0)
     {
      // データがない場合は2022年1月1日を設定
      lastEventTime = D'01.01.2022';
     }

// イベントデータの差分取り込み
//   ImportEconomicEvents(handle, lastEventTime);

   return INIT_SUCCEEDED;
  }
//+------------------------------------------------------------------+
//| 初期化関数                                                       |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
// オブジェクト削除
   ObjectsDeleteAll(0);
// データベースを閉じる
   DatabaseClose(handle);
  }
//+------------------------------------------------------------------+
//| インジケータの計算処理                                           |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
// 画面上にインジケータデータ（イベント）を表示
   int start;
//---
   if(prev_calculated==0)
      start=0;
   else
      start=prev_calculated-1;
//--- main loop
   for(int i=start; i<rates_total && !IsStopped(); i++)
     {
      EventDataBuffer[1] = 0;
      if(SetLastEvent(time[i]))
        {
         EventDataBuffer[1] = 1;
         CreateVerticalLine((string)time[i] + "_" + event_data.EventName,time[i]);
        };

     }

   return rates_total;
  }
//+------------------------------------------------------------------+
//| 縦線を作成する関数                                                   |
//+------------------------------------------------------------------+
void CreateVerticalLine(string name, datetime time)
  {
// オブジェクトが存在する場合は削除
   if(ObjectFind(0, name) >= 0)
      ObjectDelete(0, name);

// 縦線を作成
   if(ObjectCreate(0, name, OBJ_VLINE, 0, time, 0))
     {
      //ObjectSetInteger(0, name, OBJPROP_COLOR, clr);
      ObjectSetInteger(0, name, OBJPROP_STYLE, STYLE_DOT);
      //ObjectSetInteger(0, name, OBJPROP_WIDTH, width);
     }
   else
     {
      Print("縦線オブジェクトの作成に失敗しました。エラー: ", GetLastError());
     }
  }
//+------------------------------------------------------------------+
//| 最後に登録されたイベントの日時を取得する関数                   |
//+------------------------------------------------------------------+
bool SetLastEvent(datetime dt)
  {
   string sql = "SELECT EventName,EventTime FROM " + tableName;
   sql = sql + " WHERE EventTime = '" + TimeToString(dt, TIME_DATE | TIME_MINUTES) + "'";
   int request = DatabasePrepare(handle, sql);
   if(request == INVALID_HANDLE)
     {
      Print("SQLクエリの準備に失敗しました。エラーコード: ", GetLastError());
      return false;
     }

// リクエストのレコードの数だけ繰り返す
   int i=0;
   while(DatabaseReadBind(request,event_data))
      i=i+1;
   DatabaseFinalize(request);  // 必ずリクエストの後に終了処理
   if(i==0)
      return false;
   else
      return true;
  }
//+------------------------------------------------------------------+
//| 最後に登録されたイベントの日時を取得する関数                   |
//+------------------------------------------------------------------+
datetime GetLastEventTime()
  {
   string sql = "SELECT MAX(EventTime) FROM " + tableName;
   int request = DatabasePrepare(handle, sql);
   if(request == INVALID_HANDLE)
     {
      Print("SQLクエリの準備に失敗しました。エラーコード: ", GetLastError());
      return 0;
     }

   string result;
   for(int i=0; DatabaseRead(request); i++) // リクエストのレコードの数だけ繰り返す
     {
      if(DatabaseColumnName(request,0,result))
         return StringToTime(result);
      else
        {
         Print(i, " データを取得  エラーコード: ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(handle);
         return 0;
        }
     }
   DatabaseFinalize(request);  // 必ずリクエストの後に終了処理
   return 0; // データがない場合は0を返す
  }
//+------------------------------------------------------------------+
//| 経済イベントの差分データを取り込む関数                         |
//+------------------------------------------------------------------+
void ImportEconomicEvents(datetime lastEventTime)
  {
//--- 影響のある通貨のリスト
   string currencies[] = {"GBP", "JPY", "USD", "EUR"}; // 必要に応じて拡張

   MqlCalendarEvent events[];
   datetime date_from = lastEventTime;  // 最後のイベント日時から開始
   datetime date_to = TimeCurrent();    // 本日まで

// 通貨ごとにイベントデータを取得して差分取り込み
   for(int c = 0; c < ArraySize(currencies); c++)
     {
      // 各通貨のイベントを取得
      int count = CalendarEventByCurrency(currencies[c], events);
      for(int i = 0; i < count; i++)
        {
         // 高重要度イベントに絞る
         if(events[i].importance != CALENDAR_IMPORTANCE_HIGH)
            continue;

         MqlCalendarValue values[];
         if(CalendarValueHistoryByEvent(events[i].id, values, date_from, date_to))
           {
            for(int j = 0; j < ArraySize(values); j++)
              {
               MqlDateTime tm;
               TimeToStruct(values[j].time, tm);
               if(tm.hour == 0 && tm.min == 0)
                  continue; // 日付だけのイベントを除外

               // DBにイベントデータを挿入
               string sql = "INSERT INTO " + tableName + " (Currency, EventName, EventTime, Importance) VALUES ('" +
                            currencies[c] + "', '" + events[i].name + "', '" + TimeToString(values[j].time, TIME_DATE | TIME_MINUTES) + "', " + IntegerToString(events[i].importance) + ");";
               if(!DatabaseExecute(handle, sql))
                 {
                  Print("データの挿入に失敗しました。エラーコード: ", GetLastError());
                  return;
                 }
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+
