//+------------------------------------------------------------------+
//|                                                 CandleShapes.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 1
#property indicator_plots   1
#property indicator_type1   DRAW_NONE
#include <SATCommon.mqh>
//--- 指標バッファ
double InvisibleBuffer[];
double smallShadowRatio = 0.07;
double longShadowRatio = 0.7;
color color_up = clrLightBlue;
color color_down = clrLightPink;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,InvisibleBuffer,INDICATOR_DATA);

//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---

   int start;
//---
   if(prev_calculated==0)
      start=0;
   else
      start=prev_calculated-1;
//--- main loop
   for(int i=start; i<rates_total && !IsStopped(); i++)
     {
      if(2 >= i || i >= rates_total-1)
         continue;
      //---
      double candleSize=high[i]-low[i];
      double candleSize2=high[i-1]-low[i-1];
      double candleMidPoint2=high[i-1]-(candleSize2/2);
      bool isBullish = open[i] < close[i];
      bool isBearish = open[i] > close[i];
      double upperShadow = isBullish ? (high[i] - close[i]) : (high[i] - open[i]);
      double lowerShadow = isBullish ? (open[i] - low[i]) : (close[i] - low[i]);
      // カラカサ・トンカチ判定
      if(upperShadow < candleSize * smallShadowRatio &&
         lowerShadow > candleSize * longShadowRatio)
        {
         createObj(time[i], low[i], 217, color_up, "Hammer");
         InvisibleBuffer[i] = CANDLE_HAMMER_H;
        }

      // 逆のカラカサ・トンカチ
      if(lowerShadow < candleSize * smallShadowRatio &&
         upperShadow > candleSize * longShadowRatio)
        {
         createObj(time[i], high[i], 218, color_down, "Hammer");
         InvisibleBuffer[i] = CANDLE_HAMMER_L;
        }
      // 強気の飲み込み
      if(open[i]<close[i])
        {
         if(open[i-1]>close[i-1])
           {
            if(high[i]>high[i-1]&&low[i]<low[i-1])
              {
               if(close[i]>open[i-1]&&open[i]<close[i-1])
                 {
                  createObj(time[i],low[i],217, color_up,"Bullish Engulfing");
                  InvisibleBuffer[i] = CANDLE_BULLISH_ENGULFING;
                 }
              }
           }
        }
      // 弱気の飲み込み
      if(open[i]>close[i])
        {
         if(open[i-1]<close[i-1])
           {
            if(high[i]>high[i-1]&&low[i]<low[i-1])
              {
               if(close[i]<open[i-1]&&open[i]>close[i-1])
                 {
                  createObj(time[i],high[i],218, color_down,"Bearish Engulfing");
                  InvisibleBuffer[i] = CANDLE_BEARISH_ENGULFING;
                 }
              }
           }
        }
      // 切り込み線パターン
      if(open[i]<close[i])
        {
         if(open[i-1]<close[i-1])
           {
            if(open[i]<low[i-1])
              {
               if(close[i]>candleMidPoint2&&close[i]<high[i-1])
                 {
                  createObj(time[i],low[i],217, color_up,"Piercing");
                  InvisibleBuffer[i] = CANDLE_PIERCING;
                 }
              }
           }
        }
      // かぶせ線パターン
      if(open[i]>close[i])
        {
         if(open[i-1]<close[i-1])
           {
            if(open[i]>high[i-1])
              {
               if(close[i]<candleMidPoint2&&close[i]>low[i-1])
                 {
                  createObj(time[i],high[i],218, color_down,"Dark Cloud");
                  InvisibleBuffer[i] = CANDLE_DARK_CLOUD;
                 }
              }
           }
        }
      // スリーインサイドアップ
      if(open[i-2]>close[i-2])
        {
         if(open[i-1]<close[i-1])
           {
            if(open[i-1]>low[i-2]&&close[i-1]<high[i-2])
              {
               if(open[i]<close[i]&&open[i]>open[i-1]&&open[i]<close[i-1])
                 {
                  if(close[i]>high[i-2])
                    {
                     createObj(time[i],low[i],217, color_up,"3 Inside Up");
                     InvisibleBuffer[i] = CANDLE_INSIDE_UP;
                    }
                 }
              }
           }

        }
      // スリーインサイドダウン
      if(open[i-2]<close[i-2])
        {
         if(open[i-1]>close[i-1])
           {
            if(open[i-1]<high[i-2]&&close[i-1]>low[i-2])
              {
               if(open[i]>close[i]&&open[i]<open[i-1]&&open[i]>close[i-1])
                 {
                  if(close[i]<low[i-1])
                    {
                     createObj(time[i],high[i],218, color_down,"3 Inside Down");
                     InvisibleBuffer[i] = CANDLE_INSIDE_DOWN;
                    }
                 }
              }
           }
        }
     }
// チャート更新時にラインを更新
   MqlRates yesterday_rates[];
   int copied = CopyRates(_Symbol,PERIOD_D1,1,1,yesterday_rates);
   if(copied==1)
     {
      DrawYesterdayLine("yesterday_high",yesterday_rates[0].high);
      DrawYesterdayLine("yesterday_low",yesterday_rates[0].low);
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
void createObj(datetime time, double price, int arrawCode, color clr, string txt)
  {
   string objName=" ";
   StringConcatenate(objName, "Signal@",time, "at",DoubleToString(price,_Digits),"(",arrawCode,")");
//Print(objName);
   if(ObjectCreate(0,objName,OBJ_ARROW,0,time,price))
     {
      ObjectSetInteger(0,objName,OBJPROP_ARROWCODE,arrawCode);
      ObjectSetInteger(0,objName,OBJPROP_COLOR,clr);
      if(clr==color_up)
         ObjectSetInteger(0,objName,OBJPROP_ANCHOR,ANCHOR_TOP);
      if(clr==color_down)
         ObjectSetInteger(0,objName,OBJPROP_ANCHOR,ANCHOR_BOTTOM);
     }
   string candleName=objName+txt;
   if(ObjectCreate(0,candleName,OBJ_TEXT,0,time,price))
     {
      ObjectSetString(0,candleName,OBJPROP_TEXT," "+txt);
      ObjectSetInteger(0,candleName,OBJPROP_COLOR,clr);
     }
  }
//+------------------------------------------------------------------+
//| ラインを描画                                           |
//+------------------------------------------------------------------+
void DrawYesterdayLine(string lineName, double price)
  {
// ラインが既に存在していれば削除
   if(ObjectFind(0, lineName) != -1)
      ObjectDelete(0, lineName);

// ラインを作成
   if(ObjectCreate(0, lineName, OBJ_HLINE, 0, 0, price))
     {
      // ラインのプロパティを設定
      ObjectSetInteger(0, lineName, OBJPROP_COLOR, clrGray);      // 色
      ObjectSetInteger(0, lineName, OBJPROP_WIDTH, 2);           // 太さ
      ObjectSetInteger(0, lineName, OBJPROP_STYLE, STYLE_SOLID); // スタイル
     }
   else
     {
      Print("ラインの描画に失敗しました");
     }
  }
//+------------------------------------------------------------------+
