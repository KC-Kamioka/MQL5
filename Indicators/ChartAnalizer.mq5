//+------------------------------------------------------------------+
//|                                                ChartAnalizer.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 1
#property indicator_plots   1
#property indicator_type1   DRAW_LINE
#include <SATCommon.mqh>
//--- 指標バッファ
double ChikoSpanBuffer[];

int handle_ima;
int c_span_period = 20;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,ChikoSpanBuffer,INDICATOR_DATA);
//---
   PlotIndexSetInteger(0,PLOT_SHIFT,-c_span_period);
//---
// Chiko Span
   handle_ima = iMA(_Symbol, _Period, 1, 0, MODE_SMA, PRICE_CLOSE);
   if(handle_ima == INVALID_HANDLE)
      return(INIT_FAILED);
//---
//ArraySetAsSeries(ChikoSpanBuffer,false);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   if(CopyBuffer(handle_ima, 0, 0,rates_total,ChikoSpanBuffer) < 0)
     {
      Print("Error copying buffer: ", GetLastError());
      return prev_calculated;
     }
//---
   int start;
//---
   if(prev_calculated==0)
      start=0;
   else
      start=prev_calculated-1;
//--- main loop
   for(int i=start; i<rates_total && !IsStopped(); i++)
     {
      //---
      if(i < c_span_period)
         continue;
      //---
      double highest = ArrayMaximum(ChikoSpanBuffer,i,c_span_period);
      double lowest = ArrayMinimum(ChikoSpanBuffer,i,c_span_period);
      DrawRectangle("bg_" + (string)rd_signal_bar,bg_color_b,time[rd_signal_bar],DBL_MAX,time[i],DBL_MIN);
      
     }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
