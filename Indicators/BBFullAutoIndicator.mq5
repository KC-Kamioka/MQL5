//+------------------------------------------------------------------+
//|                                          BBFullAuto Indicator.mq5 |
//|                             Copyright 2000-2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2000-2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property description "BBFullAuto Indicator"
#include <BFACommon.mqh>
//--- indicator settings
#property indicator_chart_window
#property indicator_buffers 10
#property indicator_plots   9
#property indicator_type1   DRAW_FILLING
#property indicator_type2   DRAW_LINE
#property indicator_type3   DRAW_LINE
#property indicator_type4   DRAW_LINE
#property indicator_type5   DRAW_LINE
#property indicator_type6   DRAW_NONE
#property indicator_type7   DRAW_NONE
#property indicator_type8   DRAW_NONE
#property indicator_label1   "Cloud"
#property indicator_label2   "BBPlus1"
#property indicator_label3   "BBMinus1"
#property indicator_label4   "BBPlus2"
#property indicator_label5   "BBMinus2"
#property indicator_label6   "BBWalkUpCount"
#property indicator_label7   "BBWalkDownCount"
#property indicator_label8   "ExtUpper2Sigma"
#property indicator_label9   "ExtLower2Sigma"
#property indicator_color1  0x462305;0x0A0A48;
//--- input parameters
int InpTenkan=9;     // Tenkan-sen
int InpKijun=26;     // Kijun-sen
int InpSenkou=52;    // Senkou Span B
//--- indicator buffers
double    ExtSpanABuffer[];
double    ExtSpanBBuffer[];
double    ExtUpper1BandBuffer[];
double    ExtLower1BandBuffer[];
double    ExtUpper2BandBuffer[];
double    ExtLower2BandBuffer[];
double    ExtUpperBandWalkBuffer[];
double    ExtLowerBandWalkBuffer[];
double    ExtUpper2Sigma[];
double    ExtLower2Sigma[];
//---
color bg_color_b = StringToColor("15,15,50");
color bg_color_s = StringToColor("45,0,20");
color sp_color_b = StringToColor("0,71,171");
color sp_color_s = StringToColor("80,20,20");
bool spanmodel_up = false;
int sp_signal_bar = 0;
double sp_high = 0;
double sp_low = 0;
//---
int handle_bb_1;
int handle_bb_2;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
void OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,ExtSpanABuffer,INDICATOR_DATA);
   SetIndexBuffer(1,ExtSpanBBuffer,INDICATOR_DATA);
   SetIndexBuffer(2,ExtUpper1BandBuffer,INDICATOR_DATA);
   SetIndexBuffer(3,ExtLower1BandBuffer,INDICATOR_DATA);
   SetIndexBuffer(4,ExtUpper2BandBuffer,INDICATOR_DATA);
   SetIndexBuffer(5,ExtLower2BandBuffer,INDICATOR_DATA);
   SetIndexBuffer(6,ExtUpperBandWalkBuffer,INDICATOR_DATA);
   SetIndexBuffer(7,ExtLowerBandWalkBuffer,INDICATOR_DATA);
   SetIndexBuffer(8,ExtUpper2Sigma,INDICATOR_DATA);
   SetIndexBuffer(9,ExtLower2Sigma,INDICATOR_DATA);
   PlotIndexSetInteger(0,PLOT_SHIFT,26);
//---
// Bolinger Band 1σ
   handle_bb_1 = iBands(_Symbol, _Period, bb_period, 0, 1.0, PRICE_CLOSE);
   if(handle_bb_1 == INVALID_HANDLE)
      return;
// Bolinger Band 2σ
   handle_bb_2 = iBands(_Symbol, _Period, bb_period, 0, 2.0, PRICE_CLOSE);
   if(handle_bb_2 == INVALID_HANDLE)
      return;
//---
   IndicatorSetInteger(INDICATOR_DIGITS,_Digits+1);
  }
//+------------------------------------------------------------------+
//| OnDeinit: インジケーター削除時の処理                             |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
// ラインを削除
   ObjectsDeleteAll(0);
  }
//+------------------------------------------------------------------+
//| Ichimoku Kinko Hyo                                               |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   if(CopyBuffer(handle_bb_1, UPPER_BAND, 0, rates_total, ExtUpper1BandBuffer) < 0)
      return 0;
//---
   if(CopyBuffer(handle_bb_1, LOWER_BAND, 0, rates_total, ExtLower1BandBuffer) < 0)
      return 0;
//---
   if(CopyBuffer(handle_bb_2, UPPER_BAND, 0, rates_total, ExtUpper2BandBuffer) < 0)
      return 0;
//---
   if(CopyBuffer(handle_bb_2, LOWER_BAND, 0, rates_total, ExtLower2BandBuffer) < 0)
      return 0;
//---
   int start;
//---
   if(prev_calculated==0)
      start=0;
   else
      start=prev_calculated-1;
//--- main loop
   for(int i=start; i<rates_total && !IsStopped(); i++)
     {
      //--- tenkan sen
      double price_max=Highest(high,InpTenkan,i);
      double price_min=Lowest(low,InpTenkan,i);
      double tenkan_sen=(price_max+price_min)/2.0;
      //--- kijun sen
      price_max=Highest(high,InpKijun,i);
      price_min=Lowest(low,InpKijun,i);
      double kijun_sen=(price_max+price_min)/2.0;
      //--- senkou span a
      ExtSpanABuffer[i]=(tenkan_sen+kijun_sen)/2.0;
      //--- senkou span b
      price_max=Highest(high,InpSenkou,i);
      price_min=Lowest(low,InpSenkou,i);
      ExtSpanBBuffer[i]=(price_max+price_min)/2.0;
      //--- band walk
      if(i>0)
        {
         if(ExtUpper1BandBuffer[i] < close[i])
            ExtUpperBandWalkBuffer[i]=ExtUpperBandWalkBuffer[i-1]+1;
         if(ExtUpper1BandBuffer[i] >= close[i])
            ExtUpperBandWalkBuffer[i]=0;
         if(ExtLower1BandBuffer[i] > close[i])
            ExtLowerBandWalkBuffer[i]=ExtLowerBandWalkBuffer[i-1]+1;
         if(ExtLower1BandBuffer[i] <= close[i])
            ExtLowerBandWalkBuffer[i]=0;
         //--- band 2 ext
         if(ExtUpper2BandBuffer[i] < close[i])
            ExtUpper2Sigma[i]=1;
         if(ExtLower2BandBuffer[i] > close[i])
            ExtLower2Sigma[i]=1;
        }
     }
   return(rates_total);
  }
//+------------------------------------------------------------------+
//| get price_max value for range                                      |
//+------------------------------------------------------------------+
double Highest(const double& array[],const int range,int from_index)
  {
   double res=0;
   int idx=ArrayMaximum(array,from_index-range,range);
   if(idx>=0)
      res=array[idx];
   return(res);
  }
//+------------------------------------------------------------------+
//| get price_min value for range                                       |
//+------------------------------------------------------------------+
double Lowest(const double& array[],const int range,int from_index)
  {
   double res=0;
   int idx=ArrayMinimum(array,from_index-range,range);
   if(idx>=0)
      res=array[idx];
   return(res);
  }
//+------------------------------------------------------------------+
