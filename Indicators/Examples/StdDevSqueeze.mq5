//+------------------------------------------------------------------+
//|                                                   StdDevSqueeze.mq5 |
//|                        Copyright 2025, Your Name                  |
//|                                             https://www.mql5.com  |
//+------------------------------------------------------------------+
#property indicator_separate_window
#property indicator_buffers 1
#property indicator_plots   1
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed

//--- input parameters
input int    StdDevPeriod = 20;       // 標準偏差の期間
input int    MAMAPeriod = 5;          // 移動平均の期間
input double SqueezeThreshold = 0.5;  // スクイーズ閾値

//--- indicator buffers
double ExtSqueezeBuffer[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
   //--- indicator buffers mapping
   SetIndexBuffer(0, ExtSqueezeBuffer, INDICATOR_DATA);
   //--- name for DataWindow and indicator subwindow label
   IndicatorSetString(INDICATOR_SHORTNAME, "StdDev Squeeze");
   //--- set index label
   PlotIndexSetString(0, PLOT_LABEL, "Squeeze");
   //---
   return (INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
   //--- check for enough bars
   if (rates_total < StdDevPeriod + MAMAPeriod)
      return (0);

   int start = prev_calculated > 0 ? prev_calculated - 1 : StdDevPeriod + MAMAPeriod;

   for (int i = start; i < rates_total; i++)
     {
      //--- calculate standard deviation
      double stddev = iStdDev(NULL, 0, StdDevPeriod, 0, MODE_SMA, PRICE_CLOSE, i);

      //--- calculate moving average of standard deviation
      double ma_stddev = iMAOnArray(ExtSqueezeBuffer, rates_total, MAMAPeriod, 0, MODE_SMA, i);

      //--- determine squeeze condition
      if (stddev < ma_stddev * SqueezeThreshold)
         ExtSqueezeBuffer[i] = 1;
      else
         ExtSqueezeBuffer[i] = 0;
     }

   return (rates_total);
  }
//+------------------------------------------------------------------+
