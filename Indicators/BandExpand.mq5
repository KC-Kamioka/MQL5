//+------------------------------------------------------------------+
//|                                                   BandExpand.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <BFACommon.mqh>

#property indicator_separate_window
#property indicator_buffers 1
#property indicator_plots   1
#property indicator_type1   DRAW_LINE

//--- indicator buffers
double ExtExpandBuffer[];
double ExtSMA5Buffer[];

//--- ボリンジャーバンド用ハンドル
int handle;
double UpperBandBuffer[];
double MiddleBandBuffer[];
double LowerBandBuffer[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
{
   //--- indicator buffers mapping
   SetIndexBuffer(0, ExtExpandBuffer, INDICATOR_DATA);

   IndicatorSetString(INDICATOR_SHORTNAME, "BandWidth & SMA5");

   //--- ボリンジャーバンド (±2σ) の取得
   handle = iBands(_Symbol, _Period, bb_period, 0, 2.0, PRICE_CLOSE);
   if (handle == INVALID_HANDLE)
      return INIT_FAILED;

   return INIT_SUCCEEDED;
}

//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
{
   // ボリンジャーバンドのバッファを取得
   if (CopyBuffer(handle, UPPER_BAND, 0, rates_total, UpperBandBuffer) < 0)
      return 0;
   if (CopyBuffer(handle, BASE_LINE, 0, rates_total, MiddleBandBuffer) < 0)
      return 0;
   if (CopyBuffer(handle, LOWER_BAND, 0, rates_total, LowerBandBuffer) < 0)
      return 0;
//---
   int start;
//---
   if(prev_calculated==0)
      start=0;
   else
      start=prev_calculated-1;

   // BandWidth の計算
   for (int i = start; i < rates_total && !IsStopped(); i++)
   {
         ExtExpandBuffer[i] = ((UpperBandBuffer[i] - LowerBandBuffer[i]) / MiddleBandBuffer[i]) * 100;
   }

   return rates_total;
}
//+------------------------------------------------------------------+
