//+------------------------------------------------------------------+
//|                                                   StdDevMA.mq5   |
//|                        Copyright 2025, YourName                  |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property indicator_separate_window
#property indicator_buffers 2
#property indicator_plots   2

//--- plot 1
#property indicator_label1  "Standard Deviation"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrDodgerBlue
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

//--- plot 2
#property indicator_label2  "MA of StdDev"
#property indicator_type2   DRAW_LINE
#property indicator_color2  clrRed
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1

//--- input parameters
input int    stddev_period = 20;  // 標準偏差の期間
input int    ma_period     = 5;   // 移動平均の期間

//--- indicator buffers
double ExtStdDevBuffer[];
double ExtMaStdDevBuffer[];

//--- handles for indicators
int stddev_handle;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
   //--- indicator buffers mapping
   SetIndexBuffer(0, ExtStdDevBuffer, INDICATOR_DATA);
   SetIndexBuffer(1, ExtMaStdDevBuffer, INDICATOR_DATA);

   //--- set indicator labels
   PlotIndexSetString(0, PLOT_LABEL, "Standard Deviation");
   PlotIndexSetString(1, PLOT_LABEL, "MA of StdDev");

   //--- set indicator colors
   PlotIndexSetInteger(0, PLOT_LINE_COLOR, clrDodgerBlue);
   PlotIndexSetInteger(1, PLOT_LINE_COLOR, clrRed);

   //--- create Standard Deviation indicator handle
   stddev_handle = iStdDev(_Symbol, _Period, stddev_period, 0, MODE_SMA, PRICE_CLOSE);
   if (stddev_handle == INVALID_HANDLE)
     {
      Print("Error creating StdDev handle");
      return (INIT_FAILED);
     }
   return (INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
   //--- check for sufficient bars
   if (rates_total < stddev_period + ma_period)
      return (0);

   //--- calculate Standard Deviation
   if (CopyBuffer(stddev_handle, 0, 0, rates_total, ExtStdDevBuffer) <= 0)
     {
      Print("Error copying StdDev data");
      return (0);
     }

   //--- calculate Moving Average of Standard Deviation
   for (int i = 0; i < rates_total; i++)
     {
      if (i >= ma_period - 1)
        {
         double sum = 0.0;
         for (int j = 0; j < ma_period; j++)
           {
            sum += ExtStdDevBuffer[i - j];
           }
         ExtMaStdDevBuffer[i] = sum / ma_period;
        }
      else
        {
         ExtMaStdDevBuffer[i] = 0.0;
        }
     }

   return (rates_total);
  }
//+------------------------------------------------------------------+
