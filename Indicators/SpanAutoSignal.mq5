//+------------------------------------------------------------------+
//|                                                     Ichimoku.mq5 |
//|                             Copyright 2000-2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2000-2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property description "Ichimoku Kinko Hyo"
//--- indicator settings
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   1
#property indicator_type1   DRAW_FILLING
#property indicator_color1  0x462305;0x0A0A48;
//--- input parameters
int InpTenkan=9;     // Tenkan-sen
int InpKijun=26;     // Kijun-sen
int InpSenkou=52;    // Senkou Span B
//--- indicator buffers
double    ExtSpanABuffer[];
double    ExtSpanBBuffer[];
//---
color bg_color_b = StringToColor("15,15,50");
color bg_color_s = StringToColor("45,0,20");
color sp_color_b = StringToColor("0,71,171");
color sp_color_s = StringToColor("80,20,20");
bool spanmodel_up = false;
int sp_signal_bar = 0;
double sp_high = 0;
double sp_low = 0;
//---
bool redspan_up = false;
int rd_signal_bar = 0;
double rd_high = 0;
double rd_low = 0;
int grace = 5;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
void OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,ExtSpanABuffer,INDICATOR_DATA);
   SetIndexBuffer(1,ExtSpanBBuffer,INDICATOR_DATA);
//---
   IndicatorSetInteger(INDICATOR_DIGITS,_Digits+1);
  }
//+------------------------------------------------------------------+
//| OnDeinit: インジケーター削除時の処理                             |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
// ラインを削除
   ObjectsDeleteAll(0);
  }
//+------------------------------------------------------------------+
//| Ichimoku Kinko Hyo                                               |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
   int start;
//---
   if(prev_calculated==0)
      start=0;
   else
      start=prev_calculated-1;
//--- main loop
   for(int i=start; i<rates_total && !IsStopped(); i++)
     {
      //--- tenkan sen
      double price_max=Highest(high,InpTenkan,i);
      double price_min=Lowest(low,InpTenkan,i);
      double tenkan_sen=(price_max+price_min)/2.0;
      //--- kijun sen
      price_max=Highest(high,InpKijun,i);
      price_min=Lowest(low,InpKijun,i);
      double kijun_sen=(price_max+price_min)/2.0;
      //--- senkou span a
      ExtSpanABuffer[i]=(tenkan_sen+kijun_sen)/2.0;
      //--- senkou span b
      price_max=Highest(high,InpSenkou,i);
      price_min=Lowest(low,InpSenkou,i);
      ExtSpanBBuffer[i]=(price_max+price_min)/2.0;
      //--- スパンモデルシグナル
      if(i>0)
        {
         if(ExtSpanABuffer[i-1]<ExtSpanBBuffer[i-1] && ExtSpanABuffer[i]>=ExtSpanBBuffer[i])
           {
            if(i-sp_signal_bar <= grace)
               DrawRectangle("sp_rect_" + (string)sp_signal_bar,sp_color_s,time[sp_signal_bar],sp_high,time[i],sp_low);
            DrawLine("sp_line_sell_" + (string)sp_signal_bar,sp_color_s,STYLE_SOLID,1,time[sp_signal_bar],sp_low,time[i],sp_low);
            spanmodel_up = true;
            sp_signal_bar = i;
           }
         if(ExtSpanABuffer[i-1]>ExtSpanBBuffer[i-1] && ExtSpanABuffer[i]<=ExtSpanBBuffer[i])
           {
            if(i-sp_signal_bar <= grace)
               DrawRectangle("sp_rect_" + (string)sp_signal_bar,sp_color_b,time[sp_signal_bar],sp_high,time[i],sp_low);
            DrawLine("sp_line_buy_" + (string)sp_signal_bar,sp_color_b,STYLE_SOLID,1,time[sp_signal_bar],sp_high,time[i],sp_high);
            spanmodel_up = false;
            sp_signal_bar = i;
           }
         if(spanmodel_up && i-sp_signal_bar <= grace)
           {
            if(i-sp_signal_bar < grace)
              {
               sp_high = Highest(high,i-sp_signal_bar+1,i+1);
               sp_low = Lowest(low,i-sp_signal_bar+1,i+1);
               DrawRectangle("sp_rect_" + (string)sp_signal_bar,sp_color_b,time[sp_signal_bar],sp_high,time[i],sp_low);
              }
            else
               DrawRectangle("sp_rect_" + (string)sp_signal_bar,sp_color_b,time[sp_signal_bar],sp_high,time[i-1],sp_low);
           }
         if(!spanmodel_up && i-sp_signal_bar <= grace)
           {
            if(i-sp_signal_bar < grace)
              {
               sp_high = Highest(high,i-sp_signal_bar+1,i+1);
               sp_low = Lowest(low,i-sp_signal_bar+1,i+1);
               DrawRectangle("sp_rect_" + (string)sp_signal_bar,sp_color_s,time[sp_signal_bar],sp_high,time[i],sp_low);
              }
            else
               DrawRectangle("sp_rect_" + (string)sp_signal_bar,sp_color_s,time[sp_signal_bar],sp_high,time[i-1],sp_low);
           }
        }
      //--- 赤色スパンシグナル
      if(i>InpSenkou)
        {
         if(!redspan_up)
           {
            if(spanmodel_up && Highest(high,InpSenkou-1,i-1)<close[i])
              {
               DrawRectangle("bg_" + (string)rd_signal_bar,bg_color_s,time[rd_signal_bar],DBL_MAX,time[i],DBL_MIN);
               DrawLine("rd_line_sell_" + (string)rd_signal_bar,sp_color_s,STYLE_SOLID,2,time[rd_signal_bar],rd_low,time[i],rd_low);
               DrawVLine("redspan_buy_" + TimeToString(time[i]),time[i],sp_color_b,STYLE_DASHDOTDOT);
               redspan_up = true;
               rd_signal_bar = i;
              }
           }
         if(redspan_up)
           {
            if(!spanmodel_up && Lowest(low,InpSenkou-1,i-1)>close[i])
              {
               DrawRectangle("bg_" + (string)rd_signal_bar,bg_color_b,time[rd_signal_bar],DBL_MAX,time[i],DBL_MIN);
               DrawLine("rd_line_buy_" + (string)rd_signal_bar,sp_color_b,STYLE_SOLID,2,time[rd_signal_bar],rd_high,time[i],rd_high);
               DrawVLine("redspan_sell_" + TimeToString(time[i]),time[i],sp_color_s,STYLE_DASHDOT);
               redspan_up = false;
               rd_signal_bar = i;
              }
           }
         if(redspan_up)
           {
            if(i-rd_signal_bar <= grace)
              {
               if(i-rd_signal_bar < grace)
                 {
                  rd_high = Highest(high,i-rd_signal_bar+1,i);
                  rd_low = Lowest(low,i-rd_signal_bar+1,i);
                  DrawLine("rd_line_" + (string)rd_signal_bar,sp_color_b,STYLE_SOLID,3,time[rd_signal_bar],rd_high,time[i],rd_high);
                 }
               else
                  DrawLine("rd_line_" + (string)rd_signal_bar,sp_color_b,STYLE_SOLID,3,time[rd_signal_bar],rd_high,time[i-1],rd_high);
              }
            DrawLine("rd_line_buy_" + (string)rd_signal_bar,sp_color_b,STYLE_SOLID,2,time[rd_signal_bar],rd_high,time[i],rd_high);
            DrawRectangle("bg_" + (string)rd_signal_bar,bg_color_b,time[rd_signal_bar],DBL_MAX,time[i],DBL_MIN);
           }
         else
           {
            if(i-rd_signal_bar <= grace)
              {
               if(i-rd_signal_bar < grace)
                 {
                  rd_high = Highest(high,i-rd_signal_bar+1,i);
                  rd_low = Lowest(low,i-rd_signal_bar+1,i);
                  DrawLine("rd_line_" + (string)rd_signal_bar,sp_color_s,STYLE_SOLID,3,time[rd_signal_bar],rd_low,time[i],rd_low);
                 }
               else
                  DrawLine("rd_line_" + (string)rd_signal_bar,sp_color_s,STYLE_SOLID,3,time[rd_signal_bar],rd_low,time[i-1],rd_low);
              }
            DrawLine("rd_line_sell_" + (string)rd_signal_bar,sp_color_s,STYLE_SOLID,2,time[rd_signal_bar],rd_low,time[i],rd_low);
            DrawRectangle("bg_" + (string)rd_signal_bar,bg_color_s,time[rd_signal_bar],DBL_MAX,time[i],DBL_MIN);
           }
        }
     }
// チャート更新時にラインを更新
   MqlRates yesterday_rates[];
   int copied = CopyRates(_Symbol,PERIOD_D1,1,1,yesterday_rates);
   if(copied==1)
     {
      DrawYesterdayLine("yesterday_high",yesterday_rates[0].high);
      DrawYesterdayLine("yesterday_low",yesterday_rates[0].low);
     }
// 52本の高安
   int highest52_bar = iHighest(_Symbol,PERIOD_CURRENT,MODE_HIGH,InpSenkou-1,1);
   int lowest52_bar = iLowest(_Symbol,PERIOD_CURRENT,MODE_LOW,InpSenkou-1,1);
   double high_52 = iHigh(_Symbol,PERIOD_CURRENT,highest52_bar);
   double low_52 = iLow(_Symbol,PERIOD_CURRENT,lowest52_bar);
   datetime time_52 = iTime(_Symbol,PERIOD_CURRENT,InpSenkou);
   DrawLine("52_line_high",clrGold,STYLE_DOT,1,time_52,high_52,time[rates_total-1],high_52);
   DrawLine("52_line_low",clrGold,STYLE_DOT,1,time_52,low_52,time[rates_total-1],low_52);
   DrawLine("52_line_start",clrGold,STYLE_DOT,1,time_52,high_52,time_52,low_52);
//---
   return(rates_total);
  }
//+------------------------------------------------------------------+
//| get price_max value for range                                      |
//+------------------------------------------------------------------+
double Highest(const double& array[],const int range,int from_index)
  {
   double res=0;
   int idx=ArrayMaximum(array,from_index-range,range);
   if(idx>=0)
      res=array[idx];
   return(res);
  }
//+------------------------------------------------------------------+
//| get price_min value for range                                       |
//+------------------------------------------------------------------+
double Lowest(const double& array[],const int range,int from_index)
  {
   double res=0;
   int idx=ArrayMinimum(array,from_index-range,range);
   if(idx>=0)
      res=array[idx];
   return(res);
  }
//+------------------------------------------------------------------+
//| 垂直ラインを描画                                           |
//+------------------------------------------------------------------+
void DrawVLine(string lineName, datetime time, color clr,ENUM_LINE_STYLE style)
  {
// ラインが既に存在していれば削除
   if(ObjectFind(0, lineName) != -1)
      ObjectDelete(0, lineName);

// ラインを作成
   if(ObjectCreate(0, lineName, OBJ_VLINE, 0, time, 0))
     {
      // ラインのプロパティを設定
      ObjectSetInteger(0, lineName, OBJPROP_COLOR, clr);      // 色
      ObjectSetInteger(0, lineName, OBJPROP_WIDTH, 1);           // 太さ
      ObjectSetInteger(0, lineName, OBJPROP_STYLE, style); // スタイル
     }
   else
     {
      Print("ラインの描画に失敗しました");
     }
  }
//+------------------------------------------------------------------+
//| ラインを描画                                           |
//+------------------------------------------------------------------+
void DrawYesterdayLine(string lineName, double price)
  {
// ラインが既に存在していれば削除
   if(ObjectFind(0, lineName) != -1)
      ObjectDelete(0, lineName);

// ラインを作成
   if(ObjectCreate(0, lineName, OBJ_HLINE, 0, 0, price))
     {
      // ラインのプロパティを設定
      ObjectSetInteger(0, lineName, OBJPROP_COLOR, clrGray);      // 色
      ObjectSetInteger(0, lineName, OBJPROP_WIDTH, 2);           // 太さ
      ObjectSetInteger(0, lineName, OBJPROP_STYLE, STYLE_SOLID); // スタイル
     }
   else
     {
      Print("ラインの描画に失敗しました");
     }
  }
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
void DrawRectangle(string rectangleName,color clr,datetime time1, double price1, datetime time2,double  price2)
  {
// 四角形オブジェクトを作成
   if(!ObjectCreate(0, rectangleName, OBJ_RECTANGLE, 0, time1, price1, time2, price2))
     {
      Print("四角形の作成に失敗しました: ", GetLastError());
      return;
     }

// 四角形の色を設定
   ObjectSetInteger(0, rectangleName, OBJPROP_COLOR, clr);
   ObjectSetInteger(0, rectangleName, OBJPROP_STYLE, STYLE_SOLID);
   ObjectSetInteger(0, rectangleName, OBJPROP_WIDTH, 2);

// 四角形を半透明に設定
   ObjectSetInteger(0, rectangleName, OBJPROP_BACK, true);
   ObjectSetInteger(0, rectangleName, OBJPROP_FILL, true);  // 塗りつぶし
  }

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
void DrawLine(string lineName,color clr,long style,int width,datetime time1, double price1, datetime time2,double  price2)
  {
// 線オブジェクトを作成
   if(!ObjectCreate(0, lineName, OBJ_TREND, 0, time1, price1, time2, price2))
     {
      Print("線の作成に失敗しました: ", GetLastError());
      return;
     }

// 線のプロパティを設定
   ObjectSetInteger(0, lineName, OBJPROP_COLOR, clr);      // 線の色
   ObjectSetInteger(0, lineName, OBJPROP_STYLE, style);  // 線のスタイル（点線）
   ObjectSetInteger(0, lineName, OBJPROP_WIDTH, width);          // 線の太さ
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
