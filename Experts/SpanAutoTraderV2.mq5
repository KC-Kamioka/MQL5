//+------------------------------------------------------------------+
//|                                             SpanAutoTraderV2.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <MQL5Converter\Converter.mqh>
#include <Trade\Trade.mqh>
#include <SATCommon.mqh>
//+------------------------------------------------------------------+
//| struct                                                           |
//+------------------------------------------------------------------+
struct TradeData
  {
   int               Seq;
   string            TradeSymbol;
   ENUM_TIMEFRAMES   SymbolPeriod;
   double            LotSize;
   int               Tp;
   int               Sl;
   int               OpenCd;
   string            Open;
   int               TakeProfitCd;
   string            TakeProfit;
   int               StopLossCd;
   string            StopLoss;
   string            TicketNo;
  };
//+------------------------------------------------------------------+
//| globals                                                          |
//+------------------------------------------------------------------+
int BB_Period = 21;   // ボリンジャーバンドの期間
CTrade trade;
int DBHandle;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
// 共通DB作成
   CreateSATCommon();
   if(MQLInfoInteger(MQL_TESTER))
     {
      CreateCodeMaster();
      CreateTrade();
      CreateDefault();
     }
// 指定したファイル名のDBを開く
   OpenDB();
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
// DBを閉じる
   DatabaseClose(DBHandle);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   string tradeInfo;
//---
   TradeData tradeData[];
//---
   if(!ReadDB(tradeData))
      ExpertRemove();
//---
   for(int i=0; i<ArraySize(tradeData); i++)
     {
      //---
      SetRates(tradeData[i].TradeSymbol);
      // トレード情報セット
      tradeInfo = tradeInfo
                  + (string)tradeData[i].Seq + "."
                  + " " + tradeData[i].TradeSymbol
                  + " " + EnumToString(tradeData[i].SymbolPeriod)
                  + " " + (string)tradeData[i].LotSize
                  + " " + tradeData[i].Open
                  + " " + tradeData[i].TakeProfit
                  + " " + tradeData[i].StopLoss
                  + " " + tradeData[i].TicketNo
                  + "\n";
      // クローズ済みポジションデータ削除
      if(tradeData[i].TicketNo != NULL && !PositionSelectByTicket((int)tradeData[i].TicketNo))
        {
         if(!DeleteTradeData((int)tradeData[i].TicketNo))
            ExpertRemove();
        }
      // リアルタイムポジションオープン
      if(tradeData[i].TicketNo == NULL)
         OpenPositionRealTime(tradeData[i]);
      // リアルタイムポジションクローズ
      if(tradeData[i].TicketNo != NULL)
         TakeProfit(tradeData[i]);
      // 現在のバーが確定したタイミングで処理を実行
      static datetime lastBarTime = 0;
      if(iTime(tradeData[i].TradeSymbol, tradeData[i].SymbolPeriod, 0) != lastBarTime)
        {
         lastBarTime = iTime(tradeData[i].TradeSymbol, tradeData[i].SymbolPeriod, 0);
         for(int i=0; i<ArraySize(tradeData); i++)
           {
            // ポジションオープン
            if(tradeData[i].TicketNo == NULL)
               OpenPosition(tradeData[i]);
            // ポジションクローズ
            if(tradeData[i].TicketNo != NULL)
               StopLoss(tradeData[i]);
           }
        }
     }
   Comment(tradeInfo);
  }
//+------------------------------------------------------------------+
//| Get Rates                                                        |
//+------------------------------------------------------------------+
void SetRates(string tradeSymbol)
  {
   ArraySetAsSeries(Close, true);
   ArraySetAsSeries(High, true);
   ArraySetAsSeries(Low, true);
   ArraySetAsSeries(Open, true);
   ArraySetAsSeries(Time, true);
   ArraySetAsSeries(Volume, true);

   int count = Bars(tradeSymbol, _Period);

   CopyClose(tradeSymbol, _Period, 0, count, Close);
   CopyHigh(tradeSymbol, _Period, 0, count, High);
   CopyLow(tradeSymbol, _Period, 0, count, Low);
   CopyOpen(tradeSymbol, _Period, 0, count, Open);
   CopyTime(tradeSymbol, _Period, 0, count, Time);
   CopyTickVolume(tradeSymbol, _Period, 0, count, Volume);
  }
//+------------------------------------------------------------------+
//| Bollinger Band Buy                                          |
//+------------------------------------------------------------------+
bool BollingerBandSwipeUp(TradeData &tradeData,double deviation,int mode)
  {
//---
   bool ret=false;
// ボリンジャーバンドの計算
   double line = MQL4iBands(tradeData.TradeSymbol, 0, BB_Period, deviation, 0, PRICE_CLOSE, mode, 0);
// ボリンジャーバンド+2シグマを終値が上回った場合
   if(Open[0] < line && Close[0] >= line)
      ret = true;
   return ret;
  }
//+------------------------------------------------------------------+
//| Bollinger Band Sell                                          |
//+------------------------------------------------------------------+
bool BollingerBandSwipeDown(TradeData &tradeData,double deviation,int mode)
  {
   bool ret=false;
// ボリンジャーバンドの計算
   double line = MQL4iBands(tradeData.TradeSymbol, 0, BB_Period, deviation, 0, PRICE_CLOSE, mode, 0);
// ボリンジャーバンド-2シグマを終値が下回った場合
   if(Open[0] > line && Close[0] <= line)
      ret = true;
   return ret;
  }

//+------------------------------------------------------------------+
//| Bollinger Band Buy                                          |
//+------------------------------------------------------------------+
bool BollingerBandUpperBreak(TradeData &tradeData)
  {
   bool ret=false;
// ボリンジャーバンドの計算
   double upperBand = MQL4iBands(tradeData.TradeSymbol, 0, BB_Period, 2.0,0, PRICE_CLOSE, MODE_UPPER, 1);

// ボリンジャーバンド+2シグマを終値が上回った場合
   if(upperBand > 0 && Close[1] > upperBand)
      ret = true;
   return ret;
  }
//+------------------------------------------------------------------+
//| Bollinger Band Sell                                          |
//+------------------------------------------------------------------+
bool BollingerBandLowerBreak(TradeData &tradeData)
  {
   bool ret=false;
// ボリンジャーバンドの計算
   double lowerBand = MQL4iBands(tradeData.TradeSymbol, 0, BB_Period, 2.0, 0, PRICE_CLOSE, MODE_LOWER, 1);
// ボリンジャーバンド-2シグマを終値が下回った場合
   if(lowerBand > 0 && Close[1] < lowerBand)
      ret = true;
   return ret;
  }
//+------------------------------------------------------------------+
//| Bollinger Band Buy Open                                         |
//+------------------------------------------------------------------+
bool BollingerBandUpperReturn(TradeData &tradeData)
  {
   bool ret=false;
   double upperBand = MQL4iBands(tradeData.TradeSymbol, 0, BB_Period, 1.0, 0, PRICE_CLOSE, MODE_UPPER, 1);
   double middleBand = MQL4iBands(tradeData.TradeSymbol, 0, BB_Period, 1.0, 0, PRICE_CLOSE, MODE_MAIN, 1);
// 終値が+1シグマを下回った場合
   if(Close[1] < upperBand && Close[1] > middleBand)
      ret = true;
   return ret;
  }
//+------------------------------------------------------------------+
//| Bollinger Band Sell Open                                         |
//+------------------------------------------------------------------+
bool BollingerBandLowerReturn(TradeData &tradeData)
  {
   bool ret=false;
   double lowerBand = MQL4iBands(tradeData.TradeSymbol, 0, BB_Period, 1.0, 0, PRICE_CLOSE, MODE_LOWER, 1);
   double middleBand = MQL4iBands(tradeData.TradeSymbol, 0, BB_Period, 1.0, 0, PRICE_CLOSE, MODE_MAIN, 1);
// 終値が-1シグマを上回った場合
   if(Close[1] > lowerBand && Close[1] < middleBand)
      ret = true;
   return ret;
  }
//+------------------------------------------------------------------+
//| Candle Buy Open                                         |
//+------------------------------------------------------------------+
bool ReversalUp(TradeData &tradeData)
  {
   bool ret=false;
   double point;
   SymbolInfoDouble(tradeData.TradeSymbol, SYMBOL_POINT, point);
   double preHighPrice = iHigh(tradeData.TradeSymbol,tradeData.SymbolPeriod,1)+MarketInfo(tradeData.TradeSymbol,SYMBOL_SPREAD)*point;
// 前回高値を上回った場合
   if(preHighPrice < iClose(tradeData.TradeSymbol,tradeData.SymbolPeriod,0))
     {
      ret = true;
     }
   return ret;
  }
//+------------------------------------------------------------------+
//| Candle Sell Open                                         |
//+------------------------------------------------------------------+
bool ReversalDown(TradeData &tradeData)
  {
   bool ret=false;
   double point;
   SymbolInfoDouble(tradeData.TradeSymbol, SYMBOL_POINT, point);
   double preLowPrice = iLow(tradeData.TradeSymbol,tradeData.SymbolPeriod,1)-MarketInfo(tradeData.TradeSymbol,SYMBOL_SPREAD)*point;
// 前回高値を上回った場合
   if(preLowPrice > iClose(tradeData.TradeSymbol,tradeData.SymbolPeriod,0))
     {
      ret = true;
     }
   return ret;
  }
//+------------------------------------------------------------------+
//| Buyポジションを開く関数                                          |
//+------------------------------------------------------------------+
void OpenPositionRealTime(TradeData &tradeData)
  {
   if((opLogic)tradeData.OpenCd == Now_Buy)
      OpenBuyPosition(tradeData);
   if((opLogic)tradeData.OpenCd == Now_Sell)
      OpenSellPosition(tradeData);
   if((opLogic)tradeData.OpenCd == BB_Touch_2_Sigma_Buy)
     {
      // 2σオープン
      if(BollingerBandSwipeDown(tradeData,2.0,MODE_LOWER))
         OpenBuyPosition(tradeData);
     }
   if((opLogic)tradeData.OpenCd == BB_Touch_2_Sigma_Sell)
     {
      // 2σオープン
      if(BollingerBandSwipeUp(tradeData,2.0,MODE_UPPER))
         OpenSellPosition(tradeData);
     }
   if((opLogic)tradeData.OpenCd == BB_Touch_1_Sigma_Buy)
     {
      // 1σオープン
      if(BollingerBandSwipeDown(tradeData,1.0,MODE_UPPER))
         OpenBuyPosition(tradeData);
     }
   if((opLogic)tradeData.OpenCd == BB_Touch_1_Sigma_Sell)
     {
      // 1σオープン
      if(BollingerBandSwipeUp(tradeData,1.0,MODE_LOWER))
         OpenSellPosition(tradeData);
     }
   if((opLogic)tradeData.OpenCd == BB_Touch_Center_Buy)
     {
      // センターオープン
      if(BollingerBandSwipeDown(tradeData,1.0,MODE_MAIN))
         OpenBuyPosition(tradeData);
     }
   if((opLogic)tradeData.OpenCd == BB_Touch_Center_Sell)
     {
      // センターオープン
      if(BollingerBandSwipeUp(tradeData,1.0,MODE_MAIN))
         OpenSellPosition(tradeData);
     }
   if((opLogic)tradeData.OpenCd == Reversal_Buy)
     {
      // リバーサルオープン
      if(ReversalUp(tradeData))
         OpenBuyPosition(tradeData);
     }
   if((opLogic)tradeData.OpenCd == Reversal_Sell)
     {
      // リバーサルオープン
      if(ReversalDown(tradeData))
         OpenSellPosition(tradeData);
     }
  }
//+------------------------------------------------------------------+
//| Buyポジションを開く関数                                          |
//+------------------------------------------------------------------+
void OpenPosition(TradeData &tradeData)
  {
   if((opLogic)tradeData.OpenCd == BB_Break_Buy)
     {
      // ボリンジャーバンド+2σブレイクオープン
      if(BollingerBandUpperBreak(tradeData))
         OpenBuyPosition(tradeData);
     }
   if((opLogic)tradeData.OpenCd == BB_Break_Sell)
     {
      // ボリンジャーバンド-2σブレイクオープン
      if(BollingerBandLowerBreak(tradeData))
         OpenSellPosition(tradeData);
     }
   if((opLogic)tradeData.OpenCd == BB_Return_Buy)
     {
      // LowerReturn
      if(BollingerBandLowerReturn(tradeData))
         OpenBuyPosition(tradeData);
     }
   if((opLogic)tradeData.OpenCd == BB_Return_Sell)
     {
      // UpperReturn
      if(BollingerBandUpperReturn(tradeData))
         OpenSellPosition(tradeData);
     }
  }
//+------------------------------------------------------------------+
//| Buyポジションを開く関数                                          |
//+------------------------------------------------------------------+
void OpenBuyPosition(TradeData &tradeData)
  {
   double point;
   SymbolInfoDouble(tradeData.TradeSymbol, SYMBOL_POINT, point);
   double askPrice = SymbolInfoDouble(tradeData.TradeSymbol, SYMBOL_ASK);
   double bidPrice = SymbolInfoDouble(tradeData.TradeSymbol, SYMBOL_BID);
   //double sl = askPrice-tradeData.Sl*point;
   //double tp = bidPrice+tradeData.Tp*point;
   double sl = askPrice-2000*point;
   double tp = 0;
   if(tradeData.TradeSymbol == NULL)
      tradeData.TradeSymbol=_Symbol;

   int ticket = OrderSend(tradeData.TradeSymbol, OP_BUY, tradeData.LotSize, askPrice, 3, sl, tp, EnumToString((opLogic)tradeData.OpenCd), (int)ChartID(), 0, clrBlue);
   if(ticket < 0)
     {
      Print("Error opening buy position: ", GetLastError());
     }
   else
     {
      Print("Buy position opened successfully. Ticket: ", ticket);
      tradeData.TicketNo = (string)ticket;
      if(!UpdateTicketNo(tradeData))
         ExpertRemove();
     }
  }
//+------------------------------------------------------------------+
//| Sellポジションを開く関数                                         |
//+------------------------------------------------------------------+
void OpenSellPosition(TradeData &tradeData)
  {
   double point;
   SymbolInfoDouble(tradeData.TradeSymbol, SYMBOL_POINT, point);
   double askPrice = SymbolInfoDouble(tradeData.TradeSymbol, SYMBOL_ASK);
   double bidPrice = SymbolInfoDouble(tradeData.TradeSymbol, SYMBOL_BID);
   //double sl = bidPrice+tradeData.Sl*point;
   //double tp = askPrice-tradeData.Tp*point;
   double sl = bidPrice+2000*point;
   double tp = 0;
   if(tradeData.TradeSymbol == NULL)
      tradeData.TradeSymbol=_Symbol;

   int ticket = OrderSend(tradeData.TradeSymbol, OP_SELL, tradeData.LotSize, bidPrice, 3, sl, tp, EnumToString((opLogic)tradeData.OpenCd), (int)ChartID(), 0, clrRed);

   if(ticket < 0)
     {
      Print("Error opening sell position: ", GetLastError());
     }
   else
     {
      Print("Sell position opened successfully. Ticket: ", ticket);
      tradeData.TicketNo = (string)ticket;
      if(!UpdateTicketNo(tradeData))
         ExpertRemove();
     }
  }
//+------------------------------------------------------------------+
//| ポジションをクローズする関数                                |
//+------------------------------------------------------------------+
void TakeProfit(TradeData &tradeData)
  {
   if(!PositionSelectByTicket((int)tradeData.TicketNo))
     {
      int  position_total =PositionsTotal();
      for(int i=0;i<position_total;i++)
        {
         tradeData.TicketNo = (string)PositionGetTicket(i);
        }
      Print("ポジション選択失敗 エラーコード: ", GetLastError());
      return;
     }
   if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
     {
      if((tpLogic)tradeData.TakeProfitCd == BB_Touch_2_Sigma)
        {
         if(BollingerBandSwipeUp(tradeData,2.0,MODE_UPPER))
            ClosePosition(tradeData);
        }
      if((tpLogic)tradeData.TakeProfitCd == BB_Touch_1_Sigma)
        {
         if(BollingerBandSwipeUp(tradeData,1.0,MODE_LOWER))
            ClosePosition(tradeData);
        }
      if((tpLogic)tradeData.TakeProfitCd == BB_Touch_Center)
        {
         if(BollingerBandSwipeUp(tradeData,1.0,MODE_MAIN))
            ClosePosition(tradeData);
        }
      if((tpLogic)tradeData.TakeProfitCd == Trailing)
        {
         if(ReversalDown(tradeData))
            ClosePosition(tradeData);
        }
     }
   if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
     {
      if((tpLogic)tradeData.TakeProfitCd == BB_Touch_2_Sigma)
        {
         if(BollingerBandSwipeDown(tradeData,2.0,MODE_LOWER))
            ClosePosition(tradeData);
        }
      if((tpLogic)tradeData.TakeProfitCd == BB_Touch_1_Sigma)
        {
         if(BollingerBandSwipeDown(tradeData,1.0,MODE_UPPER))
            ClosePosition(tradeData);
        }
      if((tpLogic)tradeData.TakeProfitCd == BB_Touch_Center)
        {
         if(BollingerBandSwipeDown(tradeData,1.0,MODE_MAIN))
            ClosePosition(tradeData);
        }
      if((tpLogic)tradeData.TakeProfitCd == Trailing)
        {
         if(ReversalUp(tradeData))
            ClosePosition(tradeData);
        }
     }
  }
//+------------------------------------------------------------------+
//| ポジションをクローズする関数                                |
//+------------------------------------------------------------------+
void StopLoss(TradeData &tradeData)
  {
   if(!PositionSelectByTicket((int)tradeData.TicketNo))
     {
      Print("ポジション選択失敗 エラーコード: ", GetLastError());
      return;
     }
   if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
     {
      if((slLogic)tradeData.StopLossCd == BB_Break_1_Sigma)
        {
         // ボリンジャーバンド+1σクローズ
         if(BollingerBandUpperReturn(tradeData))
            ClosePosition(tradeData);
        }
      if((slLogic)tradeData.StopLossCd == BB_Break_2_Sigma)
        {
         // ボリンジャーバンド-2σブレイククローズ
         if(BollingerBandLowerBreak(tradeData))
            ClosePosition(tradeData);
        }
     }
   if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
     {
      if((slLogic)tradeData.StopLossCd == BB_Break_1_Sigma)
        {
         // ボリンジャーバンド-1σクローズ
         if(BollingerBandLowerReturn(tradeData))
            ClosePosition(tradeData);
        }
      if((slLogic)tradeData.StopLossCd == BB_Break_2_Sigma)
        {
         // ボリンジャーバンド+2σブレイククローズ
         if(BollingerBandUpperBreak(tradeData))
            ClosePosition(tradeData);
        }
     }
  }
//+------------------------------------------------------------------+
//| ポジションを修正する関数                                |
//+------------------------------------------------------------------+
void ModifyPosition(TradeData &tradeData,double sl,double tp)
  {
   if(!trade.PositionModify((int)tradeData.TicketNo,sl,tp))
     {
      Print("Error modifying position: ", GetLastError());
     }
   else
     {
      Print("Position modified successfully. Ticket: ", tradeData.TicketNo);
     }
  }
//+------------------------------------------------------------------+
//| ポジションをクローズする関数                                |
//+------------------------------------------------------------------+
void ClosePosition(TradeData &tradeData)
  {
   if(!trade.PositionClose((int)tradeData.TicketNo))
     {
      Print("Error closing position: ", GetLastError());
     }
   else
     {
      Print("Position closed successfully. Ticket: ", tradeData.TicketNo);
      if(!DeleteTradeData((int)tradeData.TicketNo))
         ExpertRemove();
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
int OpenWriteDB()
  {
// 指定したファイル名のDBを開く
   int writeDBHandle=DatabaseOpen(FileName, DATABASE_OPEN_READWRITE);
   if(writeDBHandle==INVALID_HANDLE)
     {
      Print("DB: ", FileName, " DB読込 エラーコード: ", GetLastError());
      DatabaseClose(writeDBHandle);
     }
   return writeDBHandle;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OpenDB()
  {
// 指定したファイル名のDBを開く
   DBHandle=DatabaseOpen(FileName, DATABASE_OPEN_READONLY);
   if(DBHandle==INVALID_HANDLE)
     {
      Print("DB: ", FileName, " DB読込 エラーコード: ", GetLastError());
      DatabaseClose(DBHandle);
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool UpdateTicketNo(TradeData &tradeData)
  {
// 指定したファイル名のDBを開く
   int writeDBHandle=OpenWriteDB();
   string sql="UPDATE Trade SET Symbol = '"+tradeData.TradeSymbol+"' ,TicketNo = '"+tradeData.TicketNo+"' "+"WHERE Seq = "+(string)tradeData.Seq;
   if(!DatabaseExecute(writeDBHandle, sql))
     {
      Print("データの更新 エラーコード: ", GetLastError());
      DatabaseClose(writeDBHandle);
      return false;
     }
// DBを閉じる
   DatabaseClose(writeDBHandle);
   return true;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool DeleteTradeData(int ticket)
  {
// 指定したファイル名のDBを開く
   int writeDBHandle=OpenWriteDB();
   string sql="DELETE FROM Trade"+" "+"WHERE TicketNo = "+(string)ticket;
   if(!DatabaseExecute(writeDBHandle, sql))
     {
      Print("データの削除 エラーコード: ", GetLastError());
      DatabaseClose(writeDBHandle);
      return false;
     }
// DBを閉じる
   DatabaseClose(writeDBHandle);
   return true;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool ReadDB(TradeData &tradeData[])
  {
//---
   string TableName="Trade";
// リクエストハンドルを作成
   string sql = "SELECT";
   sql = sql + " " + "Seq";
   sql = sql + "," + "Symbol";
   sql = sql + "," + "PeriodCd";
   sql = sql + "," + "LotSize";
   sql = sql + "," + "TpPoint";
   sql = sql + "," + "SlPoint";
   sql = sql + "," + "OpenCd";
   sql = sql + "," + "Open";
   sql = sql + "," + "TakeProfitCd";
   sql = sql + "," + "TakeProfit";
   sql = sql + "," + "StopLossCd";
   sql = sql + "," + "StopLoss";
   sql = sql + "," + "TicketNo";
   sql = sql + " " + "FROM "+TableName;
   sql = sql + " " + "ORDER BY TicketNo IS NULL";
   int request=DatabasePrepare(DBHandle, sql);
   if(request==INVALID_HANDLE)
     {
      Print("リクエスト エラーコード: ", GetLastError());
      DatabaseClose(DBHandle);
      return false;
     }

// リクエストのレコードの数だけ繰り返す
   int i=0;
   TradeData tr;
//for(int i=0; DatabaseReadBind(request,tr); i++)
   while(DatabaseReadBind(request,tr))
     {
      ArrayResize(tradeData,i+1);
      tradeData[i] = tr;
      i=i+1;
     }
// 作成されたリクエストを削除
   DatabaseFinalize(request);

   return true;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
