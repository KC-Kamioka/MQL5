//+------------------------------------------------------------------+
//|                                                 SMAutoTrader.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <Trade\Trade.mqh>
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int max_positions = 2;
double sl_point = 10000;
int b_walk_count = 9;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
struct values_struct
  {
   double            close;
   double            high;
   double            low;
   double            open;
   double            bb_plus_2_sigma;
   double            bb_plus_1_sigma;
   double            sma;
   double            bb_miuns_1_sigma;
   double            bb_miuns_2_sigma;
   double            band_walk_up_count;
   double            band_walk_down_count;
   double            band_2_above;
   double            band_2_below;
   double            s_span_a;
   double            s_span_b;
   double            rsi;
   double            macd_main;
   double            macd_signal;
   double            sd;
  };

struct handle_struct
  {
   int               sma;
   int               bfa;
   int               rsi;
   int               macd;
   int               sd;
  };

struct pos_info
  {
   ulong             ticket;
   ulong             magic;
   ulong             op_time;
  };

enum ENUM_BFA_MAGIC
  {
   BB_BREAK_B,
   BB_BREAK_S,
   MACD_CROSS_B,
   MACD_CROSS_S
  };
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int bb_period = 21;
double lot_size=0.1;
int element_count=30;
ENUM_TIMEFRAMES tf_upper = PERIOD_H1;
ENUM_TIMEFRAMES tf_middle = PERIOD_M15;
ENUM_TIMEFRAMES tf_lower = PERIOD_M5;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
CTrade trade;
handle_struct handle_upper;
handle_struct handle_middle;
handle_struct handle_lower;
values_struct values_upper[];
values_struct values_middle[];
values_struct values_lower[];
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   ArrayResize(values_upper,element_count);
   ArrayResize(values_middle,element_count);
   ArrayResize(values_lower,element_count);
//---
   if(!SetHandles(tf_upper,handle_upper))
      return(INIT_FAILED);
   if(!SetHandles(tf_middle,handle_middle))
      return(INIT_FAILED);
   if(!SetHandles(tf_lower,handle_lower))
      return(INIT_FAILED);

   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
// オブジェクト削除
   ObjectsDeleteAll(0);
  }//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//--- 初期化処理
   if(!SetValues(tf_upper, handle_upper, values_upper))
      return;
   if(!SetValues(tf_middle, handle_middle, values_middle))
      return;
   if(!SetValues(tf_lower, handle_lower, values_lower))
      return;

//--- ポジション情報取得
   pos_info pi[];
   SetPosInfo(pi);

//---
   MqlDateTime stm;
   datetime tm = TimeCurrent(stm);
   if(stm.hour >= 19)
     {
      LosscutAll(pi);
      return;
     }

//--- ロスカット（エグジットのタイミング確認）
   ExitBBBreak(pi);
//ExitLightly(pi);

//--- スプレッドチェック
   if(SymbolInfoInteger(_Symbol, SYMBOL_SPREAD) > 10)
      return;

//--- タイミング確認用の静的変数
   static datetime lastBarTimeExit = 0; // エグジット用
   static datetime lastBarTimeEntry = 0; // エントリー用

//--- ロスカット（エグジットのタイミング確認）
   if(CheckTiming(tf_lower, lastBarTimeExit))
     {
      OpenBBBreak(pi);
      //OpenLightly(pi);
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OpenLightly(pos_info &pi[])
  {
//---
//--- 環境認識
   if(AboveCloud(values_upper[1]))
     {
      if(AboveUpper1Sigma(values_upper[1]))
        {
         if(values_upper[1].rsi <= 70)
           {
            if(SDRise(values_upper,1))
              {
               //---　詳細分析
               //if(AboveCloud(values_middle[0]))
               //  {
               //   if(AboveCenter(values_middle[0]) && CenterPositive(values_middle,0))
               //     { && MACDPlus(values_middle[1])
               if(MACDGoldenCross(values_middle,1))
                 {
                  if(SDRise(values_middle,1))
                    {
                     //---　エントリートリガー
                     //---
                     OpenBuyPosition(pi,MACD_CROSS_B);
                    }
                 }
              }
           }
        }
     }
//--- 環境認識
   if(BelowCloud(values_upper[1]))
     {
      if(BelowLower1Sigma(values_upper[1]))
        {
         if(values_upper[1].rsi >= 30)
           {
            if(SDRise(values_upper,1))
              {
               //---　詳細分析
               //if(BelowCloud(values_middle[0]))
               //  {
               //   if(BelowCenter(values_middle[0]) && CenterNegative(values_middle,0))
               //     { && MACDMinus(values_middle[1])
               if(MACDDeadCross(values_middle,1))
                 {
                  if(SDRise(values_middle,1))
                    {
                     //---　エントリートリガー
                     //---
                     OpenSellPosition(pi,MACD_CROSS_S);
                    }
                 }
               //    }
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void ExitLightly(pos_info &pi[])
  {
//---
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      //---
      if(iBarShift(_Symbol,tf_lower,pi[i].op_time) > 0)
        {
         if(pi[i].magic == MACD_CROSS_B)
           {
            if(Below2Sigma(values_lower[1]))
               PositionCloseByTicket(pi[i].ticket);
           }
         if(pi[i].magic == MACD_CROSS_S)
           {
            if(Above2Sigma(values_lower[1]))
               PositionCloseByTicket(pi[i].ticket);
           }
        }
      //---
      if(iBarShift(_Symbol,tf_middle,pi[i].op_time) > 0)
        {
         if(pi[i].magic == MACD_CROSS_B)
           {
            if(BelowCenter(values_middle[1]))
               PositionCloseByTicket(pi[i].ticket);
           }
         if(pi[i].magic == MACD_CROSS_S)
           {
            if(AboveCenter(values_middle[1]))
               PositionCloseByTicket(pi[i].ticket);
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OpenBBBreak(pos_info &pi[])
  {
//---
//--- 環境認識
   if(AboveCloud(values_upper[1]))
     {
      if(Above2Sigma(values_upper[1]))
        {
         //---　詳細分析
         if(AboveUpper1Sigma(values_middle[1]))
           {
            //---　エントリートリガー
            if(values_lower[1].macd_main > values_lower[1].macd_signal)
              {
               if(AboveCenter(values_lower[1]))
                  OpenBuyPosition(pi,BB_BREAK_B);
              }
           }
        }
     }
//---
//--- 環境認識
   if(BelowCloud(values_upper[1]))
     {
      if(Below2Sigma(values_upper[1]))
        {
         //---　詳細分析
         if(BelowLower1Sigma(values_middle[1]))
           {
            //---　エントリートリガー
            if(values_lower[1].macd_main < values_lower[1].macd_signal)
              {
               if(BelowCenter(values_lower[1]))
                  OpenSellPosition(pi,BB_BREAK_S);
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void ExitBBBreak(pos_info &pi[])
  {
//---
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      //---
      if(iBarShift(_Symbol,tf_lower,pi[i].op_time) > 0)
        {
         if(pi[i].magic == BB_BREAK_B)
           {
            if(BelowCenter(values_lower[1]))
               PositionCloseByTicket(pi[i].ticket);
           }
         if(pi[i].magic == BB_BREAK_S)
           {
            if(AboveCenter(values_lower[1]))
               PositionCloseByTicket(pi[i].ticket);
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void PositionCloseByTicket(ulong ticket)
  {
   trade.PositionClose(ticket);
  }
//+------------------------------------------------------------------+
//| エグジットやエントリーのタイミング確認関数                         |
//+------------------------------------------------------------------+
bool CheckTiming(ENUM_TIMEFRAMES tf, datetime &lastBarTime)
  {
   datetime currentBarTime = iTime(_Symbol, tf, 0);

// 現在のバーが更新されていない場合はスキップ
   if(currentBarTime == lastBarTime)
      return false;

// バーの更新時刻を記録
   lastBarTime = currentBarTime;
   return true;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void BreakEven(ulong ticket, double sl)
  {
//---
   PositionSelectByTicket(ticket);
//---
   if(!trade.PositionModify(ticket,PositionGetDouble(POSITION_PRICE_OPEN),0))
      Print("Error modified position: ", GetLastError());
   else
      Print("modified successfully.");
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool StopTrailing(ulong ticket)
  {
//---
   if(!PositionSelectByTicket(ticket))
      return false;
//---
   double op = PositionGetDouble(POSITION_PRICE_OPEN);
   double pos_sl = PositionGetDouble(POSITION_SL);
   double sl = MathAbs(op-pos_sl) / _Point;
//---
   if(sl == sl_point)
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OpenBuyPosition(pos_info &pi[], ENUM_BFA_MAGIC magic)
  {
//---
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      if(pi[i].magic == magic)
         return;
     }
//---
   trade.SetExpertMagicNumber(magic);
//---
   double askPrice = SymbolInfoDouble(_Symbol, SYMBOL_ASK);
   double sl = askPrice-sl_point*_Point;
   double tp = 0;
   if(!trade.PositionOpen(_Symbol,ORDER_TYPE_BUY,lot_size,askPrice,sl,tp,EnumToString(magic)))
      Print("Error opening buy position: ", GetLastError());
   else
      Print("Buy position opened successfully.");
//---
   CreateText(EnumToString(magic),TimeCurrent(),askPrice);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OpenSellPosition(pos_info &pi[], ENUM_BFA_MAGIC magic)
  {
//---
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      if(pi[i].magic == magic)
         return;
     }
//---
   trade.SetExpertMagicNumber(magic);
//---
   double bidPrice = SymbolInfoDouble(_Symbol, SYMBOL_BID);
   double sl = bidPrice+sl_point*_Point;
   double tp = 0;
   if(!trade.PositionOpen(_Symbol,ORDER_TYPE_SELL,lot_size,bidPrice,sl,tp,EnumToString(magic)))
      Print("Error opening sell position: ", GetLastError());
   else
      Print("Buy position opened successfully.");
//---
   CreateText(EnumToString(magic),TimeCurrent(),bidPrice);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool CreateText(string text,datetime time,double price)
  {
   string name = TimeToString(time) + "_" + text;
//--- エラー値をリセットする
   ResetLastError();
//--- テキストオブジェクトを作成する
   if(!ObjectCreate(0,name,OBJ_TEXT,0,time,price))
     {
      Print(__FUNCTION__,
            ": failed to create \"Text\" object! Error code = ",GetLastError());
      return(false);
     }
//--- テキストを設定する
   ObjectSetString(0,name,OBJPROP_TEXT,text);
//--- テキストの傾斜を設定する
//   ObjectSetDouble(0,name,OBJPROP_ANGLE,angle);
//--- アンカーの種類を設定
//   ObjectSetInteger(0,name,OBJPROP_ANCHOR,anchor);
//--- 色を設定
//   ObjectSetInteger(0,name,OBJPROP_COLOR,clr);
//---
   return(true);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void LosscutAll(pos_info &pi[])
  {
   for(int i=ArraySize(pi)-1;i>=0;i--)
      PositionCloseByTicket(pi[i].ticket);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void SetPosInfo(pos_info &pi[])
  {
//---
   int total = PositionsTotal();
   if(total == 0)
      return;
   ArrayResize(pi,total);
//---
   for(int i=total-1;i>=0;i--)
     {
      //--- 自動的にポジションを選択して、そのプロパティにアクセスし、次のポジションのチケットを取得する
      ulong ticket=PositionGetTicket(i);
      if(ticket==0)
         continue;
      pi[i].ticket = ticket;
      pi[i].magic = PositionGetInteger(POSITION_MAGIC);
      pi[i].op_time = (datetime)PositionGetInteger(POSITION_TIME);
     }
   return;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool SDRise(values_struct &values[], int idx)
  {
//---
   if(values[idx+1].sd >= values[idx].sd)
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool BandExpand(values_struct &values[], int idx)
  {
//---
//   if(values[idx+2].sd <= values[idx+1].sd)
//      return false;
//---
   if(values[idx+1].bb_plus_2_sigma >= values[idx].bb_plus_2_sigma)
      return false;
//---
   if(values[idx+1].bb_miuns_2_sigma <= values[idx].bb_miuns_2_sigma)
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool RSIRange(values_struct &values)
  {
   if(40 < values.rsi && values.rsi < 60)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool Above2Sigma(values_struct &values)
  {
   if(values.bb_plus_2_sigma < values.close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool Below2Sigma(values_struct &values)
  {
   if(values.bb_miuns_2_sigma > values.close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool BreakHigh(values_struct &values[], int shift)
  {
   double buffer[];
   ArrayResize(buffer,shift);
   for(int i=0;i<shift;i++)
      buffer[i] = values[i].high;
   int idx = ArrayMaximum(buffer,2);
   if(buffer[idx] < values[1].close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool BreakLow(values_struct &values[], int shift)
  {
   double buffer[];
   ArrayResize(buffer,shift);
   for(int i=0;i<shift;i++)
      buffer[i] = values[i].low;
   int idx = ArrayMinimum(buffer,2);
   if(buffer[idx] > values[1].close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool Break2SigmaUp(values_struct &values[])
  {
   if(values[2].bb_plus_2_sigma >= values[2].close && values[1].bb_plus_2_sigma < values[1].close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool Break2SigmaDown(values_struct &values[])
  {
   if(values[2].bb_miuns_2_sigma <= values[2].close && values[1].bb_miuns_2_sigma > values[1].close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool AboveUpper1Sigma(values_struct &values)
  {
   if(values.bb_plus_1_sigma < values.close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool BelowLower1Sigma(values_struct &values)
  {
   if(values.bb_miuns_1_sigma > values.close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool AboveLower1Sigma(values_struct &values)
  {
   if(values.bb_miuns_1_sigma < values.close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool BelowUpper1Sigma(values_struct &values)
  {
   if(values.bb_plus_1_sigma  > values.close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool CenterNegative(values_struct &values[],int idx)
  {
   if(values[idx+1].sma > values[idx].close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool CenterPositive(values_struct &values[],int idx)
  {
   if(values[idx+1].sma < values[idx].close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool BelowCenter(values_struct &values)
  {
   if(values.sma > values.close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool AboveCenter(values_struct &values)
  {
   if(values.sma < values.close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool Return1SigmaUp(values_struct &values[],int idx)
  {
   if(values[idx+1].bb_miuns_1_sigma >= values[idx+1].close && values[idx].bb_miuns_1_sigma < values[idx].close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool Return1SigmaDown(values_struct &values[],int idx)
  {
   if(values[idx+1].bb_plus_1_sigma <= values[idx+1].close && values[idx].bb_plus_1_sigma > values[idx].close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool CloudPositive(values_struct &values)
  {
   if(values.s_span_a > values.s_span_b)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool CloudNegative(values_struct &values)
  {
   if(values.s_span_a < values.s_span_b)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool MaAboveCloud(values_struct &values)
  {
   if(values.s_span_a < values.sma && values.s_span_b < values.sma)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool MaBelowCloud(values_struct &values)
  {
   if(values.s_span_a > values.sma && values.s_span_b > values.sma)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool AboveCloud(values_struct &values)
  {
   if(values.s_span_a < values.close && values.s_span_b < values.close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool BelowCloud(values_struct &values)
  {
   if(values.s_span_a > values.close && values.s_span_b > values.close)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool MACDPositive(values_struct &values[],int idx)
  {
   if(values[idx+1].macd_main > values[idx].macd_main)
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool MACDNegative(values_struct &values[],int idx)
  {
   if(values[idx+1].macd_main < values[idx].macd_main)
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool MACDPlus(values_struct &values)
  {
   if(values.macd_main < 0)
      return false;
   if(values.macd_signal < 0)
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool MACDMinus(values_struct &values)
  {
   if(values.macd_main > 0)
      return false;
   if(values.macd_signal > 0)
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool MACDGoldenCross(values_struct &values[],int idx)
  {
   if(values[idx+1].macd_main <= values[idx+1].macd_signal && values[idx].macd_main > values[idx].macd_signal)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool MACDDeadCross(values_struct &values[],int idx)
  {
   if(values[idx+1].macd_main >= values[idx+1].macd_signal && values[idx].macd_main < values[idx].macd_signal)
      return true;
   return false;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool SetHandles(ENUM_TIMEFRAMES tf,handle_struct &handle)
  {
// Bolinger Band 3σ
   handle.sma = iMA(_Symbol, tf, bb_period, 0, MODE_SMA, PRICE_CLOSE);
   if(handle.sma == INVALID_HANDLE)
      return false;

// Ichimoku Cloud
   handle.bfa = iCustom(_Symbol,tf,"BBFullAutoIndicator");
   if(handle.bfa == INVALID_HANDLE)
      return false;

// RSI
   handle.rsi = iRSI(_Symbol, tf,14,PRICE_CLOSE);
   if(handle.rsi == INVALID_HANDLE)
      return false;

// MACD
   handle.macd = iMACD(_Symbol, tf,12,26,9,PRICE_CLOSE);
   if(handle.macd == INVALID_HANDLE)
      return false;
   /*
   // ADX
   handle.adx = iADX(_Symbol, tf,14);
   if(handle.adx == INVALID_HANDLE)
      return false;

// Standard Deviation
   handle.sd = iStdDev(_Symbol, tf, bb_period, 0, MODE_SMA, PRICE_CLOSE);
   if(handle.sd == INVALID_HANDLE)
      return false;
   */

//---
   return true;
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool SetValues(ENUM_TIMEFRAMES tf,handle_struct &handle,values_struct &values[])
  {
   double buffer[];
   ArrayResize(buffer,element_count);
   ArraySetAsSeries(buffer,true);
// rates
   CopyClose(_Symbol, tf, 0, element_count, buffer);
   for(int i=element_count-1;i>=0;i--)
      values[i].close = buffer[i];
   CopyHigh(_Symbol, tf, 0, element_count, buffer);
   for(int i=element_count-1;i>=0;i--)
      values[i].high = buffer[i];
   CopyLow(_Symbol, tf, 0, element_count, buffer);
   for(int i=element_count-1;i>=0;i--)
      values[i].low = buffer[i];
   CopyOpen(_Symbol, tf, 0, element_count, buffer);
   for(int i=element_count-1;i>=0;i--)
      values[i].open = buffer[i];

// Bolinger Band Center
   if(CopyBuffer(handle.sma, 0, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].sma= buffer[i];

// ichimoku
   if(CopyBuffer(handle.bfa, 0, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].s_span_a= buffer[i];
   if(CopyBuffer(handle.bfa, 1, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].s_span_b= buffer[i];

// Bolinger Band +1σ
   if(CopyBuffer(handle.bfa, 2, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].bb_plus_1_sigma = buffer[i];

// Bolinger Band -1σ
   if(CopyBuffer(handle.bfa, 3, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].bb_miuns_1_sigma= buffer[i];

// Bolinger Band +2σ
   if(CopyBuffer(handle.bfa, 4, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].bb_plus_2_sigma = buffer[i];

// Bolinger Band -2σ
   if(CopyBuffer(handle.bfa, 5, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].bb_miuns_2_sigma= buffer[i];

// Band Walk Up
   if(CopyBuffer(handle.bfa, 6, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].band_walk_up_count = buffer[i];

// Band Walk Down
   if(CopyBuffer(handle.bfa, 7, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].band_walk_down_count = buffer[i];


   if(CopyBuffer(handle.bfa, 8, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].band_2_above= buffer[i];
   if(CopyBuffer(handle.bfa, 9, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].band_2_below= buffer[i];

// RSI
   if(CopyBuffer(handle.rsi, 0, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].rsi = buffer[i];

// MACD
   if(CopyBuffer(handle.macd, MAIN_LINE, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].macd_main = buffer[i];
   if(CopyBuffer(handle.macd, SIGNAL_LINE, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].macd_signal = buffer[i];
   /*
   // ADX
   if(CopyBuffer(handle.adx, MAIN_LINE, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].adx_main = buffer[i];
   if(CopyBuffer(handle.adx, PLUSDI_LINE, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].adx_di_p = buffer[i];
   if(CopyBuffer(handle.adx, MINUSDI_LINE, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].adx_di_m = buffer[i];

// Standard Deviation
   if(CopyBuffer(handle.sd, 0, 0, element_count, buffer) < 0)
      return false;
   for(int i=element_count-1;i>=0;i--)
      values[i].sd = buffer[i];
   */


//---
   return true;
  }
//+------------------------------------------------------------------+
