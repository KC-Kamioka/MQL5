//+------------------------------------------------------------------+
//|                                               SpanAutoSignal.mqh |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
//+------------------------------------------------------------------+
//| Include                                                          |
//+------------------------------------------------------------------+
#include <Common.mqh>
//+------------------------------------------------------------------+
//| Valiables                                                        |
//+------------------------------------------------------------------+
color clrBuffer1[]= {clrBlue,clrRed};
color clrBuffer2[]= {clrAquamarine,clrLightPink};
color clrBuffer3[]= {clrLightCyan,clrLinen};
//+------------------------------------------------------------------+
//| draw indicator                                                   |
//+------------------------------------------------------------------+
bool DrawSASIndicator(
   int            i
   ,double         &io_SMSignalBuffer[]
   ,double         &io_SMSignalTermBuffer[]
   ,double         &io_SMUpGraceStBuffer[]
   ,double         &io_SMUpGraceEdBuffer[]
   ,double         &io_SMDwGraceStBuffer[]
   ,double         &io_SMDwGraceEdBuffer[]
   ,double         &io_SMGraceHighBuffer[]
   ,double         &io_SMGraceLowBuffer[]
   ,double         &io_SMSignalEdBuffer[]
   ,double         &io_RSSignalBuffer[]
   ,double         &io_RSSignalTermBuffer[]
   ,double         &io_RSUpGraceStBuffer[]
   ,double         &io_RSUpGraceEdBuffer[]
   ,double         &io_RSDwGraceStBuffer[]
   ,double         &io_RSDwGraceEdBuffer[]
   ,double         &io_RSGraceHighBuffer[]
   ,double         &io_RSGraceLowBuffer[]
   ,double         &io_RSSignalEdBuffer[]
   ,double         &io_High52Buffer[]
   ,double         &io_Low52Buffer[]
   ,double         &io_Y_HighBuffer[]
   ,double         &io_Y_LowBuffer[]
)
  {
//+------------------------------------------------------------------+
//| SpanModel grace term                                             |
//+------------------------------------------------------------------+
//---
   datetime dtPreSmUpGraceSt=CnvDouble2Time(io_SMUpGraceStBuffer[i+1]);
   datetime dtPreSmUpGraceEd=CnvDouble2Time(io_SMUpGraceEdBuffer[i+1]);
   datetime dtSmUpGraceSt=CnvDouble2Time(io_SMUpGraceStBuffer[i]);
   datetime dtSmUpGraceEd=CnvDouble2Time(io_SMUpGraceEdBuffer[i]);
//---
   datetime dtPreSmDwGraceSt=CnvDouble2Time(io_SMDwGraceStBuffer[i+1]);
   datetime dtPreSmDwGraceEd=CnvDouble2Time(io_SMDwGraceEdBuffer[i+1]);
   datetime dtSmDwGraceSt=CnvDouble2Time(io_SMDwGraceStBuffer[i]);
   datetime dtSmDwGraceEd=CnvDouble2Time(io_SMDwGraceEdBuffer[i]);
//---
   datetime dtSmSignalEd=CnvDouble2Time(io_SMSignalEdBuffer[i]);
//--- シグナル
   if(io_SMSignalTermBuffer[i]==0)
     {
      if(io_SMSignalBuffer[i]==UP)
        {
         string strObjName="SMSignalB_"+TimeToString(dtSmUpGraceSt);
         if(!DeleteObject(strObjName))
            return false;
         if(!LineCreate(strObjName,OBJ_TREND,dtSmUpGraceSt,EMPTY_VALUE,dtSmUpGraceSt,0,clrBuffer1[0],STYLE_DASHDOT,1,true,true))
            return false;
        }
      if(io_SMSignalBuffer[i]==DOWN)
        {
         string strObjName="SMSignalS_"+TimeToString(dtSmDwGraceSt);
         if(!DeleteObject(strObjName))
            return false;
         if(!LineCreate(strObjName,OBJ_TREND,dtSmDwGraceSt,EMPTY_VALUE,dtSmDwGraceSt,0,clrBuffer1[1],STYLE_DASHDOT,1,true,true))
            return false;
        }
     }
   if(io_SMSignalBuffer[i]==UP)
     {
      //--- 猶予期間中
      string strObjName="SMSignalBGrace_"+TimeToString(dtSmUpGraceSt);
      if(!DeleteObject(strObjName))
         return false;
      if(!LineCreate(strObjName,OBJ_RECTANGLE,dtSmUpGraceSt,io_SMGraceHighBuffer[i],dtSmUpGraceEd,io_SMGraceLowBuffer[i],clrBuffer2[0],STYLE_SOLID,1,true,true))
         return false;
      //--- サポート・レジスタンス
      strObjName="SMSignalRes_"+TimeToString(dtSmUpGraceEd);
      if(!DeleteObject(strObjName))
         return false;
      if(!LineCreate(strObjName,OBJ_TREND,dtSmUpGraceEd,io_SMGraceHighBuffer[i],dtSmSignalEd,io_SMGraceHighBuffer[i],clrBuffer2[0],STYLE_SOLID,1,true,true))
         return false;
     }
   if(io_SMSignalBuffer[i]==DOWN)
     {
      //--- 猶予期間中
      string strObjName="SMSignalSGrace_"+TimeToString(dtSmDwGraceSt);
      if(!DeleteObject(strObjName))
         return false;
      if(!LineCreate(strObjName,OBJ_RECTANGLE,dtSmDwGraceSt,io_SMGraceHighBuffer[i],dtSmDwGraceEd,io_SMGraceLowBuffer[i],clrBuffer2[1],STYLE_SOLID,1,true,true))
         return false;
      //--- サポート・レジスタンス
      strObjName="SMSignalSup_"+TimeToString(dtSmDwGraceEd);
      if(!DeleteObject(strObjName))
         return false;
      if(!LineCreate(strObjName,OBJ_TREND,dtSmDwGraceEd,io_SMGraceLowBuffer[i],dtSmSignalEd,io_SMGraceLowBuffer[i],clrBuffer2[1],STYLE_SOLID,1,true,true))
         return false;
     }
//+------------------------------------------------------------------+
//| RedSpan grace term                                             |
//+------------------------------------------------------------------+
//---
   datetime dtPreRsUpGraceSt=CnvDouble2Time(io_RSUpGraceStBuffer[i+1]);
   datetime dtPreRsUpGraceEd=CnvDouble2Time(io_RSUpGraceEdBuffer[i+1]);
   datetime dtRsUpGraceSt=CnvDouble2Time(io_RSUpGraceStBuffer[i]);
   datetime dtRsUpGraceEd=CnvDouble2Time(io_RSUpGraceEdBuffer[i]);
//---
   datetime dtPreRsDwGraceSt=CnvDouble2Time(io_RSDwGraceStBuffer[i+1]);
   datetime dtPreRsDwGraceEd=CnvDouble2Time(io_RSDwGraceEdBuffer[i+1]);
   datetime dtRsDwGraceSt=CnvDouble2Time(io_RSDwGraceStBuffer[i]);
   datetime dtRsDwGraceEd=CnvDouble2Time(io_RSDwGraceEdBuffer[i]);
//---
   datetime dtPreRsSignalEd=CnvDouble2Time(io_RSSignalEdBuffer[i+1]);
   datetime dtRsSignalEd=CnvDouble2Time(io_RSSignalEdBuffer[i]);
//--- シグナル
   if(io_RSSignalTermBuffer[i]==0)
     {
      if(io_RSSignalBuffer[i]==UP)
        {
         string strObjName="RSSignalB_"+TimeToString(dtRsUpGraceSt);
         if(!DeleteObject(strObjName))
            return false;
         if(!LineCreate(strObjName,OBJ_TREND,dtRsUpGraceSt,EMPTY_VALUE,dtRsUpGraceSt,0,clrBuffer1[0],STYLE_DOT,1,true,true))
            return false;
        }
      if(io_RSSignalBuffer[i]==DOWN)
        {
         string strObjName="RSSignalS_"+TimeToString(dtRsDwGraceSt);
         if(!DeleteObject(strObjName))
            return false;
         if(!LineCreate(strObjName,OBJ_TREND,dtRsDwGraceSt,EMPTY_VALUE,dtRsDwGraceSt,0,clrBuffer1[1],STYLE_DOT,1,true,true))
            return false;
        }
     }
   if(io_RSSignalBuffer[i]==UP)
     {
      //--- 猶予期間中
      string strObjName="RSSignalBGrace_"+TimeToString(dtRsUpGraceSt);
      if(!DeleteObject(strObjName))
         return false;
      if(!LineCreate(strObjName,OBJ_TREND,dtRsUpGraceSt,io_RSGraceHighBuffer[i],dtRsUpGraceEd,io_RSGraceHighBuffer[i],clrBuffer1[0],STYLE_SOLID,3,true,false))
         return false;
      //--- サポート・レジスタンス
      strObjName="RSSignalRes_"+TimeToString(dtRsUpGraceEd);
      if(!DeleteObject(strObjName))
         return false;
      if(!LineCreate(strObjName,OBJ_TREND,dtRsUpGraceEd,io_RSGraceHighBuffer[i],dtRsSignalEd,io_RSGraceHighBuffer[i],clrBuffer2[0],STYLE_SOLID,2,true,true))
         return false;
      //--- Backgroud
      if(i>0)
        {
         strObjName="BKColor_"+TimeToString(dtRsUpGraceSt);
         if(!DeleteObject(strObjName))
            return false;
         if(!LineCreate(strObjName,OBJ_RECTANGLE,dtRsUpGraceSt,EMPTY_VALUE,dtRsSignalEd,0,clrBuffer3[0],STYLE_SOLID,1,true,true))
            return false;
        }
      else
        {
         if(io_RSSignalBuffer[i+1]==DOWN)
           {
            strObjName="BKColor_"+TimeToString(dtPreRsDwGraceSt);
            if(!DeleteObject(strObjName))
               return false;
            if(!LineCreate(strObjName,OBJ_RECTANGLE,dtPreRsDwGraceSt,EMPTY_VALUE,dtPreRsSignalEd,0,clrBuffer3[1],STYLE_SOLID,1,true,true))
               return false;
           }
        }
     }
   if(io_RSSignalBuffer[i]==DOWN)
     {
      //--- 猶予期間中
      string strObjName="RSSignalSGrace_"+TimeToString(dtRsDwGraceSt);
      if(!DeleteObject(strObjName))
         return false;
      if(!LineCreate(strObjName,OBJ_TREND,dtRsDwGraceSt,io_RSGraceLowBuffer[i],dtRsDwGraceEd,io_RSGraceLowBuffer[i],clrBuffer1[1],STYLE_SOLID,3,true,false))
         return false;
      //--- サポート・レジスタンス
      strObjName="RSSignalSup_"+TimeToString(dtRsDwGraceEd);
      if(!DeleteObject(strObjName))
         return false;
      if(!LineCreate(strObjName,OBJ_TREND,dtRsDwGraceEd,io_RSGraceLowBuffer[i],dtRsSignalEd,io_RSGraceLowBuffer[i],clrBuffer2[1],STYLE_SOLID,2,true,true))
         return false;
      //--- Backgroud
      if(i>0)
        {
         strObjName="BKColor_"+TimeToString(dtRsDwGraceSt);
         if(!DeleteObject(strObjName))
            return false;
         if(!LineCreate(strObjName,OBJ_RECTANGLE,dtRsDwGraceSt,EMPTY_VALUE,dtRsSignalEd,0,clrBuffer3[1],STYLE_SOLID,1,true,true))
            return false;
        }
      else
        {
         if(io_RSSignalBuffer[i+1]==UP)
           {
            strObjName="BKColor_"+TimeToString(dtPreRsUpGraceSt);
            if(!DeleteObject(strObjName))
               return false;
            if(!LineCreate(strObjName,OBJ_RECTANGLE,dtPreRsUpGraceSt,EMPTY_VALUE,dtPreRsSignalEd,0,clrBuffer3[0],STYLE_SOLID,1,true,true))
               return false;
           }
        }
     }
//+------------------------------------------------------------------+
//| Other                                                        |
//+------------------------------------------------------------------+
//--- line 52
   datetime dtLineSt=iTime(_Symbol,_Period,i+SM_SENKO_B-1);
   if(!LineCreate("high52",OBJ_TREND,dtLineSt,io_High52Buffer[i],dtRsSignalEd,io_High52Buffer[i],clrGold,STYLE_DOT))
      return false;
   if(!LineCreate("low52",OBJ_TREND,dtLineSt,io_Low52Buffer[i],dtRsSignalEd,io_Low52Buffer[i],clrGold,STYLE_DOT))
      return false;
   if(!LineCreate("line52",OBJ_TREND,dtLineSt,io_High52Buffer[i],dtLineSt,io_Low52Buffer[i],clrGold,STYLE_DOT))
      return false;
//--- yesterday high low
   if(_Period<PERIOD_D1)
     {
      int intFirstVisibleBar=(int)ChartGetInteger(0,CHART_FIRST_VISIBLE_BAR,0);
      datetime dtFrom=iTime(_Symbol,_Period,intFirstVisibleBar);
      if(!LineCreate("YesterdayHighLine",OBJ_TREND,dtFrom,io_Y_HighBuffer[i],dtRsSignalEd,io_Y_HighBuffer[i],clrCornflowerBlue,STYLE_SOLID,2))
         return false;
      if(!LineCreate("YesterdayLowLine",OBJ_TREND,dtFrom,io_Y_LowBuffer[i],dtRsSignalEd,io_Y_LowBuffer[i],clrCornflowerBlue,STYLE_SOLID,2))
         return false;
      //--- ラベル設定
      if(!TextCreate(0,"YesterdayHigh",dtFrom,io_Y_HighBuffer[i],"Y_H",8,clrBlack))
         return false;
      if(!TextCreate(0,"YesterdayLow",dtFrom,io_Y_LowBuffer[i],"Y_L",8,clrBlack))
         return false;
     }
   return true;
  }
//+------------------------------------------------------------------+
//| delete objects                                                   |
//+------------------------------------------------------------------+
bool ChartObjectDeleteAll(int i)
  {
   string strObjName=NULL;
   strObjName="SMSignalB_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="SMSignalS_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="SMSignalBGrace_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="SMSignalSGrace_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="SMSignalRes_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="SMSignalSup_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="RSSignalB_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="RSSignalS_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="RSSignalBGrace_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="RSSignalSGrace_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="RSSignalRes_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="RSSignalSup_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="BKColor_"+TimeToString(iTime(_Symbol,_Period,i));
   if(!DeleteObject(strObjName))
      return false;
   strObjName="high52";
   if(!DeleteObject(strObjName))
      return false;
   strObjName="low52";
   if(!DeleteObject(strObjName))
      return false;
   strObjName="line52";
   if(!DeleteObject(strObjName))
      return false;
   strObjName="YesterdayHighLine";
   if(!DeleteObject(strObjName))
      return false;
   strObjName="YesterdayLowLine";
   if(!DeleteObject(strObjName))
      return false;
   strObjName="YesterdayHigh";
   if(!DeleteObject(strObjName))
      return false;
   strObjName="YesterdayLow";
   if(!DeleteObject(strObjName))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
