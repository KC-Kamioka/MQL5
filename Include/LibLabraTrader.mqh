//+------------------------------------------------------------------+
//|                                               LibLabraTrader.mqh |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
//+------------------------------------------------------------------+
//| Include files                                                    |
//+------------------------------------------------------------------+
#include <DBCommon.mqh>
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
#import "LibLabraTrader.ex5"
bool GetClose(string in_strSymbol,ENUM_TIMEFRAMES in_enmTimeFrames,double &close[],int start,int amount);
ENM_TRADE_STATUS OpenMarketOrder(STR_LB_ORDERS &io_od);
ENM_TRADE_STATUS CloseMarketOrder(STR_LB_ORDERS &io_od);
ENM_TRADE_STATUS OpenSigma2Break(STR_LB_ORDERS &io_od);
ENM_TRADE_STATUS OpenTouchSigmaLv1(STR_LB_ORDERS &io_od);
ENM_TRADE_STATUS OpenTouchSigmaLv2(STR_LB_ORDERS &io_od);
ENM_TRADE_STATUS OpenReturnSigma(STR_LB_ORDERS &io_od);
ENM_TRADE_STATUS TrailingStop(STR_LB_ORDERS &io_od);
ENM_TRADE_STATUS StopSigma(STR_LB_ORDERS &io_od);
ENM_TRADE_STATUS LimitTouchSigma(STR_LB_ORDERS &io_od);
ENM_TRADE_STATUS LimitTouchCSpan(STR_LB_ORDERS &io_od);
int UpdateStatus(ENM_TRADE_STATUS in_enm);
bool isNewBar(string symbol, ENUM_TIMEFRAMES tf);
string StringEncodeBase64(const string text);
#import
//+------------------------------------------------------------------+
