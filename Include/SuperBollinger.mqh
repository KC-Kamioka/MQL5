//+------------------------------------------------------------------+
//|                                               SuperBollinger.mqh |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
//+------------------------------------------------------------------+
//| Include files                                                    |
//+------------------------------------------------------------------+
#include <Common.mqh>
//+------------------------------------------------------------------+
//| set indicator values                                                 |
//+------------------------------------------------------------------+
void SetCSpanAttacker(
   int i
   ,double in_high
   ,double in_low
   ,double in_close
   ,double in_Prehigh
   ,double in_Prelow
   ,double &io_CSpanAttackerUp[]
   ,double &io_CSpanAttackerDw[]
   ,double &io_CSpanDirection[]
   ,double &io_UpperS1Buffer[]
   ,double &io_LowerS1Buffer[]
)
  {
   io_CSpanAttackerUp[i]=0.0;
   io_CSpanAttackerDw[i]=0.0;
   io_CSpanDirection[i]=io_CSpanDirection[i+1];
   if(io_CSpanDirection[i]!=UP && in_close>in_Prehigh && io_UpperS1Buffer[i]<in_close)
     {
      io_CSpanDirection[i]=UP;
      io_CSpanAttackerUp[i]=in_low;
     }
   if(io_CSpanDirection[i]!=DOWN && in_close<in_Prelow && io_LowerS1Buffer[i]>in_close)
     {
      io_CSpanDirection[i]=DOWN;
      io_CSpanAttackerDw[i]=in_high;
     }
  }
//+------------------------------------------------------------------+
//| set indicator values                                                 |
//+------------------------------------------------------------------+
void SetOver2Sigma(
int i
,double in_high
,double in_low
,double in_close
,double &io_Over2Sigma[]
,double &io_UpperS2Buffer[]
,double &io_LowerS2Buffer[]
)
  {
   io_Over2Sigma[i]=0.0;
   if(io_UpperS2Buffer[i]<in_close)
      io_Over2Sigma[i]=in_high;
   if(io_LowerS2Buffer[i]>in_close)
      io_Over2Sigma[i]=in_low;
  }
//+------------------------------------------------------------------+
