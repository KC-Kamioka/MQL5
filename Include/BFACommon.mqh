//+------------------------------------------------------------------+
//|                                                    BFACommon.mqh |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
// マジックナンバー
#define BB_BREAK_B_1_SIGMA_EXIT     10010101
#define BB_BREAK_B_1_TRAILING       10010102
#define BB_BREAK_S_1_SIGMA_EXIT     10010201
#define BB_BREAK_S_1_TRAILING       10010202
#define BB_REVERSAL_B_CENTER_EXIT   10020101
#define BB_REVERSAL_S_CENTER_EXIT   10020201
#define MACD_CROSS_B                10030101
#define MACD_CROSS_S                10030102
#define BB_RETURN_B_2_SIGMA_EXIT    10040101
#define BB_RETURN_S_2_SIGMA_EXIT    10040201
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int max_positions = 2;
double sl_point = 10000;
int b_walk_count = 9;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
struct values_struct
  {
   double            close;
   double            high;
   double            low;
   double            open;
   double            bb_plus_2_sigma;
   double            bb_plus_1_sigma;
   double            sma;
   double            bb_miuns_1_sigma;
   double            bb_miuns_2_sigma;
   double            band_walk_up_count;
   double            band_walk_down_count;
   double            band_2_above;
   double            band_2_below;
   double            s_span_a;
   double            s_span_b;
   double            rsi;
   double            macd_main;
   double            macd_signal;
   double            adx_main;
   double            adx_di_p;
   double            adx_di_m;
   double            sd;
  };

struct handle_struct
  {
   int               sma;
   int               bfa;
   int               rsi;
   int               macd;
   int               adx;
   int               sd;
  };

struct pos_info
  {
   ulong             ticket;
   ulong             magic;
   ulong             op_time;
  };

struct events_struct
  {
   string            EventName;
   string            EventTime;
  };
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int bb_period = 21;
double lot_size=0.1;
int element_count=30;
ENUM_TIMEFRAMES tf_upper = PERIOD_H1;
ENUM_TIMEFRAMES tf_middle = PERIOD_M15;
ENUM_TIMEFRAMES tf_lower = PERIOD_M5;
//+------------------------------------------------------------------+