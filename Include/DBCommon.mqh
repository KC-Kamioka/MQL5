//+------------------------------------------------------------------+
//|                                                     DBCommon.mqh |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
//+------------------------------------------------------------------+
//| Include                                                          |
//+------------------------------------------------------------------+
#include <Common.mqh>
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
#define L_DB_NAME       "LabraTrader"
#define LA_DB_NAME      "LabraAutoTrader"
//+------------------------------------------------------------------+
//| enum                                                             |
//+------------------------------------------------------------------+
//---
enum ENM_OP_AUTO
  {
   OP_NOW
   ,OP_BREAK_2SIGMA //"Break 2Sigma"
   ,OP_RT_CENTER    //"Return Center"
   ,OP_RT_1SIGMA    //"Return 1Sigma"
   ,OP_RT_2SIGMA    //"Return 2Sigma"
   ,OP_RT_TC_1SIGMA //"Return Touch 1Sigma"
   ,OP_TC_CENTER    //"Touch Center"
   ,OP_TC_1SIGMA    //"Touch 1Sigma"
   ,OP_TC_2SIGMA    //"Touch 2Sigma"
  };
//---
enum ENM_LIMIT
  {
   L_NONE
   ,L_CENTER
   ,L_1SIGMA
   ,L_2SIGMA
  };
//---
enum ENM_STOP
  {
   S_NONE
   ,S_CENTER
   ,S_1SIGMA
   ,S_2SIGMA
   ,S_2SIGMA_X2
   ,S_TRAILING
   ,S_NOW
  };
//---
enum ENM_CATEGORY
  {
   ORDER
   ,PERIOD
   ,OP_AUTO
   ,LIMIT
   ,STOP
  };
//+------------------------------------------------------------------+
//| structs                                                          |
//+------------------------------------------------------------------+
//---
struct STR_LB_WAITING
  {
   int               intSeq;
   string            strSymbol;
   double            dblLots;
   ENUM_TIMEFRAMES   enmTimeFrame;
   ENUM_ORDER_TYPE   enmOrderType;
   ENM_OP_AUTO       enmOpenType;
   ENM_LIMIT         enmLimitType;
   ENM_STOP          enmStopType;
  };
//---
struct STR_LB_ORDERS
  {
   string            strOpDateTime;
   string            strSymbol;
   double            dblLots;
   ENUM_TIMEFRAMES   enmTimeFrame;
   ENUM_ORDER_TYPE   enmOrderType;
   ENM_OP_AUTO       enmOpenType;
   double            dblOpPrice;
   ulong             ulTicketNo;
   ENM_LIMIT         enmLimitType;
   ENM_STOP          enmStopType;
  };
//---
struct STR_LB_CONDITIONS
  {
   ENM_LIMIT         enmLimitType;
   ENM_STOP          enmStopType;
  };
//---
struct STR_LB_CALENDER
  {
   ulong                              id;                   // 値ID
   ulong                              event_id;             // イベントID
   string                             country;             // 国
   ENUM_CALENDAR_EVENT_SECTOR     sector;           // イベントの重要度
   ENUM_CALENDAR_EVENT_IMPORTANCE     importance;           // イベントの重要度
   string                             name;                 // イベント名
   datetime                           time;                 // イベントの日時
  };
//+------------------------------------------------------------------+
//| CodeMaster,Conditions初期化
//+------------------------------------------------------------------+
bool DBInitCommon()
  {
   string sql_init[1];
   sql_init[0]="DROP TABLE IF EXISTS CODEMASTER";
   if(!DBExecute(sql_init))
      return false;
   sql_init[0]="CREATE TABLE IF NOT EXISTS CODEMASTER";
   sql_init[0]=sql_init[0]+"(";
   sql_init[0]=sql_init[0]+" "+"CATEGORY INT";
   sql_init[0]=sql_init[0]+","+"CATEGORY_NAME TEXT";
   sql_init[0]=sql_init[0]+","+"CODE INT";
   sql_init[0]=sql_init[0]+","+"NAME TEXT";
   sql_init[0]=sql_init[0]+","+"PRIMARY KEY(CATEGORY,CODE)";
   sql_init[0]=sql_init[0]+")";
   if(!DBExecute(sql_init))
      return false;

   string sql_cd[27]=
     {
      "INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)ORDER+",'"+EnumToString(ORDER)+"',"+(string)ORDER_TYPE_BUY+",'"+EnumToString(ORDER_TYPE_BUY)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)ORDER+",'"+EnumToString(ORDER)+"',"+(string)ORDER_TYPE_SELL+",'"+EnumToString(ORDER_TYPE_SELL)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)PERIOD+",'"+EnumToString(PERIOD)+"',"+(string)PERIOD_M5+",'"+EnumToString(PERIOD_M5)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)PERIOD+",'"+EnumToString(PERIOD)+"',"+(string)PERIOD_M15+",'"+EnumToString(PERIOD_M15)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)PERIOD+",'"+EnumToString(PERIOD)+"',"+(string)PERIOD_H1+",'"+EnumToString(PERIOD_H1)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)PERIOD+",'"+EnumToString(PERIOD)+"',"+(string)PERIOD_H4+",'"+EnumToString(PERIOD_H4)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)PERIOD+",'"+EnumToString(PERIOD)+"',"+(string)PERIOD_D1+",'"+EnumToString(PERIOD_D1)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)OP_AUTO+",'"+EnumToString(OP_AUTO)+"',"+(string)OP_NOW+",'"+EnumToString(OP_NOW)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)OP_AUTO+",'"+EnumToString(OP_AUTO)+"',"+(string)OP_BREAK_2SIGMA+",'"+EnumToString(OP_BREAK_2SIGMA)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)OP_AUTO+",'"+EnumToString(OP_AUTO)+"',"+(string)OP_RT_CENTER+",'"+EnumToString(OP_RT_CENTER)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)OP_AUTO+",'"+EnumToString(OP_AUTO)+"',"+(string)OP_RT_1SIGMA+",'"+EnumToString(OP_RT_1SIGMA)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)OP_AUTO+",'"+EnumToString(OP_AUTO)+"',"+(string)OP_RT_2SIGMA+",'"+EnumToString(OP_RT_2SIGMA)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)OP_AUTO+",'"+EnumToString(OP_AUTO)+"',"+(string)OP_RT_TC_1SIGMA+",'"+EnumToString(OP_RT_TC_1SIGMA)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)OP_AUTO+",'"+EnumToString(OP_AUTO)+"',"+(string)OP_TC_1SIGMA+",'"+EnumToString(OP_TC_1SIGMA)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)OP_AUTO+",'"+EnumToString(OP_AUTO)+"',"+(string)OP_TC_2SIGMA+",'"+EnumToString(OP_TC_2SIGMA)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)OP_AUTO+",'"+EnumToString(OP_AUTO)+"',"+(string)OP_TC_CENTER+",'"+EnumToString(OP_TC_CENTER)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)LIMIT+",'"+EnumToString(LIMIT)+"',"+(string)L_NONE+",'')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)LIMIT+",'"+EnumToString(LIMIT)+"',"+(string)L_CENTER+",'"+EnumToString(L_CENTER)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)LIMIT+",'"+EnumToString(LIMIT)+"',"+(string)L_1SIGMA+",'"+EnumToString(L_1SIGMA)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)LIMIT+",'"+EnumToString(LIMIT)+"',"+(string)L_2SIGMA+",'"+EnumToString(L_2SIGMA)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)STOP+",'"+EnumToString(STOP)+"',"+(string)S_NONE+",'')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)STOP+",'"+EnumToString(STOP)+"',"+(string)S_CENTER+",'"+EnumToString(S_CENTER)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)STOP+",'"+EnumToString(STOP)+"',"+(string)S_1SIGMA+",'"+EnumToString(S_1SIGMA)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)STOP+",'"+EnumToString(STOP)+"',"+(string)S_2SIGMA+",'"+EnumToString(S_2SIGMA)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)STOP+",'"+EnumToString(STOP)+"',"+(string)S_2SIGMA_X2+",'"+EnumToString(S_2SIGMA_X2)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)STOP+",'"+EnumToString(STOP)+"',"+(string)S_TRAILING+",'"+EnumToString(S_TRAILING)+"')"
      ,"INSERT INTO CODEMASTER (CATEGORY,CATEGORY_NAME,CODE,NAME) VALUES ("+(string)STOP+",'"+EnumToString(STOP)+"',"+(string)S_NOW+",'"+EnumToString(S_NOW)+"')"
     };
   if(!DBExecute(sql_cd))
      return false;

   sql_init[0]="DROP TABLE IF EXISTS CONDITIONS";
   if(!DBExecute(sql_init))
      return false;

   sql_init[0]="CREATE TABLE IF NOT EXISTS CONDITIONS";
   sql_init[0]=sql_init[0]+"(";
   sql_init[0]=sql_init[0]+" "+"OPEN_TYPE INT";
   sql_init[0]=sql_init[0]+","+"LIMIT_TYPE INT";
   sql_init[0]=sql_init[0]+","+"STOP_TYPE INT";
   sql_init[0]=sql_init[0]+")";
   if(!DBExecute(sql_init))
      return false;

// default conditions
   string sql_cnd[9]=
     {
      "INSERT INTO CONDITIONS (OPEN_TYPE,LIMIT_TYPE,STOP_TYPE) VALUES ("+(string)OP_NOW+","+(string)L_NONE+","+(string)S_NONE+")"
      ,"INSERT INTO CONDITIONS (OPEN_TYPE,LIMIT_TYPE,STOP_TYPE) VALUES ("+(string)OP_BREAK_2SIGMA+","+(string)L_NONE+","+(string)S_1SIGMA+")"
      ,"INSERT INTO CONDITIONS (OPEN_TYPE,LIMIT_TYPE,STOP_TYPE) VALUES ("+(string)OP_RT_CENTER+","+(string)L_2SIGMA+","+(string)S_2SIGMA+")"
      ,"INSERT INTO CONDITIONS (OPEN_TYPE,LIMIT_TYPE,STOP_TYPE) VALUES ("+(string)OP_RT_1SIGMA+","+(string)L_2SIGMA+","+(string)S_2SIGMA+")"
      ,"INSERT INTO CONDITIONS (OPEN_TYPE,LIMIT_TYPE,STOP_TYPE) VALUES ("+(string)OP_RT_2SIGMA+","+(string)L_2SIGMA+","+(string)S_2SIGMA+")"
      ,"INSERT INTO CONDITIONS (OPEN_TYPE,LIMIT_TYPE,STOP_TYPE) VALUES ("+(string)OP_RT_TC_1SIGMA+","+(string)L_2SIGMA+","+(string)S_2SIGMA+")"
      ,"INSERT INTO CONDITIONS (OPEN_TYPE,LIMIT_TYPE,STOP_TYPE) VALUES ("+(string)OP_TC_CENTER+","+(string)L_NONE+","+(string)S_2SIGMA+")"
      ,"INSERT INTO CONDITIONS (OPEN_TYPE,LIMIT_TYPE,STOP_TYPE) VALUES ("+(string)OP_TC_1SIGMA+","+(string)L_NONE+","+(string)S_2SIGMA+")"
      ,"INSERT INTO CONDITIONS (OPEN_TYPE,LIMIT_TYPE,STOP_TYPE) VALUES ("+(string)OP_TC_2SIGMA+","+(string)L_2SIGMA+","+(string)S_2SIGMA+")"
     };
   if(!DBExecute(sql_cnd))
      return false;
//---
   sql_init[0]="DROP TABLE IF EXISTS CONFIG";
   if(!DBExecute(sql_init))
      return false;
   sql_init[0]="CREATE TABLE IF NOT EXISTS CONFIG";
   sql_init[0]=sql_init[0]+"(";
   sql_init[0]=sql_init[0]+" "+"SYMBOL TEXT PRIMARY KEY";
   sql_init[0]=sql_init[0]+","+"VOLUME_MIN REAL";
   sql_init[0]=sql_init[0]+","+"VOLUME_MAX REAL";
   sql_init[0]=sql_init[0]+","+"VOLUME_STEP REAL";
   sql_init[0]=sql_init[0]+","+"VOLUME_INIT REAL";
   sql_init[0]=sql_init[0]+","+"PORT INT";
   sql_init[0]=sql_init[0]+","+"IP TEXT";
   sql_init[0]=sql_init[0]+")";
   if(!DBExecute(sql_init))
      return false;
   for(int i=0; i<ArraySize(MySymbols); i++)
     {
      if(
         (AccountInfoInteger(ACCOUNT_LOGIN)==900002407 && (ENUM_SYMBOL_SECTOR)SymbolInfoInteger(MySymbols[i],SYMBOL_SECTOR)==SECTOR_CURRENCY)
         || (AccountInfoInteger(ACCOUNT_LOGIN)==900005356 && (ENUM_SYMBOL_SECTOR)SymbolInfoInteger(MySymbols[i],SYMBOL_SECTOR)==SECTOR_UNDEFINED)
      )
        {
         sql_init[0]="INSERT INTO CONFIG VALUES";
         sql_init[0]=sql_init[0]+"("+"'"+MySymbols[i]+"'";
         sql_init[0]=sql_init[0]+","+DoubleToString(SymbolInfoDouble(MySymbols[i],SYMBOL_VOLUME_MIN),2);
         sql_init[0]=sql_init[0]+","+DoubleToString(SymbolInfoDouble(MySymbols[i],SYMBOL_VOLUME_MAX),2);
         sql_init[0]=sql_init[0]+","+DoubleToString(SymbolInfoDouble(MySymbols[i],SYMBOL_VOLUME_STEP),2);
         sql_init[0]=sql_init[0]+","+DoubleToString(SymbolInfoDouble(MySymbols[i],SYMBOL_VOLUME_MIN),2);
         sql_init[0]=sql_init[0]+","+StringSubstr((string)AccountInfoInteger(ACCOUNT_LOGIN),4,5);
         sql_init[0]=sql_init[0]+","+"'127.0.0.1'";
         sql_init[0]=sql_init[0]+")";
         if(!DBExecute(sql_init))
            return false;
        }
     }
//---
   sql_init[0]="DROP TABLE IF EXISTS WAITING";
   if(!DBExecute(sql_init))
      return false;
   sql_init[0]="CREATE TABLE IF NOT EXISTS WAITING";
   sql_init[0]=sql_init[0]+"(";
   sql_init[0]=sql_init[0]+" "+"SEQ INT PRIMARY KEY";
   sql_init[0]=sql_init[0]+","+"SYMBOL TEXT NOT NULL";
   sql_init[0]=sql_init[0]+","+"LOTS REAL NOT NULL";
   sql_init[0]=sql_init[0]+","+"TIME_FRAME INT NOT NULL";
   sql_init[0]=sql_init[0]+","+"ORDER_TYPE INT NOT NULL";
   sql_init[0]=sql_init[0]+","+"OPEN_TYPE INT";
   sql_init[0]=sql_init[0]+","+"LIMIT_TYPE INT";
   sql_init[0]=sql_init[0]+","+"STOP_TYPE INT";
   sql_init[0]=sql_init[0]+")";
   if(!DBExecute(sql_init))
      return false;
//---
   sql_init[0]="DROP TABLE IF EXISTS ORDERS";
   if(!DBExecute(sql_init))
      return false;
   sql_init[0]="CREATE TABLE IF NOT EXISTS ORDERS";
   sql_init[0]=sql_init[0]+"(";
   sql_init[0]=sql_init[0]+" "+"TICKET_NO TEXT PRIMARY KEY";
   sql_init[0]=sql_init[0]+","+"SYMBOL TEXT NOT NULL";
   sql_init[0]=sql_init[0]+","+"LOTS REAL NOT NULL";
   sql_init[0]=sql_init[0]+","+"TIME_FRAME INT NOT NULL";
   sql_init[0]=sql_init[0]+","+"DATE TEXT";
   sql_init[0]=sql_init[0]+","+"ORDER_TYPE INT NOT NULL";
   sql_init[0]=sql_init[0]+","+"OPEN_TYPE INT";
   sql_init[0]=sql_init[0]+","+"OP_PRICE REAL";
   sql_init[0]=sql_init[0]+","+"LIMIT_TYPE INT";
   sql_init[0]=sql_init[0]+","+"STOP_TYPE INT";
   sql_init[0]=sql_init[0]+")";
   if(!DBExecute(sql_init))
      return false;
//---
   sql_init[0]="DROP TABLE IF EXISTS EVENTS";
   if(!DBExecute(sql_init))
      return false;
//---
   sql_init[0]="DROP TABLE IF EXISTS LOG";
   if(!DBExecute(sql_init))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadToList(string sql,string &val[])
  {
//---
   ArrayFree(val);
//--- open the database in the folder
   int db=DatabaseOpen(L_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
      return false;
//--- create a query and get a handle for it
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", L_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//---
   string strVal;
   for(int i=0; DatabaseRead(request); i++)
     {
      //--- read the values of each field from the obtained entry
      if(DatabaseColumnText(request, 0, strVal))
        {
         ArrayResize(val,ArraySize(val)+1);
         val[i]=strVal;
        }
      else
        {
         Print(i, ": DatabaseRead() failed with code ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  };
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool DBInsertEvents(STR_LB_CALENDER &calender[])
  {
//---
   string sql[];
   ArrayResize(sql,ArraySize(calender));
   for(int i=0; i<ArraySize(calender); i++)
     {
      sql[i]="REPLACE INTO EVENTS VALUES";
      sql[i]=sql[i]+"("+(string)(int)calender[i].id;
      sql[i]=sql[i]+","+(string)(int)calender[i].event_id;
      sql[i]=sql[i]+","+"'"+(string)calender[i].time+"'";
      sql[i]=sql[i]+","+"'"+calender[i].country+"'";
      sql[i]=sql[i]+","+"'"+EnumToString((ENUM_CALENDAR_EVENT_SECTOR)calender[i].sector)+"'";
      sql[i]=sql[i]+","+"'"+EnumToString((ENUM_CALENDAR_EVENT_IMPORTANCE)calender[i].importance)+"'";
      sql[i]=sql[i]+","+"'"+calender[i].name+"'";
      sql[i]=sql[i]+")";
     }
   if(!DBExecute(sql))
      return false;
   return true;
  };
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool DBInsertTestWaiting()
  {
//---
   int i=0;
   STR_LB_WAITING wait[];
//--- OP_BREAK_2SIGMA
   ArrayResize(wait,i+1);
   wait[i].strSymbol=EnumToString(EURUSD);
   wait[i].dblLots=0.1;
   wait[i].enmOrderType=ORDER_TYPE_BUY;
   wait[i].enmTimeFrame=_Period;
   wait[i].enmOpenType=OP_BREAK_2SIGMA;
   wait[i].enmLimitType=L_NONE;
   wait[i].enmStopType=S_1SIGMA;
   i++;
//--- OP_BREAK_2SIGMA
   ArrayResize(wait,i+1);
   wait[i].strSymbol=EnumToString(EURUSD);
   wait[i].dblLots=0.1;
   wait[i].enmOrderType=ORDER_TYPE_BUY;
   wait[i].enmTimeFrame=_Period;
   wait[i].enmOpenType=OP_BREAK_2SIGMA;
   wait[i].enmLimitType=L_NONE;
   wait[i].enmStopType=S_TRAILING;
   i++;
//--- OP_BREAK_2SIGMA
   ArrayResize(wait,i+1);
   wait[i].strSymbol=EnumToString(EURUSD);
   wait[i].dblLots=0.1;
   wait[i].enmOrderType=ORDER_TYPE_SELL;
   wait[i].enmTimeFrame=_Period;
   wait[i].enmOpenType=OP_BREAK_2SIGMA;
   wait[i].enmLimitType=L_NONE;
   wait[i].enmStopType=S_1SIGMA;
   i++;
//--- OP_BREAK_2SIGMA
   ArrayResize(wait,i+1);
   wait[i].strSymbol=EnumToString(EURUSD);
   wait[i].dblLots=0.1;
   wait[i].enmOrderType=ORDER_TYPE_SELL;
   wait[i].enmTimeFrame=_Period;
   wait[i].enmOpenType=OP_BREAK_2SIGMA;
   wait[i].enmLimitType=L_NONE;
   wait[i].enmStopType=S_TRAILING;
   i++;
//--- OP_RT_TC_1SIGMA
   ArrayResize(wait,i+1);
   wait[i].strSymbol=EnumToString(EURUSD);
   wait[i].dblLots=0.1;
   wait[i].enmOrderType=ORDER_TYPE_SELL;
   wait[i].enmTimeFrame=_Period;
   wait[i].enmOpenType=OP_RT_TC_1SIGMA;
   wait[i].enmLimitType=L_2SIGMA;
   wait[i].enmStopType=S_2SIGMA;
   i++;
//---
   for(i=0; i<ArraySize(wait); i++)
     {
      string sql[1];
      sql[0]="INSERT INTO WAITING VALUES";
      sql[0]=sql[0]+"("+"(SELECT IFNULL(COUNT(*),0)+1 FROM WAITING)";
      sql[0]=sql[0]+","+"'"+wait[i].strSymbol+"'";
      sql[0]=sql[0]+","+(string)wait[i].dblLots;
      sql[0]=sql[0]+","+(string)(int)wait[i].enmTimeFrame;
      sql[0]=sql[0]+","+(string)(int)wait[i].enmOrderType;
      sql[0]=sql[0]+","+(string)(int)wait[i].enmOpenType;
      sql[0]=sql[0]+","+(string)(int)wait[i].enmLimitType;
      sql[0]=sql[0]+","+(string)(int)wait[i].enmStopType;
      sql[0]=sql[0]+")";
      if(!DBExecute(sql))
         return false;
     }
   return true;
  };
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool UpdateLots(double lots)
  {
   string sql[1];
   sql[0]="UPDATE CONFIG";
   sql[0]=sql[0]+" "+"SET";
   sql[0]=sql[0]+" "+"LOTS"+"="+DoubleToString(lots,1);
   if(!DBExecute(sql))
      return false;
   return true;
  };
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadConditions(ENM_OP_AUTO code,STR_LB_CONDITIONS &con)
  {
//---
   ResetLastError();
//--- open the database in the folder
   int db=DatabaseOpen(L_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", L_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"LIMIT_TYPE";
   sql=sql+","+"STOP_TYPE";
   sql=sql+" "+"FROM CONDITIONS";
   sql=sql+" "+"WHERE OPEN_TYPE = "+(string)code;
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", L_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//--- read the values of each field from the obtained entry
   if(!DatabaseReadBind(request,con))
     {
      Print("DB: ", L_DB_NAME, " DatabaseReadBind failed with code ", GetLastError()," ",sql);
      DatabaseFinalize(request);
      DatabaseClose(db);
      return false;
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  };
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadIpAddress(string &io_ip)
  {
//---
//--- open the database in the folder
   int db=DatabaseOpen(L_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", L_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"IP";
   sql=sql+" "+"FROM CONFIG";
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", L_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//---
   for(int i=0; DatabaseRead(request); i++)
     {
      //--- read the values of each field from the obtained entry
      if(!DatabaseColumnText(request, 0, io_ip))
        {
         Print(i, ": DatabaseRead() failed with code ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  };
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadWaiting(STR_LB_WAITING &io_data[])
  {
//---
   ResetLastError();
   ArrayFree(io_data);
//--- open the database in the folder
   int db=DatabaseOpen(L_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", L_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"SEQ";
   sql=sql+","+"SYMBOL";
   sql=sql+","+"LOTS";
   sql=sql+","+"TIME_FRAME";
   sql=sql+","+"ORDER_TYPE";
   sql=sql+","+"OPEN_TYPE";
   sql=sql+","+"LIMIT_TYPE";
   sql=sql+","+"STOP_TYPE";
   sql=sql+" "+"FROM WAITING";
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {

      Print("DB: ", L_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseFinalize(request);
      DatabaseClose(db);
      return false;
     }
//--- print all entries with the salary greater than 15000
   STR_LB_WAITING wk_data;
   for(int i=0; DatabaseReadBind(request, wk_data); i++)
     {
      if(GetLastError()!=0)
        {
         Print("DB: ", L_DB_NAME, " DatabaseReadBind failed with code ", GetLastError()," ",sql);
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
      ArrayResize(io_data,i+1);
      io_data[i]=wk_data;
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadOrders(STR_LB_ORDERS &io_data[],ulong ticket_no=0)
  {
//---
   ResetLastError();
   ArrayFree(io_data);
//--- open the database in the folder
   int db=DatabaseOpen(L_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", L_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"DATE";
   sql=sql+","+"SYMBOL";
   sql=sql+","+"LOTS";
   sql=sql+","+"TIME_FRAME";
   sql=sql+","+"ORDER_TYPE";
   sql=sql+","+"OPEN_TYPE";
   sql=sql+","+"OP_PRICE";
   sql=sql+","+"TICKET_NO";
   sql=sql+","+"LIMIT_TYPE";
   sql=sql+","+"STOP_TYPE";
   sql=sql+" "+"FROM ORDERS";
   if(ticket_no!=0)
      sql=sql+" "+"WHERE TICKET_NO = "+(string)ticket_no;
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", L_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//--- print all entries with the salary greater than 15000
   STR_LB_ORDERS wk_data;
   for(int i=0; DatabaseReadBind(request, wk_data); i++)
     {
      if(GetLastError()!=0)
        {
         Print("DB: ", L_DB_NAME, " DatabaseReadBind failed with code ", GetLastError()," ",sql);
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
      ArrayResize(io_data,i+1);
      io_data[i]=wk_data;
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool HasTicketNo(ulong ticket_no)
  {
//---
   ResetLastError();
//--- open the database in the folder
   int db=DatabaseOpen(L_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", L_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"COUNT(*)";
   sql=sql+" "+"FROM ORDERS";
   sql=sql+" "+"WHERE TICKET_NO="+(string)ticket_no;
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", L_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//--- print all entries with the salary greater than 15000
   int intCount=0;
   for(int i=0; DatabaseRead(request); i++)
     {
      //--- read the values of each field from the obtained entry
      if(!DatabaseColumnInteger(request, 0, intCount))
        {
         Print(i, ": DatabaseRead() failed with code ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   if(intCount==0)
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool DeleteWaiting(STR_LB_WAITING &wait)
  {
//---
   string sql[1];
   sql[0]="DELETE FROM WAITING";
   sql[0]=sql[0]+" "+"WHERE SEQ="+(string)wait.intSeq;
   if(!DBExecute(sql))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool DeleteOrder(ulong ticket)
  {
//---
   string sql[1];
   sql[0]="DELETE FROM ORDERS";
   sql[0]=sql[0]+" "+"WHERE TICKET_NO="+(string)ticket;
   if(!DBExecute(sql))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Sql実行
//+------------------------------------------------------------------+
bool DBExecute(string &sql[],string strTableName=NULL)
  {
   if(strTableName==NULL)
      strTableName=L_DB_NAME;
//--- create or open the database in the common terminal folder
   int db=DatabaseOpen(strTableName, DATABASE_OPEN_READWRITE | DATABASE_OPEN_CREATE | DATABASE_OPEN_COMMON);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", strTableName, " open failed with code ", GetLastError());
      return false;
     }
   if(!DatabaseTransactionBegin(db))
     {
      Print("DB: ", strTableName, " begin transaction failed with code ", GetLastError());
      return false;
     }
   for(int i=0; i<ArraySize(sql); i++)
     {
      if(!DatabaseExecute(db,sql[i]))
        {
         Print("DB: ", strTableName, " execute failed with code ", GetLastError()," ",sql[i]);
         DatabaseClose(db);
         return false;
        }
      Print("DB: ", strTableName, " commit succeed!"," sql: ",sql[i]);
     }
   if(!DatabaseTransactionCommit(db))
     {
      Print("DB: ", strTableName, " commit failed with code ", GetLastError());
      return false;
     }
   DatabaseClose(db);
   return true;
  }
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool DBInsertOrder(MqlTradeResult &result,STR_LB_WAITING &wait)
  {
//---
   string sql[1];
   sql[0]="INSERT INTO ORDERS";
   sql[0]=sql[0]+"("+"DATE";
   sql[0]=sql[0]+","+"SYMBOL";
   sql[0]=sql[0]+","+"LOTS";
   sql[0]=sql[0]+","+"TIME_FRAME";
   sql[0]=sql[0]+","+"ORDER_TYPE";
   sql[0]=sql[0]+","+"OPEN_TYPE";
   sql[0]=sql[0]+","+"OP_PRICE";
   sql[0]=sql[0]+","+"TICKET_NO";
   sql[0]=sql[0]+","+"LIMIT_TYPE";
   sql[0]=sql[0]+","+"STOP_TYPE";
   sql[0]=sql[0]+") SELECT";
   sql[0]=sql[0]+" "+"'"+(string)iTime(wait.strSymbol,wait.enmTimeFrame,0)+"'";
   sql[0]=sql[0]+","+"'"+wait.strSymbol+"'";
   sql[0]=sql[0]+","+DoubleToString(wait.dblLots,1);
   sql[0]=sql[0]+","+(string)wait.enmTimeFrame;
   sql[0]=sql[0]+","+(string)wait.enmOrderType;
   sql[0]=sql[0]+","+(string)wait.enmOpenType;
   sql[0]=sql[0]+","+DoubleToString(result.price,(int)SymbolInfoInteger(wait.strSymbol,SYMBOL_DIGITS));
   sql[0]=sql[0]+","+(string)result.order;
   sql[0]=sql[0]+","+(string)wait.enmLimitType;
   sql[0]=sql[0]+","+(string)wait.enmStopType;
   if(!DBExecute(sql))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool DBUpdateOrder(STR_LB_ORDERS &io_order)
  {
   string sql[1];
   sql[0]="UPDATE ORDERS";
   sql[0]=sql[0]+" "+"SET";
   sql[0]=sql[0]+" "+"OPEN_TYPE"+"="+(string)io_order.enmOpenType;
   sql[0]=sql[0]+","+"LIMIT_TYPE"+"="+(string)io_order.enmLimitType;
   sql[0]=sql[0]+","+"STOP_TYPE"+"="+(string)io_order.enmStopType;
   sql[0]=sql[0]+" "+"WHERE TICKET_NO"+"="+(string)io_order.ulTicketNo;
   if(!DBExecute(sql))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
