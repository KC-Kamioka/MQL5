//+------------------------------------------------------------------+
//|                                                    SATCommon.mqh |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
string CommonDbName="SATCommon.sqlite";
string FileName="SpanAutoTrader.sqlite";
//+------------------------------------------------------------------+
//| enums                                                            |
//+------------------------------------------------------------------+
enum ENUM_CANDLE_SHAPES
  {
   CANDLE_NONE,
   CANDLE_HAMMER_H,
   CANDLE_HAMMER_L,
   CANDLE_BULLISH_ENGULFING,
   CANDLE_BEARISH_ENGULFING,
   CANDLE_PIERCING,
   CANDLE_DARK_CLOUD,
   CANDLE_INSIDE_UP,
   CANDLE_INSIDE_DOWN
  };
enum opLogic
  {
   Unknown,
   Now_Buy,
   Now_Sell,
   BB_Break_Buy,
   BB_Break_Sell,
   BB_Return_Buy,
   BB_Return_Sell,
   BB_Touch_1_Sigma_Buy,
   BB_Touch_1_Sigma_Sell,
   BB_Touch_Center_Buy,
   BB_Touch_Center_Sell,
   BB_Touch_2_Sigma_Buy,
   BB_Touch_2_Sigma_Sell,
   Reversal_Buy,
   Reversal_Sell
  };
enum tpLogic
  {
   tpNone,
   BB_Touch_Center,
   BB_Touch_1_Sigma,
   BB_Touch_2_Sigma,
   Trailing
  };
enum slLogic
  {
   slNone,
   BB_Break_Center,
   BB_Break_1_Sigma,
   BB_Break_2_Sigma
  };
//+------------------------------------------------------------------+
//| Create CodeMaster                                                |
//+------------------------------------------------------------------+
void CreateSATCommon()
  {
// SQLiteファイル名
   string TableName="Common";

// 指定したファイル名のDBを開く、なければ新規作成
   int handle=DatabaseOpen(CommonDbName, DATABASE_OPEN_READWRITE | DATABASE_OPEN_CREATE | DATABASE_OPEN_COMMON);
   if(handle==INVALID_HANDLE)
     {
      Print("DB: ", CommonDbName, "新規作成 エラーコード: ", GetLastError());
      return;
     }

// DB内のテーブルの存在を確認
   if(DatabaseTableExists(handle, TableName))
     {
      // テーブルが既に存在する場合は、それを削除
      if(!DatabaseExecute(handle, "DROP TABLE "+TableName))
        {
         Print("テーブルの削除に失敗しました。"+TableName+" エラーコード: ", GetLastError());
         DatabaseClose(handle);
         return;
        }
     }

// テーブルの新規作成
   if(!DatabaseExecute(handle, "CREATE TABLE "+TableName+"("
                       "Key          TEXT NOT NULL,"
                       "Value        TEXT NOT NULL );"))
     {
      Print("DB: ", FileName, "テーブル新規作成 エラーコード: ", GetLastError());
      DatabaseClose(handle);
      return;
     }

// DBにデータを追加する
   string sqls[1];
   int idx = 0;
   sqls[idx] = "INSERT INTO "+TableName+" (Key,Value) VALUES ('dbPath','" + TerminalInfoString(TERMINAL_DATA_PATH) + "');";
   for(int i=0;i<ArraySize(sqls);i++)
     {
      if(!DatabaseExecute(handle, sqls[i]))
        {
         Print("DB: ", FileName, "データの追加 エラーコード: ", GetLastError());
         DatabaseClose(handle);
         return;
        }
     }
// DBを閉じる
   DatabaseClose(handle);
  }
//+------------------------------------------------------------------+
//| Create CodeMaster                                                |
//+------------------------------------------------------------------+
void CreateCodeMaster()
  {
// SQLiteファイル名
   string TableName="CodeMaster";

// 指定したファイル名のDBを開く、なければ新規作成
   int handle=DatabaseOpen(FileName, DATABASE_OPEN_READWRITE | DATABASE_OPEN_CREATE);
   if(handle==INVALID_HANDLE)
     {
      Print("DB: ", FileName, "新規作成 エラーコード: ", GetLastError());
      return;
     }

// DB内のテーブルの存在を確認
   if(DatabaseTableExists(handle, TableName))
     {
      // テーブルが既に存在する場合は、それを削除
      if(!DatabaseExecute(handle, "DROP TABLE "+TableName))
        {
         Print("テーブルの削除に失敗しました。"+TableName+" エラーコード: ", GetLastError());
         DatabaseClose(handle);
         return;
        }
     }

// テーブルの新規作成
   if(!DatabaseExecute(handle, "CREATE TABLE "+TableName+"("
                       "Category          TEXT NOT NULL,"
                       "Code              INTEGER NOT NULL,"
                       "Name              TEXT NOT NULL );"))
     {
      Print("DB: ", FileName, "テーブル新規作成 エラーコード: ", GetLastError());
      DatabaseClose(handle);
      return;
     }

// DBにデータを追加する
   string sqls[35];
   int idx = 0;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'period'"+","+(string)PERIOD_M5+",'"+EnumToString(PERIOD_M5)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'period'"+","+(string)PERIOD_M15+",'"+EnumToString(PERIOD_M15)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'period'"+","+(string)PERIOD_M30+",'"+EnumToString(PERIOD_M30)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'period'"+","+(string)PERIOD_H1+",'"+EnumToString(PERIOD_H1)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'period'"+","+(string)PERIOD_H4+",'"+EnumToString(PERIOD_H4)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opNow'"+","+(string)Now_Buy+",'"+EnumToString(Now_Buy)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opNow'"+","+(string)Now_Sell+",'"+EnumToString(Now_Sell)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)Unknown+",'"+EnumToString(Unknown)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)BB_Break_Buy+",'"+EnumToString(BB_Break_Buy)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)BB_Break_Sell+",'"+EnumToString(BB_Break_Sell)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)BB_Return_Buy+",'"+EnumToString(BB_Return_Buy)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)BB_Return_Sell+",'"+EnumToString(BB_Return_Sell)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)BB_Touch_Center_Buy+",'"+EnumToString(BB_Touch_Center_Buy)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)BB_Touch_Center_Sell+",'"+EnumToString(BB_Touch_Center_Sell)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)BB_Touch_1_Sigma_Buy+",'"+EnumToString(BB_Touch_1_Sigma_Buy)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)BB_Touch_1_Sigma_Sell+",'"+EnumToString(BB_Touch_1_Sigma_Sell)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)BB_Touch_2_Sigma_Buy+",'"+EnumToString(BB_Touch_2_Sigma_Buy)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)BB_Touch_2_Sigma_Sell+",'"+EnumToString(BB_Touch_2_Sigma_Sell)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)Reversal_Buy+",'"+EnumToString(Reversal_Buy)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'opLogic'"+","+(string)Reversal_Sell+",'"+EnumToString(Reversal_Sell)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'tpLogic'"+","+(string)tpNone+",'"+EnumToString(tpNone)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'tpLogic'"+","+(string)Trailing+",'"+EnumToString(Trailing)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'tpLogic'"+","+(string)BB_Touch_Center+",'"+EnumToString(BB_Touch_Center)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'tpLogic'"+","+(string)BB_Touch_1_Sigma+",'"+EnumToString(BB_Touch_1_Sigma)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'tpLogic'"+","+(string)BB_Touch_2_Sigma+",'"+EnumToString(BB_Touch_2_Sigma)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'slLogic'"+","+(string)slNone+",'"+EnumToString(slNone)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'slLogic'"+","+(string)BB_Break_Center+",'"+EnumToString(BB_Break_Center)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'slLogic'"+","+(string)BB_Break_1_Sigma+",'"+EnumToString(BB_Break_1_Sigma)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ("+"'slLogic'"+","+(string)BB_Break_2_Sigma+",'"+EnumToString(BB_Break_2_Sigma)+"');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ('symbol',0,'USDJPY');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ('symbol',1,'GBPJPY');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ('symbol',2,'EURJPY');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ('symbol',3,'AUDJPY');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ('symbol',4,'EURUSD');";
   idx = idx + 1;
   sqls[idx] = "INSERT INTO "+TableName+" (Category,Code,Name) VALUES ('symbol',5,'GBPUSD');";

   for(int i=0;i<ArraySize(sqls);i++)
     {
      if(!DatabaseExecute(handle, sqls[i]))
        {
         Print("DB: ", FileName, "データの追加 エラーコード: ", GetLastError());
         DatabaseClose(handle);
         return;
        }
     }
// DBを閉じる
   DatabaseClose(handle);
  }
//+------------------------------------------------------------------+
//| Create Trade                                                |
//+------------------------------------------------------------------+
void CreateTrade()
  {
// SQLiteファイル名
   string TableName="Trade";

// 指定したファイル名のDBを開く、なければ新規作成
   int handle=DatabaseOpen(FileName, DATABASE_OPEN_READWRITE | DATABASE_OPEN_CREATE);
   if(handle==INVALID_HANDLE)
     {
      Print("DB: ", FileName, "新規作成 エラーコード: ", GetLastError());
      return;
     }

// DB内のテーブルの存在を確認
   if(DatabaseTableExists(handle, TableName))
     {
      // テーブルが既に存在する場合は、それを削除
      if(!DatabaseExecute(handle, "DROP TABLE "+TableName))
        {
         Print("テーブルの削除に失敗しました。"+TableName+" エラーコード: ", GetLastError());
         DatabaseClose(handle);
         return;
        }
     }

// テーブルの新規作成
   if(!DatabaseExecute(handle, "CREATE TABLE "+TableName+"("
                       "Seq               INTEGER PRIMARY KEY,"
                       "Symbol            TEXT,"
                       "PeriodCd          INTEGER NOT NULL,"
                       "Period            TEXT,"
                       "LotSize           REAL NOT NULL,"
                       "TpPoint           REAL NOT NULL,"
                       "SlPoint           REAL NOT NULL,"
                       "OpenCd            INTEGER,"
                       "Open              TEXT,"
                       "TakeProfitCd      INTEGER NOT NULL,"
                       "TakeProfit        TEXT NOT NULL,"
                       "StopLossCd        INTEGER NOT NULL,"
                       "StopLoss          TEXT NOT NULL,"
                       "TicketNo          TEXT);"))
     {
      Print("DB: ", FileName, "テーブル新規作成 エラーコード: ", GetLastError());
      DatabaseClose(handle);
      return;
     }

// DBを閉じる
   DatabaseClose(handle);
  }
//+------------------------------------------------------------------+
//| Create Default                                                |
//+------------------------------------------------------------------+
void CreateDefault()
  {
// SQLiteファイル名
   string TableName="DefaultSetting";

// 指定したファイル名のDBを開く、なければ新規作成
   int handle=DatabaseOpen(FileName, DATABASE_OPEN_READWRITE | DATABASE_OPEN_CREATE);
   if(handle==INVALID_HANDLE)
     {
      Print("DB: ", FileName, "新規作成 エラーコード: ", GetLastError());
      return;
     }

// DB内のテーブルの存在を確認
   if(DatabaseTableExists(handle, TableName))
     {
      // テーブルが既に存在する場合は、それを削除
      if(!DatabaseExecute(handle, "DROP TABLE "+TableName))
        {
         Print("テーブルの削除に失敗しました。"+TableName+" エラーコード: ", GetLastError());
         DatabaseClose(handle);
         return;
        }
     }

// テーブルの新規作成
   if(!DatabaseExecute(handle, "CREATE TABLE "+TableName+"("
                       "Symbol            TEXT,"
                       "PeriodCd          INTEGER,"
                       "LotSize           REAL NOT NULL,"
                       "TpPoint           REAL NOT NULL,"
                       "SlPoint           REAL NOT NULL,"
                       "OpenCd            INTEGER,"
                       "TakeProfitCd      INTEGER NOT NULL,"
                       "StopLossCd        INTEGER NOT NULL);"))
     {
      Print("DB: ", FileName, "テーブル新規作成 エラーコード: ", GetLastError());
      DatabaseClose(handle);
      return;
     }
     
   string sqls[1];
   int idx = 0;
   sqls[idx] = "INSERT INTO "+TableName+" (Symbol,PeriodCd,LotSize,TpPoint,SlPoint,OpenCd,TakeProfitCd,StopLossCd) VALUES ('GBPJPY',16385,0.1,0,100,3,0,3);";

   for(int i=0;i<ArraySize(sqls);i++)
     {
      if(!DatabaseExecute(handle, sqls[i]))
        {
         Print("DB: ", FileName, "データの追加 エラーコード: ", GetLastError());
         DatabaseClose(handle);
         return;
        }
     }
// DBを閉じる
   DatabaseClose(handle);
  }
//+------------------------------------------------------------------+
