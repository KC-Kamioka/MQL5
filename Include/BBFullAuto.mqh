//+------------------------------------------------------------------+
//|                                                   BBFullAuto.mqh |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#include <BFACommon.mqh>
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
#import "stdlib.ex5"
string ErrorDescription(int error_code);
#import
#import "BFACommon.ex5"
bool StopTrailing(ulong ticket);
void LosscutAll(pos_info &pi[]);
void PositionCloseByTicket(ulong ticket);
bool CenterPositive(ENUM_TIMEFRAMES tf);
bool CenterNegative(ENUM_TIMEFRAMES tf);
bool IsRange(ENUM_TIMEFRAMES tf);
bool Above2Sigma(ENUM_TIMEFRAMES tf);
bool Below2Sigma(ENUM_TIMEFRAMES tf);
bool Break2SigmaUp(ENUM_TIMEFRAMES tf);
bool Break2SigmaDown(ENUM_TIMEFRAMES tf);
bool AboveUpper1Sigma(ENUM_TIMEFRAMES tf);
bool BelowLower1Sigma(ENUM_TIMEFRAMES tf);
bool ReversalBuyExit();
bool ReversalSellExit();
bool AboveLower1Sigma(ENUM_TIMEFRAMES tf);
bool BelowUpper1Sigma(ENUM_TIMEFRAMES tf);
bool BelowCenter(ENUM_TIMEFRAMES tf);
bool AboveCenter(ENUM_TIMEFRAMES tf);
bool Return1SigmaDown(ENUM_TIMEFRAMES tf);
bool AboveCloud(ENUM_TIMEFRAMES tf);
bool UnderCloud(ENUM_TIMEFRAMES tf);
bool CloudUp(ENUM_TIMEFRAMES tf);
bool CloudDown(ENUM_TIMEFRAMES tf);
bool MACDGoldenCross();
bool MACDDeadCross();
bool Return1SigmaUp(ENUM_TIMEFRAMES tf);
bool ReturnCenterDown(ENUM_TIMEFRAMES tf);
bool ReturnCenterUp(ENUM_TIMEFRAMES tf);
void ReversalOrderModify(ulong magic, double price, double sl = 0);
void ModifyPosition(ulong ticket, double sl);
void BreakEven(ulong ticket, double sl);
void OrderBuyPosition(pos_info &pi[], ulong magic, double price, double sl = 0);
void OrderSellPosition(pos_info &pi[], ulong magic, double price, double sl = 0);
void OpenBuyPosition(pos_info &pi[], ulong magic, double sl = 0);
void OpenSellPosition(pos_info &pi[], ulong magic, double sl = 0);
void SetPosInfo(pos_info &pi[]);
bool IsExistPostion(pos_info &pi[],ulong magic);
bool SetHandles(ENUM_TIMEFRAMES tf,handle_struct &handle);
bool SetValues(ENUM_TIMEFRAMES tf,handle_struct &handle,values_struct &values[],values_struct &max_values,values_struct &min_values);
#import
#import "BFABreak2Sigma.ex5"
void OpenBBBreak2Sigma(
   pos_info &pi[],
   values_struct &values_lower[],
   values_struct &max_value_lower,
   values_struct &min_value_lower
);
void LosscutBBB(
   pos_info &pi[],
   values_struct &values_lower[],
   values_struct &max_value_lower,
   values_struct &min_value_lower
);
#import
#import "BFAReversal.ex5"
void OpenBBReversal(pos_info &pi[]);
void TakeProfitBBReversal(pos_info &pi[]);
void LosscutBBReversal(pos_info &pi[]);
#import
#import "BFAReturn.ex5"
void OpenBBReturn(pos_info &pi[]);
void TakeProfitBBReturn(pos_info &pi[]);
void LosscutBBReturn(pos_info &pi[]);
#import
#import "BFAMACDCross.ex5"
void OpenMACDCross(pos_info &pi[]);
void LosscutMACDCross(pos_info &pi[]);
#import

//+------------------------------------------------------------------+
