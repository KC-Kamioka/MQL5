//+------------------------------------------------------------------+
//|                                                  TesterPanel.mqh |
//|                        Copyright 2011, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Include files                                                    |
//+------------------------------------------------------------------+
#include <DBTradigTester.mqh>
#include <TradigTester.mqh>
#include <Controls\Dialog.mqh>
#include <Controls\Button.mqh>
#include <Controls\DatePicker.mqh>
#include <Controls\ComboBox.mqh>
#include <Controls\ListView.mqh>
#include <Controls\Label.mqh>
#include <Controls\Edit.mqh>
#include <Controls\SpinEdit.mqh>
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
//---インデントとギャップ
#define INDENT_TOP                        (5)
#define INDENT_LEFT                       (10)
#define CONTROLS_GAP_X                    (5)
#define CONTROLS_GAP_Y                    (5)
#define CONTROLS_GAP_X_S                  (3)
#define CONTROLS_GAP_Y_S                  (3)
#define CLM_WIDTH                         (130)
#define ROW_HEIGHT                        (20)
#define ROW_1                             (INDENT_TOP)
#define ROW_2                             (ROW_1+ROW_HEIGHT)
#define ROW_3                             (ROW_2+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_4                             (ROW_3+CONTROLS_GAP_Y*4+ROW_HEIGHT)
#define ROW_5                             (ROW_4+ROW_HEIGHT)
#define ROW_6                             (ROW_5+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_7                             (ROW_6+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_8                             (ROW_7+CONTROLS_GAP_Y*4+ROW_HEIGHT)
#define ROW_9                             (ROW_8+ROW_HEIGHT)
#define ROW_10                            (ROW_9+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_11                            (ROW_10+ROW_HEIGHT)
#define ROW_12                            (ROW_11+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_13                            (ROW_12+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_1_D                           (INDENT_TOP)
#define ROW_2_D                           (ROW_1_D+ROW_HEIGHT)
#define ROW_3_D                           (ROW_2_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_4_D                           (ROW_3_D+ROW_HEIGHT)
#define ROW_5_D                           (ROW_4_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_6_D                           (ROW_5_D+ROW_HEIGHT)
#define ROW_7_D                           (ROW_6_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_8_D                           (ROW_7_D+ROW_HEIGHT)
#define ROW_9_D                           (ROW_8_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_10_D                          (ROW_9_D+ROW_HEIGHT)
#define ROW_11_D                          (ROW_10_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_12_D                          (ROW_11_D+ROW_HEIGHT)
#define ROW_13_D                          (ROW_12_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_14_D                          (ROW_13_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define CLM_1                             (INDENT_LEFT)
#define CLM_2                             (CLM_1+CONTROLS_GAP_Y+CLM_WIDTH)
#define CLM_3                             (CLM_2+CONTROLS_GAP_Y+CLM_WIDTH)
#define CLM_4                             (CLM_3+CONTROLS_GAP_Y+20)
#define CLM_5                             (CLM_4+CONTROLS_GAP_Y+CLM_WIDTH)
#define CLM_6                             (CLM_5+CONTROLS_GAP_Y+CLM_WIDTH+30)
#define CLM_7                             (CLM_6+CONTROLS_GAP_Y+CLM_WIDTH)
#define YEAR_WIDTH                        (50)
#define YEAR_HEIGHT                       (20)
#define MONTH_WIDTH                       (40)
#define MONTH_HEIGHT                      (20)
#define DAY_WIDTH                         (40)
#define DAY_HEIGHT                        (20)
#define TIME_WIDTH                        (40)
#define TIME_HEIGHT                       (20)
#define BUTTON_WIDTH                      (100)
#define BUTTON_HEIGHT                     (20)
#define BUTTON_WIDTH_S                    (49)
#define BUTTON_WIDTH_B                    (130)
#define LIST_WIDTH                        (100)
#define LIST_HEIGHT                       (110)
#define LABEL_WIDTH                       (100)
#define LABEL_HEIGHT                      (20)
#define EDIT_WIDTH                        (130)
#define EDIT_HEIGHT                       (20)
#define EDIT_WIDTH_B                      (220)
#define COMBO_WIDTH                       (130)
#define COMBO_HEIGHT                      (20)
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENM_EDT
  {
   EDT_LOTS
   ,EDT_PL
   ,EDT_PF
   ,EDT_WR
   ,EDT_PT
   ,EDT_DT
   ,EDT_LOTS_DTL
   ,EDT_TN
   ,EDT_PL_DTL
  };
enum ENM_CMB
  {
   CMB_OTP
   ,CMB_LTP
   ,CMB_LTF
   ,CMB_ORTP
   ,CMB_STP
   ,CMB_STF
  };
enum ENM_BTN
  {
   BTN_ST
   ,BTN_BUY
   ,BTN_SELL
   ,BTN_CL
   ,BTN_SET
   ,BTN_MOD
  };
enum ENM_LST
  {
   LST_WAIT
   ,LST_POS
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct STR_LABEL
  {
   string            name;
   int               x1;
   int               y1;
   int               x2;
   int               y2;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct STR_BUTTON
  {
   string            name;
   int               x1;
   int               y1;
   int               x2;
   int               y2;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct STR_EDIT
  {
   string            name;
   ENUM_ALIGN_MODE   enmAlign;
   bool              read_only;
   string            init;
   int               x1;
   int               y1;
   int               x2;
   int               y2;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct STR_COMBO
  {
   string            name;
   string            init_list[];
   int               x1;
   int               y1;
   int               x2;
   int               y2;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct STR_LIST
  {
   string            name;
   int               x1;
   int               y1;
   int               x2;
   int               y2;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
STR_LABEL sl[17];
STR_BUTTON sb[6];
STR_EDIT se[9];
STR_COMBO sc[6];
STR_LIST s_lst[2];
//+------------------------------------------------------------------+
//| Class CPanelDialog                                       |
//| Usage: main dialog of the Controls application                   |
//+------------------------------------------------------------------+
class CPanelDialog : public CAppDialog
  {
private:
   CComboBox         m_cmbYear,m_cmbMonth,m_cmbDay,m_cmbHour,m_cmbMin,m_cmbSymbol,m_cmbOrders[];
   CButton           m_btnPrev,m_btnNext,m_buttons[];
   CListView         m_lists[];
   CLabel            m_lblConfig,m_lblDetail,m_lblPos,m_lblTitles[];
   CEdit             m_edits[];

public:
                     CPanelDialog(void);
                    ~CPanelDialog(void);
   //--- create
   virtual bool      Create(const long chart,const string name,const int subwin,const int x1,const int y1,const int x2,const int y2);
   //---チャートイベントハンドラ
   virtual bool      OnEvent(const int id,const long &lparam,const double &dparam,const string &sparam);

private:
   virtual datetime  GetCurrentDateTime(void);
   virtual void      ResetItems(void);
   virtual void      ClearItems(void);


protected:
   //従属コントロールの作成
   bool              CreateCmbDatetime(void);
   bool              CreateBtnPrevNext(void);
   bool              CreateCmbSymbols(void);
   bool              CreateLstPostions(CListView &m_list,STR_LIST &io_sl);
   bool              CreateButton(CButton &m_button,STR_BUTTON &sb);
   bool              CreateLabel(CLabel &m_label,STR_LABEL &sl);
   bool              CreateEdit(CEdit &m_edit,STR_EDIT &se);
   bool              CreateCmbOrders(CComboBox &m_cmbOrder,STR_COMBO &sc);
   //依存コントロールイベントのハンドラ
   void              OnClickCmbHour(void);
   void              OnClickCmbMin(void);
   void              OnClickBtnStart(void);
   void              OnClickBtnPrev(void);
   void              OnClickBtnNext(void);
   void              OnClickEdtLots(void);
   void              OnClickBtnBuy(void);
   void              OnClickBtnSell(void);
   void              OnClickBtnClose(void);
   void              OnClickBtnModify(void);
   void              OnClickLstOrders(void);
   void              OnClickCmbOrders(void);
   void              OnCheckLots(void);
   void              ResetAll(void);
  };
//+------------------------------------------------------------------+
//|イベントの取り扱い |
//+------------------------------------------------------------------+
EVENT_MAP_BEGIN(CPanelDialog)
ON_EVENT(ON_CLICK,m_cmbHour,OnClickCmbHour)
ON_EVENT(ON_CLICK,m_cmbMin,OnClickCmbMin)
ON_EVENT(ON_CLICK,m_buttons[BTN_ST],OnClickBtnStart)
ON_EVENT(ON_CLICK,m_btnPrev,OnClickBtnPrev)
ON_EVENT(ON_CLICK,m_btnNext,OnClickBtnNext)
ON_EVENT(ON_CLICK,m_lists[LST_POS],OnClickLstOrders)
ON_EVENT(ON_CLICK,m_edits[EDT_LOTS],OnClickEdtLots)
ON_EVENT(ON_CLICK,m_buttons[BTN_BUY],OnClickBtnBuy)
ON_EVENT(ON_CLICK,m_buttons[BTN_SELL],OnClickBtnSell)
ON_EVENT(ON_CLICK,m_buttons[BTN_CL],OnClickBtnClose)
ON_EVENT(ON_CLICK,m_buttons[BTN_MOD],OnClickBtnModify)
ON_EVENT(ON_END_EDIT,m_edits[EDT_LOTS],OnCheckLots)
ON_NO_ID_EVENT(ON_TESTER_RESET_ALL,ResetAll)
EVENT_MAP_END(CAppDialog)
//+------------------------------------------------------------------+
//|コンストラクタ |
//+------------------------------------------------------------------+
CPanelDialog::CPanelDialog(void)
  {
  }
//+------------------------------------------------------------------+
//|デストラクタ |
//+------------------------------------------------------------------+
CPanelDialog::~CPanelDialog(void)
  {
  }
//+------------------------------------------------------------------+
//|作成 |
//+------------------------------------------------------------------+
bool CPanelDialog::Create(const long chart,const string name,const int subwin,const int x1,const int y1,const int x2,const int y2)
  {
   if(!CAppDialog::Create(chart,name,subwin,x1,y1,x2,y2))
      return(false);
//---
   if(!SetLabelValues())
      return(false);
   ArrayResize(m_lblTitles,ArraySize(sl));
   for(int i=0; i<ArraySize(sl); i++)
     {
      if(!CreateLabel(m_lblTitles[i],sl[i]))
         return(false);
     }
//---
   if(!CreateCmbDatetime())
      return(false);
   if(!CreateBtnPrevNext())
      return(false);
   if(!CreateCmbSymbols())
      return(false);
//---
   if(!SetButtonValues())
      return(false);
   ArrayResize(m_buttons,ArraySize(sb));
   for(int i=0; i<ArraySize(sb); i++)
     {
      if(!CreateButton(m_buttons[i],sb[i]))
         return(false);
     }
//---
   if(!SetEditValues())
      return(false);
   ArrayResize(m_edits,ArraySize(se));
   for(int i=0; i<ArraySize(se); i++)
     {
      if(!CreateEdit(m_edits[i],se[i]))
         return(false);
     }
//---
   if(!SetListViewValues())
      return(false);
   ArrayResize(m_lists,ArraySize(s_lst));
   for(int i=0; i<ArraySize(s_lst); i++)
     {
      if(!CreateLstPostions(m_lists[i],s_lst[i]))
         return(false);
     }
//---
   if(!SetComboValues())
      return(false);
   ArrayResize(m_cmbOrders,ArraySize(sc));
   for(int i=0; i<ArraySize(sc); i++)
     {
      if(!CreateCmbOrders(m_cmbOrders[i],sc[i]))
         return(false);
     }
//---
   ClearItems();
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the "CmbDatetime" combo                                       |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateCmbDatetime(void)
  {
   string year[];
   string month[];
   string day[];
   string hour[];
   string min[];
   MqlDateTime dt;
//---
   if(!ReadTesterHistoryDateList(_Symbol,_Period,year,month,day,hour,min))
      return false;
   if(!ReadTesterDate(dt))
      return false;
//---座標
   int x1=CLM_1+CONTROLS_GAP_X;
   int y1=ROW_2;
   int x2=x1+YEAR_WIDTH;
   int y2=y1+YEAR_HEIGHT;
//--- create
   if(!m_cmbYear.Create(0,"CmbDate",1,x1,y1,x2,y2))
      return(false);
   for(int i=0; i<ArraySize(year); i++)
      if(!m_cmbYear.AddItem(year[i]))
         return(false);
   m_cmbYear.SelectByText((string)dt.year);
   if(!Add(m_cmbYear))
      return(false);
//---座標
   x1=x1+YEAR_WIDTH+CONTROLS_GAP_X_S;
   y1=ROW_2;
   x2=x1+MONTH_WIDTH;
   y2=y1+MONTH_HEIGHT;
//--- create
   if(!m_cmbMonth.Create(0,"CmbMonth",1,x1,y1,x2,y2))
      return(false);
   for(int i=0; i<ArraySize(month); i++)
      if(!m_cmbMonth.AddItem(month[i]))
         return(false);
   m_cmbMonth.SelectByText(ZeroPadding(dt.mon));
   if(!Add(m_cmbMonth))
      return(false);
//---座標
   x1=x1+MONTH_WIDTH+CONTROLS_GAP_X_S;
   y1=ROW_2;
   x2=x1+DAY_WIDTH;
   y2=y1+DAY_HEIGHT;
//--- create
   if(!m_cmbDay.Create(0,"CmbDay",1,x1,y1,x2,y2))
      return(false);
   for(int i=0; i<ArraySize(day); i++)
      if(!m_cmbDay.AddItem(day[i]))
         return(false);
   m_cmbDay.SelectByText(ZeroPadding(dt.day));
   if(!Add(m_cmbDay))
      return(false);
//---座標
   x1=x1+DAY_WIDTH+CONTROLS_GAP_X_S;
   y1=ROW_2;
   x2=x1+TIME_WIDTH;
   y2=y1+TIME_HEIGHT;
//--- create
   if(!m_cmbHour.Create(0,"CmbHour",1,x1,y1,x2,y2))
      return(false);
   for(int i=0; i<ArraySize(hour); i++)
      if(!m_cmbHour.AddItem(hour[i]))
         return(false);
   m_cmbHour.SelectByText(ZeroPadding(dt.hour));
   if(!Add(m_cmbHour))
      return(false);
//---座標
   x1=x1+TIME_WIDTH+CONTROLS_GAP_X_S;
   y1=ROW_2;
   x2=x1+TIME_WIDTH;
   y2=y1+TIME_HEIGHT;
//--- create
   if(!m_cmbMin.Create(0,"CmbMin",1,x1,y1,x2,y2))
      return(false);
   for(int i=0; i<ArraySize(min); i++)
      if(!m_cmbMin.AddItem(min[i]))
         return(false);
   m_cmbMin.SelectByText(ZeroPadding(dt.min));
   if(!Add(m_cmbMin))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the "Prev" button                                      |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateBtnPrevNext(void)
  {
//---座標
   int x1=CLM_2+CONTROLS_GAP_X;
   int y1=ROW_3;
   int x2=x1+BUTTON_WIDTH_S;
   int y2=y1+BUTTON_HEIGHT;
//--- create
   if(!m_btnPrev.Create(0,"Prev",1,x1,y1,x2,y2))
      return(false);
   if(!m_btnPrev.Text("<<Prev"))
      return(false);
   if(!Add(m_btnPrev))
      return(false);
//---座標
   x1=x1+BUTTON_WIDTH_S+2;
   x2=x1+BUTTON_WIDTH_S;
//--- create
   if(!m_btnNext.Create(0,"Next",1,x1,y1,x2,y2))
      return(false);
   if(!m_btnNext.Text("Next>>"))
      return(false);
   if(!Add(m_btnNext))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the "" combo                                       |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateCmbSymbols(void)
  {
//---座標
   int x1=CLM_1+CONTROLS_GAP_X;
   int y1=ROW_5;
   int x2=x1+COMBO_WIDTH;
   int y2=y1+COMBO_HEIGHT;
//--- create
   if(!m_cmbSymbol.Create(0,"CmbSymbols",1,x1,y1,x2,y2))
      return(false);
   for(int i=0; i<ArraySize(MySymbols); i++)
     {
      if(!m_cmbSymbol.AddItem(MySymbols[i]))
         return(false);
     }
   if(!m_cmbSymbol.SelectByText(_Symbol))
      return(false);
   if(!Add(m_cmbSymbol))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Set label                                                        |
//+------------------------------------------------------------------+
bool SetLabelValues()
  {
   int i=0;
   sl[i].name="Tester Config";
   sl[i].x1=CLM_1;
   sl[i].y1=ROW_1;
   i++;
   sl[i].name="Order";
   sl[i].x1=CLM_1;
   sl[i].y1=ROW_4;
   i++;
   sl[i].name="ProfitTotal";
   sl[i].x1=CLM_1;
   sl[i].y1=ROW_8;
   i++;
   sl[i].name="ProfitFactor";
   sl[i].x1=CLM_1;
   sl[i].y1=ROW_10;
   i++;
   sl[i].name="WinRates";
   sl[i].x1=CLM_1;
   sl[i].y1=ROW_12;
//---
   i++;
   sl[i].name="WaitingList";
   sl[i].x1=CLM_4;
   sl[i].y1=ROW_1_D;
   i++;
   sl[i].name="PositionList";
   sl[i].x1=CLM_4;
   sl[i].y1=ROW_7_D;
   i++;
   sl[i].name="Date";
   sl[i].x1=CLM_5;
   sl[i].y1=ROW_1_D;
   i++;
   sl[i].name="Lots";
   sl[i].x1=CLM_5;
   sl[i].y1=ROW_3_D;
   i++;
   sl[i].name="OrderType";
   sl[i].x1=CLM_5;
   sl[i].y1=ROW_5_D;
   i++;
   sl[i].name="TicketNo";
   sl[i].x1=CLM_5;
   sl[i].y1=ROW_7_D;
   i++;
   sl[i].name="Profit";
   sl[i].x1=CLM_5;
   sl[i].y1=ROW_9_D;
   i++;
   sl[i].name="OpenType";
   sl[i].x1=CLM_6;
   sl[i].y1=ROW_1_D;
   i++;
   sl[i].name="LimitType";
   sl[i].x1=CLM_6;
   sl[i].y1=ROW_3_D;
   i++;
   sl[i].name="LimitTF";
   sl[i].x1=CLM_6;
   sl[i].y1=ROW_5_D;
   i++;
   sl[i].name="StopType";
   sl[i].x1=CLM_6;
   sl[i].y1=ROW_7_D;
   i++;
   sl[i].name="StopTF";
   sl[i].x1=CLM_6;
   sl[i].y1=ROW_9_D;
   for(i=0; i<ArraySize(sl); i++)
     {
      //---Label
      sl[i].x2=sl[i].x1+LABEL_WIDTH;
      sl[i].y2=sl[i].y1+LABEL_HEIGHT;
     }
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Set Button                                        |
//+------------------------------------------------------------------+
bool SetButtonValues()
  {
   sb[BTN_ST].name="Start";
   sb[BTN_ST].x1=CLM_1+CONTROLS_GAP_X;
   sb[BTN_ST].y1=ROW_3;
   sb[BTN_BUY].name="Buy";
   sb[BTN_BUY].x1=CLM_2+CONTROLS_GAP_X;
   sb[BTN_BUY].y1=ROW_5;
   sb[BTN_SELL].name="Sell";
   sb[BTN_SELL].x1=CLM_2+CONTROLS_GAP_X;
   sb[BTN_SELL].y1=ROW_6;
   sb[BTN_CL].name="Close";
   sb[BTN_CL].x1=CLM_2+CONTROLS_GAP_X;
   sb[BTN_CL].y1=ROW_7;
   sb[BTN_MOD].name="Modify";
   sb[BTN_MOD].x1=CLM_4+CONTROLS_GAP_X;
   sb[BTN_MOD].y1=ROW_13_D;
   sb[BTN_SET].name="Set";
   sb[BTN_SET].x1=CLM_6+CONTROLS_GAP_X;
   sb[BTN_SET].y1=ROW_13_D;
   for(int i=0; i<ArraySize(sb); i++)
     {
      sb[i].x2=sb[i].x1+BUTTON_WIDTH;
      sb[i].y2=sb[i].y1+BUTTON_HEIGHT;
     }
   sb[BTN_ST].x2=sb[BTN_ST].x1+BUTTON_WIDTH_B;
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Set Edit                                        |
//+------------------------------------------------------------------+
bool SetEditValues()
  {
   se[EDT_LOTS].name="Lots";
   se[EDT_LOTS].enmAlign=ALIGN_RIGHT;
   se[EDT_LOTS].x1=CLM_1+CONTROLS_GAP_X;
   se[EDT_LOTS].y1=ROW_6;
   se[EDT_LOTS].init="0.1";
   se[EDT_PL].name="PL";
   se[EDT_PL].enmAlign=ALIGN_RIGHT;
   se[EDT_PL].x1=CLM_1+CONTROLS_GAP_X;
   se[EDT_PL].y1=ROW_7;
   se[EDT_PL].init="0";
   se[EDT_PT].name="ProfitTotal";
   se[EDT_PT].x1=CLM_1+CONTROLS_GAP_X;
   se[EDT_PT].y1=ROW_9;
   se[EDT_PT].init="0";
   se[EDT_PF].name="ProfitFactor";
   se[EDT_PF].x1=CLM_1+CONTROLS_GAP_X;
   se[EDT_PF].y1=ROW_11;
   se[EDT_PF].init="0.0";
   se[EDT_WR].name="WinRates";
   se[EDT_WR].x1=CLM_1+CONTROLS_GAP_X;
   se[EDT_WR].y1=ROW_13;
   se[EDT_WR].init="0.0";
   se[EDT_DT].name="Date";
   se[EDT_DT].x1=CLM_5+CONTROLS_GAP_X;
   se[EDT_DT].y1=ROW_2_D;
   se[EDT_LOTS_DTL].name="Lots_dtl";
   se[EDT_LOTS_DTL].enmAlign=ALIGN_RIGHT;
   se[EDT_LOTS_DTL].x1=CLM_5+CONTROLS_GAP_X;
   se[EDT_LOTS_DTL].y1=ROW_4_D;
   se[EDT_TN].name="TicketNo";
   se[EDT_TN].x1=CLM_5+CONTROLS_GAP_X;;
   se[EDT_TN].y1=ROW_8_D;
   se[EDT_PL_DTL].name="Profit";
   se[EDT_PL_DTL].enmAlign=ALIGN_RIGHT;
   se[EDT_PL_DTL].x1=CLM_5+CONTROLS_GAP_X;
   se[EDT_PL_DTL].y1=ROW_10_D;
   for(int i=0; i<ArraySize(se); i++)
     {
      //---Edit
      se[i].x2=se[i].x1+EDIT_WIDTH;
      se[i].y2=se[i].y1+EDIT_HEIGHT;

      //---
      se[i].read_only=true;
     }
   se[EDT_PT].x2=se[EDT_PT].x1+EDIT_WIDTH_B;
   se[EDT_PF].x2=se[EDT_PF].x1+EDIT_WIDTH_B;
   se[EDT_WR].x2=se[EDT_WR].x1+EDIT_WIDTH_B;
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Set ListView                                        |
//+------------------------------------------------------------------+
bool SetListViewValues()
  {
   s_lst[LST_WAIT].name="WaitingList";
   s_lst[LST_WAIT].x1=CLM_4+CONTROLS_GAP_X;
   s_lst[LST_WAIT].y1=ROW_2_D;
   s_lst[LST_POS].name="PostionList";
   s_lst[LST_POS].x1=CLM_4+CONTROLS_GAP_X;
   s_lst[LST_POS].y1=ROW_8_D;
   for(int i=0; i<ArraySize(s_lst); i++)
     {
      s_lst[i].x2=s_lst[i].x1+LIST_WIDTH;
      s_lst[i].y2=s_lst[i].y1+LIST_HEIGHT;
     }
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Set ComboBox                                        |
//+------------------------------------------------------------------+
bool SetComboValues()
  {
   string strVal[];
//---
   sc[CMB_ORTP].name="OrderType";
   sc[CMB_ORTP].x1=CLM_5+CONTROLS_GAP_X;
   sc[CMB_ORTP].y1=ROW_6_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)ORDER,strVal,T_DB_NAME);
   ArrayCopy(sc[CMB_ORTP].init_list,strVal);
   sc[CMB_OTP].name="OpenType";
   sc[CMB_OTP].x1=CLM_6+CONTROLS_GAP_X;
   sc[CMB_OTP].y1=ROW_2_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)OP_AUTO,strVal,T_DB_NAME);
   ArrayCopy(sc[CMB_OTP].init_list,strVal);
   sc[CMB_LTP].name="LimitType";
   sc[CMB_LTP].x1=CLM_6+CONTROLS_GAP_X;
   sc[CMB_LTP].y1=ROW_4_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)LIMIT,strVal,T_DB_NAME);
   ArrayCopy(sc[CMB_LTP].init_list,strVal);
   sc[CMB_LTF].name="LimitTF";
   sc[CMB_LTF].x1=CLM_6+CONTROLS_GAP_X;
   sc[CMB_LTF].y1=ROW_6_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)PERIOD,strVal,T_DB_NAME);
   ArrayCopy(sc[CMB_LTF].init_list,strVal);
   sc[CMB_STP].name="StopType";
   sc[CMB_STP].x1=CLM_6+CONTROLS_GAP_X;
   sc[CMB_STP].y1=ROW_8_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)STOP,strVal,T_DB_NAME);
   ArrayCopy(sc[CMB_STP].init_list,strVal);
   sc[CMB_STF].name="StopTF";
   sc[CMB_STF].x1=CLM_6+CONTROLS_GAP_X;
   sc[CMB_STF].y1=ROW_10_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)PERIOD,strVal,T_DB_NAME);
   ArrayCopy(sc[CMB_STF].init_list,strVal);
//---
   for(int i=0; i<ArraySize(sc); i++)
     {
      sc[i].x2=sc[i].x1+EDIT_WIDTH;
      sc[i].y2=sc[i].y1+EDIT_HEIGHT;
     }
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the list                                         |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateLstPostions(CListView &m_list,STR_LIST &io_sl)
  {
//--- create
   if(!m_list.Create(0,"Lst_"+io_sl.name,1,io_sl.x1,io_sl.y1,io_sl.x2,io_sl.y2))
      return(false);
//---
   if(!Add(m_list))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the Button                                      |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateButton(CButton &m_button,STR_BUTTON &io_sb)
  {
//--- create
   if(!m_button.Create(0,"Btn_"+io_sb.name,1,io_sb.x1,io_sb.y1,io_sb.x2,io_sb.y2))
      return(false);
   if(!m_button.Text(io_sb.name))
      return(false);
   if(!Add(m_button))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the label                                                 |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateLabel(CLabel &m_label,STR_LABEL &io_sl)
  {
//--- create
   if(!m_label.Create(0,"Lbl_"+io_sl.name,1,io_sl.x1,io_sl.y1,io_sl.x2,io_sl.y2))
      return(false);
   if(!m_label.Text(io_sl.name))
      return(false);
   if(!Add(m_label))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the Edit                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateEdit(CEdit &m_edit,STR_EDIT &io_se)
  {
   if(!m_edit.Create(0,"Edt_"+io_se.name,1,io_se.x1,io_se.y1,io_se.x2,io_se.y2))
      return(false);
   if(!m_edit.ReadOnly(io_se.read_only))
      return(false);
   if(!m_edit.TextAlign(io_se.enmAlign))
      return(false);
   if(!m_edit.Text(io_se.init))
      return(false);
   if(!Add(m_edit))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the Combo                                              |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateCmbOrders(CComboBox &m_cmbOrder,STR_COMBO &io_sc)
  {
   if(!m_cmbOrder.Create(0,"Cmb_"+io_sc.name,1,io_sc.x1,io_sc.y1,io_sc.x2,io_sc.y2))
      return(false);
   for(int i=0; i<ArraySize(io_sc.init_list); i++)
      if(!m_cmbOrder.AddItem(io_sc.init_list[i]))
         return(false);
   if(!Add(m_cmbOrder))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickCmbHour(void)
  {
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickCmbMin(void)
  {
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnStart(void)
  {
//---
   datetime dtStart=GetCurrentDateTime();
//---
   if(!UpdTesterDate(dtStart))
      return;
//---
   if(!DBInitTradingTester())
      return;
//---
   if(!m_lists[LST_WAIT].ItemsClear())
      return;
//---
   if(!m_lists[LST_POS].ItemsClear())
      return;
//---
   SendEvent(ON_TESTER_START,dtStart);
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnPrev(void)
  {
//---
   MqlDateTime mdt;
   datetime dtNow=GetCurrentDateTime();
   int intBarNo=iBarShift(_Symbol,_Period,dtNow);
   datetime dtPrev=iTime(_Symbol,_Period,intBarNo+1);
   TimeToStruct(dtPrev,mdt);
//---
   m_cmbYear.SelectByText(ZeroPadding(mdt.year));
   m_cmbMonth.SelectByText(ZeroPadding(mdt.mon));
   m_cmbDay.SelectByText(ZeroPadding(mdt.day));
   m_cmbHour.SelectByText(ZeroPadding(mdt.hour));
   m_cmbMin.SelectByText(ZeroPadding(mdt.min));
//---
   ResetItems();
//---
   SendEvent(ON_TESTER_PREV,dtPrev);
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnNext(void)
  {
//---
   MqlDateTime mdt;
   datetime dtNow=GetCurrentDateTime();
   int intBarNo=iBarShift(_Symbol,_Period,dtNow);
   Print(GetLastError());
   if(intBarNo==0)
      return;
   datetime dtNext=iTime(_Symbol,_Period,intBarNo-1);
   Print(GetLastError());
   TimeToStruct(dtNext,mdt);
   m_cmbYear.SelectByText(ZeroPadding(mdt.year));
   m_cmbMonth.SelectByText(ZeroPadding(mdt.mon));
   m_cmbDay.SelectByText(ZeroPadding(mdt.day));
   m_cmbHour.SelectByText(ZeroPadding(mdt.hour));
   m_cmbMin.SelectByText(ZeroPadding(mdt.min));
//---
   STR_LB_ORDERS orders[];
   if(!ReadOrders(orders,T_DB_NAME))
      return;
//---
   double dblSpread=iSpread(_Symbol,_Period,intBarNo);
   for(int i=0; i<ArraySize(orders); i++)
     {
      //---
      STR_LB_HISTORY_CANDLE hs_cnd[];
      if(!ReadHistoryCandle(hs_cnd,orders[i].enmLimitTimeframe,dtNow,dtNext))
         continue;
      //---
      STR_LB_HISTORY_SUPERBOLLINGER hs_sb[];
      if(!ReadHistorySuperBollinger(hs_sb,orders[i].enmLimitTimeframe,dtNext,dtNext))
         continue;
      //---
      if(orders[i].enmOrderType==ORDER_TYPE_BUY)
        {
         if(orders[i].enmLimitType==L_2SIGMA)
           {
            if(hs_cnd[0].high>hs_sb[0].up_s2)
              {
               if(!DBUpdateProfit(orders[i].ulTicketNo,orders[i].enmOrderType,hs_sb[0].up_s2,dblSpread))
                  return;
               EventChartCustom(0,ON_TESTER_CLOSE,orders[i].ulTicketNo,hs_sb[0].up_s2,TimeToString(dtNext));
              }
           }
        }
      //---
      if(orders[i].enmOrderType==ORDER_TYPE_SELL)
        {
         if(orders[i].enmLimitType==L_2SIGMA)
           {
            if(hs_cnd[0].low<hs_sb[0].lw_s2)
              {
               if(!DBUpdateProfit(orders[i].ulTicketNo,orders[i].enmOrderType,hs_sb[0].lw_s2,dblSpread))
                  return;
               EventChartCustom(0,ON_TESTER_CLOSE,orders[i].ulTicketNo,hs_sb[0].lw_s2,TimeToString(dtNext));
              }
           }
        }
     }
//---
   for(int i=0; i<ArraySize(orders); i++)
     {
      //---
      STR_LB_HISTORY_CANDLE hs_cnd[];
      if(!ReadHistoryCandle(hs_cnd,orders[i].enmStopTimeframe,dtNow,dtNext))
         continue;
      //---
      STR_LB_HISTORY_SUPERBOLLINGER hs_sb[];
      if(!ReadHistorySuperBollinger(hs_sb,orders[i].enmStopTimeframe,dtNext,dtNext))
         continue;
      //---
      if(orders[i].enmOrderType==ORDER_TYPE_BUY)
        {
         if(orders[i].enmStopType==S_2SIGMA)
           {
            if(hs_sb[0].lw_s2>hs_cnd[0].close)
              {
               if(!DBUpdateProfit(orders[i].ulTicketNo,orders[i].enmOrderType,hs_cnd[0].close,dblSpread))
                  return;
               EventChartCustom(0,ON_TESTER_CLOSE,orders[i].ulTicketNo,hs_cnd[0].close,TimeToString(dtNext));
              }
           }
         if(orders[i].enmStopType==S_TRAILING)
           {
            if(hs_cnd[1].low>hs_cnd[0].close)
              {
               if(!DBUpdateProfit(orders[i].ulTicketNo,orders[i].enmOrderType,hs_cnd[1].low,dblSpread))
                  return;
               EventChartCustom(0,ON_TESTER_CLOSE,orders[i].ulTicketNo,hs_cnd[1].low,TimeToString(dtNext));
              }
           }
        }
      //---
      if(orders[i].enmOrderType==ORDER_TYPE_SELL)
        {
         if(orders[i].enmStopType==S_2SIGMA)
           {
            if(hs_sb[0].up_s2<hs_cnd[0].close)
              {
               if(!DBUpdateProfit(orders[i].ulTicketNo,orders[i].enmOrderType,hs_cnd[0].close,dblSpread))
                  return;
               EventChartCustom(0,ON_TESTER_CLOSE,orders[i].ulTicketNo,hs_cnd[0].close,TimeToString(dtNext));
              }
           }
         if(orders[i].enmStopType==S_TRAILING)
           {
            if(hs_cnd[1].high<hs_cnd[0].close)
              {
               if(!DBUpdateProfit(orders[i].ulTicketNo,orders[i].enmOrderType,hs_cnd[1].high,dblSpread))
                  return;
               EventChartCustom(0,ON_TESTER_CLOSE,orders[i].ulTicketNo,hs_cnd[1].high,TimeToString(dtNext));
              }
           }
        }
     }
//---
   ResetItems();
//---
   SendEvent(ON_TESTER_NEXT,dtNext);
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickEdtLots(void)
  {
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickLstOrders(void)
  {
//---
   STR_LB_ORDERS data[];
   if(!ReadOrders(data,T_DB_NAME,(ulong)m_lists[LST_POS].Select()))
      return;
   if(ArraySize(data)!=1)
     {
      Print("data select error");
      return;
     }
//---
   m_cmbOrders[CMB_OTP].SelectByText(EnumToString(data[0].enmOpenType));
   m_cmbOrders[CMB_LTP].SelectByText(EnumToString(data[0].enmLimitType));
   m_cmbOrders[CMB_LTF].SelectByText(EnumToString(data[0].enmLimitTimeframe));
   m_cmbOrders[CMB_ORTP].SelectByText(EnumToString(data[0].enmOrderType));
   m_cmbOrders[CMB_STP].SelectByText(EnumToString(data[0].enmStopType));
   m_cmbOrders[CMB_STF].SelectByText(EnumToString(data[0].enmStopTimeframe));
//---
   m_edits[EDT_DT].Text(TimeToString(StringToTime(data[0].strOpDateTime)));
   m_edits[EDT_LOTS_DTL].Text((string)data[0].dblLots);
   m_edits[EDT_TN].Text((string)data[0].ulTicketNo);
   m_edits[EDT_PL_DTL].Text((string)data[0].intProfit);
   m_edits[EDT_PL_DTL].Color(data[0].intProfit<0?clrRed:clrBlack);
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnBuy(void)
  {
//---
   datetime dtNow=GetCurrentDateTime();
   int intBarNo=iBarShift(_Symbol,_Period,dtNow);
   double dblClose=iClose(_Symbol,_Period,intBarNo);
//---
   STR_LB_ORDERS order;
   order.strOpDateTime=(string)dtNow;
   order.enmOrderType=ORDER_TYPE_BUY;
   order.dblLots=(double)m_edits[EDT_LOTS].Text();
   order.dblOpPrice=dblClose;
   order.enmLimitType=(ENM_LIMIT)CnvNameToCode(m_cmbOrders[CMB_LTP].Select(),LIMIT,T_DB_NAME);
   order.enmLimitTimeframe=(ENUM_TIMEFRAMES)CnvNameToCode(m_cmbOrders[CMB_LTF].Select(),PERIOD,T_DB_NAME);
   order.enmStopType=(ENM_STOP)CnvNameToCode(m_cmbOrders[CMB_STP].Select(),STOP,T_DB_NAME);
   order.enmStopTimeframe=(ENUM_TIMEFRAMES)CnvNameToCode(m_cmbOrders[CMB_STF].Select(),PERIOD,T_DB_NAME);
   if(!DBInsertTesterOrder(order))
      return;
//---
   EventChartCustom(0,ON_TESTER_BUY,0,dblClose,TimeToString(dtNow));
   ChartRedraw(0);
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnSell(void)
  {
//---
   datetime dtNow=GetCurrentDateTime();
   int intBarNo=iBarShift(_Symbol,_Period,dtNow);
   double dblClose=iClose(_Symbol,_Period,intBarNo);
//---
   STR_LB_ORDERS order;
   order.strOpDateTime=(string)dtNow;
   order.enmOrderType=ORDER_TYPE_SELL;
   order.dblLots=(double)m_edits[EDT_LOTS].Text();
   order.dblOpPrice=dblClose;
   order.enmLimitType=(ENM_LIMIT)CnvNameToCode(m_cmbOrders[CMB_LTP].Select(),LIMIT,T_DB_NAME);
   order.enmLimitTimeframe=(ENUM_TIMEFRAMES)CnvNameToCode(m_cmbOrders[CMB_LTF].Select(),PERIOD,T_DB_NAME);
   order.enmStopType=(ENM_STOP)CnvNameToCode(m_cmbOrders[CMB_STP].Select(),STOP,T_DB_NAME);
   order.enmStopTimeframe=(ENUM_TIMEFRAMES)CnvNameToCode(m_cmbOrders[CMB_STF].Select(),PERIOD,T_DB_NAME);
   if(!DBInsertTesterOrder(order))
      return;
//---
   EventChartCustom(0,ON_TESTER_SELL,0,dblClose,TimeToString(dtNow));
   ChartRedraw(0);
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnClose(void)
  {
//---
   datetime dtNow=GetCurrentDateTime();
   int intBarNo=iBarShift(_Symbol,_Period,dtNow);
   double dblClose=iClose(_Symbol,_Period,intBarNo);
//---
   ulong ticket=(ulong)m_lists[LST_POS].Select();
   STR_LB_ORDERS orders[];
   if(!ReadOrders(orders,T_DB_NAME,ticket))
      return;
//---
   for(int i=0; i<ArraySize(orders); i++)
      EventChartCustom(0,ON_TESTER_CLOSE,orders[i].ulTicketNo,dblClose,TimeToString(dtNow));
   ChartRedraw(0);
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnModify(void)
  {
//---
   ulong ticket=(ulong)m_lists[LST_POS].Select();
   if(ticket==0)
      return;
//---
   STR_LB_ORDERS order;
   order.ulTicketNo=ticket;
   order.enmLimitType=(ENM_LIMIT)CnvNameToCode(m_cmbOrders[CMB_LTP].Select(),LIMIT,T_DB_NAME);
   order.enmLimitTimeframe=(ENUM_TIMEFRAMES)CnvNameToCode(m_cmbOrders[CMB_LTF].Select(),PERIOD,T_DB_NAME);
   order.enmStopType=(ENM_STOP)CnvNameToCode(m_cmbOrders[CMB_STP].Select(),STOP,T_DB_NAME);
   order.enmStopTimeframe=(ENUM_TIMEFRAMES)CnvNameToCode(m_cmbOrders[CMB_STF].Select(),PERIOD,T_DB_NAME);
   if(!DBUpdateOrder(order,T_DB_NAME))
      return;
//---
   ResetAll();
   PlaySound("ok.wav");
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
datetime CPanelDialog::GetCurrentDateTime(void)
  {
   string strYear=m_cmbYear.Select();
   string strMonth=m_cmbMonth.Select();
   string strDay=m_cmbDay.Select();
   string strHour=m_cmbHour.Select();
   string strMin=m_cmbMin.Select();
   return StringToTime(strYear+"."+strMonth+"."+strDay+" "+strHour+":"+strMin);
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnCheckLots(void)
  {
   double val=(double)m_edits[EDT_LOTS].Text();
   if(val<0.1)
      val=0.1;
   m_edits[EDT_LOTS].Text(DoubleToString(val,1));
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::ResetItems(void)
  {
//---
   string sql=NULL;
   datetime dtNow=GetCurrentDateTime();
   int intBarNo=iBarShift(_Symbol,_Period,dtNow);
   double dblClose=iClose(_Symbol,_Period,intBarNo);
   double dblSpread=iSpread(_Symbol,_Period,intBarNo);
   STR_LB_ORDERS orders[];
//---
   if(!ReadOrders(orders,T_DB_NAME))
      return;
   for(int i=0; i<ArraySize(orders); i++)
     {
      if(!DBUpdateProfit(orders[i].ulTicketNo,orders[i].enmOrderType,dblClose,dblSpread))
         return;
     }
//---
   sql="SELECT SUM(PROFIT) FROM ORDERS";
   int intProfit=(int)ReadResult(sql);
   m_edits[EDT_PL].Text((string)intProfit);
   m_edits[EDT_PL].Color(intProfit<0?clrRed:clrBlack);
//---
   sql="SELECT";
   sql=sql+" "+"ROUND(";
   sql=sql+" "+"CAST (";
   sql=sql+" "+"SUM";
   sql=sql+"("+"CASE WHEN PROFIT>=0 THEN PROFIT ELSE 0 END"+")";
   sql=sql+" "+"AS REAL)";
   sql=sql+"/";
   sql=sql+" "+"CAST (";
   sql=sql+" "+"ABS";
   sql=sql+"("+"SUM";
   sql=sql+"("+"CASE WHEN PROFIT<0 THEN PROFIT ELSE 0 END"+")";
   sql=sql+")";
   sql=sql+" "+"AS REAL)";
   sql=sql+",1)";
   sql=sql+" "+"FROM RESULTS";
   double dblProfitFactor=(double)ReadResult(sql);
   m_edits[EDT_PF].Text(DoubleToString(dblProfitFactor,1));
//---
   sql="SELECT SUM(PROFIT) FROM RESULTS";
   int intProfitTotal=(int)ReadResult(sql);
   m_edits[EDT_PT].Text((string)intProfitTotal);
   m_edits[EDT_PT].Color(intProfitTotal<0?clrRed:clrBlack);
//---
   sql="SELECT";
   sql=sql+" "+"ROUND(";
   sql=sql+"(";
   sql=sql+" "+"CAST (";
   sql=sql+"("+"SELECT SUM(CASE WHEN PROFIT>=0 THEN 1 ELSE 0 END) FROM RESULTS"+")";
   sql=sql+" "+"AS REAL)";
   sql=sql+"/";
   sql=sql+" "+"CAST (";
   sql=sql+"("+"SELECT COUNT(*) FROM RESULTS"+")";
   sql=sql+" "+"AS REAL)";
   sql=sql+"*"+"100";
   sql=sql+")"+",1)";
   double dblWinRates=(double)ReadResult(sql);
   m_edits[EDT_WR].Text(DoubleToString(dblWinRates,1));
//---
   if(StringLen(m_lists[LST_POS].Select())==0)
      return;
//---
   if(!ReadOrders(orders,T_DB_NAME,(ulong)m_lists[LST_POS].Select()))
      return;
   if(ArraySize(orders)!=1)
      return;
   m_edits[EDT_PL_DTL].Text((string)orders[0].intProfit);
   m_edits[EDT_PL_DTL].Color(orders[0].intProfit<0?clrRed:clrBlack);
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::ClearItems(void)
  {
//---
   if(!m_lists[LST_POS].ItemsClear())
      return;
//---
   STR_LB_ORDERS orders[];
   if(!ReadOrders(orders,T_DB_NAME))
      return;
   for(int i=0; i<ArraySize(orders); i++)
      if(!m_lists[LST_POS].ItemAdd((string)orders[i].ulTicketNo))
         return;
//---
   m_cmbOrders[CMB_OTP].Select(0);
   m_cmbOrders[CMB_LTP].Select(0);
   m_cmbOrders[CMB_LTF].SelectByText(EnumToString(_Period));
   m_cmbOrders[CMB_ORTP].Select(0);
   m_cmbOrders[CMB_STP].Select(0);
   m_cmbOrders[CMB_STF].SelectByText(EnumToString(_Period));
//---
   m_edits[EDT_DT].Text(NULL);
   m_edits[EDT_LOTS_DTL].Text(NULL);
   m_edits[EDT_TN].Text(NULL);
   m_edits[EDT_PL_DTL].Text(NULL);
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::ResetAll(void)
  {
//---
   ResetItems();
//---
   ClearItems();
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void SendEvent(ushort CustomId,datetime dt)
  {
   long currChart,prevChart=ChartFirst();
   int i=0,limit=100;
   while(i<limit)
     {
      EventChartCustom(prevChart,CustomId,_Period,0,TimeToString(dt));
      currChart=ChartNext(prevChart);
      if(currChart<0)
         break;
      prevChart=currChart;
      i++;
     }
  }
//+------------------------------------------------------------------+
