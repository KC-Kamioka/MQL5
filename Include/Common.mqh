//+------------------------------------------------------------------+
//|                                                       Common.mqh |
//|                        Copyright 2021, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
#define SM_TENKAN   9
#define SM_KIJUN   26
#define SM_SENKO_B   52
#define SB_SHIFT   21
#define SM_SHIFT   26
#define SP_MAGIC_NO   9803426
#define ON_TESTER_START         (200) // "change time" event
#define ON_TESTER_PREV          (210) // "change time" event
#define ON_TESTER_NEXT          (220) // "change time" event
#define ON_TESTER_BUY           (230) // "buy" event
#define ON_TESTER_SELL          (240) // "sell" event
#define ON_TESTER_CLOSE         (250) // "close" event
#define ON_TESTER_RESET_ALL     (260)

//+------------------------------------------------------------------+
//| Include files                                                    |
//+------------------------------------------------------------------+
#include <JAson.mqh>
//+------------------------------------------------------------------+
//| Global variables                                                 |
//+------------------------------------------------------------------+
int GRACE_PERIOD=5;
string MySymbols[6]=
  {
   "USDJPY"
   ,"EURUSD"
   ,"GBPUSD"
   ,"AUDUSD"
   ,"JP225"
   ,"US30"
  };
//+------------------------------------------------------------------+
//| Enum                                                             |
//+------------------------------------------------------------------+
enum ENUM_SS_INDEX
  {
   SS_BLUE_SPAN
   ,SS_RED_SPAN
   ,SS_CHIKO_SPAN
   ,SS_MIDDLE
   ,SS_UP_1_SIGMA
   ,SS_LW_1_SIGMA
   ,SS_UP_2_SIGMA
   ,SS_LW_2_SIGMA
  };
enum ENUM_CC_INDEX
  {
   CNDL_CL
   ,CNDL_CLR
   ,EVENT
   ,BB_M
   ,BB_U_S1
   ,BB_L_S1
   ,BB_U_S2
   ,BB_L_S2
   ,SMA_200
   ,SMA_100
   ,SMA_75
  };
enum ENUM_LB_INDEX
  {
   LB_SMA_100
   ,LB_SMA_200
  };
enum ENUM_SM_INDEX
  {
   BLUE_SPAN
   ,RED_SPAN
   ,BLUE_SPAN_LINE
   ,RED_SPAN_LINE
  };
enum ENUM_REASON
  {
   TP_MODIFY
   ,STOP_CONDITION
   ,NOT_BREAK
   ,MIDDLE_ABOVE
   ,MIDDLE_BELOW
   ,MACD_SIG_MINUS
   ,MACD_SIG_PLUS
   ,MACD_AGAINST
   ,SMA_AGAINST
   ,RSI_EXCESS
   ,RSI_NOT_ENOUGH
   ,ATR_WEAK
   ,NOT_EXPAND
  };
long lgMagicNoBase[] =
  {
   100011
   ,100012
   ,100031
   ,100032
   ,100041
   ,100042
   ,100051
  };
enum ENUM_SAS_INDEX
  {
   SM_SIGNAL
   ,SM_TERM
   ,SM_UP_GRACE_ST
   ,SM_UP_GRACE_ED
   ,SM_DW_GRACE_ST
   ,SM_DW_GRACE_ED
   ,SM_GRACE_HIGH
   ,SM_GRACE_LOW
   ,SM_SIGNAL_ED
   ,RS_SIGNAL
   ,RS_TERM
   ,RS_UP_GRACE_ST
   ,RS_UP_GRACE_ED
   ,RS_DW_GRACE_ST
   ,RS_DW_GRACE_ED
   ,RS_GRACE_HIGH
   ,RS_GRACE_LOW
   ,RS_SIGNAL_ED
   ,HIGH_52
   ,LOW_52
   ,Y_HIGH
   ,Y_LOW
  };
enum ENUM_SB_INDEX
  {
   MIDDLE
   ,UP_1_SIGMA
   ,LW_1_SIGMA
   ,UP_2_SIGMA
   ,LW_2_SIGMA
   ,UP_3_SIGMA
   ,LW_3_SIGMA
   /*
   ,CHIKOUSPAN_21
   ,OV_2_SIGMA
   ,CS_DIR
   ,CS_AT_UP
   ,CS_AT_DW
   */
  };
enum ENUM_SB_INDEX_SB
  {
   SM_MACD
   ,STATUS_CLR
   ,MACD_SIGNAL
   ,STATUS
  };
enum ENM_TRADE_STATUS
  {
   PEND,
   OPENED,
   CLOSED,
   CANCELED
  };
enum ENM_SPAN_STATUS
  {
   NONE,
   UP,
   DOWN
  };
enum ENUM_SYMBOLS
  {
   USDJPY
   ,EURUSD
   ,GBPUSD
   ,AUDUSD
   ,JP225
   ,US30
  };
enum ENM_LOG_TYPE
  {
   INFO
   ,WARN
   ,ERROR
  };
enum ENM_SB_STATUS
  {
    UNKNOWN           // 0
   ,RANGE           // 0
   ,TREND_UP       // 2
   ,TREND_DOWN     // 9
   ,DOWN_TURN       // 2
   ,REBOUND     // 9
  };
//+------------------------------------------------------------------+
//| struct                                                           |
//+------------------------------------------------------------------+
struct STR_EVENT
  {
   ulong             id;
   string            currency;
   string            time;
   string            name;
  };
struct STR_CONDITIONS
  {
   string            optype;
   string            lmtype;
   string            sttype;
  };
struct STR_POSITION_INFO
  {
   double            dblProfit;
   int               intTicketNo;
   string            strLmMethod;
   string            strSlMethod;
  };
struct STR_LIST_CONTENT
  {
   string            strText;
   long              lgValue;
  };
struct STR_SPAN_SIGNAL
  {
   int               intSignal;
   datetime          dtGraceSt;
   datetime          dtGraceEd;
   double            dblGraceHigh;
   double            dblGraceLow;
   datetime          dtSignalSt;
   datetime          dtSignalEd;
  };
struct STR_IND_DATA
  {
   int               intSeq;
   datetime          dtTime;
   double            dblHigh;
   double            dblLow;
   double            dblOpen;
   double            dblClose;
   double            dblMiddle;
   double            dblUpperSigma1;
   double            dblLowerSigma1;
   double            dblUpperSigma2;
   double            dblLowerSigma2;
   double            dblUpperSigma3;
   double            dblLowerSigma3;
   double            dblBlueSpan;
   double            dblRedSpan;
   STR_SPAN_SIGNAL   sp;
   STR_SPAN_SIGNAL   rd;
  };
struct STR_PREDICT
  {
   string            strDatetime;
   int               intTrend;
  };
//+------------------------------------------------------------------+
//| ボリンジャーバンド
//+------------------------------------------------------------------+
bool CreateBBHandle(int &handle,double dblDeviation,ENUM_TIMEFRAMES in_enmTimeFrames=PERIOD_CURRENT)
  {
//--- 指標ハンドルを作成する
   handle=iBands(NULL,in_enmTimeFrames,21,0,dblDeviation,PRICE_CLOSE);
   if(handle==INVALID_HANDLE)
     {
      //--- 失敗した事実とエラーコードを出力する
      PrintFormat("Failed to create handle of the iBands indicator for the symbol %s/%s, error code %d",
                  _Symbol,
                  EnumToString(in_enmTimeFrames),
                  GetLastError());
      //--- 指標が早期に中止された
      return(false);
     }
   return(true);
  }
//+------------------------------------------------------------------+
//| Array fill
//+------------------------------------------------------------------+
void FillBuffer(double &io_buf[],int start,int amount,double val)
  {
   for(int i=start; i<start+amount+1; i++)
      io_buf[i]=val;
  }
//+------------------------------------------------------------------+
//| カスタムインジケータハンドル作成
//+------------------------------------------------------------------+
bool CreateCustomHandle(int &handle,string in_strSymbol,ENUM_TIMEFRAMES in_tf,string in_strIndicatorName)
  {
//--- 指標ハンドルを作成する
   handle=iCustom(in_strSymbol,in_tf,in_strIndicatorName);
   if(handle==INVALID_HANDLE)
     {
      //--- 失敗した事実とエラーコードを出力する
      PrintFormat("Failed to create handle of "+in_strIndicatorName+" for the symbol %s/%s, error code %d",
                  in_strSymbol,
                  EnumToString(in_tf),
                  GetLastError());
      //--- 指標が早期に中止された
      return(false);
     }
   return(true);
  }
//+------------------------------------------------------------------+
//| 配列の値を取得
//+------------------------------------------------------------------+
datetime GetDateTime(datetime &in_dtBuf[],int in_idx)
  {
   datetime out_datetime=NULL;
   ResetLastError();
   out_datetime=in_dtBuf[in_idx];
   if(_LastError!=ERR_SUCCESS)
      PrintFormat("Failed to get datetime value, error code %d",GetLastError());
   return out_datetime;
  }
//+------------------------------------------------------------------+
//| スパンモデル
//+------------------------------------------------------------------+
bool CreateSMHandle(int &handle)
  {
//--- 指標ハンドルを作成する
   handle=iCustom(NULL,PERIOD_CURRENT,"SpanModel");
   if(handle==INVALID_HANDLE)
     {
      //--- 失敗した事実とエラーコードを出力する
      PrintFormat("Failed to create handle of the SpanModel indicator for the symbol %s/%s, error code %d",
                  _Symbol,
                  EnumToString(PERIOD_CURRENT),
                  GetLastError());
      //--- 指標が早期に中止された
      return(false);
     }
   return(true);
  }
//+------------------------------------------------------------------+
//| 一目均衡表
//+------------------------------------------------------------------+
bool CreateICHandle(int &handle)
  {
//--- 指標ハンドルを作成する
   handle=iIchimoku(NULL,PERIOD_CURRENT,SM_TENKAN,SM_KIJUN,SM_SENKO_B);
   if(handle==INVALID_HANDLE)
     {
      //--- 失敗した事実とエラーコードを出力する
      PrintFormat("Failed to create handle of the iIchimoku indicator for the symbol %s/%s, error code %d",
                  _Symbol,
                  EnumToString(PERIOD_CURRENT),
                  GetLastError());
      //--- 指標が早期に中止された
      return(false);
     }
   return(true);
  }
//+------------------------------------------------------------------+
//| RSI
//+------------------------------------------------------------------+
bool CreateRSIHandle(int &handle)
  {
//--- 指標ハンドルを作成する
   handle=iRSI(_Symbol,_Period,14,handle);
   if(handle==INVALID_HANDLE)
     {
      //--- 失敗した事実とエラーコードを出力する
      PrintFormat("Failed to create handle of the RSI indicator for the symbol %s/%s, error code %d",
                  _Symbol,
                  EnumToString(_Period),
                  GetLastError());
      //--- 指標が早期に中止された
      return(false);
     }
   return(true);
  }
//+------------------------------------------------------------------+
//| RSI
//+------------------------------------------------------------------+
bool CreateADXHandle(int &handle,ENUM_TIMEFRAMES in_tf)
  {
//--- 指標ハンドルを作成する
   handle=iADX(_Symbol,in_tf,SB_SHIFT);
   if(handle==INVALID_HANDLE)
     {
      //--- 失敗した事実とエラーコードを出力する
      PrintFormat("Failed to create handle of the ADX indicator for the symbol %s/%s, error code %d",
                  _Symbol,
                  EnumToString(PERIOD_CURRENT),
                  GetLastError());
      //--- 指標が早期に中止された
      return(false);
     }
   return(true);
  }
//+------------------------------------------------------------------+
//| 移動平均線
//+------------------------------------------------------------------+
bool CreateSMAHandle(int &handle,ENUM_TIMEFRAMES in_tf,int term)
  {
//--- 指標ハンドルを作成する
   handle=iMA(_Symbol,in_tf,term,0,MODE_SMA,handle);
   if(handle==INVALID_HANDLE)
     {
      //--- 失敗した事実とエラーコードを出力する
      PrintFormat("Failed to create handle of the iRSI indicator for the symbol %s/%s, error code %d",
                  _Symbol,
                  EnumToString(PERIOD_CURRENT),
                  GetLastError());
      //--- 指標が早期に中止された
      return(false);
     }
   return(true);
  }
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool CreateMACDHandle(int &handle)
  {
//--- 指標ハンドルを作成する
   handle=iMACD(_Symbol,_Period,12,SM_KIJUN,SM_TENKAN,handle);
   if(handle==INVALID_HANDLE)
     {
      //--- 失敗した事実とエラーコードを出力する
      PrintFormat("Failed to create handle of the MACD indicator for the symbol %s/%s, error code %d",
                  _Symbol,
                  EnumToString(_Period),
                  GetLastError());
      //--- 指標が早期に中止された
      return(false);
     }
   return(true);
  }
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool CreateStochasticHandle(int &handle)
  {
//--- 指標ハンドルを作成する
   handle=iStochastic(_Symbol,_Period,5,3,3,MODE_SMA,STO_LOWHIGH);
   if(handle==INVALID_HANDLE)
     {
      //--- 失敗した事実とエラーコードを出力する
      PrintFormat("Failed to create handle of the Stochastic indicator for the symbol %s/%s, error code %d",
                  _Symbol,
                  EnumToString(_Period),
                  GetLastError());
      //--- 指標が早期に中止された
      return(false);
     }
   return(true);
  }
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool CreateATRHandle(int &handle,ENUM_TIMEFRAMES in_tf)
  {
//--- 指標ハンドルを作成する
   handle=iATR(_Symbol,in_tf,14);
   if(handle==INVALID_HANDLE)
     {
      //--- 失敗した事実とエラーコードを出力する
      PrintFormat("Failed to create handle of the MACD indicator for the symbol %s/%s, error code %d",
                  _Symbol,
                  EnumToString(PERIOD_CURRENT),
                  GetLastError());
      //--- 指標が早期に中止された
      return(false);
     }
   return(true);
  }
//+-------------------------------------------------------------------+
//| 右側の境界線からの価格チャートのシフトが有効かをチェック                           |
//+-------------------------------------------------------------------+
bool ChartShiftGet(bool &result,const long chart_ID=0)
  {
//--- プロパティ値を取得する変数を準備する
   long value;
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を受け取る
   if(!ChartGetInteger(chart_ID,CHART_SHIFT,0,value))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- チャートプロパティの値をメモリ内に格納する
   result=value;
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 期間中の最安値・最高値取得
//+------------------------------------------------------------------+
bool GetHighLow(datetime dtFrom,datetime dtTo,double &io_high,double &io_low)
  {
   double dblBuff[];
//--- 高値
   ArrayFree(dblBuff);
   if(CopyHigh(NULL,PERIOD_CURRENT,dtFrom,dtTo,dblBuff)<0)
      return false;
   io_high=dblBuff[ArrayMaximum(dblBuff)];
//--- 安値
   ArrayFree(dblBuff);
   if(CopyLow(NULL,PERIOD_CURRENT,dtFrom,dtTo,dblBuff)<0)
      return false;
   io_low=dblBuff[ArrayMinimum(dblBuff)];
   return true;
  }
//+------------------------------------------------------------------+
//| チャート幅をピクセル数で取得する                                          |
//+------------------------------------------------------------------+
int ChartWidthInPixels(const long chart_ID=0)
  {
//--- プロパティ値を取得する変数を準備する
   long result=-1;
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を受け取る
   if(!ChartGetInteger(chart_ID,CHART_WIDTH_IN_PIXELS,0,result))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
     }
//--- チャートプロパティの値を返す
   return((int)result);
  }
//+------------------------------------------------------------------+
//| チャートの高さをピクセル数で取得する                                        |
//+------------------------------------------------------------------+
int ChartHeightInPixelsGet(const long chart_ID=0,const int sub_window=0)
  {
//--- プロパティ値を取得する変数を準備する
   long result=-1;
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を受け取る
   if(!ChartGetInteger(chart_ID,CHART_HEIGHT_IN_PIXELS,sub_window,result))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
     }
//--- チャートプロパティの値を返す
   return((int)result);
  }
//+------------------------------------------------------------------+
//| 四角形ラベルを作成する                                                 |
//+------------------------------------------------------------------+
bool RectLabelCreate(const long            chart_ID=0,               // チャート識別子
                     const string           name="RectLabel",         // ラベル名
                     const int              sub_window=0,             // サブウィンドウ番号
                     const int              x=0,                     // X 座標
                     const int              y=0,                     // Y 座標
                     const int              width=50,                 // 幅
                     const int              height=18,               // 高さ
                     const color            back_clr=clrWhite, // 背景色
                     const ENUM_BORDER_TYPE border=BORDER_FLAT,     // 境界線の種類
                     const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER, // アンカーに使用されるチャートのコーナー
                     const color            clr=clrNONE,               // フラット境界線の色 (Flat)
                     const ENUM_LINE_STYLE  style=STYLE_SOLID,       // フラット境界スタイル
                     const int              line_width=1,             //フラット境界幅
                     const bool             back=false,               // 背景で表示する
                     const bool             selection=false,         // 強調表示して移動
                     const bool             hidden=true,             // オブジェクトリストに隠す
                     const long             z_order=0)               // マウスクリックの優先順位
  {
//--- エラー値をリセットする
   ResetLastError();
//--- 四角形ラベルを作成する
   if(!ObjectCreate(chart_ID,name,OBJ_RECTANGLE_LABEL,sub_window,0,0))
     {
      Print(__FUNCTION__,
            ": failed to create a rectangle label! Error code = ",GetLastError());
      return(false);
     }
//--- ラベル座標を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
//--- ラベルサイズを設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width);
   ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height);
//--- 背景色を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_BGCOLOR,back_clr);
//--- 境界線を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_TYPE,border);
//--- ポイント座標が相対的に定義されているチャートのコーナーを設定
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner);
//--- フラット境界線色を設定する（Flat モード）
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- フラット境界線スタイルを設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style);
//--- フラット境界線幅を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,line_width);
//--- 前景（false）または背景（true）に表示
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- マウスでラベルを移動させるモードを有効（true）か無効（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- オブジェクトリストのグラフィックオブジェクトを非表示（true）か表示（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- チャートのマウスクリックのイベントを受信するための優先順位を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CheckLoadHistory()
  {
   datetime first_date=0;
   datetime times[100];
//--- データがあるかをチェックする
   SeriesInfoInteger(_Symbol,_Period,SERIES_FIRSTDATE,first_date);
   if(first_date>0 && first_date<=TimeCurrent())
     {
      //Print("Already available data in timeseries are enough");
      return true;
     }
//--- 指標の場合自身のデータの読み込みはリクエストしない
   if(MQL5InfoInteger(MQL5_PROGRAM_TYPE)==PROGRAM_INDICATOR)
     {
      Print("Indicator mustn't load its own data");
      return true;
     }
//--- 2 回目の試み
   if(SeriesInfoInteger(_Symbol,PERIOD_M1,SERIES_TERMINAL_FIRSTDATE,first_date))
     {
      //--- 時系列を構築するために読み込まれたデータ
      if(first_date>0)
        {
         //--- 時系列の構築を強制する
         CopyTime(_Symbol,_Period,first_date+PeriodSeconds(_Period),1,times);
         //--- 日にちをチェックする
         if(SeriesInfoInteger(_Symbol,_Period,SERIES_FIRSTDATE,first_date))
            if(first_date>0 && first_date<=TimeCurrent())
              {
               Print("Timeseries is built from available terminal data");
               return true;
              }
        }
     }
//--- 端末オプションでのチャートのバーの最大数
   int max_bars=TerminalInfoInteger(TERMINAL_MAXBARS);
//--- シンボル履歴情報を読み込む
   datetime first_server_date=0;
   while(!SeriesInfoInteger(_Symbol,PERIOD_M1,SERIES_SERVER_FIRSTDATE,first_server_date) && !IsStopped())
     {
      Print("Loading...");
      Sleep(5);
     }
//--- ステップごとにデータを読み込む
   int fail_cnt=0;
   while(!IsStopped())
     {
      //--- 時系列の構築を待つ
      while(!SeriesInfoInteger(_Symbol,_Period,SERIES_SYNCHRONIZED) && !IsStopped())
        {
         Print("Loading...");
         Sleep(5);
        }
      //--- 構築されたバーをリクエストする
      int bars=Bars(_Symbol,_Period);
      if(bars>0)
        {
         if(bars>=max_bars)
           {
            Print("More requested bars than can be drawn in the chart");
            return true;
           }
         //--- 初日をリクエストする
         if(SeriesInfoInteger(_Symbol,_Period,SERIES_FIRSTDATE,first_date))
            if(first_date>0 && first_date<=TimeCurrent())
              {
               Print("All data loaded");
               return true;
              }
        }
      //--- 次の部分のコピーはデータ読み込みを強制する
      int copied=CopyTime(_Symbol,_Period,bars,100,times);
      if(copied>0)
        {
         //--- データをチェックする
         if(times[0]<=TimeCurrent())
           {
            Print("All data loaded");
            return true;
           }
         if(bars+copied>=max_bars)
           {
            Print("More requested bars than can be drawn in the chart");
            return true;
           }
         fail_cnt=0;
        }
      else
        {
         //--- 100 以下の失敗の試み
         fail_cnt++;
         if(fail_cnt>=100)
           {
            Print("Loading failed");
            return false;
           }
         Sleep(10);
        }
     }
//--- 停止した
   Print("Execution stopped by user");
   return false;
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromBuffers(double &buf[],int idx,int ind_handle,int start,int amount)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyBuffer(ind_handle,idx,start,amount,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from the indicator, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| チャートの右側の境界線への自動シフトが                                     |
//| 新しいティックの到着時に有効かをチェック                                      |
//+------------------------------------------------------------------+
bool ChartAutoscrollGet(bool &result,const long chart_ID=0)
  {
//--- プロパティ値を取得する変数を準備する
   long value;
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を受け取る
   if(!ChartGetInteger(chart_ID,CHART_AUTOSCROLL,0,value))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- チャートプロパティの値をメモリ内に格納する
   result=value;
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 新しいティックの到着時のチャートの右側の境界線への自動シフトを                    |
//| 有効または無効にする                                                  |
//+------------------------------------------------------------------+
bool ChartAutoscrollSet(const bool value,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を設定する
   if(!ChartSetInteger(chart_ID,CHART_AUTOSCROLL,0,value))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromRates(MqlRates &buf[],int start,int amount)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyRates(_Symbol,_Period,start,amount,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from rates, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool CheckBufferSize(string bufname,int size,int idx)
  {
   if(idx<0)
     {
      Print(bufname," ","index is unavailable.(",idx,")");
      return false;
     }
   if(size<=idx)
     {
      Print(bufname," ","overflow."," ","Buffer size = ",size," ","index = ",idx);
      return false;
     }
   return true;
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromRatesByTime(MqlRates &buf[],ENUM_TIMEFRAMES in_tf,datetime start,datetime end)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyRates(_Symbol,in_tf,start,end,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from rates, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| チャートの固定スケールのモードが有効かをチェック                                |
//+------------------------------------------------------------------+
bool ChartScaleFixGet(bool &result,const long chart_ID=0)
  {
//--- プロパティ値を取得する変数を準備する
   long value;
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を受け取る
   if(!ChartGetInteger(chart_ID,CHART_SCALEFIX,0,value))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- チャートプロパティの値をメモリ内に格納する
   result=value;
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| テキストオブジェクトを作成する                                             |
//+------------------------------------------------------------------+
bool TextCreate(
   long chart_ID=0,
   const string            name="Text",             // オブジェクト名
   datetime                time=0,                   // アンカーポイントの時刻
   double                  price=0,                 // アンカーポイントの価格
   const string            text="Text",             // テキスト
   const int               font_size=8,             // フォントサイズ
   const color             clr=clrWhite,               // 色
   const int               sub_window=0,
   const double            angle=0.0,               // テキストの傾斜
   const ENUM_ANCHOR_POINT anchor=ANCHOR_LEFT_LOWER, // アンカーの種類
   const bool              back=false,               // 背景で表示する
   const bool              selection=false,         // 強調表示して移動
   const bool              hidden=true,             // ブジェクトリストに隠れている
   const long              z_order=0)               // マウスクリックの優先順位
  {
   string font="Arial120";
//--- エラー値をリセットする
   ResetLastError();
//--- オブジェクト移動
   if(ObjectFind(0,name)>=0)
      DeleteObject(name);
//--- テキストオブジェクトを作成する
   if(!ObjectCreate(chart_ID,name,OBJ_TEXT,sub_window,time,price))
     {
      Print(__FUNCTION__,
            ": failed to create \"Text\" object! Error code = ",GetLastError());
      return(false);
     }
//--- テキストを設定する
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text);
//--- テキストフォントを設定する
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font);
//--- フォントサイズを設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size);
//--- テキストの傾斜を設定する
   ObjectSetDouble(chart_ID,name,OBJPROP_ANGLE,angle);
//--- アンカーの種類を設定
   ObjectSetInteger(chart_ID,name,OBJPROP_ANCHOR,anchor);
//--- 色を設定
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- 前景（false）または背景（true）に表示
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- マウスでオブジェクトを移動させるモードを有効（true）か無効（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- オブジェクトリストのグラフィックオブジェクトを非表示（true）か表示（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- チャートのマウスクリックのイベントを受信するための優先順位を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromOpen(double &buf[],int start,int amount)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyOpen(_Symbol,_Period,0,amount,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from open, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromHigh(double &buf[],int start,int amount)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyHigh(_Symbol,_Period,0,amount,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from high, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromLow(double &buf[],int start,int amount)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyLow(_Symbol,_Period,0,amount,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from low, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromTime(datetime &buf[],int start,int amount)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyTime(_Symbol,_Period,start,amount,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from close, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromClose(double &buf[],int start,int amount)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyClose(_Symbol,_Period,start,amount,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from close, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| ラベルを作成する                                                      |
//+------------------------------------------------------------------+
bool LabelCreate(const long              chart_ID=0,               // チャート識別子
                 const string            name="Label",             // ラベル名
                 const int               sub_window=0,             // サブウィンドウ番号
                 const int               x=0,                     // X 座標
                 const int               y=0,                     // Y 座標
                 const ENUM_BASE_CORNER  corner=CORNER_LEFT_UPPER, // アンカーに使用されるチャートのコーナー
                 const string            text="Label",             // テキスト
                 const string            font="Arial",             // フォント
                 const int               font_size=10,             // フォントサイズ
                 const color             clr=clrRed,               // 色
                 const double            angle=0.0,               // テキストの傾斜
                 const ENUM_ANCHOR_POINT anchor=ANCHOR_LEFT_UPPER, // アンカーの種類
                 const bool              back=false,               // 背景で表示する
                 const bool              selection=false,         // 強調表示して移動
                 const bool              hidden=true,             // オブジェクトリストに隠す
                 const long              z_order=0)               // マウスクリックの優先順位
  {
//--- エラー値をリセットする
   ResetLastError();
//--- ラベルを作成する
   if(!ObjectCreate(chart_ID,name,OBJ_LABEL,sub_window,0,0))
     {
      Print(__FUNCTION__,
            ": failed to create text label! Error code = ",GetLastError());
      return(false);
     }
//--- ラベル座標を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
//--- ポイント座標が相対的に定義されているチャートのコーナーを設定
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner);
//--- テキストを設定する
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text);
//--- テキストフォントを設定する
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font);
//--- フォントサイズを設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size);
//--- テキストの傾斜を設定する
   ObjectSetDouble(chart_ID,name,OBJPROP_ANGLE,angle);
//--- アンカーの種類を設定
   ObjectSetInteger(chart_ID,name,OBJPROP_ANCHOR,anchor);
//--- 色を設定
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- 前景（false）または背景（true）に表示
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- マウスでラベルを移動させるモードを有効（true）か無効（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- オブジェクトリストのグラフィックオブジェクトを非表示（true）か表示（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- チャートのマウスクリックのイベントを受信するための優先順位を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| ラベルテキストを変更する                                                 |
//+------------------------------------------------------------------+
bool LabelTextChange(const long   chart_ID=0,   // チャート識別子
                     const string name="Label", // オブジェクト名
                     const string text="Text") // テキスト
  {
//--- エラー値をリセットする
   ResetLastError();
//--- オブジェクトのテキストを変更する
   if(!ObjectSetString(chart_ID,name,OBJPROP_TEXT,text))
     {
      Print(__FUNCTION__,
            ": failed to change the text! Error code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 与えられた座標で記号を作成する                                         |
//+------------------------------------------------------------------+
bool ArrowCreate(
   string name
   ,datetime time
   ,double price
   ,uchar arrow_code
   ,ENUM_ARROW_ANCHOR anchor
   ,color clr
   ,int width=1
   ,bool back=false
)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- オブジェクト削除
   ObjectDelete(0,name);
//--- 矢印を作成
   if(!ObjectCreate(0,name,OBJ_ARROW,0,time,price))
     {
      Print(__FUNCTION__,
            ": failed to create an arrow! Error code = ",GetLastError());
      return(false);
     }
//--- 矢印コードを設定
   ObjectSetInteger(0,name,OBJPROP_ARROWCODE,arrow_code);
//--- アンカーの種類を設定
   ObjectSetInteger(0,name,OBJPROP_ANCHOR,anchor);
//--- 矢印の色を設定
   ObjectSetInteger(0,name,OBJPROP_COLOR,clr);
//--- 境界線のスタイルを設定
   ObjectSetInteger(0,name,OBJPROP_STYLE,STYLE_SOLID);
//--- 矢印のサイズを設定
   ObjectSetInteger(0,name,OBJPROP_WIDTH,width);
//--- 前景（false）または背景（true）に表示
   ObjectSetInteger(0,name,OBJPROP_BACK,back);
//--- 実行成功
   return(true);
  }
//+----------------------------------------------------------------------+
//| メインウィンドウかサブウィンドウでチャートの最大値を取得する                             |
//+----------------------------------------------------------------------+
double ChartPrice(ENUM_CHART_PROPERTY_DOUBLE in_enmChartProperty)
  {
//--- 結果取得のために変数を準備する
   double result=EMPTY_VALUE;
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を受け取る
   if(!ChartGetDouble(0,in_enmChartProperty,0,result))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
     }
//--- チャートプロパティの値を返す
   return(result);
  }
//+----------------------------------------------------------------------+
//| チャートウィンドウに表示されている（可視）バーの数を取得する                            |
//+----------------------------------------------------------------------+
int ChartVisibleBars(const long chart_ID=0)
  {
//--- プロパティ値を取得する変数を準備する
   long result=-1;
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を受け取る
   if(!ChartGetInteger(chart_ID,CHART_VISIBLE_BARS,0,result))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
     }
//--- チャートプロパティの値を返す
   return((int)result);
  }
//+------------------------------------------------------------------------------+
//| チャート上に最初に表示されるバーの番号を取得する                                          |
//| バーの索引付けは時系列に従い、最新のバーは、最小のインデックスを持っている                       |
//+------------------------------------------------------------------------------+
int ChartFirstVisibleBar(const long chart_ID=0)
  {
//--- プロパティ値を取得する変数を準備する
   long result=-1;
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を受け取る
   if(!ChartGetInteger(chart_ID,CHART_FIRST_VISIBLE_BAR,0,result))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
     }
//--- チャートプロパティの値を返す
   return((int)result);
  }
//+------------------------------------------------------------------+
//| 配列検索
//+------------------------------------------------------------------+
int ArrayIndexOf(ulong &array[],ulong value)
  {
   int intRtn=-1;
   for(int i=0; i<ArraySize(array); i++)
     {
      if(array[i]==value)
        {
         intRtn=i;
         break;
        }
     }
   return intRtn;
  }
//+------------------------------------------------------------------+
//| 買いサインを作成する                                                  |
//+------------------------------------------------------------------+
bool ArrowBuyCreate(const long            chart_ID=0,       // チャート識別子
                    const string          name="ArrowBuy",   // 記号名
                    const int             sub_window=0,     // サブウィンドウ番号
                    datetime              time=0,           // アンカーポイントの時刻
                    double                price=0,           // アンカーポイントの価格
                    const color           clr=C'3,95,172',   // 記号の色
                    const ENUM_LINE_STYLE style=STYLE_SOLID, // 強調表示時の線のスタイル
                    const int             width=1,           // 強調表示時の線のサイズ
                    const bool            back=false,       // 背景で表示する
                    const bool            selection=false,   // 強調表示して移動
                    const bool            hidden=true,       // オブジェクトリストに隠す
                    const long            z_order=0)         // マウスクリックの優先順位
  {
//--- エラー値をリセットする
   ResetLastError();
//--- 記号を作成する
   if(!ObjectCreate(chart_ID,name,OBJ_ARROW_BUY,sub_window,time,price))
     {
      Print(__FUNCTION__,
            ": failed to create \"Buy\" sign! Error code = ",GetLastError());
      return(false);
     }
//--- 記号の色を設定
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- 強調表示時の線のスタイルを設定
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style);
//--- 強調表示時の線のサイズを設定
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,width);
//--- 前景（false）または背景（true）に表示
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- マウスで記号を移動させるモードを有効（true）か無効（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- オブジェクトリストのグラフィックオブジェクトを非表示（true）か表示（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- チャートのマウスクリックのイベントを受信するための優先順位を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 売りサインを作成する                                                   |
//+------------------------------------------------------------------+
bool ArrowSellCreate(const long            chart_ID=0,       // チャート識別子
                     const string          name="ArrowSell", // 記号名
                     const int             sub_window=0,     // サブウィンドウ番号
                     datetime              time=0,           // アンカーポイントの時刻
                     double                price=0,           // アンカーポイントの価格
                     const color           clr=C'225,68,29', // 記号の色
                     const ENUM_LINE_STYLE style=STYLE_SOLID, // 強調表示時の線のスタイル
                     const int             width=1,           // 強調表示時の線のサイズ
                     const bool            back=false,       // 背景で表示する
                     const bool            selection=false,   // 強調表示して移動
                     const bool            hidden=true,       // オブジェクトリストに隠す
                     const long            z_order=0)         // マウスクリックの優先順位
  {
//--- エラー値をリセットする
   ResetLastError();
//--- 記号を作成する
   if(!ObjectCreate(chart_ID,name,OBJ_ARROW_SELL,sub_window,time,price))
     {
      Print(__FUNCTION__,
            ": failed to create \"Sell\" sign! Error code = ",GetLastError());
      return(false);
     }
//--- 記号の色を設定
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- 強調表示時の線のスタイルを設定
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style);
//--- 強調表示時の線のサイズを設定
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,width);
//--- 前景（false）または背景（true）に表示
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- マウスで記号を移動させるモードを有効（true）か無効（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- オブジェクトリストのグラフィックオブジェクトを非表示（true）か表示（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- チャートのマウスクリックのイベントを受信するための優先順位を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 与えられた座標でラインを作成する                                         |
//+------------------------------------------------------------------+
bool LineCreate(
   string name
   ,ENUM_OBJECT objType
   ,datetime time1
   ,double price1
   ,datetime time2
   ,double price2
   ,color clr
   ,ENUM_LINE_STYLE style=STYLE_SOLID
   ,int width=1
   ,bool fill=true
   ,bool back=false
   ,int subwindow=0
)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- 与えられた座標で四角形を作成する
   if(!ObjectCreate(0,name,objType,subwindow,time1,price1,time2,price2))
     {
      Print(__FUNCTION__,"("+name+")",
            ": failed to create! Error code = ",GetLastError(),
            " time1=",TimeToString(time1),
            " price1=",DoubleToString(price1,Digits()),
            " time2=",TimeToString(time2),
            " price2=",DoubleToString(price2,Digits())
           );
      return(false);
     }
//--- 四角形の色を設定
   ObjectSetInteger(0,name,OBJPROP_COLOR,clr);
//--- 四角形の線のスタイルを設定
   ObjectSetInteger(0,name,OBJPROP_STYLE,style);
//--- 四角形の線の幅を設定s
   ObjectSetInteger(0,name,OBJPROP_WIDTH,width);
//--- 四角形を色で塗りつぶすモードを有効（true）か無効（false）にする
   ObjectSetInteger(0,name,OBJPROP_FILL,fill);
//--- 前景（false）または背景（true）に表示
   ObjectSetInteger(0,name,OBJPROP_BACK,back);
//--- 前景（false）または背景（true）に表示
   ObjectSetString(0,name,OBJPROP_TOOLTIP,"\n");
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 垂直線を作成する                                                     |
//+------------------------------------------------------------------+
bool VLineCreate(const long            chart_ID=0,
                 const string          name="VLine",     // 線の名称
                 datetime              time=0,           // 線の時間
                 const color           clr=clrRed,       // 線の色
                 const ENUM_LINE_STYLE style=STYLE_SOLID, // 線のスタイル
                 const int             width=1,           // 線の幅
                 const bool            back=false,       // 背景で表示する
                 const bool            selection=false,   // 強調表示して移動
                 const bool            ray=false,         // 線が下に続く
                 const bool            hidden=true,       // オブジェクトリストに隠す
                 const long            z_order=0)         // マウスクリックの優先順位
  {
//--- 線の時間が設定されていない場合、最後のバーで描画する
   if(!time)
      time=TimeCurrent();
//--- エラー値をリセットする
   ResetLastError();
//--- 垂直線を作成する
   if(!ObjectCreate(chart_ID,name,OBJ_VLINE,0,time,0))
     {
      Print(__FUNCTION__,
            ": failed to create a vertical line! Error code = ",GetLastError());
      return(false);
     }
//--- 線の色を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- 線の表示スタイルを設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style);
//--- 線の幅を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,width);
//--- 前景（false）または背景（true）に表示
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- マウスで線を移動させるモードを有効（true）か無効（false）にする
//--- ObjectCreate 関数を使用してグラフィックオブジェクトを作成する際、オブジェクトは
//--- デフォルトではハイライトされたり動かされたり出来ない。このメソッド内では、選択パラメータは
//--- デフォルトでは true でハイライトと移動を可能にする。
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- チャートサブウィンドウで線を表示するモードを有効（true）か無効（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_RAY,ray);
//--- オブジェクトリストのグラフィックオブジェクトを非表示（true）か表示（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- チャートのマウスクリックのイベントを受信するための優先順位を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 水平線を作成する                                                     |
//+------------------------------------------------------------------+
bool HLineCreate(const long            chart_ID=0,
                 const string          name="HLine",     // 線の名称
                 double                price=0,           // 線の価格
                 const color           clr=clrRed,       // 線の色
                 const ENUM_LINE_STYLE style=STYLE_DASHDOT, // 線のスタイル
                 const int             width=1,           // 線の幅
                 const bool            back=false,       // 背景で表示する
                 const bool            selection=false,   // 強調表示して移動
                 const bool            hidden=true,       // オブジェクトリストに隠す
                 const long            z_order=0)         // マウスクリックの優先順位
  {
//--- エラー値をリセットする
   ResetLastError();
//--- 水平線を作成する
   if(!ObjectCreate(chart_ID,name,OBJ_HLINE,0,0,price))
     {
      Print(__FUNCTION__,
            ": failed to create a horizontal line! Error code = ",GetLastError());
      return(false);
     }
//--- 線の色を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- 線の表示スタイルを設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style);
//--- 線の幅を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,width);
//--- 前景（false）または背景（true）に表示
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- マウスで線を移動させるモードを有効（true）か無効（false）にする
//--- ObjectCreate 関数を使用してグラフィックオブジェクトを作成する際、オブジェクトは
//--- デフォルトではハイライトされたり動かされたり出来ない。このメソッド内では、選択パラメータは
//--- デフォルトでは true でハイライトと移動を可能にする。
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- オブジェクトリストのグラフィックオブジェクトを非表示（true）か表示（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- チャートのマウスクリックのイベントを受信するための優先順位を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| オブジェクト移動                                                    |
//+------------------------------------------------------------------+
bool MyObjectMove(string name,datetime time,double price)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- 垂直線を移動する
   if(!ObjectMove(0,name,0,time,price))
     {
      Print(__FUNCTION__,
            ": failed to move the vertical line! Error code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| オブジェクト削除                                                    |
//+------------------------------------------------------------------+
bool DeleteObject(string name, int sub_win=0) // 線の名称
  {
//--- エラー値をリセットする
   ResetLastError();
//--- オブジェクトを検索する
   if(ObjectFind(sub_win,name)<0)
      return(true);
//--- オブジェクトを削除する
   if(!ObjectDelete(sub_win,name))
     {
      Print(__FUNCTION__,
            ": failed to delete the vertical line! Error code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| オブジェクト削除                                                    |
//+------------------------------------------------------------------+
bool DeleteObject2(string name) // 線の名称
  {
//--- エラー値をリセットする
   ResetLastError();
//--- オブジェクトを検索する
   if(ObjectFind(0,name)<0)
     {
      Print("Object "+name+" not found");
      return(true);
     }
//--- オブジェクトを削除する
   if(!ObjectDelete(0,name))
     {
      Print(__FUNCTION__,
            ": failed to delete the vertical line! Error code = ",GetLastError());
      Print("Object delete "+name);
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| ボタンを作成する                                                      |
//+------------------------------------------------------------------+
bool ButtonCreate(const long              chart_ID=0,               // チャート識別子
                  const string            name="Button",           // ボタン名
                  const int               sub_window=0,             // サブウィンドウ番号
                  const int               x=0,                     // X 座標
                  const int               y=0,                     // Y 座標
                  const int               width=50,                 // ボタンの幅
                  const int               height=18,               // ボタンの高さ
                  const ENUM_BASE_CORNER  corner=CORNER_LEFT_UPPER, // アンカーに使用されるチャートのコーナー
                  const string            text="Button",           // テキスト
                  const string            font="Arial",             // フォント
                  const int               font_size=10,             // フォントサイズ
                  const color             clr=clrBlack,             // テキストの色
                  const color             back_clr=C'236,233,216', // 背景色
                  const color             border_clr=clrNONE,       // 境界線の色
                  const bool              state=false,             // 押される/放される
                  const bool              back=false,               // 背景で表示する
                  const bool              selection=false,         // 強調表示して移動
                  const bool              hidden=true,             // オブジェクトリストに隠す
                  const long              z_order=0)               // マウスクリックの優先順位
  {
//--- エラー値をリセットする
   ResetLastError();
//--- ボタンを作成する
   if(!ObjectCreate(chart_ID,name,OBJ_BUTTON,sub_window,0,0))
     {
      Print(__FUNCTION__,
            ": failed to create the button! Error code = ",GetLastError());
      return(false);
     }
//--- ボタン座標を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
//--- ボタンサイズを設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width);
   ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height);
//--- ポイント座標が相対的に定義されているチャートのコーナーを設定
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner);
//--- テキストを設定する
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text);
//--- テキストフォントを設定する
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font);
//--- フォントサイズを設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size);
//--- テキストの色を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- 背景色を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_BGCOLOR,back_clr);
//--- 境界線の色を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_COLOR,border_clr);
//--- 前景（false）または背景（true）に表示
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- ボタンの状態を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_STATE,state);
//--- マウスでのボタンを移動させるモードを有効（true）か無効（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- オブジェクトリストのグラフィックオブジェクトを非表示（true）か表示（false）にする
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- チャートのマウスクリックのイベントを受信するための優先順位を設定する
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 固定スケールモードを有効または無効にする                                    |
//+------------------------------------------------------------------+
bool ChartScaleFixSet(const bool value,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を設定する
   if(!ChartSetInteger(chart_ID,CHART_SCALEFIX,0,value))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| チャートの固定された最小限を設定する                                       |
//| プロパティ値をかえるには、CHART_SCALEFIX プロパティ                           |
//| 値を予め true にすることが必要                                          |
//+------------------------------------------------------------------+
bool ChartFixedMinSet(const double value,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を設定する
   if(!ChartSetDouble(chart_ID,CHART_FIXED_MIN,value))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| チャートの固定された最大限を設定する                                       |
//| プロパティ値をかえるには、CHART_SCALEFIX プロパティ                           |
//| 値を予め true にすることが必要                                          |
//+------------------------------------------------------------------+
bool ChartFixedMaxSet(const double value,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を設定する
   if(!ChartSetDouble(chart_ID,CHART_FIXED_MAX,value))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }

//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromBuffersByTime(double &buf[],int idx,int ind_handle,datetime start,datetime end)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyBuffer(ind_handle,idx,start,end,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat(__FUNCTION__+"(index:"+(string)idx+" "+"from:"+(string)start+" "+"to:"+(string)end+")"+" "+"Failed to copy data from the indicator, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromCloseByTime(double &buf[],ENUM_TIMEFRAMES in_tf,datetime start,datetime end)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyClose(_Symbol,in_tf,start,end,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from the indicator, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromTickVolumeByTime(long &buf[],ENUM_TIMEFRAMES in_tf,datetime start,datetime end)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyTickVolume(_Symbol,in_tf,start,end,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from the indicator, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromRealVolumeByTime(long &buf[],ENUM_TIMEFRAMES in_tf,datetime start,datetime end)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyRealVolume(_Symbol,in_tf,start,end,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from the indicator, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromSpreadByTime(int &buf[],ENUM_TIMEFRAMES in_tf,datetime start,datetime end)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopySpread(_Symbol,in_tf,start,end,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from the indicator, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//|0埋め |
//+------------------------------------------------------------------+
string ZeroPadding(long in_val)
  {
   string strVal=(string)in_val;
   if(StringLen(strVal)==1)
      strVal="0"+strVal;
   return strVal;
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromTimeByTime(datetime &buf[],ENUM_TIMEFRAMES in_tf,datetime start,datetime end)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyTime(_Symbol,in_tf,start,end,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from open, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromOpenByTime(double &buf[],ENUM_TIMEFRAMES in_tf,datetime start,datetime end)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyOpen(_Symbol,in_tf,start,end,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from open, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromHighByTime(double &buf[],ENUM_TIMEFRAMES in_tf,datetime start,datetime end)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyHigh(_Symbol,in_tf,start,end,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from the indicator, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 指標バッファを記入する                                     |
//+------------------------------------------------------------------+
bool FillArraysFromLowByTime(double &buf[],ENUM_TIMEFRAMES in_tf,datetime start,datetime end)
  {
//--- エラーコードをリセットする
   ResetLastError();
//--- 配列の一部を記入する
   if(CopyLow(_Symbol,in_tf,start,end,buf)<0)
     {
      //--- 複製が失敗したら、エラーコードを出す
      PrintFormat("Failed to copy data from the indicator, error code %d",GetLastError());
      //--- ゼロ結果で終了。 指標は計算されていないと見なされる
      return(false);
     }
//--- 全てが成功
   return(true);
  }
//+------------------------------------------------------------------+
//| convert datetime to double                                       |
//+------------------------------------------------------------------+
double CnvTime2Double(datetime dt)
  {
   MqlDateTime mdt;
   TimeToStruct(dt,mdt);
   return (double)(
             (string)mdt.year
             +ZeroPadding(mdt.mon)
             +ZeroPadding(mdt.day)
             +ZeroPadding(mdt.hour)
             +ZeroPadding(mdt.min)
          );
  }
//+------------------------------------------------------------------+
//| convert double to datetime                                       |
//+------------------------------------------------------------------+
datetime CnvDouble2Time(double dt)
  {
   if((int)dt==0)
      return NULL;
   long lgDateTime=(long)dt;
   string strDateTime=(string)lgDateTime;
   string strYear=StringSubstr(strDateTime,0,4);
   string strMonth=StringSubstr(strDateTime,4,2);
   string strDay=StringSubstr(strDateTime,6,2);
   string strHour=StringSubstr(strDateTime,8,2);
   string strMin=StringSubstr(strDateTime,10,2);
   string strCnvDateTime=(
                            strYear+"."+
                            strMonth+"."+
                            strDay+" "+
                            strHour+":"+
                            strMin
                         );
   return StringToTime(strCnvDateTime);
  }
//+------------------------------------------------------------------+
//| Function: is number                                              |
//+------------------------------------------------------------------+
bool IsNumber(string str)
  {

//文字数が０文字ならfalse
   if(StringLen(str) == 0)
      return false;

//１文字づつ数値かどうか判定する
   int p_count = 0; //ピリオドのカウント用
   for(int i = 0; i <= StringLen(str) - 1; i++)
     {
      string s = StringSubstr(str, i, 1);
      if(s == "0")
         continue;
      if(s == "1")
         continue;
      if(s == "2")
         continue;
      if(s == "3")
         continue;
      if(s == "4")
         continue;
      if(s == "5")
         continue;
      if(s == "6")
         continue;
      if(s == "7")
         continue;
      if(s == "8")
         continue;
      if(s == "9")
         continue;
      if(s == ".")
        {
         p_count++;
         continue;
        }
      //上記に該当しなければfalse
      return false;
     }
//ピリオドが２回以上出現していたらfalse
   if(p_count > 1)
      return false;

//ここまで到達できたらtrue
   return true;
  }
//+------------------------------------------------------------------+
//| 日本時間へ変換（夏時間・冬時間）
//| convertToJapanTime()
//|    とすれば現在のサーバ時間を日本時間で返す
//| convertToJapanTime(Time[n])
//|    などパラメータに指定時間を渡せば指定時間を日本時間に変換して返す
//+------------------------------------------------------------------+
datetime convertToJapanTime(datetime day = 0)
  {
   MqlDateTime cjtm; // 時間構造体
   day = day == 0 ? TimeCurrent() : day; // 対象サーバ時間
   datetime time_summer = 21600; // ６時間
   datetime time_winter = 25200; // ７時間
   int target_dow = 0; // 日曜日
   int start_st_n = 2; // 夏時間開始3月第2週
   int end_st_n = 1; // 夏時間終了11月第1週
   TimeToStruct(day, cjtm); // 構造体の変数に変換
   string year = (string)cjtm.year; // 対象年
// 対象年の3月1日と11月1日の曜日
   TimeToStruct(StringToTime(year + ".03.01"), cjtm);
   int fdo_mar = cjtm.day_of_week;
   TimeToStruct(StringToTime(year + ".11.01"), cjtm);
   int fdo_nov = cjtm.day_of_week;
// 3月第2日曜日
   int start_st_day = (target_dow < fdo_mar ? target_dow + 7 : target_dow)
                      - fdo_mar + 7 * start_st_n - 6;
// 11月第1日曜日
   int end_st_day = (target_dow < fdo_nov ? target_dow + 7 : target_dow)
                    - fdo_nov + 7 * end_st_n - 6;
// 対象年の夏時間開始日と終了日を確定
   datetime start_st = StringToTime(year + ".03." + (string)start_st_day);
   datetime end_st = StringToTime(year + ".11." + (string)end_st_day);
// 日本時間を返す
   return day += start_st <= day && day <= end_st
                 ? time_summer : time_winter;
  }
/*
//+------------------------------------------------------------------+
//| チャートの背景色を設定する                                              |
//+------------------------------------------------------------------+
bool ChartBackColorSet(const color clr,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- チャートの背景色を設定する
   if(!ChartSetInteger(chart_ID,CHART_COLOR_BACKGROUND,clr))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| チャートグリッドの色を設定する                                             |
//+------------------------------------------------------------------+
bool ChartGridColorSet(const color clr,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- チャートグリッドの色を設定する
   if(!ChartSetInteger(chart_ID,CHART_COLOR_GRID,clr))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| ボリュームとマーケットエントリーレベルの色を設定する                               |
//+------------------------------------------------------------------+
bool ChartVolumeColorSet(const color clr,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- ボリュームとマーケットエントリーレベルの色を設定する
   if(!ChartSetInteger(chart_ID,CHART_COLOR_VOLUME,clr))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| バーとその影、及び強気ローソク足の縁の色を設定する                             |
//+------------------------------------------------------------------+
bool ChartUpColorSet(const color clr,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- バーとその影、及び強気ローソク足の縁の色を設定する
   if(!ChartSetInteger(chart_ID,CHART_COLOR_CHART_UP,clr))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+-------------------------------------------------------------------------------+
//| ダウンバーとその影、及び弱気ローソク足の縁の色を設定する                                      |
//+-------------------------------------------------------------------------------+
bool ChartDownColorSet(const color clr,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- ダウンバーとその影、及び弱気ローソク足の縁の色を設定する
   if(!ChartSetInteger(chart_ID,CHART_COLOR_CHART_DOWN,clr))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 強気ローソク足の実体の色を設定する                                       |
//+------------------------------------------------------------------+
bool ChartBullColorSet(const color clr,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- 強気ローソク足の実体の色を設定する
   if(!ChartSetInteger(chart_ID,CHART_COLOR_CANDLE_BULL,clr))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 弱気ローソク足の実体の色を設定する                                       |
//+------------------------------------------------------------------+
bool ChartBearColorSet(const color clr,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- 弱気ローソク足の実体の色を設定する
   if(!ChartSetInteger(chart_ID,CHART_COLOR_CANDLE_BEAR,clr))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 軸、スケールとOHLCラインの色を設定する                                      |
//+------------------------------------------------------------------+
bool ChartForeColorSet(const color clr,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- 軸、スケールとOHLCラインの色を設定する
   if(!ChartSetInteger(chart_ID,CHART_COLOR_FOREGROUND,clr))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 価格チャートの前景での表示を有効または無効にする                             |
//+------------------------------------------------------------------+
bool ChartForegroundSet(const bool value,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- プロパティ値を設定する
   if(!ChartSetInteger(chart_ID,CHART_FOREGROUND,0,value))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| チャートの線と同時線の色を設定する                                        |
//+------------------------------------------------------------------+
bool ChartLineColorSet(const color clr,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- チャートの線と同時線の色を設定する
   if(!ChartSetInteger(chart_ID,CHART_COLOR_CHART_LINE,clr))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 最後の約定価格の線の色を設定する                                        |
//+------------------------------------------------------------------+
bool ChartLastColorSet(const color clr,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- 最後の約定価格の線の色を設定する
   if(!ChartSetInteger(chart_ID,CHART_COLOR_LAST,clr))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 逆指値レベル（決済逆指値及び決済指値）の色を設定する                         |
//+------------------------------------------------------------------+
bool ChartStopLevelColorSet(const color clr,const long chart_ID=0)
  {
//--- エラー値をリセットする
   ResetLastError();
//--- 逆指値レベル（決済逆指値及び決済指値）の色を設定する
   if(!ChartSetInteger(chart_ID,CHART_COLOR_STOP_LEVEL,clr))
     {
      //--- エキスパート操作ログでエラーメッセージを表示する
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- 実行成功
   return(true);
  }
//+------------------------------------------------------------------+
//| 売り線の色を設定する　                                                 |
//+------------------------------------------------------------------+
bool ChartBidColorSet(const color clr,const long chart_ID=0)
 {
//--- エラー値をリセットする
  ResetLastError();
//--- 売り線の色を設定する
  if(!ChartSetInteger(chart_ID,CHART_COLOR_BID,clr))
    {
    //--- エキスパート操作ログでエラーメッセージを表示する
    Print(__FUNCTION__+", Error Code = ",GetLastError());
    return(false);
    }
//--- 実行成功
  return(true);
 }
//+------------------------------------------------------------------+
//| 買値線の色を設定する                                                 |
//+------------------------------------------------------------------+
bool ChartAskColorSet(const color clr,const long chart_ID=0)
 {
//--- エラー値をリセットする
  ResetLastError();
//--- 買値線の色を設定する
  if(!ChartSetInteger(chart_ID,CHART_COLOR_ASK,clr))
    {
    //--- エキスパート操作ログでエラーメッセージを表示する
    Print(__FUNCTION__+", Error Code = ",GetLastError());
    return(false);
    }
//--- 実行成功
  return(true);
 }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool ChartSetCommon()
  {
//---
   if(!ChartForegroundSet(true))
      return false;
//---
   if(!ChartGridColorSet(clrNONE))
      return false;
//---
   if(!ChartForeColorSet(clrBlack))
      return false;
//---
   if(!ChartBackColorSet(clrWhite))
      return false;
//---
   if(!ChartVolumeColorSet(clrNONE))
      return false;
//---
   if(!ChartUpColorSet(clrBlack))
      return false;
//---
   if(!ChartDownColorSet(clrBlack))
      return false;
//---
   if(!ChartLineColorSet(clrBlack))
      return false;
//---
   if(!ChartBullColorSet(clrWhite))
      return false;
//---
   if(!ChartBearColorSet(clrBlack))
      return false;
//---
   if(!ChartLastColorSet(clrBlack))
      return false;
//---
   if(!ChartStopLevelColorSet(clrRed))
      return false;
//---
   if(!ChartBidColorSet(clrGray))
      return false;
//---
   if(!ChartAskColorSet(clrGray))
      return false;
   return true;
  }
  */

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SetEvent(STR_EVENT &io_ev[],datetime date_from,datetime date_to)
  {
   ArrayFree(io_ev);
   string currency[2];
   currency[0]=StringSubstr(_Symbol,0,3);
   currency[1]=StringSubstr(_Symbol,3,3);
//---
   for(int i=0; i<ArraySize(currency); i++)
      SetEventCalender(io_ev,currency[i],date_from,date_to);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SetEventCalender(STR_EVENT &io_ev[],string currency_name,datetime date_from,datetime date_to)
  {
   MqlCalendarValue values[];
//---
   if(!CalendarValueHistory(values,date_from,date_to,NULL,currency_name))
     {
      PrintFormat("Error!Failed to receive events for currency_name=%s",currency_name);
      PrintFormat("Error code: %d",GetLastError());
      return;
     }
//---
   MqlCalendarEvent event;
   for(int i=0; i<ArraySize(values); i++)
     {
      CalendarEventById(values[i].event_id,event);
      if((ENUM_CALENDAR_EVENT_IMPORTANCE)event.importance==CALENDAR_IMPORTANCE_HIGH)
        {
         int count=ArraySize(io_ev);
         ArrayResize(io_ev,count+1);
         io_ev[count].id=values[i].id;
         io_ev[count].currency=currency_name;
         io_ev[count].time=(string)values[i].time;
         io_ev[count].name=event.name;
         count++;
        }
     }
  }
//+------------------------------------------------------------------+
