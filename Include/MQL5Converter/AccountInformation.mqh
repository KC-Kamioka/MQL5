#ifndef CONVERTER
   #include "Converter.mqh"
#endif

double AccountBalance()
{
   return(AccountInfoDouble(ACCOUNT_BALANCE));
}

double AccountCredit()
{
   return(AccountInfoDouble(ACCOUNT_CREDIT));
}

string AccountCompany()
{
   return(AccountInfoString(ACCOUNT_COMPANY));
}

string AccountCurrency()
{
   return(AccountInfoString(ACCOUNT_CURRENCY));
}

double AccountEquity()
{
   return(AccountInfoDouble(ACCOUNT_EQUITY));
}

double AccountFreeMargin()
{
   double balance = AccountInfoDouble(ACCOUNT_BALANCE);
   double margin = AccountInfoDouble(ACCOUNT_MARGIN);
   double profit = AccountInfoDouble(ACCOUNT_PROFIT);
   
   return(balance - margin + profit);
}

int AccountLeverage()
{
   return((int)AccountInfoInteger(ACCOUNT_LEVERAGE));
}

double AccountMargin()
{
   return(AccountInfoDouble(ACCOUNT_MARGIN));
}

double AccountFreeMarginCheck(string symbol, int cmd, double volume)
{
   ENUM_ORDER_TYPE order_type = IntegerToOrderType(cmd);
   double price, margin;
   
   if(order_type == ORDER_TYPE_BUY || order_type == ORDER_TYPE_BUY_LIMIT || order_type == ORDER_TYPE_BUY_STOP) {
      price = SymbolInfoDouble(symbol, SYMBOL_ASK);
   } else {
      price = SymbolInfoDouble(symbol, SYMBOL_BID);
   }
   
   if(OrderCalcMargin(order_type, symbol, volume, price, margin)) {
      return(margin);
   }
   return(0);
   
}

string AccountName()
{
   return(AccountInfoString(ACCOUNT_NAME));
}

int AccountNumber()
{
   return((int)AccountInfoInteger(ACCOUNT_LOGIN));
}

double AccountProfit()
{
   return(AccountInfoDouble(ACCOUNT_PROFIT));
}

string AccountServer()
{
   return(AccountInfoString(ACCOUNT_SERVER));
}

int AccountStopoutLevel()
{
   return((int)AccountInfoDouble(ACCOUNT_MARGIN_SO_SO));
}

int AccountStopoutMode()
{
   return((int)AccountInfoInteger(ACCOUNT_MARGIN_SO_MODE));
}




