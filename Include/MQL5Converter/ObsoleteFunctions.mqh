#include "Checkup.mqh"
#include "ChartOperations.mqh"
#include "TimeseriesAndIndicatorsAccess.mqh"
#include "TradeFunctions.mqh"

int BarsPerWindow()
{
   return(WindowBarsPerChart());
}

string ClientTerminalName()
{
   return(TerminalName());
}

datetime CurTime()
{
   return(TimeCurrent());
}

datetime CurTime(MqlDateTime &dt_struct)
{
   return(TimeCurrent(dt_struct));
}

string CompanyName()
{
   return(TerminalCompany());
}

int FirstVisibleBar()
{
   return(WindowFirstVisibleBar());
}

double Highest(string symbol, int timeframe, int type, int count, int start)
{
   return(iHighest(symbol, timeframe, type, count, start));
}

int HistoryTotal()
{
   return(OrdersHistoryTotal());
}

datetime LocalTime()
{
   return(TimeLocal());
}

datetime LocalTime(MqlDateTime &dt_struct)
{
   return(TimeLocal(dt_struct));
}

double Lowest(string symbol, int timeframe, int type, int count, int start)
{
   return(iLowest(symbol, timeframe, type, count, start));
}

void ObjectsRedraw()
{
   WindowRedraw();
}

double PriceOnDropped()
{
   return(WindowPriceOnDropped());
}

bool ScreenShot(string filename, int size_x, int size_y, int start_bar=-1, int chart_scale=-1, int chart_mode=-1)
{
   return(WindowScreenShot(filename, size_x, size_y, start_bar, chart_scale, chart_mode));
}

string ServerAddress()
{
   return(AccountInfoString(ACCOUNT_SERVER));
}

datetime TimeOnDropped()
{
   return(WindowTimeOnDropped());
}
