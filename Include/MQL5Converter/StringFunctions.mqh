string MQL4StringTrimLeft(const string text)
{
   string result = text;
   StringTrimLeft(result);
   return(result);
}

string MQL4StringTrimRight(const string text)
{
   string result = text;
   StringTrimRight(result);
   return(result);
}

ushort StringGetChar(string string_value, int pos)
{
   return(StringGetCharacter(string_value, pos));
}

string StringSetChar(string string_var, int pos, ushort value)
{
   string str = string_var;
   StringSetCharacter(str, pos, value);
   return(str);
}
