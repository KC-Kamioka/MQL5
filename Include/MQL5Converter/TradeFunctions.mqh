#ifndef CONVERTER
   #include "Converter.mqh"
#endif

ulong last_selected_ticket;

bool OrderClose(ulong ticket, double lots, double price, int slippage, color arrow_color)
{
   MqlTradeRequest request = {};
   MqlTradeResult result = {};
   
   request.order = ticket;
   request.volume = lots;
   request.price = price;
   request.deviation = slippage;
   request.action = TRADE_ACTION_CLOSE_BY;
   
   return(OrderSend(request, result));
}

bool OrderCloseBy(ulong ticket, int opposite, color arrow_color)
{
   MqlTradeRequest request1 = {};
   MqlTradeRequest request2 = {};
   MqlTradeResult result1 = {};
   MqlTradeResult result2 = {};
   
   request1.order = ticket;
   request1.action = TRADE_ACTION_CLOSE_BY;
   request2.order = ticket;
   request2.action = TRADE_ACTION_CLOSE_BY;
   
   if(!OrderSend(request1, result2)) {
      return(false);
   }
   if(!OrderSend(request2, result2)) {
      return(false);
   }
   
   return(true);
}

double OrderClosePrice()
{
   return(HistoryOrderGetDouble(last_selected_ticket, ORDER_PRICE_CURRENT));
}

datetime OrderCloseTime()
{
   return((datetime)HistoryOrderGetInteger(last_selected_ticket, ORDER_TIME_DONE));
}

string OrderComment()
{
   return(OrderGetString(ORDER_COMMENT));
}

double OrderCommission()
{
   return(0);
}

bool OrderDelete(int ticket, color arrow_color)
{
   MqlTradeRequest request = {};
   MqlTradeResult result = {};
   
   request.order = ticket;
   request.action = TRADE_ACTION_REMOVE;
   
   return(OrderSend(request, result));
}

double OrderExpiration()
{
   return(0);
}

double OrderLots()
{
   return(OrderGetDouble(ORDER_VOLUME_CURRENT));
}

int OrderMagicNumber()
{
   return((int)OrderGetInteger(ORDER_MAGIC));
}

bool OrderModify(int ticket, double price, double stoploss, double takeprofit, int expiration, color arrow_color)
{
   MqlTradeRequest request = {};
   MqlTradeResult result = {};
   
   request.action = TRADE_ACTION_MODIFY;
   request.order = ticket;
   request.price = price;
   request.sl = stoploss;
   request.tp = takeprofit;
   request.expiration = expiration;

   return(OrderSend(request, result));
}

double OrderOpenPrice()
{
   return(OrderGetDouble(ORDER_PRICE_OPEN));
}

datetime OrderOpenTime()
{
   return((datetime)OrderGetInteger(ORDER_TIME_SETUP));
}

double OrderProfit()
{
   return(PositionGetDouble(POSITION_PROFIT));
}

int OrderSend(string symbol, int cmd, double volume, double price, int slippage, double stoploss, double takeprofit, string comment=NULL, int magic=0, datetime expiration=0, color arrow=clrNONE)
{
   MqlTradeRequest request = {};
   MqlTradeResult result = {};

   int FillingMode=(int)SymbolInfoInteger(_Symbol,SYMBOL_FILLING_MODE);
   if(FillingMode == 2){
      request.type_filling = ORDER_FILLING_IOC;
   }

   request.action = TRADE_ACTION_DEAL;
   request.type = IntegerToOrderType(cmd);
   request.magic = magic;
   request.symbol = symbol;
   request.volume = volume;
   request.price = price;
   request.sl = stoploss;
   request.tp = takeprofit;
   request.deviation = slippage;
   request.expiration = expiration;
   request.comment = comment;
   
   if(!OrderSend(request, result)) {
      return(-1);
   }
   
   return((int)result.order);
}

int OrdersHistoryTotal()
{
   if(!HistorySelect(0,TimeCurrent())) {
      return(0);
   }
   return(HistoryOrdersTotal());
}

double OrderStopLoss()
{
   return(OrderGetDouble(ORDER_SL));
}

double OrderSwap()
{
   return(PositionGetDouble(POSITION_SWAP));
}

string OrderSymbol()
{
   return(OrderGetString(ORDER_SYMBOL));
}

double OrderTakeProfit()
{
   return(OrderGetDouble(ORDER_TP));
}

int OrderTicket()
{
   return((int)OrderGetInteger(ORDER_TICKET));
}

int OrderType()
{
   return(OrderTypeToInteger((ENUM_ORDER_TYPE)OrderGetInteger(ORDER_TYPE)));
}
