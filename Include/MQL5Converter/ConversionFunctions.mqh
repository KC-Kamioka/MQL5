string CharToStr(uchar char_code)
{
   return(CharToString(char_code));
}

string DoubleToStr(double value, int digits)
{
   return(DoubleToString(value, digits));
}

double StrToDouble(string value)
{
   return(StringToDouble(value));
}

int StrToInteger(string value)
{
   return((int)StringToInteger(value));
}

datetime StrToDatetime(string value)
{
   return(StringToTime(value));
}
/*
string StrToTime(string value)
{
    return(StringToTime(value));
}
*/
string TimeToStr(datetime value, int mode=TIME_DATE|TIME_MINUTES)
{
   return(TimeToString(value, mode));
}
