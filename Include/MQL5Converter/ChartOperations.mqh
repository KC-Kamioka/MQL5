#include "Converter.mqh"

int WindowBarsPerChart()
{
   return((int)ChartGetInteger(0, CHART_VISIBLE_BARS, 0));
}

string WindowExpertName()
{
   return(MQLInfoString(MQL_PROGRAM_NAME));
}

int WindowFind(string name)
{
   if((ENUM_PROGRAM_TYPE)MQLInfoInteger(MQL_PROGRAM_TYPE) == PROGRAM_INDICATOR) {
      return(ChartWindowFind());
   } else {
      return(ChartWindowFind(0, name));  
   }
   
   return(-1);
}

int WindowFirstVisibleBar()
{
   return((int)ChartGetInteger(0, CHART_FIRST_VISIBLE_BAR, 0));
}

int WindowHandle(string symbol, int timeframe)
{
   ENUM_TIMEFRAMES tf = IntegerToTimeframe(timeframe);
   long handle = 0;
   long prev = 0;
   long total = ChartGetInteger(0, CHART_WINDOWS_TOTAL);
   
   for(int i = 0; i < total; i++) {
      if(i == 0) {
         handle = ChartFirst();
      } else {
         handle = ChartNext(prev);
      }
      
      if(handle < 0) {
         return(0);
      }
      
      if(ChartSymbol(handle) == symbol && ChartPeriod(handle) == tf) {
         return((int)handle);
      }
      prev = handle;
   }
   return(0);
}

bool WindowIsVisible(int index)
{
   return((bool)ChartGetInteger(0, CHART_WINDOW_IS_VISIBLE, index));
}

int WindowOnDropped()
{
   return(ChartWindowOnDropped());
}

double WindowPriceMax(int index=0)
{
   return(ChartGetDouble(0, CHART_PRICE_MAX, index));
}

double WindowPriceMin(int index=0)
{
   return(ChartGetDouble(0, CHART_PRICE_MIN, index));
}

double WindowPriceOnDropped()
{
   return(ChartPriceOnDropped());
}

void WindowRedraw()
{
   ChartRedraw(0);
}

bool WindowScreenShot(string filename, int size_x, int size_y, int start_bar=-1, int chart_scale=-1, int chart_mode=-1)
{
   if(chart_scale > 0 && chart_scale <= 5) {
      ChartSetInteger(0, CHART_SCALE, chart_scale);
   }
   
   switch(chart_mode) {
      case 0:
         ChartSetInteger(0, CHART_MODE, CHART_BARS);
         break;
      case 1:
         ChartSetInteger(0, CHART_MODE, CHART_CANDLES);
         break;
      case 2:
         ChartSetInteger(0, CHART_MODE, CHART_LINE);
         break;
   }
   
   if(start_bar < 0) {
      return(ChartScreenShot(0, filename, size_x, size_y, ALIGN_RIGHT));
   }

   return(ChartScreenShot(0, filename, size_x, size_y, ALIGN_LEFT));
}

datetime WindowTimeOnDropped()
{
   return(ChartTimeOnDropped());
}

int WindowsTotal()
{
   return((int)ChartGetInteger(0, CHART_WINDOWS_TOTAL));
}

int WindowXOnDropped()
{
   return(ChartXOnDropped());
}

int WindowYOnDropped()
{
   return(ChartYOnDropped());
}

