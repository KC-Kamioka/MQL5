bool IsConnected()
{
   return((TerminalInfoInteger(TERMINAL_CONNECTED) == 1) ? true : false);
}

bool IsDemo()
{
   return((AccountInfoInteger(ACCOUNT_TRADE_MODE) == ACCOUNT_TRADE_MODE_DEMO) ? true : false);
}

bool IsDllsAllowed()
{
   return((MQLInfoInteger(MQL_DLLS_ALLOWED) == 1) ? true : false);
}

bool IsExpertEnabled()
{
   return((bool)AccountInfoInteger(ACCOUNT_TRADE_EXPERT));
}

bool IsLibrariesAllowed()
{
   return(true);
}

bool IsOptimization()
{
   return((MQLInfoInteger(MQL_OPTIMIZATION) == 1) ? true : false);
}

bool IsTesting()
{
   return((MQLInfoInteger(MQL_TESTER) == 1) ? true : false);
}

bool IsTradeAllowed()
{
   return((bool)AccountInfoInteger(ACCOUNT_TRADE_ALLOWED));
}

bool IsTradeContextBusy()
{
   return(false);
}

bool IsVisualMode()
{
   return((MQLInfoInteger(MQL_VISUAL_MODE) == 1) ? true : false);
}

string TerminalCompany()
{
   return(TerminalInfoString(TERMINAL_COMPANY));
}

string TerminalName()
{
   return(TerminalInfoString(TERMINAL_NAME));
}

string TerminalPath()
{
   return(TerminalInfoString(TERMINAL_PATH));
}