#ifndef CONVERTER
   #include "Converter.mqh"
#endif

template<typename T>
bool MQL4ArraySort(T &array[], int count=WHOLE_ARRAY, int start=0, int direction=MODE_ASCEND)
{
   bool flag = (direction == MODE_DESCEND) ? false : true;
   
   ArraySetAsSeries(array, flag);
   return(ArraySort(array));
}

int ArrayCopyRates(MqlRates &rates_array[], string symbol=NULL, int timeframe=0)
{
   int count = Bars(symbol, IntegerToTimeframe(timeframe));
   
   double open[], low[], high[], close[];
   long tick_volume[], real_volume[];
   int spread[];
   datetime time[];
   
   CopyOpen(symbol, IntegerToTimeframe(timeframe), 0, count, open);
   CopyLow(symbol, IntegerToTimeframe(timeframe), 0, count, low);
   CopyHigh(symbol, IntegerToTimeframe(timeframe), 0, count, high);
   CopyClose(symbol, IntegerToTimeframe(timeframe), 0, count, close);
   CopyTickVolume(symbol, IntegerToTimeframe(timeframe), 0, count, tick_volume);
   CopyRealVolume(symbol, IntegerToTimeframe(timeframe), 0, count, real_volume);
   CopySpread(symbol, IntegerToTimeframe(timeframe), 0, count, spread);
   CopyTime(symbol, IntegerToTimeframe(timeframe), 0, count, time);
   
   ArrayResize(rates_array, count);
   for(int i = 0; i < count; i++) {
      rates_array[i].open        = open[i];
      rates_array[i].low         = low[i];
      rates_array[i].high        = high[i];
      rates_array[i].close       = close[i];
      rates_array[i].tick_volume = tick_volume[i];
      rates_array[i].real_volume = real_volume[i];
      rates_array[i].spread      = spread[i];
      rates_array[i].time        = time[i];
   }
   
   return(count);
}

template<typename T>
int ArrayCopyRates(T &dest_array[], string symbol=NULL, int timeframe=0)
{
   int count = Bars(symbol, IntegerToTimeframe(timeframe));
   
   double open[], low[], high[], close[];
   long tick_volume[];
   datetime time[];
   
   CopyOpen(symbol, IntegerToTimeframe(timeframe), 0, count, open);
   CopyLow(symbol, IntegerToTimeframe(timeframe), 0, count, low);
   CopyHigh(symbol, IntegerToTimeframe(timeframe), 0, count, high);
   CopyClose(symbol, IntegerToTimeframe(timeframe), 0, count, close);
   CopyTickVolume(symbol, IntegerToTimeframe(timeframe), 0, count, tick_volume);
   CopyTime(symbol, IntegerToTimeframe(timeframe), 0, count, time);
   
   for(int i = 0; i < count; i++) {
      dest_array[i][0] = time[i];
      dest_array[i][1] = open[i];
      dest_array[i][2] = low[i];
      dest_array[i][3] = high[i];
      dest_array[i][4] = close[i];
      dest_array[i][5] = tick_volume[i];
   }
   
   return(count);
}

template<typename T>
int ArrayCopySeries(T &array[], int series_index, string symbol=NULL, int timeframe=0)
{
   int count = Bars(symbol, IntegerToTimeframe(timeframe));
   
   switch(series_index) {
      case 0: // MODE_OPEN
         return(CopyOpen(symbol, IntegerToTimeframe(timeframe), 0, count, array));
      case 1: // MODE_LOW
         return(CopyLow(symbol, IntegerToTimeframe(timeframe), 0, count, array));
      case 2: // MODE_HIGH
         return(CopyHigh(symbol, IntegerToTimeframe(timeframe), 0, count, array));
      case 3: // MODE_CLOSE
         return(CopyClose(symbol, IntegerToTimeframe(timeframe), 0, count, array));
      case 5: // MODE_TIME
         return(CopyTime(symbol, IntegerToTimeframe(timeframe), 0, count, array));
   }
   
   return(-1);
}