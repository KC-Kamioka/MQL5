#ifndef CONVERTER
   #include "Converter.mqh"
#endif

double MQL4iAC(string symbol, int timeframe, int shift)
{
   double buffer[1];
   int handle = iAC(symbol, IntegerToTimeframe(timeframe));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iAD(string symbol, int timeframe, int shift)
{
   double buffer[1];
   int handle = iAD(symbol, IntegerToTimeframe(timeframe), VOLUME_TICK);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iADX(string symbol, int timeframe, int period, int applied_price, int mode, int shift)
{
   double buffer[1];
   int handle = iADX(symbol, IntegerToTimeframe(timeframe), period);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, mode, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iAlligator(string symbol, int timeframe, int jaw_period, int jaw_shift, int teeth_period, int teeth_shift, int lips_period, int lips_shift, int ma_method, int applied_price, int mode, int shift)
{
   double buffer[1];
   int handle = iAlligator(symbol, IntegerToTimeframe(timeframe), jaw_period, jaw_shift, teeth_period, teeth_shift, lips_period, lips_shift, IntegerToMAMethod(ma_method), IntegerToAppliedPrice(applied_price));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, mode, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iAO(string symbol, int timeframe, int shift)
{
   double buffer[1];
   int handle = iAO(symbol, IntegerToTimeframe(timeframe));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iATR(string symbol, int timeframe, int period, int shift)
{
   double buffer[1];
   int handle = iATR(symbol, IntegerToTimeframe(timeframe), period);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iBearsPower(string symbol, int timeframe, int period, int applied_price, int shift)
{
   double buffer[1];
   int handle = iBearsPower(symbol, IntegerToTimeframe(timeframe), period);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iBands(string symbol, int timeframe, int period, double deviation, int bands_shift, int applied_price, int mode, int shift)
{
   double buffer[1];
   int handle = iBands(symbol, IntegerToTimeframe(timeframe), period, bands_shift, deviation, IntegerToAppliedPrice(applied_price));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, mode, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iBullsPower(string symbol, int timeframe, int period, int applied_price, int shift)
{
   double buffer[1];
   int handle = iBullsPower(symbol, IntegerToTimeframe(timeframe), period);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iCCI(string symbol, int timeframe, int period, int applied_price, int shift)
{
   double buffer[1];
   int handle = iCCI(symbol, IntegerToTimeframe(timeframe), period, IntegerToAppliedPrice(applied_price));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iDeMarker(string symbol, int timeframe, int period, int shift)
{
   double buffer[1];
   int handle = iDeMarker(symbol, IntegerToTimeframe(timeframe), period);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iEnvelopes(string symbol, int timeframe, int ma_period, int ma_method, int ma_shift, int applied_price, double deviation, int mode, int shift)
{
   if(mode == 0) {
      return(MQL4iMA(symbol, timeframe, ma_period, ma_shift, ma_method, applied_price, shift));
   }
   
   double buffer[1];
   int handle = iEnvelopes(symbol, IntegerToTimeframe(timeframe), ma_period, ma_shift, IntegerToMAMethod(ma_method), IntegerToAppliedPrice(applied_price), deviation);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, mode - 1, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iForce(string symbol, int timeframe, int period, int ma_method, int applied_price, int shift)
{
   double buffer[1];
   int handle = iForce(symbol, IntegerToTimeframe(timeframe), period, IntegerToMAMethod(ma_method), VOLUME_TICK);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iFractals(string symbol, int timeframe, int mode, int shift)
{
   double buffer[1];
   int handle = iFractals(symbol, IntegerToTimeframe(timeframe));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, mode, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iGator(string symbol, int timeframe, int jaw_period, int jaw_shift, int teeth_period, int teeth_shift, int lips_period, int lips_shift, int ma_method, int applied_price, int mode, int shift)
{
   double buffer[1];
   int handle = iGator(symbol, IntegerToTimeframe(timeframe), jaw_period, jaw_shift, teeth_period, teeth_shift, lips_period, lips_shift, IntegerToMAMethod(ma_method), IntegerToAppliedPrice(applied_price));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, mode, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iIchimoku(string symbol, int timeframe, int tenkan_sen, int kijun_sen, int senkou_span_b, int mode, int shift)
{
   double buffer[1];
   int handle = iIchimoku(symbol, IntegerToTimeframe(timeframe), tenkan_sen, kijun_sen, senkou_span_b);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, mode - 1, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iBWMFI(string symbol, int timeframe, int shift)
{
   double buffer[1];
   int handle = iBWMFI(symbol, IntegerToTimeframe(timeframe), VOLUME_TICK);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iMomentum(string symbol, int timeframe, int period, int applied_price, int shift)
{
   double buffer[1];
   int handle = iMomentum(symbol, IntegerToTimeframe(timeframe), period, IntegerToAppliedPrice(applied_price));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iMFI(string symbol, int timeframe, int period, int shift)
{
   double buffer[1];
   int handle = iMFI(symbol, IntegerToTimeframe(timeframe), period, VOLUME_TICK);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iMA(string symbol, int timeframe, int ma_period, int ma_shift, int ma_method, int applied_price, int shift)
{
   double buffer[1];
   int handle = iMA(symbol, IntegerToTimeframe(timeframe), ma_period, ma_shift, IntegerToMAMethod(ma_method), IntegerToAppliedPrice(applied_price));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iOsMA(string symbol, int timeframe, int fast_ma_period, int slow_ma_period, int signal_period, int applied_price, int shift)
{
   double buffer[1];
   int handle = iOsMA(symbol, IntegerToTimeframe(timeframe), fast_ma_period, slow_ma_period, signal_period, IntegerToAppliedPrice(applied_price));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iMACD(string symbol, int timeframe, int fast_ma_period, int slow_ma_period, int signal_period, int applied_price, int mode, int shift)
{
   double buffer[1];
   int handle = iMACD(symbol, IntegerToTimeframe(timeframe), fast_ma_period, slow_ma_period, signal_period, IntegerToAppliedPrice(applied_price));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, mode, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iOBV(string symbol, int timeframe, int applied_price, int shift)
{
   double buffer[1];
   int handle = iOBV(symbol, IntegerToTimeframe(timeframe), VOLUME_TICK);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iSAR(string symbol, int timeframe, double step, double maximum, int shift)
{
   double buffer[1];
   int handle = iSAR(symbol, IntegerToTimeframe(timeframe), step, maximum);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iRSI(string symbol, int timeframe, int period, int applied_price, int shift)
{
   double buffer[1];
   int handle = iRSI(symbol, IntegerToTimeframe(timeframe), period, IntegerToAppliedPrice(applied_price));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iRVI(string symbol, int timeframe, int period, int mode, int shift)
{
   double buffer[1];
   int handle = iRVI(symbol, IntegerToTimeframe(timeframe), period);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, mode, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iStdDev(string symbol, int timeframe, int ma_period, int ma_shift, int ma_method, int applied_price, int shift)
{
   double buffer[1];
   int handle = iStdDev(symbol, IntegerToTimeframe(timeframe), ma_period, ma_shift, IntegerToMAMethod(ma_method), IntegerToAppliedPrice(applied_price));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iStochastic(string symbol, int timeframe, int Kperiod, int Dperiod, int slowing, int method, int price_filed, int mode, int shift)
{
   double buffer[1];
   int handle = iStochastic(symbol, IntegerToTimeframe(timeframe), Kperiod, Dperiod, slowing, IntegerToMAMethod(method), IntegerToStoPrice(price_filed));
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, mode, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}

double MQL4iWPR(string symbol, int timeframe, int period, int shift)
{
   double buffer[1];
   int handle = iWPR(symbol, IntegerToTimeframe(timeframe), period);
   
   if (handle == INVALID_HANDLE) {
      return(0);
   }
   
   if(CopyBuffer(handle, 0, shift, 1, buffer) < 0) {
      return(0);
   }
   return(buffer[0]);
}
