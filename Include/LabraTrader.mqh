//+------------------------------------------------------------------+
//|                                                  TesterPanel.mqh |
//|                        Copyright 2011, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Include files                                                    |
//+------------------------------------------------------------------+
#include <DBCommon.mqh>
#include <Trade\Trade.mqh>
#include <Controls\Dialog.mqh>
#include <Controls\Button.mqh>
#include <Controls\DatePicker.mqh>
#include <Controls\ComboBox.mqh>
#include <Controls\ListView.mqh>
#include <Controls\Label.mqh>
#include <Controls\Edit.mqh>
#include <Controls\SpinEdit.mqh>
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
//---インデントとギャップ
#define INDENT_TOP                        (5)
#define INDENT_LEFT                       (10)
#define CONTROLS_GAP_X                    (5)
#define CONTROLS_GAP_Y                    (5)
#define CONTROLS_GAP_X_S                  (3)
#define CONTROLS_GAP_Y_S                  (3)
#define CLM_WIDTH                         (175)
#define ROW_HEIGHT                        (20)
#define ROW_1                             (INDENT_TOP)
#define ROW_2                             (ROW_1+ROW_HEIGHT)
#define ROW_3                             (ROW_2+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_4                             (ROW_3+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_5                             (ROW_4+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_6                             (ROW_5+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_7                             (ROW_6+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_8                             (ROW_7+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_9                             (ROW_8+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_10                            (ROW_9+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_11                            (ROW_10+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_12                            (ROW_11+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_13                            (ROW_12+CONTROLS_GAP_Y_S+ROW_HEIGHT)
#define ROW_1_D                           (INDENT_TOP)
#define ROW_2_D                           (ROW_1_D+ROW_HEIGHT)
#define ROW_3_D                           (ROW_2_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_4_D                           (ROW_3_D+ROW_HEIGHT)
#define ROW_5_D                           (ROW_4_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_6_D                           (ROW_5_D+ROW_HEIGHT)
#define ROW_7_D                           (ROW_6_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_8_D                           (ROW_7_D+ROW_HEIGHT)
#define ROW_9_D                           (ROW_8_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_10_D                          (ROW_9_D+ROW_HEIGHT)
#define ROW_11_D                          (ROW_10_D+CONTROLS_GAP_Y+ROW_HEIGHT)
#define ROW_12_D                          (ROW_11_D+ROW_HEIGHT)
#define ROW_13_D                          (ROW_12_D+CONTROLS_GAP_Y*2+ROW_HEIGHT)
#define CLM_1                             (INDENT_LEFT)
#define CLM_2                             (CLM_1+CONTROLS_GAP_Y+CLM_WIDTH)
#define CLM_3                             (CLM_2+CONTROLS_GAP_Y+CLM_WIDTH)
#define CLM_4                             (CLM_3+CONTROLS_GAP_Y+CLM_WIDTH-50)
#define CLM_5                             (CLM_4+CONTROLS_GAP_Y+CLM_WIDTH)
#define CLM_6                             (CLM_5+CONTROLS_GAP_Y+CLM_WIDTH)
#define CLM_7                             (CLM_6+CONTROLS_GAP_Y+CLM_WIDTH)
#define YEAR_WIDTH                        (50)
#define YEAR_HEIGHT                       (20)
#define MONTH_WIDTH                       (40)
#define MONTH_HEIGHT                      (20)
#define DAY_WIDTH                         (40)
#define DAY_HEIGHT                        (20)
#define TIME_WIDTH                        (40)
#define TIME_HEIGHT                       (20)
#define BUTTON_WIDTH                      (100)
#define BUTTON_HEIGHT                     (20)
#define LIST_WIDTH                        (100)
#define LIST_HEIGHT                       (110)
#define LABEL_WIDTH                       (70)
#define LABEL_HEIGHT                      (20)
#define EDIT_WIDTH                        (150)
#define EDIT_HEIGHT                       (20)
#define EDIT_WIDTH_B                      (220)
#define COMBO_WIDTH                       (150)
#define COMBO_HEIGHT                      (20)
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
enum ENM_EDT
  {
   EDT_DT
   ,EDT_LOTS
   ,EDT_TN
   ,EDT_PL
  };
enum ENM_CMB
  {
   CMB_OTP
   ,CMB_LTP
   ,CMB_LTF
   ,CMB_ORTP
   ,CMB_STP
   ,CMB_STF
  };
enum ENM_BTN
  {
   BTN_BUY
   ,BTN_SELL
   ,BTN_CL
   ,BTN_SET
   ,BTN_MOD
   ,BTN_CLR
   ,BTN_DEL
  };
enum ENM_LST
  {
   LST_WAIT
   ,LST_POS
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct STR_LABEL
  {
   string            name;
   int               x1;
   int               y1;
   int               x2;
   int               y2;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct STR_BUTTON
  {
   string            name;
   int               x1;
   int               y1;
   int               x2;
   int               y2;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct STR_EDIT
  {
   string            name;
   ENUM_ALIGN_MODE   enmAlign;
   bool              read_only;
   int               x1;
   int               y1;
   int               x2;
   int               y2;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct STR_COMBO
  {
   string            name;
   string            init_list[];
   int               x1;
   int               y1;
   int               x2;
   int               y2;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
struct STR_LIST
  {
   string            name;
   int               x1;
   int               y1;
   int               x2;
   int               y2;
  };
//+------------------------------------------------------------------+
//| Valiable                                                         |
//+------------------------------------------------------------------+
CTrade ExtTrade;
STR_LABEL s_lbl[12];
STR_BUTTON s_btn[7];
STR_EDIT s_edt[4];
STR_COMBO s_cmb[6];
STR_LIST s_lst[2];
//+------------------------------------------------------------------+
//| Class CPanelDialog                                       |
//| Usage: main dialog of the Controls application                   |
//+------------------------------------------------------------------+
class CPanelDialog : public CAppDialog
  {
private:
   CComboBox         m_combos[];
   CButton           m_buttons[];
   CListView         m_lists[];
   CLabel            m_lblTitles[];
   CEdit             m_edits[];

public:
                     CPanelDialog(void);
                    ~CPanelDialog(void);
   //--- create
   virtual bool      Create(const long chart,const string name,const int subwin,const int x1,const int y1,const int x2,const int y2);
   //---チャートイベントハンドラ
   virtual bool      OnEvent(const int id,const long &lparam,const double &dparam,const string &sparam);

protected:
   //従属コントロールの作成
   bool              CreateLstPostions(CListView &m_lists,STR_LIST &s_lst);
   bool              CreateButton(CButton &m_button,STR_BUTTON &s_btn);
   bool              CreateLabel(CLabel &m_label,STR_LABEL &s_lbl);
   bool              CreateEdit(CEdit &m_edit,STR_EDIT &s_edt);
   bool              CreateCmbOrders(CComboBox &m_cmbOrder,STR_COMBO &s_cmb);
   //依存コントロールイベントのハンドラ
   void              OnClickBtnSet(void);
   void              OnClickBtnDelete(void);
   void              OnClickBtnClear(void);
   void              OnClickBtnBuy(void);
   void              OnClickBtnSell(void);
   void              OnClickBtnClose(void);
   void              OnClickBtnModify(void);
   void              OnClickLstWaiting(void);
   void              OnClickLstOrders(void);
   void              OnClickCmbOrders(void);
   void              OnCheckLots(void);
   void              UpdateList(void);
  };
//+------------------------------------------------------------------+
//|イベントの取り扱い |
//+------------------------------------------------------------------+
EVENT_MAP_BEGIN(CPanelDialog)
ON_EVENT(ON_CLICK,m_lists[LST_WAIT],OnClickLstWaiting)
ON_EVENT(ON_CLICK,m_lists[LST_POS],OnClickLstOrders)
ON_EVENT(ON_CLICK,m_buttons[BTN_SET],OnClickBtnSet)
ON_EVENT(ON_CLICK,m_buttons[BTN_DEL],OnClickBtnDelete)
ON_EVENT(ON_CLICK,m_buttons[BTN_CLR],OnClickBtnClear)
ON_EVENT(ON_CLICK,m_buttons[BTN_BUY],OnClickBtnBuy)
ON_EVENT(ON_CLICK,m_buttons[BTN_SELL],OnClickBtnSell)
ON_EVENT(ON_CLICK,m_buttons[BTN_CL],OnClickBtnClose)
ON_EVENT(ON_CLICK,m_buttons[BTN_MOD],OnClickBtnModify)
ON_EVENT(ON_END_EDIT,m_edits[EDT_LOTS],OnCheckLots)
ON_NO_ID_EVENT(ON_TICK_POS_CHANG,UpdateList)
EVENT_MAP_END(CAppDialog)
//+------------------------------------------------------------------+
//|コンストラクタ |
//+------------------------------------------------------------------+
CPanelDialog::CPanelDialog(void)
  {
  }
//+------------------------------------------------------------------+
//|デストラクタ |
//+------------------------------------------------------------------+
CPanelDialog::~CPanelDialog(void)
  {
  }
//+------------------------------------------------------------------+
//|作成 |
//+------------------------------------------------------------------+
bool CPanelDialog::Create(const long chart,const string name,const int subwin,const int x1,const int y1,const int x2,const int y2)
  {
   if(!CAppDialog::Create(chart,name,subwin,x1,y1,x2,y2))
      return(false);
//---
   if(!SetLabelValues())
      return(false);
   ArrayResize(m_lblTitles,ArraySize(s_lbl));
   for(int i=0; i<ArraySize(s_lbl); i++)
     {
      if(!CreateLabel(m_lblTitles[i],s_lbl[i]))
         return(false);
     }
//---
   if(!SetButtonValues())
      return(false);
   ArrayResize(m_buttons,ArraySize(s_btn));
   for(int i=0; i<ArraySize(s_btn); i++)
     {
      if(!CreateButton(m_buttons[i],s_btn[i]))
         return(false);
     }
//---
   if(!SetEditValues())
      return(false);
   ArrayResize(m_edits,ArraySize(s_edt));
   for(int i=0; i<ArraySize(s_edt); i++)
     {
      if(!CreateEdit(m_edits[i],s_edt[i]))
         return(false);
     }
//---
   if(!SetListViewValues())
      return(false);
   ArrayResize(m_lists,ArraySize(s_lst));
   for(int i=0; i<ArraySize(s_lst); i++)
     {
      if(!CreateLstPostions(m_lists[i],s_lst[i]))
         return(false);
     }
//---
   if(!SetComboValues(_Symbol))
      return(false);
   ArrayResize(m_combos,ArraySize(s_cmb));
   for(int i=0; i<ArraySize(s_cmb); i++)
     {
      if(!CreateCmbOrders(m_combos[i],s_cmb[i]))
         return(false);
     }
//---
   UpdateList();
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Set label                                                        |
//+------------------------------------------------------------------+
bool SetLabelValues()
  {
   int i=0;
//---
   s_lbl[i].name="Date";
   s_lbl[i].x1=CLM_1;
   s_lbl[i].y1=ROW_1_D;
   i++;
   s_lbl[i].name="Lots";
   s_lbl[i].x1=CLM_1;
   s_lbl[i].y1=ROW_3_D;
   i++;
   s_lbl[i].name="OrderType";
   s_lbl[i].x1=CLM_1;
   s_lbl[i].y1=ROW_5_D;
   i++;
   s_lbl[i].name="TicketNo";
   s_lbl[i].x1=CLM_1;
   s_lbl[i].y1=ROW_7_D;
   i++;
   s_lbl[i].name="Profit";
   s_lbl[i].x1=CLM_1;
   s_lbl[i].y1=ROW_9_D;
   i++;
   s_lbl[i].name="OpenType";
   s_lbl[i].x1=CLM_2;
   s_lbl[i].y1=ROW_1_D;
   i++;
   s_lbl[i].name="LimitType";
   s_lbl[i].x1=CLM_2;
   s_lbl[i].y1=ROW_3_D;
   i++;
   s_lbl[i].name="LimitTF";
   s_lbl[i].x1=CLM_2;
   s_lbl[i].y1=ROW_5_D;
   i++;
   s_lbl[i].name="StopType";
   s_lbl[i].x1=CLM_2;
   s_lbl[i].y1=ROW_7_D;
   i++;
   s_lbl[i].name="StopTF";
   s_lbl[i].x1=CLM_2;
   s_lbl[i].y1=ROW_9_D;
   i++;
   s_lbl[i].name="WaitingList";
   s_lbl[i].x1=CLM_3;
   s_lbl[i].y1=ROW_1_D;
   i++;
   s_lbl[i].name="PositionList";
   s_lbl[i].x1=CLM_3;
   s_lbl[i].y1=ROW_7_D;
   for(i=0; i<ArraySize(s_lbl); i++)
     {
      //---Label
      s_lbl[i].x2=s_lbl[i].x1+LABEL_WIDTH;
      s_lbl[i].y2=s_lbl[i].y1+LABEL_HEIGHT;
     }
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Set Button                                        |
//+------------------------------------------------------------------+
bool SetButtonValues()
  {
   s_btn[BTN_SET].name="Set";
   s_btn[BTN_SET].x1=CLM_4+CONTROLS_GAP_X;
   s_btn[BTN_SET].y1=ROW_2;
   s_btn[BTN_DEL].name="Delete";
   s_btn[BTN_DEL].x1=CLM_4+CONTROLS_GAP_X;
   s_btn[BTN_DEL].y1=ROW_3;
   s_btn[BTN_CLR].name="Clear";
   s_btn[BTN_CLR].x1=CLM_4+CONTROLS_GAP_X;
   s_btn[BTN_CLR].y1=ROW_4;
   s_btn[BTN_BUY].name="Buy";
   s_btn[BTN_BUY].x1=CLM_4+CONTROLS_GAP_X;
   s_btn[BTN_BUY].y1=ROW_8;
   s_btn[BTN_SELL].name="Sell";
   s_btn[BTN_SELL].x1=CLM_4+CONTROLS_GAP_X;
   s_btn[BTN_SELL].y1=ROW_9;
   s_btn[BTN_CL].name="Close";
   s_btn[BTN_CL].x1=CLM_4+CONTROLS_GAP_X;
   s_btn[BTN_CL].y1=ROW_10;
   s_btn[BTN_MOD].name="Modify";
   s_btn[BTN_MOD].x1=CLM_4+CONTROLS_GAP_X;
   s_btn[BTN_MOD].y1=ROW_11;
   for(int i=0; i<ArraySize(s_btn); i++)
     {
      s_btn[i].x2=s_btn[i].x1+BUTTON_WIDTH;
      s_btn[i].y2=s_btn[i].y1+BUTTON_HEIGHT;
     }
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Set Edit                                        |
//+------------------------------------------------------------------+
bool SetEditValues()
  {
   s_edt[EDT_DT].name="Date";
   s_edt[EDT_DT].x1=CLM_1+CONTROLS_GAP_X;
   s_edt[EDT_DT].y1=ROW_2_D;
   s_edt[EDT_DT].read_only=true;
   s_edt[EDT_LOTS].name="Lots";
   s_edt[EDT_LOTS].enmAlign=ALIGN_RIGHT;
   s_edt[EDT_LOTS].x1=CLM_1+CONTROLS_GAP_X;
   s_edt[EDT_LOTS].y1=ROW_4_D;
   s_edt[EDT_LOTS].read_only=false;
   s_edt[EDT_TN].name="TicketNo";
   s_edt[EDT_TN].x1=CLM_1+CONTROLS_GAP_X;;
   s_edt[EDT_TN].y1=ROW_8_D;
   s_edt[EDT_TN].read_only=true;
   s_edt[EDT_PL].name="Profit";
   s_edt[EDT_PL].enmAlign=ALIGN_RIGHT;
   s_edt[EDT_PL].x1=CLM_1+CONTROLS_GAP_X;
   s_edt[EDT_PL].y1=ROW_10_D;
   s_edt[EDT_PL].read_only=true;
   for(int i=0; i<ArraySize(s_edt); i++)
     {
      //---Edit
      s_edt[i].x2=s_edt[i].x1+EDIT_WIDTH;
      s_edt[i].y2=s_edt[i].y1+EDIT_HEIGHT;
     }
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Set ComboBox                                        |
//+------------------------------------------------------------------+
bool SetComboValues(string in_strDBName)
  {
   string strVal[];
//---
   s_cmb[CMB_ORTP].name="OrderType";
   s_cmb[CMB_ORTP].x1=CLM_1+CONTROLS_GAP_X;
   s_cmb[CMB_ORTP].y1=ROW_6_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)ORDER,strVal,_Symbol);
   ArrayCopy(s_cmb[CMB_ORTP].init_list,strVal);
   s_cmb[CMB_OTP].name="OpenType";
   s_cmb[CMB_OTP].x1=CLM_2+CONTROLS_GAP_X;
   s_cmb[CMB_OTP].y1=ROW_2_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)OP_AUTO,strVal,_Symbol);
   ArrayCopy(s_cmb[CMB_OTP].init_list,strVal);
   s_cmb[CMB_LTP].name="LimitType";
   s_cmb[CMB_LTP].x1=CLM_2+CONTROLS_GAP_X;
   s_cmb[CMB_LTP].y1=ROW_4_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)LIMIT,strVal,_Symbol);
   ArrayCopy(s_cmb[CMB_LTP].init_list,strVal);
   s_cmb[CMB_LTF].name="LimitTF";
   s_cmb[CMB_LTF].x1=CLM_2+CONTROLS_GAP_X;
   s_cmb[CMB_LTF].y1=ROW_6_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)PERIOD,strVal,_Symbol);
   ArrayCopy(s_cmb[CMB_LTF].init_list,strVal);
   s_cmb[CMB_STP].name="StopType";
   s_cmb[CMB_STP].x1=CLM_2+CONTROLS_GAP_X;
   s_cmb[CMB_STP].y1=ROW_8_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)STOP,strVal,_Symbol);
   ArrayCopy(s_cmb[CMB_STP].init_list,strVal);
   s_cmb[CMB_STF].name="StopTF";
   s_cmb[CMB_STF].x1=CLM_2+CONTROLS_GAP_X;
   s_cmb[CMB_STF].y1=ROW_10_D;
   ReadToList("SELECT NAME FROM CODEMASTER WHERE CATEGORY = "+(string)PERIOD,strVal,_Symbol);
   ArrayCopy(s_cmb[CMB_STF].init_list,strVal);
//---
   for(int i=0; i<ArraySize(s_cmb); i++)
     {
      s_cmb[i].x2=s_cmb[i].x1+EDIT_WIDTH;
      s_cmb[i].y2=s_cmb[i].y1+EDIT_HEIGHT;
     }
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Set ListView                                        |
//+------------------------------------------------------------------+
bool SetListViewValues()
  {
   s_lst[LST_WAIT].name="WaitingList";
   s_lst[LST_WAIT].x1=CLM_3+CONTROLS_GAP_X;
   s_lst[LST_WAIT].y1=ROW_2_D;
   s_lst[LST_POS].name="PostionList";
   s_lst[LST_POS].x1=CLM_3+CONTROLS_GAP_X;
   s_lst[LST_POS].y1=ROW_8_D;
   for(int i=0; i<ArraySize(s_lst); i++)
     {
      s_lst[i].x2=s_lst[i].x1+LIST_WIDTH;
      s_lst[i].y2=s_lst[i].y1+LIST_HEIGHT;
     }
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the list                                         |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateLstPostions(CListView &m_list,STR_LIST &io_sl)
  {
//--- create
   if(!m_list.Create(0,"Lst_"+io_sl.name,0,io_sl.x1,io_sl.y1,io_sl.x2,io_sl.y2))
      return(false);
//---
   if(!Add(m_list))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the Button                                      |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateButton(CButton &m_button,STR_BUTTON &io_sb)
  {
//--- create
   if(!m_button.Create(0,"Btn_"+io_sb.name,0,io_sb.x1,io_sb.y1,io_sb.x2,io_sb.y2))
      return(false);
   if(!m_button.Text(io_sb.name))
      return(false);
   if(!Add(m_button))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the label                                                 |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateLabel(CLabel &m_label,STR_LABEL &io_sl)
  {
//--- create
   if(!m_label.Create(0,"Lbl_"+io_sl.name,0,io_sl.x1,io_sl.y1,io_sl.x2,io_sl.y2))
      return(false);
   if(!m_label.Text(io_sl.name))
      return(false);
   if(!Add(m_label))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the Edit                                                  |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateEdit(CEdit &m_edit,STR_EDIT &io_se)
  {
   if(!m_edit.Create(0,"Edt_"+io_se.name,0,io_se.x1,io_se.y1,io_se.x2,io_se.y2))
      return(false);
   if(!m_edit.ReadOnly(io_se.read_only))
      return(false);
   if(!m_edit.TextAlign(io_se.enmAlign))
      return(false);
   if(!Add(m_edit))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the Combo                                              |
//+------------------------------------------------------------------+
bool CPanelDialog:: CreateCmbOrders(CComboBox &m_cmbOrder,STR_COMBO &io_sc)
  {
   if(!m_cmbOrder.Create(0,"Cmb_"+io_sc.name,0,io_sc.x1,io_sc.y1,io_sc.x2,io_sc.y2))
      return(false);
   for(int i=0; i<ArraySize(io_sc.init_list); i++)
      if(!m_cmbOrder.AddItem(io_sc.init_list[i]))
         return(false);
   if(!Add(m_cmbOrder))
      return(false);
//--- succeed
   return(true);
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickLstWaiting(void)
  {
//---
   STR_LB_WAITING data[];
   if(!ReadWaiting(data,_Symbol,(ENM_OP_AUTO)CnvNameToCode(m_lists[LST_WAIT].Select(),OP_AUTO,_Symbol)))
      return;
   if(ArraySize(data)!=1)
     {
      Print("data select error");
      return;
     }
//---
   m_combos[CMB_ORTP].Select(data[0].enmOrderType);
   m_combos[CMB_OTP].Select(data[0].enmOpenType);
   m_combos[CMB_LTP].Select(data[0].enmLimitType);
   m_combos[CMB_STP].Select(data[0].enmStopType);
//---
   m_edits[EDT_DT].Text(NULL);
   m_edits[EDT_LOTS].Text(DoubleToString(data[0].dblLots,1));
   m_edits[EDT_TN].Text(NULL);
   m_edits[EDT_PL].Text(NULL);
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnModify(void)
  {
//---
   STR_LB_ORDERS s_orders;
   s_orders.ulTicketNo=(ulong)m_edits[EDT_TN].Text();
   s_orders.dblLots=NormalizeDouble((double)m_edits[EDT_LOTS].Text(),1);
   s_orders.enmLimitType=(ENM_LIMIT)CnvNameToCode(m_combos[CMB_LTP].Select(),LIMIT,_Symbol);
   s_orders.enmStopType=(ENM_STOP)CnvNameToCode(m_combos[CMB_STP].Select(),STOP,_Symbol);
   if(!DBUpdateOrder(s_orders,_Symbol))
      return;
//---
   PlaySound("ok.wav");
   UpdateList();
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickLstOrders(void)
  {
//---
   STR_LB_ORDERS data[];
   if(!ReadOrders(data,_Symbol,(ulong)m_lists[LST_POS].Select()))
      return;
   if(ArraySize(data)!=1)
     {
      Print("data select error");
      return;
     }
//---
   m_combos[CMB_ORTP].Select(data[0].enmOrderType);
   m_combos[CMB_OTP].Select(data[0].enmOpenType);
   m_combos[CMB_LTP].Select(data[0].enmLimitType);
   m_combos[CMB_STP].Select(data[0].enmStopType);
//---
   m_edits[EDT_DT].Text(data[0].strOpDateTime);
   m_edits[EDT_LOTS].Text(DoubleToString(data[0].dblLots,1));
   m_edits[EDT_TN].Text((string)data[0].ulTicketNo);
   m_edits[EDT_PL].Text((string)data[0].intProfit);
   m_edits[EDT_PL].Color(data[0].intProfit<0?clrRed:clrBlack);
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnSet(void)
  {
//---
   string strLots=m_edits[EDT_LOTS].Text();
   if(StringLen(strLots)==0)
     {
      Alert(s_edt[EDT_LOTS].name," is required.");
      return;
     }
   string strOrderType=m_combos[CMB_ORTP].Select();
   if(StringLen(strOrderType)==0)
     {
      Alert(s_cmb[CMB_ORTP].name," is required.");
      return;
     }
   string strOpenType=m_combos[CMB_OTP].Select();
   if(StringLen(strOpenType)==0)
     {
      Alert(s_cmb[CMB_OTP].name," is required.");
      return;
     }
//---
   STR_LB_WAITING s_wait;
   s_wait.dblLots=(double)strLots;
   s_wait.enmOrderType=(ENUM_ORDER_TYPE)CnvNameToCode(strOrderType,ORDER,_Symbol);
   s_wait.enmOpenType=(ENM_OP_AUTO)CnvNameToCode(strOpenType,OP_AUTO,_Symbol);
   s_wait.enmLimitType=(ENM_LIMIT)CnvNameToCode(m_combos[CMB_LTP].Select(),LIMIT,_Symbol);
   s_wait.enmStopType=(ENM_STOP)CnvNameToCode(m_combos[CMB_STP].Select(),STOP,_Symbol);
   if(!DBInsertWaiting(s_wait,_Symbol))
      return;
   if(!UpdateLots(NormalizeDouble((double)strLots,1)))
      return;
//---
   PlaySound("ok.wav");
   UpdateList();
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnDelete(void)
  {
//---
   ENM_OP_AUTO code=(ENM_OP_AUTO)CnvNameToCode(m_lists[LST_WAIT].Select(),OP_AUTO,_Symbol);
   if(code<0)
      return;
//---
   if(!DeleteWaiting(code,_Symbol))
      return;
//---
   PlaySound("ok.wav");
   UpdateList();
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnClear(void)
  {
   UpdateList();
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnBuy(void)
  {
//---
   double dblLots=(double)m_edits[EDT_LOTS].Text();
   if(!UpdateLots(dblLots))
      return;
   if(!OpenMarketOrder(dblLots,ORDER_TYPE_BUY))
      return;
//---
   PlaySound("ok.wav");
   UpdateList();
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnSell(void)
  {
//---
   double dblLots=(double)m_edits[EDT_LOTS].Text();
   if(!UpdateLots(dblLots))
      return;
   if(!OpenMarketOrder(dblLots,ORDER_TYPE_SELL))
      return;
//---
   PlaySound("ok.wav");
   UpdateList();
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnClickBtnClose(void)
  {
//---
   ulong ticket=(ulong)m_lists[LST_POS].Select();
   STR_LB_ORDERS orders[];
   if(!ReadOrders(orders,_Symbol,ticket))
      return;
//---
   for(int i=0; i<ArraySize(orders); i++)
      CloseMarketOrder(orders[i]);
//---
   PlaySound("ok.wav");
   UpdateList();
   return;
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::UpdateList(void)
  {
//---
   STR_LB_WAITING waiting[];
   if(!ReadWaiting(waiting,_Symbol))
      ExpertRemove();
   if(!m_lists[LST_WAIT].ItemsClear())
      return;
   for(int i=0; i<ArraySize(waiting); i++)
      if(!m_lists[LST_WAIT].ItemAdd(EnumToString(waiting[i].enmOpenType)))
         return;
//---
   STR_LB_ORDERS orders[];
   if(!ReadOrders(orders,_Symbol))
      ExpertRemove();
   if(!m_lists[LST_POS].ItemsClear())
      return;
   for(int i=0; i<ArraySize(orders); i++)
      if(!m_lists[LST_POS].ItemAdd((string)orders[i].ulTicketNo))
         return;
//---
   m_combos[CMB_OTP].Select(0);
   m_combos[CMB_LTP].Select(0);
   m_combos[CMB_LTF].SelectByText(EnumToString(_Period));
   m_combos[CMB_ORTP].Select(0);
   m_combos[CMB_STP].Select(0);
   m_combos[CMB_STF].SelectByText(EnumToString(_Period));
//---
   m_edits[EDT_DT].Text(NULL);
   m_edits[EDT_LOTS].Text((string)ReadLots());
   m_edits[EDT_TN].Text(NULL);
   m_edits[EDT_PL].Text(NULL);
  }
//+------------------------------------------------------------------+
//|イベントハンドラ |
//+------------------------------------------------------------------+
void CPanelDialog::OnCheckLots(void)
  {
   double val=(double)m_edits[EDT_LOTS].Text();
   if(val<0.1)
      val=0.1;
   m_edits[EDT_LOTS].Text(DoubleToString(val,1));
  }
//+------------------------------------------------------------------+
//| open order
//+------------------------------------------------------------------+
bool OpenMarketOrder(double dblLots,ENUM_ORDER_TYPE enmOrderType)
  {
   if(!ExtTrade.PositionOpen(
         _Symbol
         ,enmOrderType
         ,dblLots
         ,SymbolInfoDouble(_Symbol,enmOrderType==ORDER_TYPE_SELL ? SYMBOL_BID:SYMBOL_ASK)
         ,0
         ,0
         ,NULL
      ))
     {
      Alert("position open error","(",GetLastError(),")");
      ExpertRemove();
     }
   return true;
  }
//+------------------------------------------------------------------+
//| close order
//+------------------------------------------------------------------+
bool CloseMarketOrder(STR_LB_ORDERS &io_od)
  {
   if(!PositionSelectByTicket(io_od.ulTicketNo))
     {
      Alert("postion cannot select.");
      return false;
     }
   if(!ExtTrade.PositionClose(io_od.ulTicketNo,3))
     {
      Alert("postion close error:"+IntegerToString(GetLastError())+" Ticket No."+(string)io_od.ulTicketNo);
      ExpertRemove();
     }
   Print("position close : ",EnumToString(io_od.enmLimitType),",",EnumToString(io_od.enmStopType));
//---
   if(!DeleteOrder(_Symbol,io_od.ulTicketNo))
      ExpertRemove();
   return true;
  }
//+------------------------------------------------------------------+
