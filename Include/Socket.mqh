//+------------------------------------------------------------------+
//|                                                       Socket.mqh |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
//+------------------------------------------------------------------+
//| Include files                                                    |
//+------------------------------------------------------------------+
#include <DBCommon.mqh>
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void SendLog(ENM_LOG_TYPE enmLogType, string strMessage)
  {
//---
   if(MQLInfoInteger(MQL_TESTER))
      return;
//---
string strIp;  
   if(!ReadIpAddress(strIp))
      return;
//---
   CJAVal jsonData;
   jsonData["type"]=EnumToString(enmLogType);
   jsonData["msg"]=strMessage;
   string tosend=jsonData.Serialize();
//---
   int socket=SocketCreate();
   if(socket!=INVALID_HANDLE)
     {
      if(SocketConnect(socket,strIp,9090,1000))
        {
         Print("Connected to "," ",strIp,":",9090);
         string received = socksend(socket, tosend) ? socketreceive(socket, 10) : "";
         Print(strMessage);
        }

      else
         Print("Connection ",strIp,":",9090," error ",GetLastError());
      SocketClose(socket);
     }
   else
      Print("Socket creation error ",GetLastError());
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool socksend(int sock,string request)
  {
   char req[];
   int  len=StringToCharArray(request,req)-1;
   if(len<0)
      return(false);
   return(SocketSend(sock,req,len)==len);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string socketreceive(int sock,int timeout)
  {
   char rsp[];
   string result="";
   uint len;
   uint timeout_check=GetTickCount()+timeout;
   do
     {
      len=SocketIsReadable(sock);
      if(len)
        {
         int rsp_len;
         rsp_len=SocketRead(sock,rsp,len,timeout);
         if(rsp_len>0)
           {
            result+=CharArrayToString(rsp,0,rsp_len);
           }
        }
     }
   while((GetTickCount()<timeout_check) && !IsStopped());
   return result;
  }
//+------------------------------------------------------------------+
