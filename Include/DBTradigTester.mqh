//+------------------------------------------------------------------+
//|                                               DBTradigTester.mqh |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#define T_DB_NAME       "TradingTester"
//+------------------------------------------------------------------+
//| Include                                                          |
//+------------------------------------------------------------------+
#include <DBCommon.mqh>
//+------------------------------------------------------------------+
//| struct                                                           |
//+------------------------------------------------------------------+
struct STR_LB_HISTORY_CANDLE
  {
   long              date;
   double            open;
   double            high;
   double            low;
   double            close;
  };
//+------------------------------------------------------------------+
//| struct                                                           |
//+------------------------------------------------------------------+
struct STR_LB_HISTORY_SPANMODEL
  {
   long              date;
   double            bluespan;
   double            redspan;
   double            bluespan_l;
   double            redspan_l;
   double            chiko_span26;
  };
//+------------------------------------------------------------------+
//| struct                                                           |
//+------------------------------------------------------------------+
struct STR_LB_HISTORY_SUPERBOLLINGER
  {
   long              date;
   double            middle;
   double            up_s1;
   double            lw_s1;
   double            up_s2;
   double            lw_s2;
   double            up_s3;
   double            lw_s3;
   double            chiko_span21;
   double            ov_2_sigma;
   int               c_dir;
   double            c_at_up;
   double            c_at_dw;
  };
//+------------------------------------------------------------------+
//| struct                                                           |
//+------------------------------------------------------------------+
struct STR_LB_HISTORY_SAS
  {
   long              date;
   int               sm_sig;
   int               sm_term;
   double            sm_up_g_st;
   double            sm_up_g_ed;
   double            sm_dw_g_st;
   double            sm_dw_g_ed;
   double            sm_g_high;
   double            sm_g_low;
   double            sm_sig_ed;
   int               rs_sig;
   int               rs_term;
   double            rs_up_g_st;
   double            rs_up_g_ed;
   double            rs_dw_g_st;
   double            rs_dw_g_ed;
   double            rs_g_high;
   double            rs_g_low;
   double            rs_sig_ed;
   double            high_52;
   double            low_52;
   double            y_high;
   double            y_low;
  };
//---
struct STR_LB_RESULTS
  {
   string            strOpDateTime;
   double            dblLots;
   ENUM_ORDER_TYPE   enmOrderType;
   string            strOpenType;
   double            dblOpPrice;
   ulong             ulTicketNo;
   string            strLimitType;
   ENUM_TIMEFRAMES   enmLimitTimeframe;
   string            strStopType;
   ENUM_TIMEFRAMES   enmStopTimeframe;
   int               intProfit;
  };
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadTesterHistoryDateList(string in_symbol,ENUM_TIMEFRAMES in_tf,string &year[],string &month[],string &day[],string &hour[],string &min[])
  {
//---
   string sql=NULL;
   string strDateTime=NULL;
//--- open the database in the folder
   int db=DatabaseOpen(T_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   sql="SELECT DISTINCT";
   sql=sql+" "+"SUBSTR(DATE,1,4)";
   sql=sql+" "+"FROM CANDLE";
   sql=sql+" "+"WHERE SYMBOL = "+"'"+in_symbol+"'";
   sql=sql+" "+"AND PERIOD = "+"'"+EnumToString(in_tf)+"'";
   sql=sql+" "+"ORDER BY SUBSTR(DATE,1,4)";
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//---
   for(int i=0; DatabaseRead(request); i++)
     {
      //--- read the values of each field from the obtained entry
      string strYear;
      if(DatabaseColumnText(request, 0, strYear))
        {
         ArrayResize(year,ArraySize(year)+1);
         year[i]=strYear;
        }
      else
        {
         Print(i, ": DatabaseRead() failed with code ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
     }
//--- create a query and get a handle for it
   sql="SELECT DISTINCT";
   sql=sql+" "+"SUBSTR(DATE,5,2)";
   sql=sql+" "+"FROM CANDLE";
   sql=sql+" "+"WHERE SYMBOL = "+"'"+in_symbol+"'";
   sql=sql+" "+"AND PERIOD = "+"'"+EnumToString(in_tf)+"'";
   sql=sql+" "+"ORDER BY SUBSTR(DATE,5,2)";
   request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//---
   for(int i=0; DatabaseRead(request); i++)
     {
      //--- read the values of each field from the obtained entry
      string strMonth;
      if(DatabaseColumnText(request, 0, strMonth))
        {
         ArrayResize(month,ArraySize(month)+1);
         month[i]=strMonth;
        }
      else
        {
         Print(i, ": DatabaseRead() failed with code ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
     }
//--- create a query and get a handle for it
   sql="SELECT DISTINCT";
   sql=sql+" "+"SUBSTR(DATE,7,2)";
   sql=sql+" "+"FROM CANDLE";
   sql=sql+" "+"WHERE SYMBOL = "+"'"+in_symbol+"'";
   sql=sql+" "+"AND PERIOD = "+"'"+EnumToString(in_tf)+"'";
   sql=sql+" "+"ORDER BY SUBSTR(DATE,7,2)";
   request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//---
   for(int i=0; DatabaseRead(request); i++)
     {
      //--- read the values of each field from the obtained entry
      string strDay;
      if(DatabaseColumnText(request, 0, strDay))
        {
         ArrayResize(day,ArraySize(day)+1);
         day[i]=strDay;
        }
      else
        {
         Print(i, ": DatabaseRead() failed with code ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
     }
//--- create a query and get a handle for it
   sql="SELECT DISTINCT";
   sql=sql+" "+"SUBSTR(DATE,9,2)";
   sql=sql+" "+"FROM CANDLE";
   sql=sql+" "+"WHERE SYMBOL = "+"'"+in_symbol+"'";
   sql=sql+" "+"AND PERIOD = "+"'"+EnumToString(in_tf)+"'";
   sql=sql+" "+"ORDER BY SUBSTR(DATE,9,2)";
   request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//---
   for(int i=0; DatabaseRead(request); i++)
     {
      //--- read the values of each field from the obtained entry
      string strHour;
      if(DatabaseColumnText(request, 0, strHour))
        {
         ArrayResize(hour,ArraySize(hour)+1);
         hour[i]=strHour;
        }
      else
        {
         Print(i, ": DatabaseRead() failed with code ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
     }
//--- create a query and get a handle for it
   sql="SELECT DISTINCT";
   sql=sql+" "+"SUBSTR(DATE,11,2)";
   sql=sql+" "+"FROM CANDLE";
   sql=sql+" "+"WHERE SYMBOL = "+"'"+in_symbol+"'";
   sql=sql+" "+"AND PERIOD = "+"'"+EnumToString(in_tf)+"'";
   sql=sql+" "+"ORDER BY SUBSTR(DATE,11,2)";
   request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//---
   for(int i=0; DatabaseRead(request); i++)
     {
      //--- read the values of each field from the obtained entry
      string strMin;
      if(DatabaseColumnText(request, 0, strMin))
        {
         ArrayResize(min,ArraySize(min)+1);
         min[i]=strMin;
        }
      else
        {
         Print(i, ": DatabaseRead() failed with code ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  };
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
string ReadResult(string sql)
  {
   string rtn=NULL;
//--- open the database in the folder
   int db=DatabaseOpen(T_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " open failed with code ", GetLastError());
      return rtn;
     }
//--- create a query and get a handle for it
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return rtn;
     }
//---
   for(int i=0; DatabaseRead(request); i++)
     {
      //--- read the values of each field from the obtained entry
      if(!DatabaseColumnText(request, 0, rtn))
        {
         Print(i, ": DatabaseRead() failed with code ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(db);
         return rtn;
        }
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return rtn;
  };
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool DBInitTradingTester()
  {
//---
   string sql[1];
   string strObjName=NULL;
//---
   sql[0]="DROP TABLE IF EXISTS TESTER_CONFIG";
   if(!DBExecute(sql,T_DB_NAME))
      return false;
   sql[0]="CREATE TABLE IF NOT EXISTS TESTER_CONFIG";
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+" "+"SEQ INT PRIMARY KEY";
   sql[0]=sql[0]+","+"DATE LONG";
   sql[0]=sql[0]+","+"TICKET_NO INT";
   sql[0]=sql[0]+")";
   if(!DBExecute(sql,T_DB_NAME))
      return false;
//---
   sql[0]="REPLACE INTO TESTER_CONFIG (SEQ,DATE,TICKET_NO)";
   sql[0]=sql[0]+"SELECT";
   sql[0]=sql[0]+" "+"1";
   sql[0]=sql[0]+","+"MIN(DATE)";
   sql[0]=sql[0]+","+"1";
   sql[0]=sql[0]+" "+"FROM CANDLE";
   sql[0]=sql[0]+" "+"WHERE PERIOD = '"+EnumToString(_Period)+"'";
   if(!DBExecute(sql,T_DB_NAME))
      return false;
//---
   sql[0]="DROP TABLE IF EXISTS RESULTS";
   if(!DBExecute(sql,T_DB_NAME))
      return false;
//---
   sql[0]="CREATE TABLE IF NOT EXISTS RESULTS";
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+" "+"DATE TEXT";
   sql[0]=sql[0]+","+"LOTS REAL";
   sql[0]=sql[0]+","+"ORDER_TYPE INT";
   sql[0]=sql[0]+","+"OPEN_TYPE INT";
   sql[0]=sql[0]+","+"OP_PRICE REAL";
   sql[0]=sql[0]+","+"TICKET_NO TEXT PRIMARY KEY";
   sql[0]=sql[0]+","+"LIMIT_TYPE INT";
   sql[0]=sql[0]+","+"LIMIT_TF INT";
   sql[0]=sql[0]+","+"STOP_TYPE INT";
   sql[0]=sql[0]+","+"STOP_TF INT";
   sql[0]=sql[0]+","+"PROFIT INT";
   sql[0]=sql[0]+")";
   if(!DBExecute(sql,T_DB_NAME))
      return false;
//---
   sql[0]="UPDATE TESTER_CONFIG SET";
   sql[0]=sql[0]+" "+"TICKET_NO=1";
   if(!DBExecute(sql,T_DB_NAME))
      return false;
//---
   sql[0]="DELETE FROM WAITING";
   if(!DBExecute(sql,T_DB_NAME))
      return false;
//---
   STR_LB_ORDERS orders[];
   if(!ReadOrders(orders))
      return false;
   for(int i=0; i<ArraySize(orders); i++)
     {
      strObjName="ArwBuy_"+orders[i].strOpDateTime;
      if(!DeleteObject(strObjName))
         return false;
      strObjName="ArwSell_"+orders[i].strOpDateTime;
      if(!DeleteObject(strObjName))
         return false;
      if(!DeleteOrder(orders[i].ulTicketNo))
         return false;
     }
//---
   STR_LB_RESULTS results[];
   if(!ReadResults(results))
      return false;
   for(int i=0; i<ArraySize(results); i++)
     {
      strObjName="ArwBuy_"+results[i].strOpDateTime;
      if(!DeleteObject(strObjName))
         return false;
      strObjName="ArwSell_"+results[i].strOpDateTime;
      if(!DeleteObject(strObjName))
         return false;
      strObjName="LineClose"+"_"+(string)results[i].ulTicketNo;
      if(!DeleteObject(strObjName))
         return false;
      if(!DeleteResults(results[i].ulTicketNo))
         return false;
     }
   return true;
  }
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool DBInsertTesterOrder(STR_LB_ORDERS &io_order)
  {
   string sql[1];
   sql[0]="INSERT INTO ORDERS";
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+" "+"TICKET_NO";
   sql[0]=sql[0]+","+"DATE";
   sql[0]=sql[0]+","+"LOTS";
   sql[0]=sql[0]+","+"OP_PRICE";
   sql[0]=sql[0]+","+"ORDER_TYPE";
   sql[0]=sql[0]+","+"OPEN_TYPE";
   sql[0]=sql[0]+","+"LIMIT_TYPE";
   sql[0]=sql[0]+","+"LIMIT_TF";
   sql[0]=sql[0]+","+"STOP_TYPE";
   sql[0]=sql[0]+","+"STOP_TF";
   sql[0]=sql[0]+")"+"VALUES"+"(";
   sql[0]=sql[0]+" "+"(SELECT MAX(TICKET_NO) FROM TESTER_CONFIG)";
   sql[0]=sql[0]+","+"'"+io_order.strOpDateTime+"'";
   sql[0]=sql[0]+","+DoubleToString(io_order.dblLots,1);
   sql[0]=sql[0]+","+DoubleToString(io_order.dblOpPrice,_Digits);
   sql[0]=sql[0]+","+(string)io_order.enmOrderType;
   sql[0]=sql[0]+","+(string)io_order.enmOpenType;
   sql[0]=sql[0]+","+(string)io_order.enmLimitType;
   sql[0]=sql[0]+","+(string)io_order.enmStopType;
   sql[0]=sql[0]+")";
   if(!DBExecute(sql,T_DB_NAME))
      return false;
//---
   sql[0]="UPDATE TESTER_CONFIG SET TICKET_NO=TICKET_NO+1";
   if(!DBExecute(sql,T_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool DBInsertResult(ulong ticket)
  {
   string sql[1];
   sql[0]="INSERT INTO RESULTS";
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+" "+"DATE";
   sql[0]=sql[0]+","+"LOTS";
   sql[0]=sql[0]+","+"ORDER_TYPE";
   sql[0]=sql[0]+","+"OPEN_TYPE";
   sql[0]=sql[0]+","+"OP_PRICE";
   sql[0]=sql[0]+","+"TICKET_NO";
   sql[0]=sql[0]+","+"LIMIT_TYPE";
   sql[0]=sql[0]+","+"LIMIT_TF";
   sql[0]=sql[0]+","+"STOP_TYPE";
   sql[0]=sql[0]+","+"STOP_TF";
   sql[0]=sql[0]+","+"PROFIT";
   sql[0]=sql[0]+")"+"SELECT";
   sql[0]=sql[0]+" "+"DATE";
   sql[0]=sql[0]+","+"LOTS";
   sql[0]=sql[0]+","+"ORDER_TYPE";
   sql[0]=sql[0]+","+"OPEN_TYPE";
   sql[0]=sql[0]+","+"OP_PRICE";
   sql[0]=sql[0]+","+"TICKET_NO";
   sql[0]=sql[0]+","+"LIMIT_TYPE";
   sql[0]=sql[0]+","+"LIMIT_TF";
   sql[0]=sql[0]+","+"STOP_TYPE";
   sql[0]=sql[0]+","+"STOP_TF";
   sql[0]=sql[0]+","+"PROFIT";
   sql[0]=sql[0]+" "+"FROM ORDERS";
   sql[0]=sql[0]+" "+"WHERE TICKET_NO="+(string)ticket;
   if(!DBExecute(sql,T_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool DBUpdateProfit(ulong ticket,ENUM_ORDER_TYPE in_ot,double price,double spread)
  {
   string sql[1];
   sql[0]="UPDATE ORDERS";
   sql[0]=sql[0]+" "+"SET";
   sql[0]=sql[0]+" "+"PROFIT";
   sql[0]=sql[0]+"="+"CAST"+"(";
   sql[0]=sql[0]+"ROUND"+"(";
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+"("+DoubleToString(price,_Digits)+"-"+"OP_PRICE"+")";
   if(in_ot==ORDER_TYPE_SELL)
      sql[0]=sql[0]+"*-1";
   sql[0]=sql[0]+"-";
   sql[0]=sql[0]+(string)(MathPow(10,_Digits*-1)*spread);
   sql[0]=sql[0]+")"+"*(LOTS*100000)";
   sql[0]=sql[0]+")";
   sql[0]=sql[0]+" "+"AS INT)";
   sql[0]=sql[0]+" "+"WHERE TICKET_NO = "+(string)ticket;
   if(!DBExecute(sql,T_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime GetDBLatestDate()
  {

// 指定したファイル名のDBを開く、なければ新規作成
   int DBHandle=DatabaseOpen(T_DB_NAME, DATABASE_OPEN_READWRITE);
   if(DBHandle==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, "新規作成 エラーコード: ", GetLastError());
      return NULL;
     }

// DB内のテーブルの存在を確認
   if(!DatabaseTableExists(DBHandle, _Symbol))
     {
      Print("DB: ", T_DB_NAME, "テーブルがない テーブル名:", _Symbol);
      return NULL;
     }
// リクエストハンドルを作成
   string sql="SELECT MAX(DATE) FROM "+_Symbol;
   sql=sql+" "+"WHERE PERIOD = "+"'"+EnumToString(_Period)+"'";;
   int request=DatabasePrepare(DBHandle, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, "リクエスト エラーコード: ", GetLastError());
      DatabaseClose(DBHandle);
      return NULL;
     }

// DB内のデータを取得してPrint()で出力
   long lngDate=0;
   for(int i=0; DatabaseRead(request); i++) // リクエストのレコードの数だけ繰り返す
     {
      if(!DatabaseColumnLong(request,0,lngDate))
        {
         Print(i, " データを取得  エラーコード: ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(DBHandle);
         return NULL;
        }
     }
// 作成されたリクエストを削除
   DatabaseFinalize(request);

// DBを閉じる
   DatabaseClose(DBHandle);
   return CnvDouble2Time((double)lngDate);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
datetime GetTesterDate()
  {
// SQLiteファイル名
   string TableName="TESTER_CONFIG";

// 指定したファイル名のDBを開く、なければ新規作成
   int DBHandle=DatabaseOpen(T_DB_NAME, DATABASE_OPEN_READWRITE);
   if(DBHandle==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, "新規作成 エラーコード: ", GetLastError());
      return NULL;
     }

// DB内のテーブルの存在を確認
   if(!DatabaseTableExists(DBHandle, TableName))
     {
      Print("DB: ", T_DB_NAME, "テーブルがない テーブル名:", TableName);
      return NULL;
     }


// リクエストハンドルを作成
   int request=DatabasePrepare(DBHandle, "SELECT DATE FROM "+TableName);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, "リクエスト エラーコード: ", GetLastError());
      DatabaseClose(DBHandle);
      return NULL;
     }

// DB内のデータを取得してPrint()で出力
   string strDate;
   for(int i=0; DatabaseRead(request); i++) // リクエストのレコードの数だけ繰り返す
     {
      if(!DatabaseColumnText(request,0,strDate))
        {
         Print(i, " データを取得  エラーコード: ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(DBHandle);
         return NULL;
        }
     }
// 作成されたリクエストを削除
   DatabaseFinalize(request);

// DBを閉じる
   DatabaseClose(DBHandle);
   return StringToTime(strDate);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool UpdTesterDate(datetime in_dt)
  {
   string sql[1];
   sql[0]="UPDATE TESTER_CONFIG";
   sql[0]=sql[0]+" "+"SET";
   sql[0]=sql[0]+" "+"DATE = '"+TimeToString(in_dt)+"'";
   if(!DBExecute(sql,T_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
datetime GetNextDateTime(long lngDt)
  {
   long lngDate=0;
//---
   string strTableName="CANDLE";
//--- open the database in the folder
   int db=DatabaseOpen(T_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"MIN(DATE)";
   sql=sql+" "+"FROM CANDLE";
   sql=sql+" "+"WHERE DATE > ";
   sql=sql+"(";
   sql=sql+" "+"SELECT";
   sql=sql+" "+"CAST(REPLACE(REPLACE(REPLACE(DATE,'.',''),' ',''),':','') AS INT)";
   sql=sql+" "+"FROM TESTER_CONFIG";
   sql=sql+")";
   sql=sql+" "+"ORDER BY";
   sql=sql+" "+"DATE";
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
   for(int i=0; DatabaseRead(request); i++) // リクエストのレコードの数だけ繰り返す
     {
      if(!DatabaseColumnLong(request,0,lngDate))
        {
         Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
         DatabaseFinalize(request);
         DatabaseClose(request);
         return NULL;
        }
     }
// 作成されたリクエストを削除
   DatabaseFinalize(request);

// DBを閉じる
   DatabaseClose(request);
   return CnvDouble2Time((double)lngDate);
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadHistoryCandle(STR_LB_HISTORY_CANDLE &io_data[],ENUM_TIMEFRAMES in_tf,datetime dtFrom,datetime dtTo)
  {
//---
   ArrayFree(io_data);
//---
   string strTableName="CANDLE";
//--- open the database in the folder
   int db=DatabaseOpen(T_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"DATE";
   sql=sql+","+"CND_OP";
   sql=sql+","+"CND_HG";
   sql=sql+","+"CND_LW";
   sql=sql+","+"CND_CL";
   sql=sql+" "+"FROM "+strTableName;
   sql=sql+" "+"WHERE SYMBOL = "+"'"+_Symbol+"'";
   sql=sql+" "+"AND PERIOD = "+"'"+EnumToString(in_tf)+"'";
   sql=sql+" "+"AND DATE BETWEEN "+(string)(long)CnvTime2Double(dtFrom)+" "+"AND"+" "+(string)(long)CnvTime2Double(dtTo);
   sql=sql+" "+"ORDER BY DATE DESC";
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//--- print all entries with the salary greater than 15000
   STR_LB_HISTORY_CANDLE wk_data;
   for(int i=0; DatabaseReadBind(request, wk_data); i++)
     {
      ArrayResize(io_data,i+1);
      io_data[i]=wk_data;
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadHistorySpanModel(STR_LB_HISTORY_SPANMODEL &io_data[],ENUM_TIMEFRAMES in_tf,datetime dtFrom,datetime dtTo)
  {
//---
   ArrayFree(io_data);
//---
   string strTableName="SPAN_MODEL";
//--- open the database in the folder
   int db=DatabaseOpen(T_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"DATE";
   sql=sql+","+"BLUE_SPAN";
   sql=sql+","+"RED_SPAN";
   sql=sql+","+"BLUE_SPAN_L";
   sql=sql+","+"RED_SPAN_L";
   sql=sql+","+"CHIKOUSPAN_26";
   sql=sql+" "+"FROM "+strTableName;
   sql=sql+" "+"WHERE PERIOD = "+"'"+EnumToString(in_tf)+"'";
   sql=sql+" "+"AND DATE BETWEEN "+(string)(long)CnvTime2Double(dtFrom)+" "+"AND"+" "+(string)(long)CnvTime2Double(dtTo);
   sql=sql+" "+"ORDER BY DATE DESC";
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//--- print all entries with the salary greater than 15000
   STR_LB_HISTORY_SPANMODEL wk_data;
   for(int i=0; DatabaseReadBind(request, wk_data); i++)
     {
      ArrayResize(io_data,i+1);
      io_data[i]=wk_data;
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadHistorySuperBollinger(STR_LB_HISTORY_SUPERBOLLINGER &io_data[],ENUM_TIMEFRAMES in_tf,datetime dtFrom,datetime dtTo)
  {
//---
   ArrayFree(io_data);
//---
   string strTableName="SUPER_BOLLINGER";
//--- open the database in the folder
   int db=DatabaseOpen(T_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"DATE";
   sql=sql+","+"MIDDLE";
   sql=sql+","+"UP_1_SIGMA";
   sql=sql+","+"LW_1_SIGMA";
   sql=sql+","+"UP_2_SIGMA";
   sql=sql+","+"LW_2_SIGMA";
   sql=sql+","+"UP_3_SIGMA";
   sql=sql+","+"LW_3_SIGMA";
   sql=sql+","+"CHIKOUSPAN_21";
   sql=sql+","+"OV_2_SIGMA";
   sql=sql+","+"CS_DIR";
   sql=sql+","+"CS_AT_UP";
   sql=sql+","+"CS_AT_DW";
   sql=sql+" "+"FROM "+strTableName;
   sql=sql+" "+"WHERE PERIOD = "+"'"+EnumToString(in_tf)+"'";
   sql=sql+" "+"AND DATE BETWEEN "+(string)(long)CnvTime2Double(dtFrom)+" "+"AND"+" "+(string)(long)CnvTime2Double(dtTo);
   sql=sql+" "+"ORDER BY DATE DESC";
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//--- print all entries with the salary greater than 15000
   STR_LB_HISTORY_SUPERBOLLINGER wk_data;
   for(int i=0; DatabaseReadBind(request, wk_data); i++)
     {
      ArrayResize(io_data,i+1);
      io_data[i]=wk_data;
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadHistorySpanAutoSignal(STR_LB_HISTORY_SAS &io_data[],ENUM_TIMEFRAMES in_tf,datetime dtFrom,datetime dtTo)
  {
//---
   ArrayFree(io_data);
//---
   string strTableName="SPAN_AUTO_SIGNAL";
//--- open the database in the folder
   int db=DatabaseOpen(T_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"DATE";
   sql=sql+","+"SM_SIGNAL";
   sql=sql+","+"SM_TERM";
   sql=sql+","+"SM_UP_GRACE_ST";
   sql=sql+","+"SM_UP_GRACE_ED";
   sql=sql+","+"SM_DW_GRACE_ST";
   sql=sql+","+"SM_DW_GRACE_ED";
   sql=sql+","+"SM_GRACE_HIGH";
   sql=sql+","+"SM_GRACE_LOW";
   sql=sql+","+"SM_SIGNAL_ED";
   sql=sql+","+"RS_SIGNAL";
   sql=sql+","+"RS_TERM";
   sql=sql+","+"RS_UP_GRACE_ST";
   sql=sql+","+"RS_UP_GRACE_ED";
   sql=sql+","+"RS_DW_GRACE_ST";
   sql=sql+","+"RS_DW_GRACE_ED";
   sql=sql+","+"RS_GRACE_HIGH";
   sql=sql+","+"RS_GRACE_LOW";
   sql=sql+","+"RS_SIGNAL_ED";
   sql=sql+","+"HIGH_52";
   sql=sql+","+"LOW_52";
   sql=sql+","+"Y_HIGH";
   sql=sql+","+"Y_LOW";
   sql=sql+" "+"FROM "+strTableName;
   sql=sql+" "+"WHERE PERIOD = "+"'"+EnumToString(in_tf)+"'";
   sql=sql+" "+"AND DATE BETWEEN "+(string)(long)CnvTime2Double(dtFrom)+" "+"AND"+" "+(string)(long)CnvTime2Double(dtTo);
   sql=sql+" "+"ORDER BY DATE DESC";
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//--- print all entries with the salary greater than 15000
   STR_LB_HISTORY_SAS wk_data;
   for(int i=0; DatabaseReadBind(request, wk_data); i++)
     {
      ArrayResize(io_data,i+1);
      io_data[i]=wk_data;
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadResults(STR_LB_RESULTS &io_data[],ulong ticket_no=0)
  {
//---
   ArrayFree(io_data);
//--- open the database in the folder
   int db=DatabaseOpen(T_DB_NAME, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"DATE";
   sql=sql+","+"LOTS";
   sql=sql+","+"ORDER_TYPE";
   sql=sql+","+"OPEN_TYPE";
   sql=sql+","+"OP_PRICE";
   sql=sql+","+"TICKET_NO";
   sql=sql+","+"LIMIT_TYPE";
   sql=sql+","+"LIMIT_TF";
   sql=sql+","+"STOP_TYPE";
   sql=sql+","+"STOP_TF";
   sql=sql+","+"PROFIT";
   sql=sql+" "+"FROM RESULTS";
   if(ticket_no!=0)
      sql=sql+" "+"WHERE TICKET_NO = "+(string)ticket_no;
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", T_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//--- print all entries with the salary greater than 15000
   STR_LB_RESULTS wk_data;
   for(int i=0; DatabaseReadBind(request, wk_data); i++)
     {
      ArrayResize(io_data,i+1);
      io_data[i]=wk_data;
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  }

//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool DeleteResults(ulong ticket)
  {
//---
   string sql[1];
   sql[0]="DELETE FROM RESULTS";
   sql[0]=sql[0]+" "+"WHERE TICKET_NO="+(string)ticket;
   if(!DBExecute(sql,T_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadTesterDate(MqlDateTime &dt)
  {
//---
   string strDateTime=NULL;
   string filename=T_DB_NAME;
//--- open the database in the folder
   int db=DatabaseOpen(filename, DATABASE_OPEN_READONLY);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", filename, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string sql="SELECT";
   sql=sql+" "+"DATE";
   sql=sql+" "+"FROM TESTER_CONFIG";
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", filename, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//---
   for(int i=0; DatabaseRead(request); i++)
     {
      //--- read the values of each field from the obtained entry
      if(DatabaseColumnText(request, 0, strDateTime))
        {
         datetime wkDt=StringToTime(strDateTime);
         if(!TimeToStruct(wkDt,dt))
            return false;
        }
      else
        {
         Print(i, ": DatabaseRead() failed with code ", GetLastError());
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  };
//+------------------------------------------------------------------+
//| Sql実行
//+------------------------------------------------------------------+
bool DBExecuteTester(string &sql[])
  {
   string strTableName=T_DB_NAME;
   int intSqlCount=ArraySize(sql);
//--- create or open the database in the common terminal folder
   int db=DatabaseOpen(strTableName, DATABASE_OPEN_READWRITE | DATABASE_OPEN_CREATE);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", strTableName, " open failed with code ", GetLastError());
      return false;
     }
   if(!DatabaseTransactionBegin(db))
     {
      Print("DB: ", strTableName, " begin transaction failed with code ", GetLastError());
      return false;
     }
   for(int i=0; i<ArraySize(sql); i++)
     {
      if(!DatabaseExecute(db,sql[i]))
        {
         Print("DB: ", strTableName, " execute failed with code ", GetLastError()," ",sql[i]);
         DatabaseClose(db);
         return false;
        }
      Comment(i,"/",intSqlCount);
     }
   if(!DatabaseTransactionCommit(db))
     {
      Print("DB: ", strTableName, " commit failed with code ", GetLastError());
      return false;
     }
   DatabaseClose(db);
   return true;
  }
//+------------------------------------------------------------------+
