//+------------------------------------------------------------------+
//|                                                 TradigTester.mqh |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
enum ENUM_BUF_INDEX
  {
   TT_BLUE_SPAN
   ,TT_RED_SPAN
   ,TT_BLUE_SPAN_L
   ,TT_RED_SPAN_L
   ,TT_CHIKOUSPAN_26
   ,TT_MIDDLE
   ,TT_UP_1_SIGMA
   ,TT_LW_1_SIGMA
   ,TT_UP_2_SIGMA
   ,TT_LW_2_SIGMA
   ,TT_UP_3_SIGMA
   ,TT_LW_3_SIGMA
   ,TT_CHIKOUSPAN_21
   ,TT_OV_2_SIGMA
   ,TT_CS_DIR
   ,TT_CS_AT_UP
   ,TT_CS_AT_DW
   ,TT_SM_SIGNAL
   ,TT_SM_TERM
   ,TT_SM_UP_GRACE_ST
   ,TT_SM_UP_GRACE_ED
   ,TT_SM_DW_GRACE_ST
   ,TT_SM_DW_GRACE_ED
   ,TT_SM_GRACE_HIGH
   ,TT_SM_GRACE_LOW
   ,TT_SM_SIGNAL_ED
   ,TT_RS_SIGNAL
   ,TT_RS_TERM
   ,TT_RS_UP_GRACE_ST
   ,TT_RS_UP_GRACE_ED
   ,TT_RS_DW_GRACE_ST
   ,TT_RS_DW_GRACE_ED
   ,TT_RS_GRACE_HIGH
   ,TT_RS_GRACE_LOW
   ,TT_RS_SIGNAL_ED
   ,TT_HIGH_52
   ,TT_LOW_52
   ,TT_Y_HIGH
   ,TT_Y_LOW
   ,TT_CND_OP
   ,TT_CND_HG
   ,TT_CND_LW
   ,TT_CND_CL
  };