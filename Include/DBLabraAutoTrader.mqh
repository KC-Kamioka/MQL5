//+------------------------------------------------------------------+
//|                                            DBLabraAutoTrader.mqh |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#define T_DB_NAME       "TradingTester"
//+------------------------------------------------------------------+
//| Include                                                          |
//+------------------------------------------------------------------+
#include <DBCommon.mqh>
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
bool InitTradeHistory()
  {
//---
   string strTableName="TRADE_HISTORY";
   string sql[1];
   sql[0]="DROP TABLE IF EXISTS "+strTableName;
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   sql[0]="CREATE TABLE IF NOT EXISTS "+strTableName;
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+" "+"POSITION_ID INT";
   sql[0]=sql[0]+","+"POSITION_OPEN_TIME TEXT";
   sql[0]=sql[0]+","+"POSITION_TYPE TEXT";
   sql[0]=sql[0]+","+"POSITION_MAGIC_NO INT";
   sql[0]=sql[0]+","+"POSITION_MAGIC TEXT";
   sql[0]=sql[0]+","+"PERSPECTIVE_H4 TEXT";
   sql[0]=sql[0]+","+"PERSPECTIVE_D1 TEXT";
   sql[0]=sql[0]+","+"POSITION_CLOSE_TIME TEXT";
   sql[0]=sql[0]+","+"POSITION_CLOSE_REASON_IDX INT";
   sql[0]=sql[0]+","+"POSITION_CLOSE_REASON TEXT";
   sql[0]=sql[0]+","+"PROFIT INT";
   sql[0]=sql[0]+","+"LOSS INT";
   sql[0]=sql[0]+","+"PRIMARY KEY(POSITION_ID)";
   sql[0]=sql[0]+")";
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   return true;
  }
  /*
//+------------------------------------------------------------------+
//| result
//+------------------------------------------------------------------+
void DBInsertOpen(ENUM_POSITION_IDX in_intPosIdx)
  {
   if(!MQLInfoInteger(MQL_TESTER))
      return;
//---
   string sql[1];
   for(int i=0; i<PositionsTotal(); i++)
     {
      //---
      ulong ticket;
      //--- リスト内の位置によって注文を返す
      if((ticket=PositionGetTicket(i))<=0)
         continue;
      if(!PositionSelectByTicket(ticket))
         continue;
      if(PositionGetInteger(POSITION_MAGIC)!=lgMagicNos[in_intPosIdx])
         continue;
      //---
      string strPerspectives[];
      string strComment = PositionGetString(POSITION_COMMENT);
      GetPerspectives(strComment,strPerspectives);
      //---
      sql[0]="INSERT INTO TRADE_HISTORY";
      sql[0]=sql[0]+"("+"POSITION_ID";
      sql[0]=sql[0]+","+"POSITION_OPEN_TIME";
      sql[0]=sql[0]+","+"POSITION_TYPE";
      sql[0]=sql[0]+","+"POSITION_MAGIC_NO";
      sql[0]=sql[0]+","+"POSITION_MAGIC";
      sql[0]=sql[0]+","+"PERSPECTIVE_H4";
      sql[0]=sql[0]+","+"PERSPECTIVE_D1";
      sql[0]=sql[0]+")"+"VALUES";
      sql[0]=sql[0]+"("+(string)PositionGetInteger(POSITION_IDENTIFIER);
      sql[0]=sql[0]+","+"'"+(string)(datetime)PositionGetInteger(POSITION_TIME)+"'";
      sql[0]=sql[0]+","+"'"+EnumToString((ENUM_POSITION_TYPE)PositionGetInteger(POSITION_TYPE))+"'";
      sql[0]=sql[0]+","+(string)PositionGetInteger(POSITION_MAGIC);
      sql[0]=sql[0]+","+"'"+EnumToString((ENUM_POSITION_IDX)ArrayBsearch(lgMagicNos,PositionGetInteger(POSITION_MAGIC)))+"'";
      sql[0]=sql[0]+","+"'"+EnumToString((ENM_SB_STATUS)strPerspectives[0])+"'";
      sql[0]=sql[0]+","+"'"+EnumToString((ENM_SB_STATUS)strPerspectives[1])+"'";
      sql[0]=sql[0]+")";
      if(!DBExecute(sql,LA_DB_NAME))
         return;
     }
  }
  */
//+------------------------------------------------------------------+
//| result
//+------------------------------------------------------------------+
void DBUpdateClose(ulong ticket,ENUM_EXIT_REASON reason)
  {
//---
   if(!MQLInfoInteger(MQL_TESTER))
      return;
//---
   if(!HistorySelectByPosition(ticket))
      return;
   string sql[1];
   sql[0]="UPDATE TRADE_HISTORY SET";
   sql[0]=sql[0]+" "+"POSITION_CLOSE_TIME = "+"'"+(string)(datetime)HistoryDealGetInteger(ticket,DEAL_TIME)+"'";
   sql[0]=sql[0]+","+"POSITION_CLOSE_REASON_IDX = "+(string)reason;
   sql[0]=sql[0]+","+"POSITION_CLOSE_REASON = "+"'"+EnumToString(reason)+"'";
   sql[0]=sql[0]+" "+"WHERE POSITION_ID = "+(string)(string)HistoryDealGetInteger(ticket,DEAL_POSITION_ID);
   if(!DBExecute(sql,LA_DB_NAME))
      return;
  }
  /*
//+------------------------------------------------------------------+
//| result
//+------------------------------------------------------------------+
void DBInsertResult(datetime dtFrom)
  {
   if(!MQLInfoInteger(MQL_TESTER))
      return;
//--- 取引履歴をリクエストする
   HistorySelect(dtFrom,TimeCurrent());
//---
   string sql[];
   int deals=HistoryDealsTotal();
   for(int i=0; i<deals; i++)
     {
      //---
      ulong ticket;
      //--- リスト内の位置によって注文を返す
      if((ticket=HistoryDealGetTicket(i))<=0)
         continue;
      if(HistoryDealGetInteger(ticket,DEAL_TYPE)==DEAL_TYPE_BALANCE)
         continue;
      if(HistoryDealGetInteger(ticket,DEAL_ENTRY)==DEAL_ENTRY_IN)
        {
         //---
         string strPerspectives[];
         string strComment = HistoryDealGetString(ticket,DEAL_COMMENT);
         GetPerspectives(strComment,strPerspectives);
         //---
         ArrayResize(sql,i+1);
         sql[i]="INSERT OR IGNORE INTO TRADE_HISTORY";
         sql[i]=sql[i]+"("+"POSITION_ID";
         sql[i]=sql[i]+","+"POSITION_OPEN_TIME";
         sql[i]=sql[i]+","+"POSITION_TYPE";
         sql[i]=sql[i]+","+"POSITION_MAGIC_NO";
         sql[i]=sql[i]+","+"POSITION_MAGIC";
         sql[i]=sql[i]+","+"PERSPECTIVE_H4";
         sql[i]=sql[i]+","+"PERSPECTIVE_D1";
         sql[i]=sql[i]+")"+"VALUES";
         sql[i]=sql[i]+"("+(string)HistoryDealGetInteger(ticket,DEAL_POSITION_ID);
         sql[i]=sql[i]+","+"'"+(string)(datetime)HistoryDealGetInteger(ticket,DEAL_TIME)+"'";
         sql[i]=sql[i]+","+"'"+EnumToString((ENUM_POSITION_TYPE)HistoryDealGetInteger(ticket,DEAL_TYPE))+"'";
         sql[i]=sql[i]+","+(string)HistoryDealGetInteger(ticket,DEAL_MAGIC);
         sql[i]=sql[i]+","+"'"+EnumToString((ENUM_POSITION_IDX)ArrayBsearch(lgMagicNos,HistoryDealGetInteger(ticket,DEAL_MAGIC)))+"'";
         sql[i]=sql[i]+","+"'"+EnumToString((ENM_SB_STATUS)strPerspectives[0])+"'";
         sql[i]=sql[i]+","+"'"+EnumToString((ENM_SB_STATUS)strPerspectives[1])+"'";
         sql[i]=sql[i]+")";
        }
      if(HistoryDealGetInteger(ticket,DEAL_ENTRY)==DEAL_ENTRY_OUT)
        {
         ArrayResize(sql,i+1);
         ENUM_EXIT_REASON reason=TAKE_PROFIT;
         double dblProfit=HistoryDealGetDouble(ticket,DEAL_PROFIT);
         double dblLoss=0.0;
         if(dblProfit<=0)
           {
            reason=STOP_LOSS;
            dblProfit=0.0;
            dblLoss=MathAbs(HistoryDealGetDouble(ticket,DEAL_PROFIT));
           }
         sql[i]="UPDATE TRADE_HISTORY SET";
         sql[i]=sql[i]+" "+"POSITION_CLOSE_TIME = "+"'"+(string)(datetime)HistoryDealGetInteger(ticket,DEAL_TIME)+"'";
         sql[i]=sql[i]+","+"POSITION_CLOSE_REASON_IDX = ";
         sql[i]=sql[i]+"("+"CASE WHEN POSITION_CLOSE_REASON_IDX IS NULL THEN";
         sql[i]=sql[i]+" "+(string)reason;
         sql[i]=sql[i]+" "+"ELSE POSITION_CLOSE_REASON_IDX END";
         sql[i]=sql[i]+")";
         sql[i]=sql[i]+","+"POSITION_CLOSE_REASON = ";
         sql[i]=sql[i]+"("+"CASE WHEN POSITION_CLOSE_REASON IS NULL THEN";
         sql[i]=sql[i]+" "+"'"+EnumToString(reason)+"'";
         sql[i]=sql[i]+" "+"ELSE POSITION_CLOSE_REASON END";
         sql[i]=sql[i]+")";
         sql[i]=sql[i]+","+"PROFIT = "+(string)(int)dblProfit;
         sql[i]=sql[i]+","+"LOSS = "+(string)(int)dblLoss;
         sql[i]=sql[i]+" "+"WHERE POSITION_ID = "+(string)HistoryDealGetInteger(ticket,DEAL_POSITION_ID);
        }
     }
   if(!DBExecute(sql,LA_DB_NAME))
      return;
  }
  */
//+------------------------------------------------------------------+
//| split
//+------------------------------------------------------------------+
void GetPerspectives(string in_strComment,string &result[])
  {
   string sep=",";               // 区切り文字
   ushort u_sep;                 // 区切り文字のコード
//--- 区切り文字のコードを取得する
   u_sep=StringGetCharacter(sep,0);
//--- 文字列を部分文字列に分ける
   StringSplit(in_strComment,u_sep,result);
  }
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
bool InitEventHistory()
  {
//---
   string strTableName="EVENT_CALENDER";
   string sql[1];
   sql[0]="DROP TABLE IF EXISTS "+strTableName;
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   sql[0]="CREATE TABLE IF NOT EXISTS "+strTableName;
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+" "+"EVENT_ID LONG";
   sql[0]=sql[0]+","+"EVENT_CURRENCY TEXT";
   sql[0]=sql[0]+","+"EVENT_TIME DATETIME";
   sql[0]=sql[0]+","+"EVENT_NAME TEXT";
   sql[0]=sql[0]+","+"PRIMARY KEY(EVENT_ID)";
   sql[0]=sql[0]+")";
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| result
//+------------------------------------------------------------------+
void InsertEventCalender(STR_EVENT &io_ev[])
  {
//---
   string sql[];
   for(int i=0; i<ArraySize(io_ev); i++)
     {
      //---
      string strDateTime=NULL;
      MqlDateTime dt;
      TimeToStruct((datetime)io_ev[i].time,dt);
      //---
      if(io_ev[i].currency=="EUR")
        {
         if(!IsLondonDST(dt))
            dt.hour=dt.hour-1;
         strDateTime=(string)StructToTime(dt);
        }
      //---
      if(io_ev[i].currency=="USD")
        {
         if(!IsNewYorkDST(dt))
            dt.hour=dt.hour-1;
         strDateTime=(string)StructToTime(dt);
        }
      StringReplace(strDateTime,".","-");
      //---
      ArrayResize(sql,i+1);
      sql[i]="INSERT OR IGNORE INTO EVENT_CALENDER";
      sql[i]=sql[i]+"("+"EVENT_ID";
      sql[i]=sql[i]+","+"EVENT_CURRENCY";
      sql[i]=sql[i]+","+"EVENT_TIME";
      sql[i]=sql[i]+","+"EVENT_NAME";
      sql[i]=sql[i]+")"+"VALUES";
      sql[i]=sql[i]+"("+(string)io_ev[i].id;
      sql[i]=sql[i]+","+"'"+io_ev[i].currency+"'";
      sql[i]=sql[i]+","+"'"+strDateTime+"'";
      sql[i]=sql[i]+","+"'"+io_ev[i].name+"'";
      sql[i]=sql[i]+")";
     }
//---
   if(!DBExecute(sql,LA_DB_NAME))
      return;
  }
//+------------------------------------------------------------------+
//|データ読み込み
//+------------------------------------------------------------------+
bool ReadEvents(STR_EVENT &io_data[],datetime dt_from,datetime dt_to)
  {
//---
   ResetLastError();
   ArrayFree(io_data);
//--- open the database in the folder
   int db=DatabaseOpen(LA_DB_NAME, DATABASE_OPEN_READONLY | DATABASE_OPEN_COMMON);
   if(db==INVALID_HANDLE)
     {
      Print("DB: ", LA_DB_NAME, " open failed with code ", GetLastError());
      return false;
     }
//--- create a query and get a handle for it
   string from=(string)dt_from;
   string to=(string)dt_to;
   StringReplace(from,".","-");
   StringReplace(to,".","-");
   string sql="SELECT";
   sql=sql+" "+"EVENT_ID";
   sql=sql+","+"EVENT_CURRENCY";
   sql=sql+","+"EVENT_TIME";
   sql=sql+","+"EVENT_NAME";
   sql=sql+" "+"FROM EVENT_CALENDER";
   sql=sql+" "+"WHERE EVENT_TIME BETWEEN "+"'"+from+"'"+" AND "+"'"+to+"'";
   int request=DatabasePrepare(db, sql);
   if(request==INVALID_HANDLE)
     {
      Print("DB: ", LA_DB_NAME, " request failed with code ", GetLastError()," ",sql);
      DatabaseClose(db);
      return false;
     }
//--- print all entries with the salary greater than 15000
   STR_EVENT wk_data;
   for(int i=0; DatabaseReadBind(request, wk_data); i++)
     {
      if(GetLastError()!=0)
        {
         Print("DB: ", LA_DB_NAME, " DatabaseReadBind failed with code ", GetLastError()," ",sql);
         DatabaseFinalize(request);
         DatabaseClose(db);
         return false;
        }
      ArrayResize(io_data,i+1);
      io_data[i]=wk_data;
     }
//--- remove the query after use
   DatabaseFinalize(request);
//--- close the database
   DatabaseClose(db);
   return true;
  }
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool IsNewYorkDST(MqlDateTime &dt)
  {
   int today = (dt.year * 10000) + (dt.mon * 100) + dt.day;
   int start = 0;
   int end = 0;
   if(dt.year >= 2007)
     {
      MqlDateTime dt0314;
      TimeToStruct((datetime)(IntegerToString(dt.year) + ".3.14"), dt0314);

      MqlDateTime dt1107;
      TimeToStruct((datetime)(IntegerToString(dt.year) + ".11.7"), dt1107);

      start = (dt.year * 10000) + (3 * 100) + (14 - dt0314.day_of_week);
      end = (dt.year * 10000) + (11 * 100) + (7 - dt1107.day_of_week);
     }
   else
     {
      MqlDateTime dt0407;
      TimeToStruct((datetime)(IntegerToString(dt.year) + ".4.7"), dt0407);

      MqlDateTime dt1031;
      TimeToStruct((datetime)(IntegerToString(dt.year) + ".10.31"), dt1031);

      start = (dt.year * 10000) + (4 * 100) + (7 - dt0407.day_of_week);
      end = (dt.year * 10000) + (10 * 100) + (31 - dt1031.day_of_week);
     }

   return(today >= start && today < end);
  }
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool IsLondonDST(MqlDateTime &dt)
  {
   int today = (dt.year * 10000) + (dt.mon * 100) + dt.day;
   int start = 0;
   int end = 0;

   MqlDateTime dt0331;
   TimeToStruct((datetime)(IntegerToString(dt.year) + ".3.31"), dt0331);

   MqlDateTime dt1031;
   TimeToStruct((datetime)(IntegerToString(dt.year) + ".10.31"), dt1031);

   start = (dt.year * 10000) + (3 * 100) + (31 - dt0331.day_of_week);
   end = (dt.year * 10000) + (10 * 100) + (31 - dt1031.day_of_week);

   return(today >= start && today < end);
  }
//+------------------------------------------------------------------+
