//+------------------------------------------------------------------+
//|                                                    BFACommon.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <BBFullAuto.mqh>
  /*
//+------------------------------------------------------------------+
//| Buyポジションを開く関数                                          |
//+------------------------------------------------------------------+
void ReversalOrderModify(ulong magic, double price) export
  {
//---
   for(int i=OrdersTotal()-1;i>=0;i--)
     {
      ulong ticket=OrderGetTicket(i);
      if(ticket==0)
         continue;
      if(magic == OrderGetInteger(ORDER_MAGIC))
         trade.OrderModify(ticket,price,0,0,ORDER_TIME_GTC,0,0);
     }
//---
   for(int i=PositionsTotal()-1;i>=0;i--)
     {
      ulong ticket=PositionGetTicket(i);
      if(ticket==0)
         continue;
      if(magic == PositionGetInteger(POSITION_MAGIC))
        {
         double open = PositionGetDouble(POSITION_PRICE_OPEN);
         double sl = 0;
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY)
            sl = open-sl_point*_Point;
         if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL)
            sl = open+sl_point*_Point;
         if(sl != PositionGetDouble(POSITION_SL))
            ModifyPosition(ticket,sl);
        }
     }
  }
//+------------------------------------------------------------------+
//| Buyポジションを開く関数                                          |
//+------------------------------------------------------------------+
void ModifyPosition(ulong ticket, double sl) export
  {
//---
   if(!trade.PositionModify(ticket,sl,0))
      Print("Error modified position: ", GetLastError());
   else
      Print("modified opened successfully.");
  }
//+------------------------------------------------------------------+
//| Buyポジションを開く関数                                          |
//+------------------------------------------------------------------+
void OrderBuyPosition(pos_info &pi[], ulong magic, double price, double sl = 0) export
  {
//---
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      if(pi[i].magic == magic)
         return;
     }
//---
   for(int i=OrdersTotal()-1;i>=0;i--)
     {
      ulong ticket=OrderGetTicket(i);
      if(ticket==0)
         continue;
      if(magic == OrderGetInteger(ORDER_MAGIC))
         trade.OrderDelete(ticket);
     }
//---
   trade.SetExpertMagicNumber(magic);
//---
   double askPrice = SymbolInfoDouble(_Symbol, SYMBOL_ASK);
   if(sl == 0)
      sl = askPrice-sl_point*_Point;
   double tp = 0;
//---
   if(!trade.BuyStop(lot_size,price,_Symbol,sl,tp,ORDER_TIME_GTC,0,(string)magic))
      Print("Error order buy position: ", GetLastError());
   else
      Print("Buy order opened successfully.");
  }
//+------------------------------------------------------------------+
//| Sellポジションを開く関数                                         |
//+------------------------------------------------------------------+
void OrderSellPosition(pos_info &pi[], ulong magic, double price, double sl = 0) export
  {
//---
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      if(pi[i].magic == magic)
         return;
     }
//---
   for(int i=OrdersTotal()-1;i>=0;i--)
     {
      ulong ticket=OrderGetTicket(i);
      if(ticket==0)
         continue;
      if(magic == OrderGetInteger(ORDER_MAGIC))
         trade.OrderDelete(ticket);
     }
//---
   trade.SetExpertMagicNumber(magic);
//---
   double bidPrice = SymbolInfoDouble(_Symbol, SYMBOL_BID);
   if(sl == 0)
      sl = bidPrice+sl_point*_Point;
   double tp = 0;
   if(!trade.SellStop(lot_size,price,_Symbol,sl,tp,ORDER_TIME_GTC,0,(string)magic))
      Print("Error order sell position: ", GetLastError());
   else
      Print("Sell order opened successfully.");
  }
*/
//+------------------------------------------------------------------+
