//+------------------------------------------------------------------+
//|                                                  BFAReversal.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <BBFullAuto.mqh>
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OpenBBReversal(pos_info &pi[]) export
  {
//---
   double preHigh = values_lower[1].high+SymbolInfoInteger(_Symbol,SYMBOL_SPREAD)*_Point;
   double preLow = values_lower[1].low-SymbolInfoInteger(_Symbol,SYMBOL_SPREAD)*_Point;
//---
   ReversalOrderModify(BB_REVERSAL_B_1_SIGMA_EXIT,preHigh);
   ReversalOrderModify(BB_REVERSAL_B_CENTER_EXIT,preHigh);
   ReversalOrderModify(BB_REVERSAL_S_1_SIGMA_EXIT,preLow);
   ReversalOrderModify(BB_REVERSAL_S_CENTER_EXIT,preLow);
//---
   if(values_upper[1].rsi < 40 || values_upper[1].rsi > 60)
      return;
//---
   if(values_lower[1].rsi < 30)
     {
      if(values_lower[1].bb_miuns_2_sigma > values_lower[1].close)
        {
         OrderBuyPosition(pi,BB_REVERSAL_B_1_SIGMA_EXIT,preHigh);
         OrderBuyPosition(pi,BB_REVERSAL_B_CENTER_EXIT,preHigh);
        }
     }
//     }
//---
   if(values_lower[1].rsi > 70)
     {
      if(values_lower[1].bb_plus_2_sigma < values_lower[1].close)
        {
         OrderSellPosition(pi,BB_REVERSAL_S_1_SIGMA_EXIT,preLow);
         OrderSellPosition(pi,BB_REVERSAL_S_CENTER_EXIT,preLow);
        }
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void TakeProfitBBReversal(pos_info &pi[]) export
  {
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      if(pi[i].magic == BB_REVERSAL_B_1_SIGMA_EXIT)
        {
         if(values_lower[0].bb_plus_2_sigma < values_lower[0].close)
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_REVERSAL_S_1_SIGMA_EXIT)
        {
         if(values_lower[0].bb_miuns_2_sigma > values_lower[0].close)
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_REVERSAL_B_CENTER_EXIT)
        {
         if(values_lower[0].bb_center < values_lower[0].close)
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_REVERSAL_S_CENTER_EXIT)
        {
         if(values_lower[0].bb_center > values_lower[0].close)
            PositionCloseByTicket(pi[i].ticket);
        }
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void LosscutBBReversal(pos_info &pi[]) export
  {
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      if(pi[i].magic == BB_REVERSAL_B_1_SIGMA_EXIT)
        {
         if(ReversalBuyExit())
            PositionCloseByTicket(pi[i].ticket);
         if(AboveUpper1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
         if(Below2Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_REVERSAL_S_1_SIGMA_EXIT)
        {
         if(ReversalSellExit())
            PositionCloseByTicket(pi[i].ticket);
         if(BelowLower1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
         if(Above2Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_REVERSAL_B_CENTER_EXIT)
        {
         if(ReversalBuyExit())
            PositionCloseByTicket(pi[i].ticket);
         if(AboveUpper1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
         if(Below2Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_REVERSAL_S_CENTER_EXIT)
        {
         if(ReversalSellExit())
            PositionCloseByTicket(pi[i].ticket);
         if(BelowLower1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
         if(Above2Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
        }
     }
  }
//+------------------------------------------------------------------+
