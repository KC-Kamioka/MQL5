//+------------------------------------------------------------------+
//|                                               BFABreak2Sigma.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <BFACommon.mqh>
#include <BBFullAuto.mqh>
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OpenBBBreak2Sigma(
   pos_info &pi[],
   values_struct &values_lower[],
   values_struct &max_value_lower,
   values_struct &min_value_lower
) export
  {
//---
   if(values_lower[2].rsi <= 60 && 60 < values_lower[1].rsi)
     {
      int highest_bar = iHighest(_Symbol,tf_lower,MODE_HIGH,bb_period,2);
      if(values_lower[highest_bar].high < values_lower[1].close)
        {
         if(Break2SigmaUp(tf_lower))
           {
            OpenBuyPosition(pi,BB_BREAK_B_1_SIGMA_EXIT);
            OpenBuyPosition(pi,BB_BREAK_B_1_TRAILING);
           }
        }
     }
//---
   if(values_lower[2].rsi >= 40 && 40 < values_lower[1].rsi)
     {
      int lowest_bar = iLowest(_Symbol,tf_lower,MODE_LOW,bb_period,2);
      if(values_lower[lowest_bar].low > values_lower[1].close)
        {
         if(Break2SigmaDown(tf_lower))
           {
            OpenSellPosition(pi,BB_BREAK_S_1_SIGMA_EXIT);
            OpenSellPosition(pi,BB_BREAK_S_1_TRAILING);
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void LosscutBBB(
   pos_info &pi[],
   values_struct &values_lower[],
   values_struct &max_value_lower,
   values_struct &min_value_lower
) export
  {
//---
   double preHigh = values_lower[1].high+SymbolInfoInteger(_Symbol,SYMBOL_SPREAD)*_Point;
   double preLow = values_lower[1].low-SymbolInfoInteger(_Symbol,SYMBOL_SPREAD)*_Point;
//---
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      if(pi[i].magic == BB_BREAK_B_1_SIGMA_EXIT)
        {
         if(Above2Sigma(tf_lower))
            BreakEven(pi[i].ticket,values_lower[1].low);
         if(BelowUpper1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_BREAK_S_1_SIGMA_EXIT)
        {
         if(Below2Sigma(tf_lower))
            BreakEven(pi[i].ticket,values_lower[1].low);
         if(AboveLower1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_BREAK_B_1_TRAILING)
        {
         if(BelowUpper1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
         if(max_value_lower.rsi > 70 || StopTrailing(pi[i].ticket))
            ModifyPosition(pi[i].ticket,preLow);
        }
      if(pi[i].magic == BB_BREAK_S_1_TRAILING)
        {
         if(AboveLower1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
         if(min_value_lower.rsi < 30 || StopTrailing(pi[i].ticket))
            ModifyPosition(pi[i].ticket,preHigh);
        }
     }
  }
//+------------------------------------------------------------------+
