//+------------------------------------------------------------------+
//|                                                    BFAReturn.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <BBFullAuto.mqh>
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OpenBBReturn(pos_info &pi[]) export
  {
//---
   if(values_upper[1].rsi < 30)
     {
      if(Return1SigmaUp(tf_lower))
        {
         if(values_lower[1].bb_plus_1_sigma > values_lower[1].close)
           {
            OpenBuyPosition(pi,BB_RETURN_B_1_SIGMA_EXIT);
            OpenBuyPosition(pi,BB_RETURN_B_CENTER_EXIT);
           }
        }
     }
//---
   if(values_upper[1].rsi > 70)
     {
      if(Return1SigmaDown(tf_lower))
        {
         if(values_lower[1].bb_miuns_1_sigma < values_lower[1].close)
           {
            OpenSellPosition(pi,BB_RETURN_S_1_SIGMA_EXIT);
            OpenSellPosition(pi,BB_RETURN_S_CENTER_EXIT);
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void TakeProfitBBReturn(pos_info &pi[]) export
  {
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      if(pi[i].magic == BB_RETURN_B_1_SIGMA_EXIT)
        {
         if(values_lower[0].bb_plus_2_sigma < values_lower[0].close)
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_RETURN_S_1_SIGMA_EXIT)
        {
         if(values_lower[0].bb_miuns_2_sigma > values_lower[0].close)
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_RETURN_B_CENTER_EXIT)
        {
         if(values_lower[0].bb_center < values_lower[0].close)
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_RETURN_S_CENTER_EXIT)
        {
         if(values_lower[0].bb_center > values_lower[0].close)
            PositionCloseByTicket(pi[i].ticket);
        }
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void LosscutBBReturn(pos_info &pi[]) export
  {
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      if(pi[i].magic == BB_RETURN_B_1_SIGMA_EXIT)
        {
         if(ReversalBuyExit())
            PositionCloseByTicket(pi[i].ticket);
         if(AboveUpper1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
         if(Below2Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_RETURN_S_1_SIGMA_EXIT)
        {
         if(ReversalSellExit())
            PositionCloseByTicket(pi[i].ticket);
         if(BelowLower1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
         if(Above2Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_RETURN_B_CENTER_EXIT)
        {
         if(ReversalBuyExit())
            PositionCloseByTicket(pi[i].ticket);
         if(AboveUpper1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
         if(Below2Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == BB_RETURN_S_CENTER_EXIT)
        {
         if(ReversalSellExit())
            PositionCloseByTicket(pi[i].ticket);
         if(BelowLower1Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
         if(Above2Sigma(tf_lower))
            PositionCloseByTicket(pi[i].ticket);
        }
     }
  }
//+------------------------------------------------------------------+
