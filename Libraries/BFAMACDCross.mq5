//+------------------------------------------------------------------+
//|                                                 BFAMACDCross.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <BBFullAuto.mqh>
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OpenMACDCross(pos_info &pi[]) export
  {
//---
   if(AboveCloud(tf_upper))
     {
      if(50 < values_lower[1].rsi && values_lower[1].rsi < 70)
        {
         if(values_lower[2].bb_center < values_lower[1].bb_center)
           {
            if(CloudUp(tf_lower))
              {
               if(MACDGoldenCross())
                  OpenBuyPosition(pi,MACD_CROSS_B);
              }
           }
        }
     }
//---
   if(UnderCloud(tf_upper))
     {
      if(30 < values_lower[1].rsi && values_lower[1].rsi < 50)
        {
         if(values_lower[2].bb_center > values_lower[1].bb_center)
           {
            if(CloudDown(tf_lower))
              {
               if(MACDDeadCross())
                  OpenSellPosition(pi,MACD_CROSS_S);
              }
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void LosscutMACDCross(pos_info &pi[]) export
  {
//---
   double preHigh = values_lower[1].high+SymbolInfoInteger(_Symbol,SYMBOL_SPREAD)*_Point;
   double preLow = values_lower[1].low-SymbolInfoInteger(_Symbol,SYMBOL_SPREAD)*_Point;
//---
   for(int i=ArraySize(pi)-1;i>=0;i--)
     {
      if(pi[i].magic == MACD_CROSS_B)
        {
         if(values_lower[1].macd_main < values_lower[1].macd_signal)
            PositionCloseByTicket(pi[i].ticket);
        }
      if(pi[i].magic == MACD_CROSS_S)
        {
         if(values_lower[1].macd_main > values_lower[1].macd_signal)
            PositionCloseByTicket(pi[i].ticket);
        }
     }
  }
//+------------------------------------------------------------------+
