//+------------------------------------------------------------------+
//|                                       CreateEconomicCalendar.mq5 |
//|                                  Copyright 2024, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2024, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
   // SQLiteファイル名
   string dbName = "EconomicEvents.sqlite";
   string tableName = "Events";

   // DBを開く、なければ新規作成
   int handle = DatabaseOpen(dbName, DATABASE_OPEN_COMMON | DATABASE_OPEN_READWRITE | DATABASE_OPEN_CREATE);
   if(handle == INVALID_HANDLE)
     {
      Print("DB: ", dbName, " 新規作成 エラーコード: ", GetLastError());
      return;
     }

   // テーブルが既に存在する場合は削除
   if(DatabaseTableExists(handle, tableName))
     {
      if(!DatabaseExecute(handle, "DROP TABLE " + tableName))
        {
         Print("テーブルの削除に失敗しました。テーブル: ", tableName, " エラーコード: ", GetLastError());
         DatabaseClose(handle);
         return;
        }
     }

   // テーブルを新規作成
   if(!DatabaseExecute(handle, "CREATE TABLE " + tableName + "("
                            "Currency        TEXT NOT NULL, "
                            "EventName       TEXT NOT NULL, "
                            "EventTime       DATETIME NOT NULL, "
                            "Importance      INTEGER NOT NULL);"))
     {
      Print("テーブルの作成に失敗しました。エラーコード: ", GetLastError());
      DatabaseClose(handle);
      return;
     }

   //--- 影響のある通貨のリスト
   string currencies[] = {"GBP", "JPY", "USD", "EUR"}; // 必要に応じて拡張

   MqlCalendarEvent events[];
   datetime date_from = D'01.01.2022';  // 2022年1月1日
   datetime date_to = TimeCurrent();                    // 本日

   for(int c = 0; c < ArraySize(currencies); c++)
     {
      // 各通貨のイベントを取得
      int count = CalendarEventByCurrency(currencies[c], events);
      for(int i = 0; i < count; i++)
        {
         // 高重要度イベントに絞る
         if(events[i].importance != CALENDAR_IMPORTANCE_HIGH)
            continue;

         MqlCalendarValue values[];
         if(CalendarValueHistoryByEvent(events[i].id, values, date_from, date_to))
           {
            for(int j = 0; j < ArraySize(values); j++)
              {
               MqlDateTime tm;
               TimeToStruct(values[j].time, tm);
               if(tm.hour == 0 && tm.min == 0)
                  continue; // 日付だけのイベントを除外

               // DBにイベントデータを挿入
               string sql = "INSERT INTO " + tableName + " (Currency, EventName, EventTime, Importance) VALUES ('" +
                            currencies[c] + "', '" + events[i].name + "', '" + TimeToString(values[j].time, TIME_DATE | TIME_MINUTES) + "', " + IntegerToString(events[i].importance) + ");";
               if(!DatabaseExecute(handle, sql))
                 {
                  Print("データの挿入に失敗しました。エラーコード: ", GetLastError());
                  DatabaseClose(handle);
                  return;
                 }
              }
           }
        }
     }

   // DBを閉じる
   DatabaseClose(handle);
  }
//+------------------------------------------------------------------+
