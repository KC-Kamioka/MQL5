import flet as ft
import DBUtils as db

def main(page: ft.Page):
    page.title = "Trade Panel"

    # ウィンドウサイズの設定

    #
    control_width = 220
    page.vertical_alignment = ft.MainAxisAlignment.START

    # 各 Dropdown のインスタンスを変数に保存
    default = db.fetch_default()
    symbol_dropdown = ft.Dropdown(
        label="Symbol",
        width=control_width,
        options=[ft.dropdown.Option(key=rec['Name'], text=rec['Name']) for rec in db.fetch_code_master("symbol")],
        value=default['Symbol'],
        autofocus=True,
    )
    period_dropdown = ft.Dropdown(
        label="Period",
        width=control_width,
        options=[ft.dropdown.Option(key=rec['Code'], text=rec['Name']) for rec in db.fetch_code_master("period")],
        value=default['PeriodCd'],
        autofocus=True,
    )
    open_dropdown = ft.Dropdown(
        label="Open",
        width=control_width,
        options=[ft.dropdown.Option(key=rec['Code'], text=rec['Name']) for rec in db.fetch_code_master("opLogic")],
        value=default['OpenCd'],
        autofocus=True,
    )
    tp_dropdown = ft.Dropdown(
        label="TakeProfit",
        width=control_width,
        options=[ft.dropdown.Option(key=rec['Code'], text=rec['Name']) for rec in db.fetch_code_master("tpLogic")],
        value=default['TakeProfitCd'],
        autofocus=True,
    )
    sl_dropdown = ft.Dropdown(
        label="StopLoss",
        width=control_width,
        options=[ft.dropdown.Option(key=rec['Code'], text=rec['Name']) for rec in db.fetch_code_master("slLogic")],
        value=default['StopLossCd'],
        autofocus=True,
    )
    lot_input_item = ft.TextField(
        label="Lot",
        text_align=ft.TextAlign.END,
        width=control_width,
        value=default['LotSize'],
        input_filter=ft.InputFilter( # 事前に文字パターン設定
            allow=True, regex_string=r"[0-9.]+$", replacement_string=""
        ),
    )
    tp_input_item = ft.TextField(
        label="Tp",
        text_align=ft.TextAlign.END,
        width=control_width,
        value=(int)(default['TpPoint']),
        input_filter=ft.InputFilter( # 事前に文字パターン設定
            allow=True, regex_string=r"[0-9]+$", replacement_string=""
        ),
    )
    sl_input_item = ft.TextField(
        label="Sl",
        text_align=ft.TextAlign.END,
        width=control_width,
        value=(int)(default['SlPoint']),
        input_filter=ft.InputFilter( # 事前に文字パターン設定
            allow=True, regex_string=r"[0-9]+$", replacement_string=""
        ),
    )
    add_button = ft.FilledButton(
        text="Add",
        on_click=lambda e: open_add_window(e),
        visible=True
    )
    add_container = ft.Container(
        content=ft.Column(
            controls=[  # controlsでアイテムをリスト形式で指定
                ft.Row(
                    controls=[  # Row内のアイテムもリストで指定
                        symbol_dropdown,
                        period_dropdown,
                        lot_input_item,
                    ],
                    alignment=ft.MainAxisAlignment.START,
                ),
                ft.Row(
                    controls=[  # Row内のアイテムもリストで指定
                        open_dropdown,
                        tp_dropdown,
                        sl_dropdown,
                    ],
                    alignment=ft.MainAxisAlignment.START,
                ),
                ft.Row(
                    controls=[  # Row内のアイテムもリストで指定
                        tp_input_item,
                        sl_input_item,
                    ],
                    alignment=ft.MainAxisAlignment.START,
                ),
                ft.Row(
                    [
                        ft.FilledButton(
                            text="Save",
                            on_click=lambda e: save_new_record(e),
                        ),
                        ft.FilledButton(
                            text="Cansel",
                            on_click=lambda e: cancel(e),
                        ),
                    ],
                    alignment=ft.MainAxisAlignment.END,
                ),
            ],
            tight=True,  # 必要に応じて間隔調整
            width=1240,
        ),
        visible=False  # 見える/見えないの制御
    )
    data_table = ft.DataTable(
        columns=[],
        rows=[],
        visible=False
    )

    # DataTableの内容を更新する関数
    def update_data_table():
        records = db.fetch_data()
        if len(records) > 0:
            # レコードがある場合にのみ、テーブルの内容を更新
            data_table.columns = [
                ft.DataColumn(ft.Text(key)) for key in records[0].keys()
            ] + [
                ft.DataColumn(ft.Text("Action"))
            ]
            data_table.rows = [
                ft.DataRow(
                    cells=[ 
                        ft.DataCell(ft.Text(value)) for value in row.values()
                    ] + [
                        ft.DataCell(
                            ft.Row(
                                [
                                    ft.IconButton(
                                        icon=ft.Icons.EDIT,
                                        icon_color="gray",
                                        on_click=lambda e, seq=row['Seq']: open_edit_window(seq),
                                    ),
                                    ft.IconButton(
                                        icon=ft.Icons.DELETE,
                                        icon_color="red",
                                        on_click=lambda e, seq=row['Seq']: handle_delete_click(seq),
                                    ),
                                ],
                                alignment=ft.MainAxisAlignment.START,
                            )
                        )
                    ]
                )
                for row in records
            ]
            page.window.width = 1410
            page.window.height = 640
            data_table.visible = True
        else:
            # データがない場合、仮のカラムを追加してエラーを回避
            data_table.columns = [ft.DataColumn(ft.Text("No Data Available"))]
            data_table.rows = []
            data_table.visible = False
            page.window.width = 720
            page.window.height = 340
            add_button.visible = False
            add_container.visible = True
        # 更新処理を行う
        page.update()


    # 初回のDataCards更新
    update_data_table()

    # 成行取引
    def handle_market_click(bs,symbol,lot,tp,sl):
        print("Saving new record...")
        db.insert_data_market(bs,symbol,lot,tp,sl)
        update_data_table()  # テーブルを再更新

    # データ削除時の処理
    def handle_delete_click(seq):
        print(f"Deleting record with Seq: {seq}")
        db.delete_data(seq)  # `delete_data` を TradeDB に実装してください
        update_data_table()  # テーブルを再更新

    # リフレッシュボタンの処理
    def handle_refresh_click(e):
        print("Refreshing data table.")
        update_data_table()

    # 新しいレコードを保存する処理
    def cancel(dialog):
        add_button.visible = True
        add_container.visible = False
        data_table.visible = True
        dialog.open = False
        update_data_table()

    # モーダルウィンドウを開く処理
    def open_add_window(e):
        add_button.visible = False
        add_container.visible = True
        data_table.visible = False
        page.update()

    # モーダルウィンドウを開く処理 (編集)
    def open_edit_window(seq):
        # 編集モードで、レコードの情報を入力フォームにセット
        edit_rec = db.fetch_one(seq)
        tp_dropdown.value = edit_rec['TakeProfitCd']
        sl_dropdown.value = edit_rec['StopLossCd']
        tp_input_item.value = edit_rec['TpPoint']
        sl_input_item.value = edit_rec['SlPoint']

        dialog = ft.AlertDialog(
            modal=True,
            title=ft.Text("Update Record"),
            content=ft.Column(
                [
                    tp_dropdown,
                    sl_dropdown,
                    tp_input_item,
                    sl_input_item,
                ],
                tight=True,
            ),
            actions=[
                ft.TextButton("Cancel", on_click=lambda e: cancel(dialog)),
                ft.TextButton(
                    "Save",
                    on_click=lambda e: edit_record(dialog, seq, tp_dropdown.value, sl_dropdown.value, tp_input_item.value, sl_input_item.value),
                ),
            ],
        )
        page.overlay.append(dialog)
        dialog.open = True
        page.update()

    # 新しいレコードを保存する処理
    def save_new_record(dialog):
        print("Saving new record...")

        selected_symbol = symbol_dropdown.value
        selected_period = period_dropdown.value
        input_lot = lot_input_item.value
        input_tp = tp_input_item.value
        input_sl = sl_input_item.value
        selected_open = open_dropdown.value
        selected_tp = tp_dropdown.value
        selected_sl = sl_dropdown.value

        # テーブルに登録
        db.insert_data(selected_symbol,selected_period, input_lot, input_tp, input_sl,selected_open,selected_tp,selected_sl)

        # 初期設定更新
        db.update_default(selected_symbol, selected_period, input_lot, input_tp, input_sl, selected_open, selected_tp, selected_sl)
        dialog.open = False
        add_button.visible = True
        add_container.visible = False
        data_table.visible = True
        update_data_table()

    # 編集されたレコードを保存する処理
    def edit_record(dialog, seq, tp_logic, sl_logic, tp, sl):
        print(seq)
        print("Edit record...")

        # テーブルに更新
        db.update_data(seq, tp_logic, sl_logic, tp, sl)

        dialog.open = False
        update_data_table()

    # ページにコンポーネントを追加
    page.add(
        ft.Column(
            [
                ft.Row(
                    [
                        ft.FilledButton(
                            text="Buy",
                            color = "white",
                            bgcolor = "blue",
                            on_click=lambda e: handle_market_click(1,symbol_dropdown.value,lot_input_item.value,tp_input_item.value,sl_input_item.value),
                        ),
                        ft.FilledButton(
                            text="Sell",
                            color = "white",
                            bgcolor = "red",
                            on_click=lambda e: handle_market_click(2,symbol_dropdown.value,lot_input_item.value,tp_input_item.value,sl_input_item.value),
                        ),
                        add_button,
                        ft.IconButton(
                            icon=ft.Icons.REFRESH,
                            on_click=handle_refresh_click,
                        ),
                    ],
                    alignment=ft.MainAxisAlignment.START,
                ),
                add_container,
                data_table,
            ],
            alignment=ft.MainAxisAlignment.CENTER,
        ),
    )

ft.app(target=main)
