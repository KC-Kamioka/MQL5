import PlotResult as pr
import matplotlib.pyplot as plt
import numpy as np
import MetaTrader5 as mt5
import pandas as pd
import mplfinance as mpf
from datetime import timedelta

# シンボル名
symbol = "EURUSD"

def GetRates(rate_from,rate_to):
    # MT5接続
    if not mt5.initialize():
        print("initialize() failed")
        mt5.shutdown()

    # 開始時間と終了時間
    start_time = pd.Timestamp(rate_from)
    end_time = pd.Timestamp(rate_to)

    # 日足データを取得
    rates = mt5.copy_rates_range(symbol, mt5.TIMEFRAME_D1, start_time, end_time)

    # MT5切断
    mt5.shutdown()

    return pd.DataFrame(rates)

def show_graph_chart(df,ax):
    period = 21

    # DATE列の最大値と最小値を取得
    max_date = pd.to_datetime(df['DATE'].max())
    min_date = pd.to_datetime(df['DATE'].min()) - timedelta(days=period)
    rates_frame = GetRates(min_date,max_date)

    # 日時をインデックスに設定
    rates_frame['time'] = pd.to_datetime(rates_frame['time'], unit='s')
    rates_frame = rates_frame.set_index('time')

    # ボリンジャーバンドの計算
    ma = rates_frame['close'].rolling(window=period).mean()
    std = rates_frame['close'].rolling(window=period).std()
    std_dev = 1
    upper_band_1 = ma + (std_dev * std)
    lower_band_1 = ma - (std_dev * std)
    std_dev = 2
    upper_band_2 = ma + (std_dev * std)
    lower_band_2 = ma - (std_dev * std)

    # ローソク足チャートにボリンジャーバンドを追加
    add_plot = [
        mpf.make_addplot(ma, color='g', ax=ax),
        mpf.make_addplot(upper_band_1, color='g', ax=ax),
        mpf.make_addplot(lower_band_1, color='g', ax=ax),
        mpf.make_addplot(upper_band_2, color='g', ax=ax),
        mpf.make_addplot(lower_band_2, color='g', ax=ax)
    ]

    #
    mpf.plot(rates_frame, type='candle', style='charles', volume=False, addplot=add_plot, xlim=(rates_frame.index[period], rates_frame.index[-1]),ax=ax)

def set_graph_1(df,axs):
    #
    axs.set_title("Time Series")
        
    df = df.set_index("DATE")
    df["PL"]=df["PROFIT"]-df["LOSS"]
    df["PL"]=df["PL"].cumsum()

    # 棒グラフの描画
    axs.bar(df.index, df['PROFIT'], color='b', alpha=0.5)
    axs.bar(df.index, df['LOSS']*-1, color='r', alpha=0.5)
    # 線グラフの描画
    axs.plot(df.index, df['PL'], color='g')
    
    # 軸の設定
    axs.grid(linestyle='dotted')
    axs.tick_params(axis='x', rotation=45)


def set_graph_2(pos_type,pspctv,df,axs):
    #
    axs.set_title(pos_type+" "+pspctv)
    
    df = df[(df["POSITION_TYPE"] == pos_type)]
    df = df.set_index(pspctv)
    df = df.groupby(df.index)[['PROFIT', 'LOSS']].sum()

    # 棒グラフの描画
    axs.bar(df.index, df['PROFIT'], color='b', alpha=0.5)
    axs.bar(df.index, df['LOSS']*-1, color='r', alpha=0.5)
    
    # 軸の設定
    axs.grid(linestyle='dotted')
    axs.tick_params(axis='x', rotation=45)

def show_graph_1(df):
    # matplotlibの描画領域の作成
    fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(16, 8),sharey=True)
    #
    set_graph_1(df,axs[0])
    #
    show_graph_chart(df,axs[1])
    #
    fig.suptitle("Trade Result", fontsize=14)
    fig.tight_layout()
    #
    plt.show()

def show_graph_2(df,title):
    # matplotlibの描画領域の作成
    fig, axs = plt.subplots(nrows=1, ncols=4, figsize=(16, 8),sharey=True)
    #
    #set_graph_1(df,axs[0,0])
    #
    set_graph_2("POSITION_TYPE_BUY","PERSPECTIVE_H4",df.copy(),axs[0])
    set_graph_2("POSITION_TYPE_SELL","PERSPECTIVE_H4",df.copy(),axs[1])
    set_graph_2("POSITION_TYPE_BUY","PERSPECTIVE_D1",df.copy(),axs[2])
    set_graph_2("POSITION_TYPE_SELL","PERSPECTIVE_D1",df.copy(),axs[3])
    #set_graph_3("POSITION_TYPE_BUY",df.copy(),axs[1])
    #set_graph_3("POSITION_TYPE_SELL",df.copy(),axs[2])
    #
    #axs[1].sharey(axs[2])
    #
    fig.suptitle(title, fontsize=14)
    fig.tight_layout()
    #
    plt.show()

def set_graph_3(pos_type,df,ax):

    df = df[(df["POSITION_TYPE"] == pos_type)]
    df = df.assign(LOSS=df['LOSS'] * -1)

    # ピボットテーブルを作成する
    pivot_p = pd.pivot_table(df, values='PROFIT', index='PERSPECTIVE_D1', columns='PERSPECTIVE_H4',fill_value=0)
    pivot_l = pd.pivot_table(df, values='LOSS', index='PERSPECTIVE_D1', columns='PERSPECTIVE_H4',fill_value=0)

    # 両方のグラフに共通するラベルを作成
    pivot_p_bar = pivot_p.plot.bar(stacked=True, ax=ax)
    pivot_l_bar = pivot_l.plot.bar(stacked=True, ax=ax)

    # 両方のグラフに共通する凡例を作成する
    handles, labels = pivot_p_bar.get_legend_handles_labels()
    pivot_l_bar.legend(handles[len(pivot_p.columns):], labels[len(pivot_p.columns):], loc='upper left')
    pivot_p_bar.legend(handles[:len(pivot_p.columns)], labels[:len(pivot_p.columns)], loc='upper right')
    
    ax.set_title(pos_type)
    ax.tick_params(axis='x', rotation=45)
    ax.grid(linestyle='dotted')

#df = pr.get_trade_history()
#show_graph(df,"test")