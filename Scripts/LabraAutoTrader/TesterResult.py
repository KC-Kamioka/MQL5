import tkinter as tk
import tkinter.ttk as ttk
import PlotResult as pr
import ShowGraph as sg
import pandas as pd

deal_types = ('DEAL_TYPE_BUY','DEAL_TYPE_SELL')

class Application(tk.Frame):
    def exit_application(self):
        self.master.destroy()
        quit()  # スクリプトを終了する

    def on_tree_time_series_click(self, event, tree):
        text = []
        item = tree.focus() # 選択されたアイテムを取得する
        text.append(tree.item(item, 'text'))
        is_exists_parent = True
        while is_exists_parent:
            item = tree.parent(item)
            if tree.item(item, 'text') == '':
                is_exists_parent = False
            else:
                text.append(tree.item(item, 'text'))
        #
        title = None
        if(len(text)==1):
            df = pr.get_trade_history()
            df = df[df['POSITION_MAGIC'] == text[0]]
            title = text[0]
        if(len(text)==2):
            df = pr.get_trade_history()
            df = df[(df['YEAR'] == text[0]) & (df['POSITION_MAGIC'] == text[1])]
            title = text[0]+" "+text[1]
        if(len(text)==3):
            df = pr.get_trade_history()
            df = df[(df['MONTH'] == text[0]) & (df['YEAR'] == text[1]) & (df['POSITION_MAGIC'] == text[2])]
            title = text[1]+"-"+text[0]+" "+text[2]
        #sg.show_graph_1(df)
        sg.show_graph_2(df,title)

    def insert_tree_time_series(self,tree):
        #
        def add_profit_factor(key,df):
            df = df.groupby(key)[['PROFIT', 'LOSS', 'COUNT']].sum()
            df['PROFIT_FACTOR']=(df['PROFIT']/df['LOSS'].abs()).round(2)
            return df
        
        # データ挿入
        df = pr.get_trade_history()
        #
        df_magic = add_profit_factor('POSITION_MAGIC',df)
        for row in df_magic.itertuples():
            magic = row.Index
            item1 = tree.insert("", "end", text=row.Index, values=(row.PROFIT,row.LOSS,row.COUNT,row.PROFIT_FACTOR))
            #
            df_year = df[df['POSITION_MAGIC'] == magic]
            df_year = add_profit_factor('YEAR',df_year)
            for row in df_year.itertuples():
                year = row.Index
                item2 = tree.insert(item1, "end", text=row.Index, values=(row.PROFIT,row.LOSS,row.COUNT,row.PROFIT_FACTOR))
                #
                df_month = df[(df['POSITION_MAGIC'] == magic) & (df['YEAR'] == year)]
                df_month = add_profit_factor('MONTH',df_month)
                for row in df_month.itertuples():
                    item3 = tree.insert(item2, "end", text=row.Index, values=(row.PROFIT,row.LOSS,row.COUNT,row.PROFIT_FACTOR))
        """
        df_year_month = add_profit_factor('YEAR_MONTH',df)
        for row in df_year_month.itertuples():
            year_month = row.Index
            item1 = tree.insert("", "end", text=row.Index, values=(row.PROFIT,row.LOSS,row.COUNT,row.PROFIT_FACTOR))
            #
            df_magic = df[df['YEAR_MONTH'] == year_month]
            df_magic = add_profit_factor('POSITION_MAGIC',df_magic)
            for row in df_magic.itertuples():
                month = row.Index
                item2 = tree.insert(item1, "end", text=row.Index, values=(row.PROFIT,row.LOSS,row.COUNT,row.PROFIT_FACTOR))
                #
                df_magic = df[(df['YEAR'] == year) & (df['MONTH'] == month)]
                df_magic = add_profit_factor('POSITION_MAGIC',df_magic)
                for row in df_magic.itertuples():
                    magic = row.Index
                    item3 = tree.insert(item2, "end", text=row.Index, values=(row.PROFIT,row.LOSS,row.COUNT,row.PROFIT_FACTOR))
                    #
                    df_pspctv = df[(df['YEAR'] == year) & (df['MONTH'] == month) & (df['POSITION_MAGIC'] == magic)]
                    df_pspctv = add_profit_factor('PERSPECTIVE',df_pspctv)
                    for row in df_pspctv.itertuples():
                        pspctv = row.Index
                        item4 = tree.insert(item3, "end", text=row.Index, values=(row.PROFIT,row.LOSS,row.COUNT,row.PROFIT_FACTOR))
                        #
                        df_pos_type = df[(df['YEAR'] == year) & (df['MONTH'] == month) & (df['POSITION_MAGIC'] == magic) & (df['PERSPECTIVE'] == pspctv)]
                        df_pos_type = add_profit_factor('POSITION_TYPE',df_pos_type)
                        for row in df_pos_type.itertuples():
                            item5 = tree.insert(item4, "end", text=row.Index, values=(row.PROFIT,row.LOSS,row.COUNT,row.PROFIT_FACTOR))
            for row2 in df_monthly.itertuples():
                child1_item = tree.insert(parent_item, "end", text=row2.Index, values=(row2.PROFIT,row2.LOSS,row2.COUNT,row2.PROFIT_FACTOR))
            #
            df_monthly = pr.get_pf_nest2(row1.Index)
            for row2 in df_monthly.itertuples():
                child1_item = tree.insert(parent_item, "end", text=row2.Index, values=(row2.PROFIT,row2.LOSS,row2.COUNT,row2.PROFIT_FACTOR))
            #
                df_magic = pr.get_pf_nest3(row1.Index,row2.Index)
                for row3 in df_magic.itertuples():
                    child2_item = tree.insert(child1_item, "end", text=row3.Index, values=(row3.PROFIT,row3.LOSS,row3.COUNT,row3.PROFIT_FACTOR))
                    """

    def insert_tree_by_method(self,tree):
        # データ挿入
        df = pr.get_profit_factor_method()
        for row1 in df.itertuples():
            parent_item = tree.insert("", "end", text=row1.Index, values=(row1.PROFIT,row1.LOSS,row1.COUNT,row1.PROFIT_FACTOR))
            #
            df_pspctv = pr.get_profit_factor_perspective(row1.Index)
            for row2 in df_pspctv.itertuples():
                child1_item = tree.insert(parent_item, "end", text=row2.Index, values=(row2.PROFIT,row2.LOSS,row2.COUNT,row2.PROFIT_FACTOR))
        
               
    def create_tree(self,tree):
        
        # 列を作る
        tree["column"] = (1, 2, 3, 4)

        # ヘッダーテキスト
        tree.heading(1, text="PROFIT")
        tree.heading(2, text="LOSS")
        tree.heading(3, text="COUNT")
        tree.heading(4, text="PROFIT_FACTOR")

        # 列の幅
        tree.column(1, width=100, anchor=tk.E)
        tree.column(2, width=100, anchor=tk.E)
        tree.column(3, width=100, anchor=tk.E)
        tree.column(4, width=100, anchor=tk.E)

        #データ初期化
        tree.delete(*tree.get_children())
                
    def __init__(self, master=None):
        
        super().__init__(master)
        self.master = master

        # ttk.Styleオブジェクトを生成する
        style = ttk.Style()
        # style.configureメソッドで設定を変更する
        style.configure("Treeview", background="black", fieldbackground="black", foreground="white")

        start,end=pr.get_result_date()
        self.master.title('LabraAutoTrader Tester Result'+" "+"( "+start+" - "+end+" )")

        #-----------------------------------------------

        # 配置用フレーム
        frame = tk.Frame(self.master)
        frm_ctl = tk.Frame(self.master)
        #frm_graph = tk.Frame(frame)
        frm_tree = tk.Frame(frame)
        # フレームをウィンドウに配置
        frame.pack(side=tk.TOP,fill=tk.BOTH,expand=True)
        frm_ctl.pack(side=tk.TOP,anchor=tk.W)
        frm_tree.pack(side=tk.TOP,fill=tk.BOTH,expand=True,padx=5,pady=5)

        #
        lbfrm_trees = []
        self.trees = []
        lbfrm_trees.append(tk.LabelFrame(frm_tree,text="TimeSeries"))
        #lbfrm_trees.append(tk.LabelFrame(frm_tree,text="Method"))
        for lbfrm in lbfrm_trees:
            lbfrm.pack(side=tk.LEFT, fill=tk.BOTH,expand=True,padx=5,pady=5)
            self.trees.append(ttk.Treeview(lbfrm,height=30))
        for tree in self.trees:
            tree.pack(side=tk.TOP, fill=tk.BOTH,expand=True,padx=5,pady=5)
            self.create_tree(tree)
        #
        self.insert_tree_time_series(self.trees[0])
        self.trees[0].bind("<Double-1>", lambda event: self.on_tree_time_series_click(event, self.trees[0]))

        #
        #self.insert_tree_by_method(self.trees[1])
        #self.trees[1].bind("<Double-1>", lambda event: self.on_tree_by_method_click(event, self.trees[1]))

root = tk.Tk()
app = Application(master=root)
app.mainloop()