import MetaTrader5 as mt5
import pandas as pd
import mplfinance as mpf

def ShowDailyChart():
    rates_frame = GetRates('2021-01-01','2021-03-01')

    # 日時をインデックスに設定
    rates_frame['time'] = pd.to_datetime(rates_frame['time'], unit='s')
    rates_frame = rates_frame.set_index('time')

    # ボリンジャーバンドの計算
    period = 21
    ma = rates_frame['close'].rolling(window=period).mean()
    std = rates_frame['close'].rolling(window=period).std()
    std_dev = 1
    upper_band_1 = ma + (std_dev * std)
    lower_band_1 = ma - (std_dev * std)
    std_dev = 2
    upper_band_2 = ma + (std_dev * std)
    lower_band_2 = ma - (std_dev * std)

    # ローソク足チャートにボリンジャーバンドを追加
    add_plot = [
        mpf.make_addplot(ma, color='g'),
        mpf.make_addplot(upper_band_1, color='g'),
        mpf.make_addplot(lower_band_1, color='g'),
        mpf.make_addplot(upper_band_2, color='g'),
        mpf.make_addplot(lower_band_2, color='g')
    ]

    mpf.plot(rates_frame, type='candle', style='charles', volume=False, figsize=(12,6), title=symbol + ' Daily Chart', addplot=add_plot, xlim=(rates_frame.index[period], rates_frame.index[-1]))
