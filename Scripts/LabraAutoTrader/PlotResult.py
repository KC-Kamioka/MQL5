import os
import pandas as pd
import sqlite3

os.chdir(os.path.dirname(os.path.abspath(__file__)))
DB_PATH = "../../Files/LabraAutoTrader.sqlite"

"""
with sqlite3.connect(DB_PATH) as conn:
    sql="DROP TABLE IF EXISTS PROFIT_LOSS_BY_DAY"
    conn.execute(sql)
    sql="CREATE TABLE PROFIT_LOSS_BY_DAY AS"
    sql=sql+" "+"SELECT"
    sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-')) AS POSITION_OPEN_TIME"
    sql=sql+","+"POSITION_MAGIC"
    sql=sql+","+"SUM(CASE WHEN DEAL_PROFIT>=0 THEN DEAL_PROFIT ELSE 0 END) AS PROFIT"
    sql=sql+","+"SUM(CASE WHEN DEAL_PROFIT<0 THEN DEAL_PROFIT ELSE 0 END) AS LOSS"
    sql=sql+","+"SUM(DEAL_PROFIT) AS PL"
    sql=sql+" "+"FROM TRADE_HISTORY"
    sql=sql+" "+"GROUP BY"
    sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-'))"
    sql=sql+","+"POSITION_MAGIC"
    sql=sql+" "+"ORDER BY"
    sql=sql+" "+"POSITION_OPEN_TIME"
    sql=sql+","+"POSITION_MAGIC"
    conn.execute(sql)

def get_pl_by_perspective():
    method='OPEN_TREND_LIGHTLY'
    with sqlite3.connect(DB_PATH) as conn:
        #
        sql="SELECT DISTINCT"
        sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-')) AS POSITION_OPEN_TIME"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"POSITION_OPEN_TIME"
        df = pd.read_sql(sql, conn)
        df = df.set_index('POSITION_OPEN_TIME')
        #
        sql="SELECT DISTINCT"
        sql=sql+" "+"PERSPECTIVE"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"PERSPECTIVE_NO"
        df_master = pd.read_sql(sql, conn)
        for row in df_master.itertuples():
            sql="SELECT"
            sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-')) AS POSITION_OPEN_TIME"
            sql=sql+","+"SUM(DEAL_PROFIT) AS "+row.PERSPECTIVE
            sql=sql+" "+"FROM TRADE_HISTORY"
            sql=sql+" "+"WHERE PERSPECTIVE = '"+row.PERSPECTIVE+"'"
            sql=sql+" "+"AND DEAL_PROFIT >= 0"
            sql=sql+" "+"AND POSITION_MAGIC = '"+method+"'"
            sql=sql+" "+"AND POSITION_TYPE = 'POSITION_TYPE_BUY'"
            sql=sql+" "+"GROUP BY"
            sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-'))"
            sql=sql+" "+"ORDER BY"
            sql=sql+" "+"POSITION_OPEN_TIME"
            df_child = pd.read_sql(sql, conn)
            df_child = df_child.set_index('POSITION_OPEN_TIME')
            df = df.join(df_child, how='left')
        df = df.fillna(0)
        return df
    
def get_master():
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT DISTINCT"
        sql=sql+" "+"POSITION_MAGIC"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"POSITION_MAGIC_NO"
        df = pd.read_sql(sql, conn)
    return df
    
def get_perspective():
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT DISTINCT"
        sql=sql+" "+"PERSPECTIVE"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"PERSPECTIVE_NO"
        df = pd.read_sql(sql, conn)
    return df

def get_profit(magic,perspective,pos_type):
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-')) AS POSITION_OPEN_TIME"
        sql=sql+","+"SUM(PROFIT) AS PROFIT"
        sql=sql+","+"SUM(LOSS) AS LOSS"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"WHERE POSITION_TYPE = '"+pos_type+"'"
        if(magic != ''):
            sql=sql+" "+"AND POSITION_MAGIC = '"+magic+"'"
        if(perspective != ''):
            sql=sql+" "+"AND PERSPECTIVE = '"+perspective+"'"
        sql=sql+" "+"GROUP BY"
        sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-'))"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"POSITION_OPEN_TIME"
        df = pd.read_sql(sql, conn)
        df = df.dropna()
        df = df.set_index("POSITION_OPEN_TIME")
    return df

def get_profit_by_pos_type(pos_type):
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"PERSPECTIVE"
        sql=sql+","+"POSITION_MAGIC"
        sql=sql+","+"PROFIT+(LOSS*-1) AS PL"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"WHERE POSITION_TYPE = '"+pos_type+"'"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"POSITION_MAGIC_NO"
        df = pd.read_sql(sql, conn)
        df = df.dropna()
    return df

def get_profit_by_perspective(perspective,pos_type):
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"POSITION_MAGIC"
        sql=sql+","+"PROFIT+(LOSS*-1) AS PL"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"WHERE POSITION_TYPE = '"+pos_type+"'"
        sql=sql+" "+"AND PERSPECTIVE = '"+perspective+"'"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"POSITION_MAGIC_NO"
        df = pd.read_sql(sql, conn)
        df = df.dropna()
    return df
#
def get_win(magic,POSITION_TYPE):
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"PERSPECTIVE"
        sql=sql+","+"COUNT(*) AS TRADE_COUNT"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"AND POSITION_TYPE = '"+POSITION_TYPE+"'"
        sql=sql+" "+"WHERE POSITION_MAGIC = '"+magic+"'"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"PERSPECTIVE"
        df = pd.read_sql(sql, conn)
    return df
#
def get_pl(magic,POSITION_TYPE):
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"A.PERSPECTIVE"
        sql=sql+","+"B.POSITION_TYPE"
        sql=sql+","+"SUM(CASE WHEN B.DEAL_PROFIT>=0 THEN B.DEAL_PROFIT ELSE 0 END) AS PROFIT"
        sql=sql+","+"SUM(CASE WHEN B.DEAL_PROFIT<0 THEN B.DEAL_PROFIT ELSE 0 END) AS LOSS"
        sql=sql+" "+"FROM"
        sql=sql+"("+"SELECT DISTINCT"
        sql=sql+" "+"PERSPECTIVE_NO"
        sql=sql+","+"PERSPECTIVE"
        sql=sql+","+"POSITION_MAGIC"
        sql=sql+","+"POSITION_TYPE"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+")"+"A"
        sql=sql+" "+"LEFT JOIN TRADE_HISTORY B"
        sql=sql+" "+"ON A.PERSPECTIVE_NO = B.PERSPECTIVE_NO"
        sql=sql+" "+"AND A.POSITION_TYPE = B.POSITION_TYPE"
        sql=sql+" "+"AND A.POSITION_TYPE = '"+POSITION_TYPE+"'"
        sql=sql+" "+"WHERE A.POSITION_MAGIC = '"+magic+"'"
        sql=sql+" "+"GROUP BY"
        sql=sql+" "+"A.PERSPECTIVE_NO"
        sql=sql+","+"B.POSITION_TYPE"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"A.PERSPECTIVE_NO"
        sql=sql+","+"B.POSITION_TYPE"
        df = pd.read_sql(sql, conn)
    return df
#
def get_trade_count(magic,pos_type):
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"A.PERSPECTIVE"
        sql=sql+","+"COUNT(*) AS T_COUNT"
        sql=sql+" "+"FROM"
        sql=sql+"("+"SELECT DISTINCT"
        sql=sql+" "+"PERSPECTIVE_NO"
        sql=sql+","+"PERSPECTIVE"
        sql=sql+","+"POSITION_MAGIC"
        sql=sql+","+"POSITION_TYPE"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+")"+"A"
        sql=sql+" "+"LEFT JOIN TRADE_HISTORY B"
        sql=sql+" "+"ON A.PERSPECTIVE_NO = B.PERSPECTIVE_NO"
        sql=sql+" "+"AND A.POSITION_TYPE = B.POSITION_TYPE"
        sql=sql+" "+"AND A.POSITION_TYPE = '"+pos_type+"'"
        sql=sql+" "+"WHERE A.POSITION_MAGIC = '"+magic+"'"
        sql=sql+" "+"GROUP BY"
        sql=sql+" "+"A.PERSPECTIVE_NO"
        sql=sql+","+"B.POSITION_TYPE"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"A.PERSPECTIVE_NO"
        sql=sql+","+"B.POSITION_TYPE"
        df = pd.read_sql(sql, conn)
    return df

def get_profit_factor_perspective(magic):
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"PERSPECTIVE"
        sql=sql+","+"SUM(PROFIT) AS PROFIT"
        sql=sql+","+"SUM(LOSS) AS LOSS"
        sql=sql+","+"COUNT(*) AS COUNT"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"WHERE POSITION_MAGIC = '"+magic+"'"
        sql=sql+" "+"GROUP BY"
        sql=sql+" "+"PERSPECTIVE"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"PERSPECTIVE_NO"
        df = pd.read_sql(sql, conn)
        df = df.dropna()
        df['PROFIT_FACTOR']=(df['PROFIT']/df['LOSS'].abs()).round(2)
        df = df.fillna(0)
        df = df.set_index("PERSPECTIVE")
    return df

def get_profit_factor_type(magic=None):
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"POSITION_TYPE"
        sql=sql+","+"SUM(PROFIT) AS PROFIT"
        sql=sql+","+"SUM(LOSS) AS LOSS"
        sql=sql+","+"COUNT(*) AS COUNT"
        sql=sql+" "+"FROM TRADE_HISTORY"
        if magic is not None:
            sql=sql+" "+"WHERE POSITION_MAGIC = '"+magic+"'"
        sql=sql+" "+"GROUP BY"
        sql=sql+" "+"POSITION_TYPE"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"POSITION_TYPE"
        df = pd.read_sql(sql, conn)
        df = df.dropna()
        df['PROFIT_FACTOR']=(df['PROFIT']/df['LOSS'].abs()).round(2)
        df = df.fillna(0)
        df = df.set_index("POSITION_TYPE")
    return df

def get_profit_factor_method():
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"POSITION_MAGIC"
        sql=sql+","+"SUM(PROFIT) AS PROFIT"
        sql=sql+","+"SUM(LOSS) AS LOSS"
        sql=sql+","+"COUNT(*) AS COUNT"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"GROUP BY"
        sql=sql+" "+"POSITION_MAGIC_NO"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"POSITION_MAGIC_NO"
        df = pd.read_sql(sql, conn)
        df = df.dropna()
        df['PROFIT_FACTOR']=(df['PROFIT']/df['LOSS'].abs()).round(2)
        df = df.fillna(0)
        df = df.set_index("POSITION_MAGIC")
    return df
"""
#
def get_result_date():
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT MIN(POSITION_OPEN_TIME) AS DATE FROM TRADE_HISTORY"
        sql=sql+" "+"UNION"
        sql=sql+" "+"SELECT MAX(POSITION_OPEN_TIME) AS DATE FROM TRADE_HISTORY"
        df = pd.read_sql(sql, conn)
    return df['DATE'][0],df['DATE'][1]
#
def get_trade_history():
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"STRFTIME('%Y-%m-%d %H:%M:%S',REPLACE(POSITION_OPEN_TIME,'.','-')) AS DATE"
        sql=sql+","+"POSITION_MAGIC"
        sql=sql+","+"POSITION_TYPE"
        sql=sql+","+"PERSPECTIVE_H4"
        sql=sql+","+"PERSPECTIVE_D1"
        sql=sql+","+"PROFIT"
        sql=sql+","+"LOSS"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"POSITION_MAGIC_NO"
        sql=sql+","+"POSITION_TYPE"
        df = pd.read_sql(sql, conn)
        df = df.fillna(0)
        df['PROFIT'] = df['PROFIT'].astype(int)
        df['LOSS'] = df['LOSS'].astype(int)
        df['YEAR'] = pd.to_datetime(df['DATE']).dt.strftime('%Y')
        df['MONTH'] = pd.to_datetime(df['DATE']).dt.strftime('%m')
        df['COUNT'] = 1
    return df
"""
#
def get_pf_by_daily(year,month):
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-')) AS DAY"
        sql=sql+","+"SUM(PROFIT) AS PROFIT"
        sql=sql+","+"SUM(LOSS) AS LOSS"
        sql=sql+","+"COUNT(*) AS COUNT"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"WHERE STRFTIME('%Y-%m',REPLACE(POSITION_OPEN_TIME,'.','-')) = '"+year+"-"+month+"'"
        sql=sql+" "+"GROUP BY"
        sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-'))"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-'))"
        df = pd.read_sql(sql, conn)
        df = df.dropna()
        df['PROFIT_FACTOR']=(df['PROFIT']/df['LOSS'].abs()).round(2)
        df = df.fillna(0)
        df = df.set_index("DAY")
    return df
#
def get_profit_factor_method_detail(magic):
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"POSITION_TYPE"
        sql=sql+","+"PERSPECTIVE"
        sql=sql+","+"SUM(PROFIT) AS PROFIT"
        sql=sql+","+"SUM(LOSS) AS LOSS"
        sql=sql+","+"COUNT(*) AS COUNT"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"WHERE POSITION_MAGIC = '"+magic+"'"
        sql=sql+" "+"GROUP BY"
        sql=sql+" "+"POSITION_TYPE"
        sql=sql+","+"PERSPECTIVE"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"PERSPECTIVE_NO"
        sql=sql+","+"POSITION_TYPE"
        df = pd.read_sql(sql, conn)
        df = df.dropna()
        df['PROFIT_FACTOR']=(df['PROFIT']/df['LOSS'].abs()).round(2)
        df = df.fillna(0)
    return df
#
def get_pf_method_by_daily(magic,perspective):
    with sqlite3.connect(DB_PATH) as conn:
        sql="SELECT"
        sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-')) AS DAY"
        sql=sql+","+"SUM(PROFIT) AS PROFIT"
        sql=sql+","+"SUM(LOSS) AS LOSS"
        sql=sql+","+"COUNT(*) AS COUNT"
        sql=sql+" "+"FROM TRADE_HISTORY"
        sql=sql+" "+"WHERE POSITION_MAGIC = '"+magic+"'"
        sql=sql+" "+"AND PERSPECTIVE = '"+perspective+"'"
        sql=sql+" "+"GROUP BY"
        sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-'))"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"STRFTIME('%Y-%m-%d',REPLACE(POSITION_OPEN_TIME,'.','-'))"
        df = pd.read_sql(sql, conn)
        df = df.dropna()
        df['PROFIT_FACTOR']=(df['PROFIT']/df['LOSS'].abs()).round(2)
        df = df.fillna(0)
        df = df.set_index("DAY")
    return df
    """