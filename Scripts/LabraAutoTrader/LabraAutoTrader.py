import pandas as pd
import sqlite3
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import talib as ta

DB_PATH = "../../../../Common/Files/LabraAutoTrader.sqlite"

with sqlite3.connect(DB_PATH) as conn:
    sql="SELECT"
<<<<<<< HEAD
    sql=sql+" "+"Time"
    sql=sql+","+"Open"
    sql=sql+","+"High"
    sql=sql+","+"Low"
    sql=sql+","+"Close"
    sql=sql+" "+"FROM CANDLE"
    df = pd.read_sql(sql, conn)

df['Time'] = pd.to_datetime(df['Time'], format='%Y.%m.%d %H:%M')
df.set_index('Time',inplace=True)

# ボリンジャーバンドを計算する
df['upper'], df['middle'], df['lower'] = ta.BBANDS(df['Close'], timeperiod=21)

# 200日移動平均線を計算する
df['sma_200'] = ta.SMA(df['Close'], timeperiod=200)

# RSIを計算する
df['rsi'] = ta.RSI(df['Close'], timeperiod=14)

# 終値の変化率を計算する
df['Close_Change'] = df['Close'].pct_change()

# 上昇・下降を表す変数を作成する
df['Close_Up'] = df['Close_Change'].apply(lambda x: 1 if x >= 0 else 0)

# 特徴量を追加する
df['middle_up'] = df['middle']>df['middle'].shift(1)
df['ratio'] = df['Close']-df['sma_200']
df['trend_up'] = df['middle']>df['sma_200']
df = df.dropna()
=======
    sql=sql+" "+"TIME"
    sql=sql+","+"OPEN"
    sql=sql+","+"HIGH"
    sql=sql+","+"LOW"
    sql=sql+","+"CLOSE"
    sql=sql+" "+"FROM CANDLE"
    df = pd.read_sql(sql, conn)

df['TIME'] = pd.to_datetime(df['TIME'], format='%Y.%m.%d %H:%M')
df.set_index('TIME',inplace=True)

# 200日移動平均
df['sma_200'] = ta.SMA(df['CLOSE'], timeperiod=200)

# MACD
df['macd'],df['macdsignal'],df['macdhist'] = ta.MACD(df['CLOSE'], fastperiod=12, slowperiod=26, signalperiod=9)

# RSI
df['rsi_14'] = ta.RSI(df['CLOSE'], timeperiod=14)

# 特徴量とターゲットを作成する
df_shift = df.shift(-1)
df['Close_Next']=df_shift['CLOSE']

# 特徴量を追加する
df['Up'] = df.apply(lambda row: row['Close_Next'] > row['CLOSE'], axis=1)

df = df.dropna()
print(df)

"""

X = df[['OPEN','HIGH','LOW','CLOSE','sma_200','macd','rsi_14']]
y = df[['Up']]
print(X)
>>>>>>> main

# 特徴量とターゲットを作成する
X = df[['Open','High','Low','Close', 'middle_up', 'ratio', 'trend_up', 'rsi']]
y = df[['Close_Up']]

# 訓練データとテストデータに分割する
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

# 線形回帰でモデルを作成する
lr = LinearRegression()
lr.fit(X_train, y_train)

# テストデータで予測を行う
y_pred = lr.predict(X_test)

# 予測精度を評価する
accuracy = lr.score(X_test, y_test)
print('Accuracy:', accuracy)

<<<<<<< HEAD
predict_df = pd.concat([y_test.reset_index(drop=False), pd.DataFrame(y_pred, columns=['pred_Close_Up'])], axis=1)
predict_df.set_index('Time',inplace=True)
predict_df = predict_df.sort_index()
=======
predict_df = pd.concat([y_test.reset_index(drop=False), pd.DataFrame(y_pred, columns=['UP'])], axis=1)
predict_df.set_index('TIME',inplace=True)
predict_df = predict_df[['UP']]
print(predict_df)
>>>>>>> main

# SQLiteに接続する
with sqlite3.connect(DB_PATH) as conn:
    # DataFrameをSQLiteのテーブルにREPLACE INTOする
    predict_df.to_sql('PREDICT', conn, if_exists='replace', index=True)

    # コミットする
<<<<<<< HEAD
    conn.commit
=======
    conn.commit()
"""
>>>>>>> main
