//+------------------------------------------------------------------+
//|                                                   InitTables.mq5 |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Include files                                                    |
//+------------------------------------------------------------------+
#include <DBCommon.mqh>
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   InitCalenderHistory();
   InitCandleHistory();
   InitSuperBollingerHistory();
   InitTradeHistory();
  }
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
bool InitCalenderHistory()
  {
//---
   string strTableName="CALENDER";
   string sql[1];
   sql[0]="DROP TABLE IF EXISTS "+strTableName;
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   sql[0]="CREATE TABLE IF NOT EXISTS "+strTableName;
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+" "+"Id TEXT";
   sql[0]=sql[0]+","+"Date TEXT";
   sql[0]=sql[0]+","+"Currency TEXT";
   sql[0]=sql[0]+","+"Name TEXT";
   sql[0]=sql[0]+","+"Importance INT";
   sql[0]=sql[0]+","+"PRIMARY KEY(Id)";
   sql[0]=sql[0]+")";
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
bool InitSpanModelHistory()
  {
//---
   string strTableName="SPAN_MODEL";
   string sql[1];
//sql[0]="DROP TABLE IF EXISTS "+strTableName;
//if(!DBExecute(sql,LA_DB_NAME))
//   return false;
   sql[0]="CREATE TABLE IF NOT EXISTS "+strTableName;
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+" "+"SYMBOL TEXT";
   sql[0]=sql[0]+","+"PERIOD TEXT";
   sql[0]=sql[0]+","+"DATE LONG";
   sql[0]=sql[0]+","+"BLUE_SPAN REAL";
   sql[0]=sql[0]+","+"RED_SPAN REAL";
   sql[0]=sql[0]+","+"BLUE_SPAN_L REAL";
   sql[0]=sql[0]+","+"RED_SPAN_L REAL";
   sql[0]=sql[0]+","+"CHIKOUSPAN_26 REAL";
   sql[0]=sql[0]+","+"PRIMARY KEY(SYMBOL,PERIOD,DATE)";
   sql[0]=sql[0]+")";
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
bool InitSuperBollingerHistory()
  {
//---
   string strTableName="SUPER_BOLLINGER";
   string sql[1];
   sql[0]="DROP TABLE IF EXISTS "+strTableName;
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   sql[0]="CREATE TABLE IF NOT EXISTS "+strTableName;
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+" "+"SYMBOL TEXT";
   sql[0]=sql[0]+","+"PERIOD TEXT";
   sql[0]=sql[0]+","+"DATE LONG";
   sql[0]=sql[0]+","+"MIDDLE REAL";
   sql[0]=sql[0]+","+"UP_1_SIGMA REAL";
   sql[0]=sql[0]+","+"LW_1_SIGMA REAL";
   sql[0]=sql[0]+","+"UP_2_SIGMA REAL";
   sql[0]=sql[0]+","+"LW_2_SIGMA REAL";
   sql[0]=sql[0]+","+"UP_3_SIGMA REAL";
   sql[0]=sql[0]+","+"LW_3_SIGMA REAL";
   sql[0]=sql[0]+","+"CHIKOUSPAN_21 REAL";
   sql[0]=sql[0]+","+"STATUS INT";
   sql[0]=sql[0]+","+"OV_2_SIGMA REAL";
   sql[0]=sql[0]+","+"CS_DIR INT";
   sql[0]=sql[0]+","+"CS_AT_UP REAL";
   sql[0]=sql[0]+","+"CS_AT_DW REAL";
   sql[0]=sql[0]+","+"PRIMARY KEY(SYMBOL,PERIOD,DATE)";
   sql[0]=sql[0]+")";
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
bool InitSASHistory()
  {
//---
   string strTableName="SPAN_AUTO_SIGNAL";
   string sql[1];
//sql[0]="DROP TABLE IF EXISTS "+strTableName;
//if(!DBExecute(sql,LA_DB_NAME))
//   return false;
   sql[0]="CREATE TABLE IF NOT EXISTS "+strTableName;
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+" "+"SYMBOL TEXT";
   sql[0]=sql[0]+","+"PERIOD TEXT";
   sql[0]=sql[0]+","+"DATE LONG";
   sql[0]=sql[0]+","+"SM_SIGNAL INT";
   sql[0]=sql[0]+","+"SM_TERM INT";
   sql[0]=sql[0]+","+"SM_UP_GRACE_ST REAL";
   sql[0]=sql[0]+","+"SM_UP_GRACE_ED REAL";
   sql[0]=sql[0]+","+"SM_DW_GRACE_ST REAL";
   sql[0]=sql[0]+","+"SM_DW_GRACE_ED REAL";
   sql[0]=sql[0]+","+"SM_GRACE_HIGH REAL";
   sql[0]=sql[0]+","+"SM_GRACE_LOW REAL";
   sql[0]=sql[0]+","+"SM_SIGNAL_ED REAL";
   sql[0]=sql[0]+","+"RS_SIGNAL INT";
   sql[0]=sql[0]+","+"RS_TERM INT";
   sql[0]=sql[0]+","+"RS_UP_GRACE_ST REAL";
   sql[0]=sql[0]+","+"RS_UP_GRACE_ED REAL";
   sql[0]=sql[0]+","+"RS_DW_GRACE_ST REAL";
   sql[0]=sql[0]+","+"RS_DW_GRACE_ED REAL";
   sql[0]=sql[0]+","+"RS_GRACE_HIGH REAL";
   sql[0]=sql[0]+","+"RS_GRACE_LOW REAL";
   sql[0]=sql[0]+","+"RS_SIGNAL_ED REAL";
   sql[0]=sql[0]+","+"HIGH_52 REAL";
   sql[0]=sql[0]+","+"LOW_52 REAL";
   sql[0]=sql[0]+","+"Y_HIGH REAL";
   sql[0]=sql[0]+","+"Y_LOW REAL";
   sql[0]=sql[0]+","+"PRIMARY KEY(SYMBOL,PERIOD,DATE)";
   sql[0]=sql[0]+")";
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
