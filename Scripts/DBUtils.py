import MetaTrader5 as mt5
import sqlite3

mt5.initialize()

# ターミナルの設定とステータスに関する情報を表示
terminal_info=mt5.terminal_info()
if terminal_info!=None:
    terminal_info_dict = mt5.terminal_info()._asdict()
commonDbFile = terminal_info_dict['commondata_path'] + "\\Files\\SATCommon.sqlite"
mt5.shutdown()

# データベースパス取得
values = []

# 1.データベースに接続
con = sqlite3.connect(commonDbFile)

# 2.sqliteを操作するカーソルオブジェクトを作成
cur = con.cursor()

# 3.テーブルの全データを取得
cur.execute("SELECT Value FROM Common WHERE Key = 'dbPath'")

# 取得したデータを出力
for row in cur:
    dbFile = row[0] + "\\MQL5\\Files\\SpanAutoTrader.sqlite"
print("dbPath="+dbFile)

# 4.データベースの接続を切断
cur.close()
con.close()

# dict_factoryの定義
def dict_factory(cursor, row):
   d = {}
   for idx, col in enumerate(cursor.description):
       d[col[0]] = row[idx]
   return d

def fetch_code_master(category):
    values = []
    # 1.データベースに接続
    con = sqlite3.connect(dbFile)
    
    # row_factoryの変更(dict_factoryに変更)
    con.row_factory = dict_factory

    # 2.sqliteを操作するカーソルオブジェクトを作成
    cur = con.cursor()

    # 3.テーブルの全データを取得
    cur.execute('SELECT Code ,Name FROM CodeMaster WHERE Category = "'+category+'"')

    # 取得したデータを出力
    for row in cur:
        values.append(row)

    # 4.データベースの接続を切断
    cur.close()
    con.close()
    return values

def fetch_data():
    values = []
    # 1.データベースに接続
    con = sqlite3.connect(dbFile)
    
    # row_factoryの変更(dict_factoryに変更)
    con.row_factory = dict_factory

    # 2.sqliteを操作するカーソルオブジェクトを作成
    cur = con.cursor()

    # 3.テーブルの全データを取得
    sql = """
    SELECT Seq, Symbol, Period, LotSize, TpPoint, SlPoint, Open, TakeProfit, StopLoss, TicketNo
    FROM Trade
    """
    cur.execute(sql)

    # 取得したデータを出力
    for row in cur:
        values.append(row)

    # 4.データベースの接続を切断
    cur.close()
    con.close()
    return values

def fetch_one(seq):
    try:
        # データベースに接続
        con = sqlite3.connect(dbFile)
    
        # row_factoryの変更(dict_factoryに変更)
        con.row_factory = dict_factory

        cur = con.cursor()

        # クエリを実行
        sql = """
        SELECT TpPoint, SlPoint, TakeProfitCd, StopLossCd, TicketNo
        FROM Trade
        WHERE Seq = ?
        """
        params = (seq,)
        cur.execute(sql, params)

        # 結果を取得
        result = cur.fetchone()

    except sqlite3.Error as e:
        print(f"Database error: {e}")
    except Exception as e:
        print(f"Unexpected error: {e}")
    finally:
        # 接続を切断
        if cur:
            cur.close()
        if con:
            con.close()

    return result

def fetch_default():
    try:
        # データベースに接続
        con = sqlite3.connect(dbFile)
    
        # row_factoryの変更(dict_factoryに変更)
        con.row_factory = dict_factory
        
        cur = con.cursor()

        # クエリを実行
        sql = """
        SELECT
            Symbol,
            PeriodCd,
            LotSize,
            TpPoint,
            SlPoint,
            OpenCd,
            TakeProfitCd,
            StopLossCd 
        FROM DefaultSetting
        """
        cur.execute(sql)

        # 結果を取得
        result = cur.fetchone()

    except sqlite3.Error as e:
        print(f"Database error: {e}")
    except Exception as e:
        print(f"Unexpected error: {e}")
    finally:
        # 接続を切断
        if cur:
            cur.close()
        if con:
            con.close()

    return result

def insert_data_market(bs,symbol,lot,tp,sl):
    # データベースに接続
    con = sqlite3.connect(dbFile)

    # sqliteを操作するカーソルオブジェクトを作成
    cur = con.cursor()
    
    try:
        # INSERTクエリの準備（サブクエリを利用してCodeMasterのNameを取得）
        sql = """
        INSERT INTO Trade (Seq, Symbol, PeriodCd, Period, LotSize, TpPoint, SlPoint, OpenCd, Open, TakeProfitCd, TakeProfit, StopLossCd, StopLoss)
        SELECT
            (SELECT Max(Seq) + 1 FROM Trade) AS Seq,
            ? AS Symbol,
            (SELECT PeriodCd FROM DefaultSetting LIMIT 1) AS PeriodCd,
            (SELECT Name FROM CodeMaster WHERE Category = 'period' AND Code = (SELECT PeriodCd FROM DefaultSetting LIMIT 1)) AS Period,
            ? AS LotSize,
            ? AS TpPoint,
            ? AS SlPoint,
            ? AS OpenCd,
            (SELECT Name FROM CodeMaster WHERE Category = 'opNow' AND Code = ?) AS Open,
            0 AS TakeProfitCd,
            (SELECT Name FROM CodeMaster WHERE Category = 'tpLogic' AND Code = 0) AS TakeProfit,
            0 AS StopLossCd,
            (SELECT Name FROM CodeMaster WHERE Category = 'slLogic' AND Code = 0) AS StopLoss
        """
        # パラメータを設定
        params = (symbol, lot, tp, sl, bs, bs)

        # クエリを実行
        cur.execute(sql, params)
        con.commit()  # データベースの変更を保存
        print("Data inserted successfully.")
    except sqlite3.Error as e:
        print(f"An error occurred: {e}")
    finally:
        # データベースの接続を切断
        cur.close()
        con.close()

def insert_data(symbol, period, lot_size, tp, sl, open_value, take_profit, stop_loss):
    # データベースに接続
    con = sqlite3.connect(dbFile)

    # sqliteを操作するカーソルオブジェクトを作成
    cur = con.cursor()
    
    try:
        # INSERTクエリの準備（サブクエリを利用してCodeMasterのNameを取得）
        sql = """
        INSERT INTO Trade (Seq, Symbol, PeriodCd, Period, LotSize, TpPoint, SlPoint, OpenCd, Open, TakeProfitCd, TakeProfit, StopLossCd, StopLoss)
        SELECT
            (SELECT Max(Seq) + 1 FROM Trade) AS Seq,
            ? AS Symbol,
            ? AS PeriodCd,
            (SELECT Name FROM CodeMaster WHERE Category = 'period' AND Code = ?) AS Period,
            ? AS LotSize,
            ? AS TpPoint,
            ? AS SlPoint,
            ? AS OpenCd,
            (SELECT Name FROM CodeMaster WHERE Category = 'opLogic' AND Code = ?) AS Open,
            ? AS TakeProfitCd,
            (SELECT Name FROM CodeMaster WHERE Category = 'tpLogic' AND Code = ?) AS TakeProfit,
            ? AS StopLossCd,
            (SELECT Name FROM CodeMaster WHERE Category = 'slLogic' AND Code = ?) AS StopLoss
        """
        # パラメータを設定
        params = (symbol, period, period, lot_size, tp, sl, open_value, open_value, take_profit, take_profit, stop_loss, stop_loss)

        # クエリを実行
        cur.execute(sql, params)
        con.commit()  # データベースの変更を保存
        print("Data inserted successfully.")
    except sqlite3.Error as e:
        print(f"An error occurred: {e}")
    finally:
        # データベースの接続を切断
        cur.close()
        con.close()

def update_default(symbol, period, lot_size, tp, sl, open_value, take_profit, stop_loss):
    # データベースに接続
    con = sqlite3.connect(dbFile)

    # sqliteを操作するカーソルオブジェクトを作成
    cur = con.cursor()
    
    try:
        # INSERTクエリの準備（サブクエリを利用してCodeMasterのNameを取得）
        sql = """
        UPDATE DefaultSetting
        SET
            Symbol = ?,
            PeriodCd = ?,
            LotSize = ?,
            TpPoint = ?,
            SlPoint = ?,
            OpenCd = ?,
            TakeProfitCd = ?,
            StopLossCd = ?
        """
        # パラメータを設定
        params = (symbol, period, lot_size, tp, sl, open_value, take_profit, stop_loss)

        # クエリを実行
        cur.execute(sql, params)
        con.commit()  # データベースの変更を保存
        print("Data updated successfully.")
    except sqlite3.Error as e:
        print(f"An error occurred: {e}")
    finally:
        # データベースの接続を切断
        cur.close()
        con.close()

def update_data(seq, tp_logic, sl_logic, tp, sl):
    # データベースに接続
    con = sqlite3.connect(dbFile)

    # sqliteを操作するカーソルオブジェクトを作成
    cur = con.cursor()
    
    try:
        # UPDATEクエリの準備（サブクエリを利用してCodeMasterのNameを取得）
        sql = """
        UPDATE Trade 
        SET
            TakeProfitCd = ?,
            TakeProfit = (SELECT Name FROM CodeMaster WHERE Category = 'tpLogic' AND Code = ?),
            StopLossCd = ?,
            StopLoss = (SELECT Name FROM CodeMaster WHERE Category = 'slLogic' AND Code = ?),
            TpPoint = ?,
            SlPoint = ?
        WHERE
            Seq = ?
        """
        # パラメータを設定
        params = (tp_logic, tp_logic, sl_logic, sl_logic, tp, sl, seq)

        # クエリを実行
        cur.execute(sql, params)
        con.commit()  # データベースの変更を保存
        print("Data updated successfully.")
    except sqlite3.Error as e:
        print(f"An error occurred: {e}")
    finally:
        # データベースの接続を切断
        cur.close()
        con.close()

def delete_data(seq):
    # データベースに接続
    con = sqlite3.connect(dbFile)

    # sqliteを操作するカーソルオブジェクトを作成
    cur = con.cursor()
    
    try:
        sql = "DELETE FROM Trade WHERE Seq = ?"

        # クエリを実行
        cur.execute(sql, (seq,))  # 修正: (seq,) とする
        con.commit()  # データベースの変更を保存
        print(f"Data with Seq={seq} deleted successfully.")
    except sqlite3.Error as e:
        print(f"An error occurred: {e}")
    finally:
        # データベースの接続を切断
        cur.close()
        con.close()
