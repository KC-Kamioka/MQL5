import pandas as pd
import requests
from bs4 import BeautifulSoup
import datetime

def GetCalender():
    #
    url = "https://www.gaikaex.com/gaikaex/mark/calendar/"
    r = requests.get(url)
    soup = BeautifulSoup(r.content, "html.parser")
    table = soup.find('div', attrs={ 'class': 'tableA01' }).tbody
    rows = table.find_all('tr')
    #
    columns=['datetime','date','time','country','title','importance','estimate','result','pastresult']
    df = pd.DataFrame(columns=columns)
    for row in rows:
        #
        data = row.find_all('td')
        if len(data) == 8:
            date = data[0].get_text()
            time = data[1].get_text()
            dt = CnvDateTime(date, time)
            country = data[2].get_text()
            title = data[3].get_text()
            importance = "★"*str(data[4]).count("★")
            estimate = data[5].get_text()
            result = data[6].get_text()
            pastresult = data[7].get_text()
        else:
            time = data[0].get_text()
            dt = CnvDateTime(date, time)
            country = data[1].get_text()
            title = data[2].get_text()
            importance = "★"*str(data[3]).count("★")
            estimate = data[4].get_text()
            result = data[5].get_text()
            pastresult = data[6].get_text()
        values = []
        values.append(dt)
        values.append(date)
        values.append(time)
        values.append(country)
        values.append(title)
        values.append(importance)
        values.append(estimate)
        values.append(result)
        values.append(pastresult)
        df = df.append(pd.Series(values, index=columns), ignore_index= True)
    df=df.fillna('')
    df=df.where(df['importance'].str[:3]=='★★★')
    df=df.query("country == '日本' | country == '米国' | country == 'ユーロ' | country == 'オーストラリア'")
    df.dropna(inplace=True)
    return df

def GetCalenderColor(dt):
    #
    t_delta = datetime.timedelta(hours=9)
    JST = datetime.timezone(t_delta, 'JST')
    now = datetime.datetime.now(JST)
    #
    if(now > dt.tz_localize(JST)):
        return 'gray'
    #
    if(datetime.datetime.strftime(now, '%Y/%m/%d') == datetime.datetime.strftime(dt, '%Y/%m/%d')):
        return 'yellow'
    return 'None'

def CnvDateTime(date, time):
    #
    t_delta = datetime.timedelta(hours=9)
    JST = datetime.timezone(t_delta, 'JST')
    now = datetime.datetime.now(JST)
    month_day=date[:date.find("(")]
    #
    if(time.count(":")==1):
        hours, minutes = time.split(":")
        if(hours.isdecimal() and minutes.isdecimal()):
            tdatetime = datetime.datetime.strptime(f"{now.year}/{month_day}", '%Y/%m/%d') + datetime.timedelta(hours=int(hours), minutes=int(minutes))
        else:
            return None
        return tdatetime
    else:
        return None
