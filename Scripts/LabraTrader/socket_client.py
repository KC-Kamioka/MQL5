import traceback
import socket
import json
import pandas as pd
import common as cm

#
def SetClientIp(port):
    try:
        # 
        ret_code=0
        ret_msg=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.WRITE
        sql="UPDATE CONFIG SET"
        sql=sql+" "+"IP="+"'"+socket.gethostbyname(socket.gethostname())+"'"
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        ret_msg=msg.decode("utf-8")
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
        ret_code=-1
    finally:
        return ret_code, ret_msg
#
def GetConfig(port):
    try:
        # 
        ret_msg=None
        df=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.READ
        sql="SELECT"
        sql=sql+" "+"SYMBOL"
        sql=sql+","+"VOLUME_MAX"
        sql=sql+","+"VOLUME_MIN"
        sql=sql+","+"VOLUME_STEP"
        sql=sql+","+"VOLUME_INIT"
        sql=sql+" "+"FROM CONFIG"
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        df = pd.read_json(msg.decode("utf-8"))
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_msg,df
#
def GetCodeMaster(port, category):
    try:
        # 
        ret_msg=None
        df=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.READ
        sql="SELECT"
        sql=sql+" "+"CATEGORY"
        sql=sql+","+"CATEGORY_NAME"
        sql=sql+","+"CODE"
        sql=sql+","+"NAME"
        sql=sql+" "+"FROM CODEMASTER"
        sql=sql+" "+"WHERE CATEGORY_NAME='"+category+"'"
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        df = pd.read_json(msg.decode("utf-8"))
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_msg,df
#
def UpdateInitLot(port,symbol,lot):
    try:
        # 
        ret_code=0
        ret_msg=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.WRITE
        sql="UPDATE CONFIG SET"
        sql=sql+" "+"VOLUME_INIT="+lot
        sql=sql+" "+"WHERE SYMBOL="+"'"+symbol+"'"
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        #
        if msg.decode("utf-8") != "200":
            ret_code=-1
        ret_msg=msg.decode("utf-8")
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_code, ret_msg
#
def SelectWaitings(port):
    try:
        # 
        ret_msg=None
        df=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.READ
        sql="SELECT"
        sql=sql+" "+"SEQ"
        sql=sql+","+"SYMBOL"
        sql=sql+","+"LOTS"
        sql=sql+","+"(SELECT NAME FROM CODEMASTER WHERE CATEGORY_NAME='PERIOD' AND CODE=TIME_FRAME) AS TIME_FRAME"
        sql=sql+","+"(SELECT NAME FROM CODEMASTER WHERE CATEGORY_NAME='ORDER' AND CODE=ORDER_TYPE) AS ORDER_TYPE"
        sql=sql+","+"(SELECT NAME FROM CODEMASTER WHERE CATEGORY_NAME='OP_AUTO' AND CODE=OPEN_TYPE) AS OPEN_TYPE"
        sql=sql+","+"(SELECT NAME FROM CODEMASTER WHERE CATEGORY_NAME='LIMIT' AND CODE=LIMIT_TYPE) AS LIMIT_TYPE"
        sql=sql+","+"(SELECT NAME FROM CODEMASTER WHERE CATEGORY_NAME='STOP' AND CODE=STOP_TYPE) AS STOP_TYPE"
        sql=sql+" "+"FROM WAITING"
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        df = pd.read_json(msg.decode("utf-8"))
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_msg,df
#
def SelectOrders(port):
    try:
        # 
        df=None
        ret_msg=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.READ
        sql="SELECT"
        sql=sql+" "+"TICKET_NO"
        sql=sql+","+"SYMBOL"
        sql=sql+","+"DATE"
        sql=sql+","+"LOTS"
        sql=sql+","+"(SELECT NAME FROM CODEMASTER WHERE CATEGORY_NAME='PERIOD' AND CODE=TIME_FRAME) AS TIME_FRAME"
        sql=sql+","+"(SELECT NAME FROM CODEMASTER WHERE CATEGORY_NAME='ORDER' AND CODE=ORDER_TYPE) AS ORDER_TYPE"
        sql=sql+","+"(SELECT NAME FROM CODEMASTER WHERE CATEGORY_NAME='OP_AUTO' AND CODE=OPEN_TYPE) AS OPEN_TYPE"
        sql=sql+","+"OP_PRICE"
        sql=sql+","+"(SELECT NAME FROM CODEMASTER WHERE CATEGORY_NAME='LIMIT' AND CODE=LIMIT_TYPE) AS LIMIT_TYPE"
        sql=sql+","+"(SELECT NAME FROM CODEMASTER WHERE CATEGORY_NAME='STOP' AND CODE=STOP_TYPE) AS STOP_TYPE"
        sql=sql+" "+"FROM ORDERS"
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        df = pd.read_json(msg.decode("utf-8"))
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_msg,df

def SendOrder(port, symbol,lot,time_frame,order_type,open_type):
    try:
        #
        ret_code=0
        ret_msg=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.WRITE
        sql="INSERT INTO WAITING"
        sql=sql+"("+"SEQ"
        sql=sql+","+"SYMBOL"
        sql=sql+","+"LOTS"
        sql=sql+","+"TIME_FRAME"
        sql=sql+","+"ORDER_TYPE"
        sql=sql+","+"OPEN_TYPE"
        sql=sql+","+"LIMIT_TYPE"
        sql=sql+","+"STOP_TYPE"
        sql=sql+")"+"SELECT"
        sql=sql+" "+"(SELECT IFNULL(MAX(SEQ),0)+1 FROM WAITING)"
        sql=sql+","+"'"+symbol+"'"+" AS SYMBOL"
        sql=sql+","+lot
        sql=sql+","+"(SELECT CODE FROM CODEMASTER WHERE CATEGORY_NAME='PERIOD' AND NAME='"+time_frame+"')"
        sql=sql+","+str(order_type)
        sql=sql+","+"OPEN_TYPE"
        sql=sql+","+"LIMIT_TYPE"
        sql=sql+","+"STOP_TYPE"
        sql=sql+" "+"FROM CONDITIONS"
        sql=sql+" "+"WHERE OPEN_TYPE = (SELECT CODE FROM CODEMASTER WHERE CATEGORY_NAME='OP_AUTO' AND NAME='"+open_type+"')"
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        #
        if msg.decode("utf-8") != "200":
            ret_code=-1
        ret_msg=msg.decode("utf-8")
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_code, ret_msg
#
def CloseMarketOrder(port, ticket_no=0):
    try:
        ret_code=0
        ret_msg=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.WRITE
        sql="UPDATE ORDERS SET"
        sql=sql+" "+"STOP_TYPE=(SELECT CODE FROM CODEMASTER WHERE CATEGORY_NAME='STOP' AND NAME='S_NOW')"
        if ticket_no!=0:
            sql=sql+" "+"WHERE TICKET_NO ="+ticket_no
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        #
        if msg.decode("utf-8") != "200":
            ret_code=-1
        ret_msg=msg.decode("utf-8")
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_code, ret_msg
#
def ModifyPreserveOrder(port, seq, tf, limit, stop):
    try:
        #
        ret_code=0
        ret_msg=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.WRITE
        sql="UPDATE WAITING SET"
        sql=sql+" "+"TIME_FRAME = (SELECT CODE FROM CODEMASTER WHERE CATEGORY_NAME='PERIOD' AND NAME='"+tf+"')"
        sql=sql+","+"LIMIT_TYPE = (SELECT CODE FROM CODEMASTER WHERE CATEGORY_NAME='LIMIT' AND NAME='"+limit+"')"
        sql=sql+","+"STOP_TYPE = (SELECT CODE FROM CODEMASTER WHERE CATEGORY_NAME='STOP' AND NAME='"+stop+"')"
        sql=sql+" "+"WHERE SEQ ="+seq
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        #
        if msg.decode("utf-8") != "200":
            ret_code=-1
        ret_msg=msg.decode("utf-8")
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_code, ret_msg

#
def DeletePreserveOrder(port,seq):
    try:
        #
        ret_code=0
        ret_msg=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.WRITE
        sql="DELETE FROM WAITING"
        sql=sql+" "+"WHERE SEQ ="+seq
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        #
        if msg.decode("utf-8") != "200":
            ret_code=-1
        ret_msg=msg.decode("utf-8")
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_code, ret_msg

#
def ModifyOrder(port,ticket_no, tf, limit, stop):
    try:
        #
        ret_code=0
        ret_msg=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.WRITE
        sql="UPDATE ORDERS SET"
        sql=sql+" "+"TIME_FRAME = (SELECT CODE FROM CODEMASTER WHERE CATEGORY_NAME='PERIOD' AND NAME='"+tf+"')"
        sql=sql+","+"LIMIT_TYPE = (SELECT CODE FROM CODEMASTER WHERE CATEGORY_NAME='LIMIT' AND NAME='"+limit+"')"
        sql=sql+","+"STOP_TYPE = (SELECT CODE FROM CODEMASTER WHERE CATEGORY_NAME='STOP' AND NAME='"+stop+"')"
        sql=sql+" "+"WHERE TICKET_NO ="+ticket_no
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        #
        if msg.decode("utf-8") != "200":
            ret_code=-1
        ret_msg=msg.decode("utf-8")
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_code, ret_msg
#
def SelectEvents(port):
    try:
        # 
        df=None
        ret_msg=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.READ
        sql="SELECT"
        sql=sql+" "+"DATETIME(REPLACE(DATE,'.','-')) AS DATE_TIME"
        sql=sql+","+"CURRENCY"
        sql=sql+","+"NAME"
        sql=sql+" "+"FROM EVENTS"
        sql=sql+" "+"WHERE DATE(REPLACE(DATE,'.','-')) >= DATE('now')"
        sql=sql+" "+"AND SECTOR NOT IN ("
        sql=sql+" "+"'CALENDAR_SECTOR_GOVERNMENT'"
        sql=sql+","+"'CALENDAR_SECTOR_BUSINESS'"
        sql=sql+","+"'CALENDAR_SECTOR_HOUSING'"
        sql=sql+")"
        sql=sql+" "+"AND IMPORTANCE = 'CALENDAR_IMPORTANCE_HIGH'"
        sql=sql+" "+"ORDER BY"
        sql=sql+" "+"DATE"
        sql=sql+","+"CURRENCY"
        sql=sql+","+"EVENT_ID"
        sql=sql+" "+"LIMIT 10"
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        df = pd.read_json(msg.decode("utf-8"))
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_msg,df
#
def GetServerLog(port):
    try:
        # 
        df=None
        ret_msg=None
        # ソケット作成
        sock = socket.socket(socket.AF_INET)
        # サーバーへ接続
        sock.connect((cm.IPADDR_SV, port))
        #
        q_type=cm.READ
        sql="SELECT"
        sql=sql+" "+"DATE"
        sql=sql+","+"TYPE"
        sql=sql+","+"MESSAGE"
        sql=sql+" "+"FROM LOG"
        send_message = {'q_type':q_type, 'sql':sql}
        sock.send(json.dumps(send_message).encode("utf-8"))
        msg = sock.recv(1024)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        df = pd.read_json(msg.decode("utf-8"))
    except Exception as e:
        ret_msg=traceback.format_exception_only(type(e), e)[0]
    finally:
        return ret_msg,df