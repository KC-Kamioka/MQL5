import tkinter as tk
import tkinter.ttk as ttk
import socket_client as sc
import common as cm
from ttkthemes import *
import numpy as np
from datetime import datetime as dt
import calender as cl

PAD_X=10
PAD_Y=10
COMBO_WIDTH=4
BTN_ST_WIDTH=23
BTN_CT_WIDTH=11
TREE_COL_WIDTH1=60
TREE_COL_WIDTH2=100
TREE_COL_WIDTH3=120
TREE_COL_WIDTH4=340
CONTROL_PAD_X=5
CONTROL_PAD_Y=5

class LabraTrader(tk.Frame):
    #
    def __init__(self,master):
        #
        style = ttk.Style()
        style.theme_use('black')
        style.configure("Buy.TButton", foreground="white", background="blue")
        style.configure("Sell.TButton", foreground="white", background="red")
        style.configure("TButton", anchor="n")
        #
        def click_close():
            self.master.destroy()
        #
        def create_form(tab, port):
            #
            def reflesh_ctls(frm):
                #
                cmb_1=frm.nametowidget("frm_ctl.cmb_0")
                cmb_1.config(state=tk.DISABLED)
                cmb_1.selection_clear()
                cmb_1.set('')
                cmb_1=frm.nametowidget("frm_ctl.cmb_1")
                cmb_1.config(state=tk.DISABLED)
                cmb_1.selection_clear()
                cmb_1.set('')
                cmb_2=frm.nametowidget("frm_ctl.cmb_2")
                cmb_2.config(state=tk.DISABLED)
                cmb_2.selection_clear()
                cmb_2.set('')
                btn_1=frm.nametowidget("frm_ctl.btn_1")
                btn_1.config(state=tk.DISABLED)
                btn_2=frm.nametowidget("frm_ctl.btn_2")
                btn_2.config(state=tk.DISABLED)
                return
            #
            def reflesh_waiting_ctls():
                #
                frm=flm_waiting.nametowidget("frm_waiting")
                #
                frm_tree=frm.nametowidget("frm_tree")
                #
                tree=frm_tree.nametowidget("tree")
                #
                err,df=sc.SelectWaitings(port)
                if err is not None:
                    insert_log(cm.ERROR, err)
                    return
                tree.delete(*tree.get_children())
                for row in df.itertuples():
                    tree.insert("", "end"
                    ,values=(
                            row.SEQ
                            ,row.SYMBOL
                            ,row.LOTS
                            ,row.TIME_FRAME
                            ,row.ORDER_TYPE
                            ,row.OPEN_TYPE
                            ,row.LIMIT_TYPE
                            ,row.STOP_TYPE
                        )
                    )
                #
                reflesh_ctls(frm)
            #
            def reflesh_order_ctls():
                #
                frm=flm_order.nametowidget("frm_order")
                #
                frm_tree=frm.nametowidget("frm_tree")
                #
                tree=frm_tree.nametowidget("tree")
                #
                err,df=sc.SelectOrders(port)
                if err is not None:
                    insert_log(cm.ERROR, err)
                    return
                tree.delete(*tree.get_children())
                for row in df.itertuples():
                    tree.insert("", "end"
                    ,values=(
                            row.TICKET_NO
                            ,row.SYMBOL
                            ,row.DATE
                            ,row.LOTS
                            ,row.TIME_FRAME
                            ,row.ORDER_TYPE
                            ,row.OPEN_TYPE
                            ,row.OP_PRICE
                            ,row.LIMIT_TYPE
                            ,row.STOP_TYPE
                        )
                    )
                #
                reflesh_ctls(frm)
                return
            #
            def reflesh_calender():
                #
                tree=flm_calender.nametowidget("frm_calender.tree")
                #
                df=cl.GetCalender()
                #
                tree.delete(*tree.get_children())
                for row in df.itertuples():
                    tree.insert("", "end"
                        ,values=(
                                row.date
                                ,row.time
                                ,row.country
                                ,row.title
                                ,row.importance
                                ,row.estimate
                                ,row.result
                                ,row.pastresult
                            )
                        , tags=(cl.GetCalenderColor(row.datetime))
                    )
                tree.tag_configure("yellow", foreground='yellow')
                tree.tag_configure("gray", foreground='gray')
            #
            def insert_log(type, message):
                #
                tree=flm_log.nametowidget("frm_log.tree")
                #
                tree.delete(*tree.get_children())
                tree.insert("", "end"
                ,values=(
                        dt.now().strftime("%Y-%m-%d %H:%M:%S")
                        ,type
                        ,message
                    )
                )
            #
            def set_lot_init(event, symbol):
                cmb_lot=flm_new_order.nametowidget("frm_new_order.cmb_lot")
                df=config[config["SYMBOL"]==symbol]
                lot_cnf=df.to_dict(orient='records')
                lot_list=np.round(np.arange(lot_cnf[0]["VOLUME_MIN"], lot_cnf[0]["VOLUME_MAX"]+lot_cnf[0]["VOLUME_STEP"], lot_cnf[0]["VOLUME_STEP"]).tolist(), decimals=3)
                cmb_lot.config(values=lot_list)
                cmb_lot.set(lot_cnf[0]["VOLUME_INIT"])
            #
            def btn_refresh_click():
                reflesh_waiting_ctls()
                reflesh_order_ctls()
                reflesh_calender()
                insert_log(cm.INFO, "refreshed.")
            #
            def create_flm_new_order():
                #
                def btn_buy_click():
                    #
                    ret_code, err=sc.SendOrder(port, cmb_symbols.get(),cmb_lot.get(),cmb_tf.get(),0,cmb_open.get())
                    if ret_code<0:
                        insert_log(cm.ERROR, err)
                        return
                    #
                    ret_code, err=sc.UpdateInitLot(port, cmb_symbols.get(),cmb_lot.get())
                    if ret_code<0:
                        insert_log(cm.ERROR, err)
                        return
                    #
                    reflesh_waiting_ctls()
                    return
                #
                def btn_sell_click():
                    #
                    ret_code, err=sc.SendOrder(port, cmb_symbols.get(),cmb_lot.get(),cmb_tf.get(),1,cmb_open.get())
                    if ret_code<0:
                        insert_log(cm.ERROR, err)
                        return
                    #
                    ret_code, err=sc.UpdateInitLot(port, cmb_symbols.get(),cmb_lot.get())
                    if ret_code<0:
                        insert_log(cm.ERROR, err)
                        return
                    #
                    reflesh_waiting_ctls()
                    return
                #
                flm_new_order = ttk.LabelFrame(frm_main, text="New Order", name="flm_new_order")
                frm_new_order = ttk.Frame(flm_new_order, name="frm_new_order")
                cmb_symbols = ttk.Combobox(frm_new_order, justify=tk.CENTER, name="cmb_symbols", state="readonly")
                cmb_lot = ttk.Combobox(frm_new_order, justify=tk.CENTER, name="cmb_lot")
                cmb_tf = ttk.Combobox(frm_new_order, justify=tk.CENTER, name="cmb_tf", state="readonly")
                cmb_open = ttk.Combobox(frm_new_order, justify=tk.CENTER, name="cmb_open", state="readonly")
                btn_buy = ttk.Button(frm_new_order, text="Buy", style="Buy.TButton", command=btn_buy_click)
                btn_sell = ttk.Button(frm_new_order, text="Sell", style="Sell.TButton", command=btn_sell_click)
                #
                frm_new_order.pack(side=tk.LEFT, anchor=tk.W, padx=PAD_X, pady=PAD_Y)
                cmb_symbols.pack(side=tk.LEFT, anchor=tk.W)
                cmb_lot.pack(side=tk.LEFT, anchor=tk.W)
                cmb_tf.pack(side=tk.LEFT, anchor=tk.W)
                cmb_open.pack(side=tk.LEFT, anchor=tk.W)
                btn_buy.pack(side=tk.LEFT, anchor=tk.W)
                btn_sell.pack(side=tk.LEFT, anchor=tk.W)
                cmb_symbols.bind('<<ComboboxSelected>>', lambda e : set_lot_init(e, cmb_symbols.get()))
                return flm_new_order
            #
            def create_flm_waiting():
                #
                def btn_modify_click():
                    ret_code, err=sc.ModifyPreserveOrder(port, cm.rec_key, cmb_tf.get(), cmb_limit.get(), cmb_stop.get())
                    if ret_code<0:
                        insert_log(cm.ERROR, err)
                    else:
                        reflesh_waiting_ctls()
                        insert_log(cm.INFO, "order modified.")
                    return
                #
                def btn_delete_click():
                    ret_code, err=sc.DeletePreserveOrder(port, cm.rec_key)
                    if ret_code<0:
                        insert_log(cm.ERROR, err)
                    else:
                        reflesh_waiting_ctls()
                        insert_log(cm.INFO, "order deleted.")
                    return
                #
                def select_record_waitings(event):
                    # 選択行の判別
                    record_id = tree.focus()
                    # 選択行のレコードを取得
                    record_values = tree.item(record_id, 'values')
                    #
                    cm.rec_key=record_values[0]
                    cmb_tf.config(state='readonly')
                    cmb_tf.set(record_values[3])
                    cmb_limit.config(state='readonly')
                    cmb_limit.set(record_values[6])
                    cmb_stop.config(state='readonly')
                    cmb_stop.set(record_values[7])
                    btn_mod.config(state=tk.READABLE)
                    btn_del.config(state=tk.READABLE)
                    #
                    reflesh_order_ctls()
                #
                flm_waiting = ttk.LabelFrame(frm_main, text="Waitings", name="flm_waiting")
                frm_waiting = ttk.Frame(flm_waiting, name="frm_waiting")
                frm_tree = ttk.Frame(frm_waiting, name="frm_tree")
                tree = ttk.Treeview(frm_tree, columns=(1,2,3,4,5,6,7,8,9), height=3, name="tree")
                ysb = ttk.Scrollbar(frm_tree, orient=tk.VERTICAL, command=tree.yview)
                frm_ctl = ttk.Frame(frm_waiting, name="frm_ctl")
                lbl_tf = ttk.Label(frm_ctl, text="Timeframe")
                cmb_tf = ttk.Combobox(frm_ctl, justify=tk.CENTER, name="cmb_0")
                lbl_limit = ttk.Label(frm_ctl, text="Limit Type")
                cmb_limit = ttk.Combobox(frm_ctl, justify=tk.CENTER, name="cmb_1")
                lbl_stop = ttk.Label(frm_ctl, text="Stop Type")
                cmb_stop = ttk.Combobox(frm_ctl, justify=tk.CENTER, name="cmb_2")
                btn_mod = ttk.Button(frm_ctl, text="Modify", name="btn_1", command=btn_modify_click)
                btn_del = ttk.Button(frm_ctl, text="Delete", name="btn_2", command=btn_delete_click)
                # 列の設定
                tree.column('#0',width=0, stretch='no')
                tree.column(1,width=TREE_COL_WIDTH1, stretch='no', anchor=tk.E)
                tree.column(2,width=TREE_COL_WIDTH1, stretch='no', anchor=tk.CENTER)
                tree.column(3,width=TREE_COL_WIDTH1, stretch='no', anchor=tk.E)
                tree.column(4,width=TREE_COL_WIDTH2, stretch='no', anchor=tk.CENTER)
                tree.column(5,width=TREE_COL_WIDTH3, stretch='no', anchor=tk.CENTER)
                tree.column(6,width=TREE_COL_WIDTH3, stretch='no', anchor=tk.CENTER)
                tree.column(7,width=TREE_COL_WIDTH2, stretch='no', anchor=tk.CENTER)
                tree.column(8,width=TREE_COL_WIDTH2, stretch='no', anchor=tk.CENTER)
                # ヘッダーテキスト
                tree.heading(1, text="Seq")
                tree.heading(2, text="Symbol")
                tree.heading(3, text="Lot")
                tree.heading(4, text="Timeframe")
                tree.heading(5, text="Order Type")
                tree.heading(6, text="Open Type")
                tree.heading(7, text="Limit Type")
                tree.heading(8, text="Stop Type")
                #
                tree.bind("<<TreeviewSelect>>", select_record_waitings)
                tree.configure(yscrollcommand=ysb.set)
                #
                frm_waiting.pack(side=tk.TOP, anchor=tk.W, fill=tk.X, padx=PAD_X, pady=PAD_Y)
                frm_tree.pack(side=tk.TOP, anchor=tk.W, fill=tk.X)
                tree.pack(side=tk.LEFT, expand=True, fill=tk.X)
                ysb.pack(side=tk.RIGHT, fill=tk.Y)
                frm_ctl.pack(side=tk.RIGHT, anchor=tk.W)
                lbl_tf.pack(side=tk.LEFT, anchor=tk.W)
                cmb_tf.pack(side=tk.LEFT, anchor=tk.W)
                lbl_limit.pack(side=tk.LEFT, anchor=tk.W)
                cmb_limit.pack(side=tk.LEFT, anchor=tk.W)
                lbl_stop.pack(side=tk.LEFT, anchor=tk.W)
                cmb_stop.pack(side=tk.LEFT, anchor=tk.W)
                btn_mod.pack(side=tk.LEFT, anchor=tk.W)
                btn_del.pack(side=tk.LEFT, anchor=tk.W)
                #
                return flm_waiting
            #
            def create_flm_order():
                #
                def btn_close_click():
                    ret_code, err=sc.CloseMarketOrder(port)
                    if ret_code<0:
                        insert_log(cm.ERROR, err)
                    else:
                        #
                        reflesh_order_ctls()
                        insert_log(cm.INFO, "send order position open.")
                    return
                #
                def btn_modify_click():
                    ret_code, err=sc.ModifyOrder(port, cm.rec_key, cmb_tf.get(), cmb_limit.get(), cmb_stop.get())
                    if ret_code<0:
                        insert_log(cm.ERROR, err)
                    else:
                        reflesh_order_ctls()
                        insert_log(cm.INFO, "order modified.")
                    return
                #
                def btn_partial_close_click():
                    ret_code, err=sc.CloseMarketOrder(port, cm.rec_key)
                    if ret_code<0:
                        insert_log(cm.ERROR, err)
                    else:
                        reflesh_order_ctls()
                        insert_log(cm.INFO, "send order position close.")
                    return
                #
                def select_record_orders(event):
                    # 選択行の判別
                    record_id = tree.focus()
                    # 選択行のレコードを取得
                    record_values = tree.item(record_id, 'values')
                    #
                    cm.rec_key=record_values[0]
                    cmb_tf.config(state='readonly')
                    cmb_tf.set(record_values[4])
                    cmb_limit.config(state='readonly')
                    cmb_limit.set(record_values[8])
                    cmb_stop.config(state='readonly')
                    cmb_stop.set(record_values[9])
                    btn_mod.config(state=tk.READABLE)
                    btn_cls.config(state=tk.READABLE)
                    #
                    reflesh_waiting_ctls()
                #
                flm_order = ttk.LabelFrame(frm_main, text="Orders", name="flm_order")
                frm_order = ttk.Frame(flm_order, name="frm_order")
                btn_close = ttk.Button(frm_order, text="Close All", command=btn_close_click)
                frm_tree = ttk.Frame(frm_order, name="frm_tree")
                tree = ttk.Treeview(frm_tree, columns=(1,2,3,4,5,6,7,8,9,10), height=3, name="tree")
                ysb = ttk.Scrollbar(frm_tree, orient=tk.VERTICAL, command=tree.yview)
                frm_ctl = ttk.Frame(frm_order, name="frm_ctl")
                lbl_tf = ttk.Label(frm_ctl, text="Timeframe")
                cmb_tf = ttk.Combobox(frm_ctl, justify=tk.CENTER, name="cmb_0")
                lbl_limit = ttk.Label(frm_ctl, text="Limit Type")
                cmb_limit = ttk.Combobox(frm_ctl, justify=tk.CENTER, name="cmb_1", state=tk.DISABLED)
                lbl_stop = ttk.Label(frm_ctl, text="Stop Type")
                cmb_stop = ttk.Combobox(frm_ctl, justify=tk.CENTER, name="cmb_2", state=tk.DISABLED)
                btn_mod = ttk.Button(frm_ctl, text="Modify", name="btn_1", state=tk.DISABLED, command=btn_modify_click)
                btn_cls = ttk.Button(frm_ctl, text="Close", name="btn_2", state=tk.DISABLED, command=btn_partial_close_click)
                # 列の設定
                tree.column('#0',width=0, stretch='no')
                tree.column(1,width=TREE_COL_WIDTH2, stretch='no', anchor=tk.E)
                tree.column(2,width=TREE_COL_WIDTH1, stretch='no', anchor=tk.CENTER)
                tree.column(3,width=TREE_COL_WIDTH3, stretch='no', anchor=tk.CENTER)
                tree.column(4,width=TREE_COL_WIDTH1, stretch='no', anchor=tk.E)
                tree.column(5,width=TREE_COL_WIDTH2, stretch='no', anchor=tk.CENTER)
                tree.column(6,width=TREE_COL_WIDTH3, stretch='no', anchor=tk.CENTER)
                tree.column(7,width=TREE_COL_WIDTH3, stretch='no', anchor=tk.CENTER)
                tree.column(8,width=TREE_COL_WIDTH2, stretch='no', anchor=tk.E)
                tree.column(9,width=TREE_COL_WIDTH2, stretch='no', anchor=tk.CENTER)
                tree.column(10,width=TREE_COL_WIDTH2, stretch='no', anchor=tk.CENTER)
                # ヘッダーテキスト
                tree.heading(1, text="Ticket Number")
                tree.heading(2, text="Symbol")
                tree.heading(3, text="Date")
                tree.heading(4, text="Lot")
                tree.heading(5, text="Timeframe")
                tree.heading(6, text="Order Type")
                tree.heading(7, text="Open Type")
                tree.heading(8, text="Open Price")
                tree.heading(9, text="Limit Type")
                tree.heading(10, text="Stop Type")
                #
                tree.bind("<<TreeviewSelect>>", select_record_orders)
                tree.configure(yscrollcommand=ysb.set)
                #
                frm_order.pack(side=tk.LEFT, anchor=tk.W, padx=PAD_X, pady=PAD_Y)
                btn_close.pack(side=tk.TOP, anchor=tk.E)
                frm_tree.pack(side=tk.TOP, anchor=tk.W, fill=tk.X)
                tree.pack(side=tk.LEFT, expand=True, fill=tk.X)
                ysb.pack(side=tk.RIGHT, fill=tk.Y)
                frm_ctl.pack(side=tk.RIGHT, anchor=tk.W)
                lbl_tf.pack(side=tk.LEFT, anchor=tk.W)
                cmb_tf.pack(side=tk.LEFT, anchor=tk.W)
                lbl_limit.pack(side=tk.LEFT, anchor=tk.W)
                cmb_limit.pack(side=tk.LEFT, anchor=tk.W)
                lbl_stop.pack(side=tk.LEFT, anchor=tk.W)
                cmb_stop.pack(side=tk.LEFT, anchor=tk.W)
                btn_mod.pack(side=tk.LEFT, anchor=tk.W)
                btn_cls.pack(side=tk.LEFT, anchor=tk.W)
                return flm_order
            #
            def create_flm_calender():
                flm_calender = ttk.LabelFrame(frm_main, text="Calender", name="flm_calender")
                frm_calender = ttk.Frame(flm_calender, name="frm_calender")
                tree = ttk.Treeview(frm_calender, columns=(1,2,3,4,5,6,7,8), name="tree")
                # 列の設定
                tree.column('#0',width=0, stretch='no')
                tree.column(1, width=TREE_COL_WIDTH1, stretch='no', anchor=tk.CENTER)
                tree.column(2, width=TREE_COL_WIDTH1, stretch='no', anchor=tk.CENTER)
                tree.column(3, width=TREE_COL_WIDTH3, stretch='no', anchor=tk.CENTER)
                tree.column(4, width=TREE_COL_WIDTH4, stretch='no', anchor=tk.CENTER)
                tree.column(5, width=TREE_COL_WIDTH2, stretch='no', anchor=tk.CENTER)
                tree.column(6, width=TREE_COL_WIDTH2, stretch='no', anchor=tk.CENTER)
                tree.column(7, width=TREE_COL_WIDTH2, stretch='no', anchor=tk.CENTER)
                tree.column(8, width=TREE_COL_WIDTH2, stretch='no', anchor=tk.CENTER)
                # ヘッダーテキスト
                tree.heading(1, text="Date")
                tree.heading(2, text="Time")
                tree.heading(3, text="Country")
                tree.heading(4, text="Event")
                tree.heading(5, text="Importance")
                tree.heading(6, text="Estimate")
                tree.heading(7, text="Result")
                tree.heading(8, text="PastResult")
                #
                ysb = ttk.Scrollbar(frm_calender, orient=tk.VERTICAL, command=tree.yview)
                tree.configure(yscrollcommand=ysb.set)
                #
                frm_calender.pack(fill=tk.X, padx=PAD_X, pady=PAD_Y)
                tree.pack(side=tk.LEFT, expand=True, fill=tk.X)
                ysb.pack(side=tk.RIGHT, fill=tk.Y)
                return flm_calender
            #
            def create_flm_log():
                flm_log = ttk.LabelFrame(frm_main, text="Log", name="flm_log")
                frm_log = ttk.Frame(flm_log, name="frm_log")
                tree = ttk.Treeview(frm_log, columns=(1,2,3), height=3, name="tree")
                # 列の設定
                tree.column('#0',width=0, stretch='no')
                tree.column(1,width=TREE_COL_WIDTH3, stretch='no', anchor=tk.CENTER)
                tree.column(2,width=TREE_COL_WIDTH1, stretch='no', anchor=tk.CENTER)
                # ヘッダーテキスト
                tree.heading(1, text="Date")
                tree.heading(2, text="Type")
                tree.heading(3, text="Message")
                #
                ysb = ttk.Scrollbar(frm_log, orient=tk.VERTICAL, command=tree.yview)
                tree.configure(yscrollcommand=ysb.set)
                #
                frm_log.pack(fill=tk.X, padx=PAD_X, pady=PAD_Y)
                tree.pack(side=tk.LEFT, expand=True, fill=tk.X)
                ysb.pack(side=tk.RIGHT, fill=tk.Y)
                return flm_log
            # 
            frm_main = ttk.Frame(tab, name="frm_main")
            frm_main.pack(side=tk.LEFT, anchor=tk.N, fill=tk.X, padx=PAD_X, pady=PAD_Y)
            #
            btn_refresh = ttk.Button(frm_main, text="Refresh", command=btn_refresh_click)
            flm_new_order = create_flm_new_order()
            flm_waiting = create_flm_waiting()
            flm_order = create_flm_order()
            flm_calender = create_flm_calender()
            flm_log = create_flm_log()
            #
            btn_refresh.grid(row=0, column=0, sticky=tk.E, padx=PAD_X)
            flm_new_order.grid(row=1, column=0, sticky=tk.W+tk.E)
            flm_waiting.grid(row=2, column=0, sticky=tk.W+tk.E)
            flm_order.grid(row=3, column=0, sticky=tk.W+tk.E)
            flm_calender.grid(row=4, column=0, sticky=tk.W+tk.E)
            flm_log.grid(row=5, column=0, sticky=tk.W+tk.E)
            #
            err,config=sc.GetConfig(port)
            if err is not None:
                return frm_main
            else:
                #
                symbol_list=config.loc[:, 'SYMBOL'].to_list()
                cmb_symbols=flm_new_order.nametowidget("frm_new_order.cmb_symbols")
                cmb_symbols.config(values=symbol_list)
                cmb_symbols.current(0)
                #
                set_lot_init(None, cmb_symbols.get())
                cmb_lot=flm_new_order.nametowidget("frm_new_order.cmb_lot")
                df=config[config["SYMBOL"]==cmb_symbols.get()]
                cmb_cnf = df.to_dict(orient='records')
                cmb_lot.set(cmb_cnf[0]["VOLUME_INIT"])
            #
            err,df=sc.GetCodeMaster(port, 'PERIOD')
            if err is not None:
                return frm_main
            else:
                #
                tf_list=df.loc[:, 'NAME'].to_list()
                cmb_tf=flm_new_order.nametowidget("frm_new_order.cmb_tf")
                cmb_tf.config(values=tf_list)
                cmb_tf.current(2)
                #
                frm_waiting_cmb_tf=flm_waiting.nametowidget("frm_waiting.frm_ctl.cmb_0")
                frm_waiting_cmb_tf.config(values=tf_list)
                frm_waiting_cmb_tf.current(2)
                #
                frm_waiting_cmb_tf=flm_order.nametowidget("frm_order.frm_ctl.cmb_0")
                frm_waiting_cmb_tf.config(values=tf_list)
                frm_waiting_cmb_tf.current(2)
            #
            err,df=sc.GetCodeMaster(port, 'OP_AUTO')
            if err is not None:
                return frm_main
            else:
                open_list=df.loc[:, 'NAME'].to_list()
                cmb_open=flm_new_order.nametowidget("frm_new_order.cmb_open")
                cmb_open.config(values=open_list)
                cmb_open.current(0)
            #
            err,df=sc.GetCodeMaster(port, 'LIMIT')
            if err is not None:
                return frm_main
            else:
                #
                limit_list=df.loc[:, 'NAME'].to_list()
                #
                frm_waiting_cmb_limit=flm_waiting.nametowidget("frm_waiting.frm_ctl.cmb_1")
                frm_waiting_cmb_limit.config(values=limit_list)
                frm_waiting_cmb_limit.current(0)
                #
                frm_order_cmb_limit=flm_order.nametowidget("frm_order.frm_ctl.cmb_1")
                frm_order_cmb_limit.config(values=limit_list)
                frm_order_cmb_limit.current(0)
            #
            err,df=sc.GetCodeMaster(port, 'STOP')
            if err is not None:
                return frm_main
            else:
                #
                stop_list=df.loc[:, 'NAME'].to_list()
                #
                frm_waiting_stop_list=flm_waiting.nametowidget("frm_waiting.frm_ctl.cmb_2")
                frm_waiting_stop_list.config(values=stop_list)
                frm_waiting_stop_list.current(0)
                #
                frm_order_stop_list=flm_order.nametowidget("frm_order.frm_ctl.cmb_2")
                frm_order_stop_list.config(values=stop_list)
                frm_order_stop_list.current(0)

            # フォームリフレッシュ
            reflesh_waiting_ctls()
            reflesh_order_ctls()
            reflesh_calender()
            return frm_main
        #
        super().__init__(master)
        #
        self.pack()
        self.master.title("LabraTrader")
        self.master.protocol("WM_DELETE_WINDOW", click_close)
        
        # Notebookウィジェットの作成
        notebook = ttk.Notebook(self.master)
        
        # タブの作成
        #tab_fx = ttk.Frame(notebook)
        tab_cfd = ttk.Frame(notebook)
        
        # notebookにタブを追加
        #notebook.add(tab_fx, text=cm.FX)
        notebook.add(tab_cfd, text=cm.CFD)
        notebook.pack(expand=True, fill=tk.BOTH)
        #
        #create_form(tab_fx, cm.PORT_FX).pack()
        create_form(tab_cfd, cm.PORT_CFD).pack()
        
#
def main():
    root = ThemedTk()
    root = LabraTrader(master=root)
    root.mainloop()

if __name__ == "__main__":
    main()