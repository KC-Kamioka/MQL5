import traceback
import socket
import sqlite3
import pandas as pd
import json
import os
import common as cm

#
def main():
    #
    db_path="../../Files/"+cm.DB_NAME+".sqlite"
    PORT=0
    #
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    with sqlite3.connect(db_path) as conn:
        sql="SELECT PORT FROM CONFIG"
        df = pd.read_sql(sql, conn)
        PORT=df.loc[:, 'PORT'].to_list()[0]
    #
    print(f"Socket start {cm.IPADDR_SV} {PORT}")
    # AF_INET：IPv4形式でソケット作成(省略可)
    sock_sv = socket.socket(socket.AF_INET)
    # IPアドレスとポート番号でバインド、タプルで指定
    sock_sv.bind((cm.IPADDR_SV, PORT))
    # サーバー有効化
    sock_sv.listen()
    # 接続・受信の無限ループ
    while True:
        # クライアントの接続受付
        sock_cl, addr = sock_sv.accept()
        print(f"Connection from {addr} has been established!")
        try:
            # ソケットから byte 形式でデータ受信
            recv_message = sock_cl.recv(1024)
            recv_dict = json.loads(recv_message.decode("utf-8"))
            # 
            if recv_dict['q_type']==cm.READ:
                sql=recv_dict['sql']
                with sqlite3.connect(db_path) as conn:
                    df_from_sql = pd.read_sql(sql, conn)
                sock_cl.send(bytes(df_from_sql.to_json(), 'utf-8'))
            # 
            if recv_dict['q_type']==cm.WRITE:
                sql=recv_dict['sql']
                with sqlite3.connect(db_path) as conn:
                    cur=conn.cursor()
                    cur.execute(sql)
                    conn.commit()
                sock_cl.send(bytes("200", 'utf-8'))
        except Exception as e:
            sock_cl.send(bytes(traceback.format_exception_only(type(e), e)[0], 'utf-8'))
        finally:
            # クライアントのソケットを閉じる
            sock_cl.close()

if __name__ == "__main__":
    main()