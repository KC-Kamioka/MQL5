//+------------------------------------------------------------------+
//|                                                         test.mq5 |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Include                                                          |
//+------------------------------------------------------------------+
#include <Common.mqh>
#include <DBLabraAutoTrader.mqh>
STR_EVENT ev[];
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   InitEventHistory();
   //int intBarShift=iBarShift(_Symbol,PERIOD_D1,D'01.01.2020')-1;
   //for(int i=intBarShift; i>=0; i--)
   //   SetEvent(iTime(_Symbol,PERIOD_D1,i+1),iTime(_Symbol,PERIOD_D1,i));
   SetEvent(ev,D'01.01.2020',0);
   InsertEventCalender(ev);
   ReadEvents(ev,D'01.01.2020',D'01.01.2021');
   Print(ArraySize(ev));
   ArrayPrint(ev);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
