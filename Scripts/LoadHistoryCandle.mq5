//+------------------------------------------------------------------+
//|                                                  LoadHistory.mq5 |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Include files                                                    |
//+------------------------------------------------------------------+
#include <DBCommon.mqh>
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
//--- Candle buffers
double         CandleBuffer1[];
double         CandleBuffer2[];
double         CandleBuffer3[];
double         CandleBuffer4[];
int handleSM,handleSB,handleSAS;
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   InitCandleHistory();
   InsertCandleHistory();
  }
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
bool InitCandleHistory()
  {
//---
   string strTableName="CANDLE";
   string sql[1];
   sql[0]="DROP TABLE IF EXISTS "+strTableName;
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   sql[0]="CREATE TABLE IF NOT EXISTS "+strTableName;
   sql[0]=sql[0]+"(";
   sql[0]=sql[0]+" "+"Time LONG";
   sql[0]=sql[0]+","+"Open REAL";
   sql[0]=sql[0]+","+"High REAL";
   sql[0]=sql[0]+","+"Low REAL";
   sql[0]=sql[0]+","+"Close REAL";
   sql[0]=sql[0]+","+"TickVolume INT";
   sql[0]=sql[0]+","+"Spread INT";
   sql[0]=sql[0]+")";
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
bool InsertCandleHistory()
  {
//---
   string sql[];
   MqlRates rates[];
   string strTableName="CANDLE";
//---
   ArrayResize(sql,1);
   for(int i=ArraySize(sql)-1; i>=0; i--)
      sql[i]="DELETE FROM "+strTableName;
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
//---
   if(!FillArraysFromRates(rates,0,iBars(_Symbol,_Period)))
      return false;
//---
   ArrayResize(sql,ArraySize(rates));
   for(int i=ArraySize(sql)-1; i>=0; i--)
     {
      sql[i]="INSERT INTO "+strTableName;
      sql[i]=sql[i]+"("+"Time";
      sql[i]=sql[i]+","+"Open";
      sql[i]=sql[i]+","+"High";
      sql[i]=sql[i]+","+"Low";
      sql[i]=sql[i]+","+"Close";
      sql[i]=sql[i]+","+"TickVolume";
      sql[i]=sql[i]+","+"Spread";
      sql[i]=sql[i]+")"+"VALUES";
      sql[i]=sql[i]+"("+"'"+TimeToString(rates[i].time)+"'";
      sql[i]=sql[i]+","+(string)(rates[i].open);
      sql[i]=sql[i]+","+(string)(rates[i].high);
      sql[i]=sql[i]+","+(string)(rates[i].low);
      sql[i]=sql[i]+","+(string)(rates[i].close);
      sql[i]=sql[i]+","+(string)(rates[i].tick_volume);
      sql[i]=sql[i]+","+(string)(rates[i].spread);
      sql[i]=sql[i]+")";
     }
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
