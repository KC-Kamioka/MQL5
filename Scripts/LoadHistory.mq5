//+------------------------------------------------------------------+
//|                                                  LoadHistory.mq5 |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Include files                                                    |
//+------------------------------------------------------------------+
#include <DBCommon.mqh>
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   InitTable();
   DBInsertRates();
  }
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
bool InitTable()
  {
   int i=0;
   string sql[1];
   sql[i]="DROP TABLE IF EXISTS CANDLE";
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   sql[i]="CREATE TABLE IF NOT EXISTS CANDLE";
   sql[i]=sql[i]+"(";
   sql[i]=sql[i]+" "+"TIME TEXT";
   sql[i]=sql[i]+","+"OPEN REAL";
   sql[i]=sql[i]+","+"HIGH REAL";
   sql[i]=sql[i]+","+"LOW REAL";
   sql[i]=sql[i]+","+"CLOSE REAL";
   sql[i]=sql[i]+","+"TICK_VOLUME INT";
   sql[i]=sql[i]+","+"SPREAD INT";
   sql[i]=sql[i]+","+"REAL_VOLUME INT";
   sql[i]=sql[i]+")";
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
//|
//+------------------------------------------------------------------+
bool DBInsertRates()
  {
//---
   MqlRates rates[];
   if(!FillArraysFromRates(rates,0,iBars(_Symbol,_Period)))
      return false;
//---
   string sql[];
   for(int i=0; i<ArraySize(rates); i++)
     {
      ArrayResize(sql,i+1);
      sql[i]="INSERT INTO CANDLE";
      sql[i]=sql[i]+"("+"TIME";
      sql[i]=sql[i]+","+"OPEN";
      sql[i]=sql[i]+","+"HIGH";
      sql[i]=sql[i]+","+"LOW";
      sql[i]=sql[i]+","+"CLOSE";
      sql[i]=sql[i]+","+"TICK_VOLUME";
      sql[i]=sql[i]+","+"SPREAD";
      sql[i]=sql[i]+","+"REAL_VOLUME";
      sql[i]=sql[i]+") VALUES";
      sql[i]=sql[i]+"("+"'"+(string)rates[i].time+"'";
      sql[i]=sql[i]+","+DoubleToString(rates[i].open,_Digits);
      sql[i]=sql[i]+","+DoubleToString(rates[i].high,_Digits);
      sql[i]=sql[i]+","+DoubleToString(rates[i].low,_Digits);
      sql[i]=sql[i]+","+DoubleToString(rates[i].close,_Digits);
      sql[i]=sql[i]+","+(string)rates[i].tick_volume;
      sql[i]=sql[i]+","+(string)rates[i].spread;
      sql[i]=sql[i]+","+(string)rates[i].real_volume;
      sql[i]=sql[i]+")";
     }
   if(!DBExecute(sql,LA_DB_NAME))
      return false;
   return true;
  }
//+------------------------------------------------------------------+
