//+------------------------------------------------------------------+
//|                                                   SAT_BuyNow.mq5 |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Include files                                                    |
//+------------------------------------------------------------------+
#include <Common.mqh>
#include <Trade\Trade.mqh>
//+------------------------------------------------------------------+
//| input                                                            |
//+------------------------------------------------------------------+
input double Lots = 0.1;
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   
  }
//+------------------------------------------------------------------+
//| open order
//+------------------------------------------------------------------+
void OpenMarketOrderBuy(double in_sl,double in_tp,string reason)
  {
//---
   if(!ExtTrade.Buy(
         NormalizeDouble(Lots,2)
         ,_Symbol
         ,0.0
         ,in_sl
         ,in_tp
         ,reason
      ))
      Print("position open error"+"("+(string)GetLastError()+").");
   else
     {
      Print("postion open reason:"+reason);
      TextCreate(
         0
         ,(string)TimeCurrent()+" "+reason
         ,TimeCurrent()
         ,rt[1].high
         ,reason
         ,8
         ,clrDeepSkyBlue
         ,0
         ,90
      );
      NotEntry=true;
     }
  }
//+------------------------------------------------------------------+
//| open order
//+------------------------------------------------------------------+
void OpenMarketOrderSell(double in_sl,double in_tp,string reason)
  {
//---
   if(!ExtTrade.Sell(
         NormalizeDouble(Lots,2)
         ,_Symbol
         ,0.0
         ,in_sl
         ,in_tp
         ,reason
      ))
      Print("position open error"+"("+(string)GetLastError()+").");
   else
     {
      Print("postion open reason:"+reason);
      TextCreate(
         0
         ,(string)TimeCurrent()+" "+reason
         ,TimeCurrent()
         ,rt[1].high
         ,reason
         ,8
         ,clrViolet
         ,0
         ,90
      );
      NotEntry=true;
     }
  }
//+------------------------------------------------------------------+
