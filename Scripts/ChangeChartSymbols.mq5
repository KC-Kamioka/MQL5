//+------------------------------------------------------------------+
//|                                           ChangeChartSymbols.mq5 |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property script_show_inputs
//+------------------------------------------------------------------+
//| Include files                                                    |
//+------------------------------------------------------------------+
#include <Common.mqh>
//+------------------------------------------------------------------+
//| Inputs                                                           |
//+------------------------------------------------------------------+
input ENUM_SYMBOLS enmSymbol;   //Symbol
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   long currChart,prevChart=ChartFirst();
   int i=0,limit=100;
   while(i<limit)
     {
      ChartSetSymbolPeriod(prevChart,MySymbols[enmSymbol],ChartPeriod(prevChart));
      currChart=ChartNext(prevChart);
      if(currChart<0)
         break;
      prevChart=currChart;
      i++;
     }
   
  }
//+------------------------------------------------------------------+
