//+------------------------------------------------------------------+
//|                                            ChangeObjectColor.mq5 |
//|                                  Copyright 2022, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   string strObjName=NULL;
   for(int i=0; i<ObjectsTotal(0); i++)
     {
      strObjName=ObjectName(0,i);
      Print(strObjName);
      ObjectSetInteger(0,strObjName,OBJPROP_COLOR,clrBlack);
     }
  }
//+------------------------------------------------------------------+
