//+------------------------------------------------------------------+
//|                                                      SQLite3Test |
//+------------------------------------------------------------------+
#property copyright "Copyright 2006-2014"
#property version   "1.00"

#include <MQH\Lib\SQLite3\SQLite3Base.mqh>

CSQLite3Base sql3;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnStart()
  {
//--- open database connection
   if(sql3.Connect("SQLite3Test.db3")!=SQLITE_OK)
      return;

//--- 1. How to create a table
   if(sql3.Query("CREATE TABLE IF NOT EXISTS `TestQuery` (`ticket` INTEGER, `open_price` DOUBLE, `comment` TEXT)")!=SQLITE_DONE)
     {
      Print(sql3.ErrorMsg());
      return;
     }

//--- 2. How to rename a table
   CSQLite3Table tbl;
   if(sql3.Query(tbl,"SELECT `name` FROM `sqlite_master` WHERE `type`='table' AND `name`='Trades'")!=SQLITE_DONE)
     {
      Print(sql3.ErrorMsg());
      return;
     }
   if(ArraySize(tbl.m_data)<=0)// no data
      if(sql3.Query("ALTER TABLE `TestQuery` RENAME TO `Trades`")!=SQLITE_DONE)
        {
         Print(sql3.ErrorMsg());
         return;
        }

//--- 3. How to add a column
   if(sql3.Query("ALTER TABLE `Trades` ADD COLUMN `profit`")!=SQLITE_DONE)
     {
      Print(sql3.ErrorMsg());
      return;
     }

//--- 4. How to add data
   if(sql3.Query("INSERT INTO `Trades` VALUES(3, 5.212, 'info', 1)")!=SQLITE_DONE)
     {
      Print(sql3.ErrorMsg());
      return;
     }

//--- 5. How to update data by condition + binding
   CSQLite3Row row;
   row.Add(5.555);
   row.Add("New price");
   if(sql3.QueryBind(row,"UPDATE `Trades` SET `open_price`=?, `comment`=?  WHERE(`ticket`=3)")!=SQLITE_DONE)
     {
      Print(sql3.ErrorMsg());
      return;
     }

//--- 6. How to get data from table
   if(sql3.Query(tbl,"SELECT * FROM `Trades`")!=SQLITE_DONE)
     {
      Print(sql3.ErrorMsg());
      return;
     }
   Print(TablePrint(tbl)); // printed in Experts log

//--- 7. How to delete all rows from table
   if(sql3.Query("DELETE FROM `Trades`")!=SQLITE_DONE)
     {
      Print(sql3.ErrorMsg());
      return;
     }

//--- 8. How to get names of all tables in database
   if(sql3.Query(tbl,"SELECT `name` FROM `sqlite_master` WHERE `type`='table' ORDER BY `name`;")!=SQLITE_DONE)
     {
      Print(sql3.ErrorMsg());
      return;
     }
   Print(TablePrint(tbl));

//--- 9. How to delete a table
   sql3.Query("DROP TABLE IF EXISTS `Trades`");

//--- 10. How to compress a database
   sql3.Query("VACUUM");
  }
//+------------------------------------------------------------------+
//| Comment Table                                                    |
//+------------------------------------------------------------------+
string TablePrint(CSQLite3Table &tbl)
  {
   string str="";
   int cs=ArraySize(tbl.m_colname);
   for(int c=0; c<cs; c++)
      str+=tbl.m_colname[c]+" | ";
   str+="\n";

   int rs=ArraySize(tbl.m_data);
   for(int r=0; r<rs; r++)
     {
      str+=string(r)+": ";
      CSQLite3Row *row=tbl.Row(r);
      if(CheckPointer(row)==POINTER_INVALID)
        {
         str+="----error row----\n";
         continue;
        }
      cs=ArraySize(row.m_data);
      for(int c=0; c<cs; c++)
         str+=string(row.m_data[c].GetString())+" | ";
      str+="\n";
     }
   return(str);
  }
//+------------------------------------------------------------------+
