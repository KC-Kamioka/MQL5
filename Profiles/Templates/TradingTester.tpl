<chart>
id=133165321909107929
symbol=USDJPY
description=US Dollar vs Japanese Yen
period_type=1
period_size=1
digits=3
tick_size=0.000000
position_time=1671674400
scale_fix=0
scale_fixed_min=131.480000
scale_fixed_max=134.060000
scale_fix11=0
scale_bar=0
scale_bar_val=1.000000
scale=16
mode=2
fore=1
grid=0
volume=0
scroll=0
shift=0
shift_size=20.224719
fixed_pos=0.000000
ticker=1
ohlc=0
one_click=0
one_click_btn=1
bidline=0
askline=0
lastline=0
days=0
descriptions=0
tradelines=0
tradehistory=0
window_left=958
window_top=0
window_right=1916
window_bottom=670
window_type=3
floating=0
floating_left=0
floating_top=0
floating_right=0
floating_bottom=0
floating_type=1
floating_toolbar=1
floating_tbstate=
background_color=16777215
foreground_color=0
barup_color=0
bardown_color=0
bullcandle_color=16777215
bearcandle_color=0
chartline_color=4294967295
volumes_color=32768
grid_color=12632256
bidline_color=12632256
askline_color=12632256
lastline_color=12632256
stops_color=17919
windows_total=1

<window>
height=100.000000
objects=85

<indicator>
name=Main
path=
apply=1
show_data=1
scale_inherit=0
scale_line=0
scale_line_percent=50
scale_line_value=0.000000
scale_fix_min=0
scale_fix_min_val=0.000000
scale_fix_max=0
scale_fix_max_val=0.000000
expertmode=0
fixed_height=-1
</indicator>

<indicator>
name=Custom Indicator
path=Indicators\TradingTester.ex5
apply=0
show_data=1
scale_inherit=0
scale_line=0
scale_line_percent=50
scale_line_value=0.000000
scale_fix_min=0
scale_fix_min_val=0.000000
scale_fix_max=0
scale_fix_max_val=0.000000
expertmode=4
fixed_height=-1

<graph>
name=BlueSpan;RedSpan
draw=7
style=0
width=1
color=15658671,13353215
</graph>

<graph>
name=BlueLine
draw=1
style=0
width=1
color=16711680
</graph>

<graph>
name=RedLine
draw=1
style=0
width=1
color=255
</graph>

<graph>
name=ChikouSpan26
draw=1
style=0
width=3
shift=-25
color=16711935
</graph>

<graph>
name=Middle
draw=1
style=0
width=2
color=16711680
</graph>

<graph>
name=Upper1Sigma
draw=1
style=0
width=2
color=32768
</graph>

<graph>
name=Lower1Sigma
draw=1
style=0
width=2
color=32768
</graph>

<graph>
name=Upper2Sigma
draw=1
style=0
width=2
color=42495
</graph>

<graph>
name=Lower2Sigma
draw=1
style=0
width=2
color=42495
</graph>

<graph>
name=Upper3Sigma
draw=1
style=0
width=2
color=16776960
</graph>

<graph>
name=Lower3Sigma
draw=1
style=0
width=2
color=16776960
</graph>

<graph>
name=ChikouSpan21
draw=1
style=0
width=3
shift=-20
color=14822282
</graph>

<graph>
name=Over2Sigma
draw=3
style=0
width=1
arrow=171
color=255
</graph>

<graph>
name=CSpanDirection
draw=0
style=0
width=1
color=-1
</graph>

<graph>
name=CSpanUp
draw=3
style=0
width=4
arrow=241
shift_y=10
color=16711680
</graph>

<graph>
name=CSpanDown
draw=3
style=0
width=4
arrow=242
shift_y=-10
color=255
</graph>

<graph>
name=SpanModelSignal
draw=0
style=0
width=1
color=
</graph>

<graph>
name=SpanModelSignalTerm
draw=0
style=0
width=1
color=
</graph>

<graph>
name=SpanModelUpGraceStBar
draw=0
style=0
width=1
color=
</graph>

<graph>
name=SpanModelUpGraceEdBar
draw=0
style=0
width=1
color=
</graph>

<graph>
name=SpanModelDownGraceStBar
draw=0
style=0
width=1
color=
</graph>

<graph>
name=SpanModelDownGraceEdBar
draw=0
style=0
width=1
color=
</graph>

<graph>
name=SpanModelGraceHigh
draw=0
style=0
width=1
color=
</graph>

<graph>
name=SpanModelGraceLow
draw=0
style=0
width=1
color=
</graph>

<graph>
name=SpanModelSignalEnd
draw=0
style=0
width=1
color=
</graph>

<graph>
name=RedSpanSignal
draw=0
style=0
width=1
color=
</graph>

<graph>
name=RedSpanSignalTerm
draw=0
style=0
width=1
color=
</graph>

<graph>
name=RedSpanUpGraceStBar
draw=0
style=0
width=1
color=
</graph>

<graph>
name=RedSpanUpGraceEdBar
draw=0
style=0
width=1
color=
</graph>

<graph>
name=RedSpanDwGraceStBar
draw=0
style=0
width=1
color=
</graph>

<graph>
name=RedSpanDwGraceEdBar
draw=0
style=0
width=1
color=
</graph>

<graph>
name=RedSpanGraceHigh
draw=0
style=0
width=1
color=
</graph>

<graph>
name=RedSpanGraceLow
draw=0
style=0
width=1
color=
</graph>

<graph>
name=RedSpanSignalEnd
draw=0
style=0
width=1
color=
</graph>

<graph>
name=High52
draw=0
style=0
width=1
color=
</graph>

<graph>
name=Low52
draw=0
style=0
width=1
color=
</graph>

<graph>
name=YesterdayHigh
draw=0
style=0
width=1
color=
</graph>

<graph>
name=YesterdayLow
draw=0
style=0
width=1
color=
</graph>

<graph>
name=Candle
draw=9
style=0
width=1
color=0,16777215,0
</graph>
</indicator>
<object>
type=2
name=high52
hidden=1
color=55295
style=2
selectable=0
ray1=0
ray2=0
date1=1671753600
date2=1672196400
value1=133.939000
value2=133.939000
</object>

<object>
type=2
name=low52
hidden=1
color=55295
style=2
selectable=0
ray1=0
ray2=0
date1=1671753600
date2=1672196400
value1=132.149000
value2=132.149000
</object>

<object>
type=2
name=RSSignalB_2022.11.02 13:00
hidden=1
color=16711680
style=2
background=1
selectable=0
ray1=0
ray2=0
date1=1667394000
date2=1667394000
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=RSSignalS_2022.11.02 21:00
hidden=1
style=2
background=1
selectable=0
ray1=0
ray2=0
date1=1667422800
date2=1667422800
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=RSSignalSGrace_2022.11.02 21:00
hidden=1
width=3
selectable=0
ray1=0
ray2=0
date1=1667422800
date2=1667437200
value1=145.664000
value2=145.664000
</object>

<object>
type=2
name=RSSignalSup_2022.11.03 01:00
hidden=1
color=12695295
width=2
background=1
selectable=0
ray1=0
ray2=0
date1=1667437200
date2=1667480400
value1=145.664000
value2=145.664000
</object>

<object>
type=20
name=BKColor_2022.11.02 21:00
hidden=1
color=15134970
background=1
selectable=0
filling=1
date1=1667422800
date2=1667480400
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=SMSignalS_2022.11.02 02:00
hidden=1
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1667354400
date2=1667354400
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalSGrace_2022.11.02 02:00
hidden=1
color=12695295
background=1
selectable=0
filling=1
date1=1667354400
date2=1667358000
value1=148.179000
value2=147.817000
</object>

<object>
type=2
name=SMSignalB_2022.11.02 03:00
hidden=1
color=16711680
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1667358000
date2=1667358000
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalBGrace_2022.11.02 03:00
hidden=1
color=13959039
background=1
selectable=0
filling=1
date1=1667358000
date2=1667372400
value1=147.965000
value2=147.155000
</object>

<object>
type=2
name=RSSignalRes_2022.11.01 02:00
hidden=1
color=13959039
width=2
background=1
selectable=0
ray1=0
ray2=0
date1=1667268000
date2=1667394000
value1=148.802000
value2=148.802000
</object>

<object>
type=20
name=BKColor_2022.12.28 01:00
hidden=1
color=16777184
background=1
selectable=0
filling=1
date1=1672189200
date2=1667394000
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=SMSignalRes_2022.11.02 07:00
hidden=1
color=13959039
background=1
selectable=0
ray1=0
ray2=0
date1=1667372400
date2=1667462400
value1=147.965000
value2=147.965000
</object>

<object>
type=2
name=RSSignalBGrace_2022.11.03 13:00
hidden=1
color=16711680
width=3
selectable=0
ray1=0
ray2=0
date1=1667480400
date2=1667494800
value1=148.448000
value2=148.448000
</object>

<object>
type=2
name=SMSignalSup_2022.11.03 12:00
hidden=1
color=12695295
background=1
selectable=0
ray1=0
ray2=0
date1=1667476800
date2=1667782800
value1=147.241000
value2=147.241000
</object>

<object>
type=2
name=RSSignalRes_2022.11.03 17:00
hidden=1
color=13959039
width=2
background=1
selectable=0
ray1=0
ray2=0
date1=1667494800
date2=1667822400
value1=148.448000
value2=148.448000
</object>

<object>
type=2
name=SMSignalS_2022.11.09 21:00
hidden=1
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1668027600
date2=1668027600
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalSGrace_2022.11.09 21:00
hidden=1
color=12695295
background=1
selectable=0
filling=1
date1=1668027600
date2=1668042000
value1=146.795000
value2=146.171000
</object>

<object>
type=2
name=SMSignalB_2022.11.10 15:00
hidden=1
color=16711680
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1668092400
date2=1668092400
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalBGrace_2022.11.10 15:00
hidden=1
color=13959039
background=1
selectable=0
filling=1
date1=1668092400
date2=1668106800
value1=146.270000
value2=141.461000
</object>

<object>
type=2
name=SMSignalRes_2022.11.10 19:00
hidden=1
color=13959039
background=1
selectable=0
ray1=0
ray2=0
date1=1668106800
date2=1668520800
value1=146.270000
value2=146.270000
</object>

<object>
type=2
name=SMSignalS_2022.11.16 03:00
hidden=1
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1668567600
date2=1668567600
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalSGrace_2022.11.16 03:00
hidden=1
color=12695295
background=1
selectable=0
filling=1
date1=1668567600
date2=1668582000
value1=140.291000
value2=139.341000
</object>

<object>
type=20
name=BKColor_2022.11.07 12:00
hidden=1
color=15134970
background=1
selectable=0
filling=1
date1=1667822400
date2=1668693600
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=RSSignalB_2022.11.17 14:00
hidden=1
color=16711680
style=2
background=1
selectable=0
ray1=0
ray2=0
date1=1668693600
date2=1668693600
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=SMSignalSup_2022.11.16 07:00
hidden=1
color=12695295
background=1
selectable=0
ray1=0
ray2=0
date1=1668582000
date2=1669201200
value1=139.341000
value2=139.341000
</object>

<object>
type=2
name=SMSignalB_2022.11.23 11:00
hidden=1
color=16711680
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1669201200
date2=1669201200
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalBGrace_2022.11.23 11:00
hidden=1
color=13959039
background=1
selectable=0
filling=1
date1=1669201200
date2=1669215600
value1=141.612000
value2=140.984000
</object>

<object>
type=2
name=RSSignalS_2022.11.23 16:00
hidden=1
style=2
background=1
selectable=0
ray1=0
ray2=0
date1=1669219200
date2=1669219200
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=RSSignalSGrace_2022.11.23 16:00
hidden=1
width=3
selectable=0
ray1=0
ray2=0
date1=1669219200
date2=1669233600
value1=139.554000
value2=139.554000
</object>

<object>
type=2
name=SMSignalRes_2022.11.23 15:00
hidden=1
color=13959039
background=1
selectable=0
ray1=0
ray2=0
date1=1669215600
date2=1669410000
value1=141.612000
value2=141.612000
</object>

<object>
type=2
name=SMSignalS_2022.11.29 00:00
hidden=1
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1669680000
date2=1669680000
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalSGrace_2022.11.29 00:00
hidden=1
color=12695295
background=1
selectable=0
filling=1
date1=1669680000
date2=1669694400
value1=139.352000
value2=138.534000
</object>

<object>
type=2
name=SMSignalSup_2022.11.29 04:00
hidden=1
color=12695295
background=1
selectable=0
ray1=0
ray2=0
date1=1669694400
date2=1669716000
value1=138.534000
value2=138.534000
</object>

<object>
type=2
name=SMSignalB_2022.11.29 10:00
hidden=1
color=16711680
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1669716000
date2=1669716000
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalBGrace_2022.11.29 10:00
hidden=1
color=13959039
background=1
selectable=0
filling=1
date1=1669716000
date2=1669730400
value1=138.408000
value2=137.864000
</object>

<object>
type=2
name=SMSignalRes_2022.11.29 14:00
hidden=1
color=13959039
background=1
selectable=0
ray1=0
ray2=0
date1=1669730400
date2=1669752000
value1=138.408000
value2=138.408000
</object>

<object>
type=2
name=RSSignalSup_2022.11.23 20:00
hidden=1
color=12695295
width=2
background=1
selectable=0
ray1=0
ray2=0
date1=1669233600
date2=1669820400
value1=139.554000
value2=139.554000
</object>

<object>
type=20
name=BKColor_2022.11.23 16:00
hidden=1
color=15134970
background=1
selectable=0
filling=1
date1=1669219200
date2=1669820400
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=RSSignalBGrace_2022.11.30 15:00
hidden=1
color=16711680
width=3
selectable=0
ray1=0
ray2=0
date1=1669820400
date2=1669834800
value1=139.896000
value2=139.896000
</object>

<object>
type=2
name=RSSignalRes_2022.11.30 19:00
hidden=1
color=13959039
width=2
background=1
selectable=0
ray1=0
ray2=0
date1=1669834800
date2=1669845600
value1=139.896000
value2=139.896000
</object>

<object>
type=2
name=SMSignalS_2022.12.01 02:00
hidden=1
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1669860000
date2=1669860000
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalSGrace_2022.12.01 02:00
hidden=1
color=12695295
background=1
selectable=0
filling=1
date1=1669860000
date2=1669874400
value1=137.542000
value2=136.488000
</object>

<object>
type=2
name=SMSignalSup_2022.12.01 06:00
hidden=1
color=12695295
background=1
selectable=0
ray1=0
ray2=0
date1=1669874400
date2=1670256000
value1=136.488000
value2=136.488000
</object>

<object>
type=2
name=SMSignalB_2022.12.05 16:00
hidden=1
color=16711680
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1670256000
date2=1670256000
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=BKColor_2022.11.30 22:00
hidden=1
color=15134970
background=1
selectable=0
filling=1
date1=1669845600
date2=1670259600
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=RSSignalB_2022.12.05 17:00
hidden=1
color=16711680
style=2
background=1
selectable=0
ray1=0
ray2=0
date1=1670259600
date2=1670259600
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalBGrace_2022.12.05 16:00
hidden=1
color=13959039
background=1
selectable=0
filling=1
date1=1670256000
date2=1670270400
value1=136.717000
value2=135.599000
</object>

<object>
type=2
name=SMSignalS_2022.12.08 15:00
hidden=1
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1670511600
date2=1670511600
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalSGrace_2022.12.08 15:00
hidden=1
color=12695295
background=1
selectable=0
filling=1
date1=1670511600
date2=1670526000
value1=136.803000
value2=136.307000
</object>

<object>
type=2
name=RSSignalS_2022.12.09 03:00
hidden=1
style=2
background=1
selectable=0
ray1=0
ray2=0
date1=1670554800
date2=1670554800
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=RSSignalSGrace_2022.12.09 03:00
hidden=1
width=3
selectable=0
ray1=0
ray2=0
date1=1670554800
date2=1670569200
value1=135.759000
value2=135.759000
</object>

<object>
type=2
name=SMSignalSup_2022.12.08 19:00
hidden=1
color=12695295
background=1
selectable=0
ray1=0
ray2=0
date1=1670526000
date2=1670814000
value1=136.307000
value2=136.307000
</object>

<object>
type=2
name=SMSignalB_2022.12.12 03:00
hidden=1
color=16711680
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1670814000
date2=1670814000
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalBGrace_2022.12.12 03:00
hidden=1
color=13959039
background=1
selectable=0
filling=1
date1=1670814000
date2=1670828400
value1=137.137000
value2=136.755000
</object>

<object>
type=2
name=RSSignalSup_2022.12.09 07:00
hidden=1
color=12695295
width=2
background=1
selectable=0
ray1=0
ray2=0
date1=1670569200
date2=1670857200
value1=135.759000
value2=135.759000
</object>

<object>
type=20
name=BKColor_2022.12.09 03:00
hidden=1
color=15134970
background=1
selectable=0
filling=1
date1=1670554800
date2=1670857200
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=RSSignalBGrace_2022.12.12 15:00
hidden=1
color=16711680
width=3
selectable=0
ray1=0
ray2=0
date1=1670857200
date2=1670871600
value1=137.682000
value2=137.682000
</object>

<object>
type=2
name=SMSignalRes_2022.12.12 07:00
hidden=1
color=13959039
background=1
selectable=0
ray1=0
ray2=0
date1=1670828400
date2=1670943600
value1=137.137000
value2=137.137000
</object>

<object>
type=2
name=RSSignalRes_2022.12.12 19:00
hidden=1
color=13959039
width=2
background=1
selectable=0
ray1=0
ray2=0
date1=1670871600
date2=1670943600
value1=137.682000
value2=137.682000
</object>

<object>
type=20
name=BKColor_2022.12.13 15:00
hidden=1
color=15134970
background=1
selectable=0
filling=1
date1=1670943600
date2=1671123600
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=RSSignalB_2022.12.15 17:00
hidden=1
color=16711680
style=2
background=1
selectable=0
ray1=0
ray2=0
date1=1671123600
date2=1671123600
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=SMSignalS_2022.12.19 02:00
hidden=1
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1671415200
date2=1671415200
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalSGrace_2022.12.19 02:00
hidden=1
color=12695295
background=1
selectable=0
filling=1
date1=1671415200
date2=1671429600
value1=136.595000
value2=135.767000
</object>

<object>
type=2
name=SMSignalSup_2022.12.19 06:00
hidden=1
color=12695295
background=1
selectable=0
ray1=0
ray2=0
date1=1671429600
date2=1671505200
value1=135.767000
value2=135.767000
</object>

<object>
type=2
name=SMSignalB_2022.12.20 03:00
hidden=1
color=16711680
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1671505200
date2=1671505200
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalBGrace_2022.12.20 03:00
hidden=1
color=13959039
background=1
selectable=0
filling=1
date1=1671505200
date2=1671512400
value1=137.471000
value2=137.024000
</object>

<object>
type=2
name=RSSignalS_2022.12.20 05:00
hidden=1
style=2
background=1
selectable=0
ray1=0
ray2=0
date1=1671512400
date2=1671512400
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=RSSignalSGrace_2022.12.20 05:00
hidden=1
width=3
selectable=0
ray1=0
ray2=0
date1=1671512400
date2=1671526800
value1=132.273000
value2=132.273000
</object>

<object>
type=2
name=SMSignalS_2022.12.20 12:00
hidden=1
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1671537600
date2=1671537600
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalSGrace_2022.12.20 12:00
hidden=1
color=12695295
background=1
selectable=0
filling=1
date1=1671537600
date2=1671552000
value1=132.904000
value2=132.058000
</object>

<object>
type=2
name=SMSignalB_2022.12.22 13:00
hidden=1
color=16711680
style=3
background=1
selectable=0
ray1=0
ray2=0
date1=1671714000
date2=1671714000
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=20
name=SMSignalBGrace_2022.12.22 13:00
hidden=1
color=13959039
background=1
selectable=0
filling=1
date1=1671714000
date2=1671728400
value1=132.720000
value2=131.904000
</object>

<object>
type=2
name=RSSignalSup_2022.12.20 09:00
hidden=1
color=12695295
width=2
background=1
selectable=0
ray1=0
ray2=0
date1=1671526800
date2=1671764400
value1=132.273000
value2=132.273000
</object>

<object>
type=20
name=BKColor_2022.12.20 05:00
hidden=1
color=15134970
background=1
selectable=0
filling=1
date1=1671512400
date2=1671764400
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=2
name=RSSignalBGrace_2022.12.23 03:00
hidden=1
color=16711680
width=3
selectable=0
ray1=0
ray2=0
date1=1671764400
date2=1671778800
value1=132.811000
value2=132.811000
</object>

<object>
type=2
name=line52
hidden=1
color=55295
style=2
selectable=0
ray1=0
ray2=0
date1=1671753600
date2=1671753600
value1=133.939000
value2=132.149000
</object>

<object>
type=2
name=YesterdayHighLine
hidden=1
color=15570276
width=2
selectable=0
ray1=0
ray2=0
date1=1671620400
date2=1672196400
value1=133.592000
value2=133.592000
</object>

<object>
type=2
name=YesterdayLowLine
hidden=1
color=15570276
width=2
selectable=0
ray1=0
ray2=0
date1=1671620400
date2=1672196400
value1=132.631000
value2=132.631000
</object>

<object>
type=2
name=SMSignalRes_2022.12.22 17:00
hidden=1
color=13959039
background=1
selectable=0
ray1=0
ray2=0
date1=1671728400
date2=1672196400
value1=132.720000
value2=132.720000
</object>

<object>
type=2
name=RSSignalRes_2022.12.23 07:00
hidden=1
color=13959039
width=2
background=1
selectable=0
ray1=0
ray2=0
date1=1671778800
date2=1672196400
value1=132.811000
value2=132.811000
</object>

<object>
type=20
name=BKColor_2022.12.23 03:00
hidden=1
color=16777184
background=1
selectable=0
filling=1
date1=1671764400
date2=1672196400
value1=179769313486231570814527423731704356798070567525844996598917476803157260780028538760589558632766878171540458953514382464
value2=0.000000
</object>

<object>
type=101
name=YesterdayHigh
hidden=1
descr=Y_H
color=0
selectable=0
angle=0
date1=1671620400
value1=133.592000
fontsz=8
fontnm=Arial120
anchorpos=2
</object>

<object>
type=101
name=YesterdayLow
hidden=1
descr=Y_L
color=0
selectable=0
angle=0
date1=1671620400
value1=132.631000
fontsz=8
fontnm=Arial120
anchorpos=2
</object>

</window>
</chart>